<?php
function genre($db, $argv, $argc)
{
  if ($argc == 4){
    $recup = $db->movies->find(array("genres" => strtolower($argv[3])));
    $comp = 0;
    foreach ($recup as $value){
      echo "title : ". $value['title'] . "\n";
      echo "year  : ". $value['year']. "\n";
      $genress = implode(", ", $value['genres']);
      echo "genre  : ". $genress. "\n";
      echo "Directors  : ". $value['directors']. "\n";
      echo "imdb_code  : ". $value['imdb_code']. "\n";
      echo "=============================================\n\n";
      $comp++;
    }
    echo "*$comp*\n"."film trouves\n";
  }
  else
    echo "vous devez entrer un genre qui existe\n";
}

function rate($db, $argv, $argc)
{
  if ($argc == 4){
    $regex = new MongoRegex("/^".$argv[3]."/");
    $where = array('rate' => $regex);
    $recup = $db->movies->find($where);
    $comp = 0;
    foreach ($recup as $value){
      echo "title : ". $value['title'] . "\n";
      echo "year  : ". $value['year']. "\n";
      $genress = implode(", ", $value['genres']);
      echo "genre  : ". $genress. "\n";
      echo "Directors  : ". $value['directors']. "\n";
      echo "imdb_code  : ". $value['imdb_code']. "\n";
      echo "=============================================\n\n";
      
      $comp++;
    }
    if ($comp == 0)
      echo "aucun film trouve\n";
    else
      echo "*$comp*\n"."film trouves\n";
  }
  else
    echo "entrez un chiffre entre 0 et 9\n";
}

function rented_movie($argv, $db)
{
  $criteria = array(
		    'login' => $argv[2] ,
		    );
  $imdb = array(
		'imdb_code' => $argv[3] ,
		);
  $code = $db->movies->findOne($imdb);
  $docu = $db->students->findOne($criteria);
  if (empty($docu))
    echo "Student doesn't exist\n";
  elseif (empty($imdb))
    echo "imdb_code doesn't exist\n";
  else {
    $student = $db->students->findOne(array("login" => $argv[2]));
    $movie = $db->movies->findOne(array("imdb_code" => $argv[3]));
    $movieid = $movie['_id'];
    $studentid = $student['_id'];
    $stock = $movie['stock'];

    if ($stock > 0)
	rent($db, $argv, $movieid, $studentid, $stock);
    else {
      echo "stock out\n";
    }
  }
}

function rent($db, $argv, $movieid, $studentid, $stock)
{
  $db->students->update(array("login" => $argv[2]),
			array('$set'=>array("rented_movies" => $movieid)));
  $db->movies->update(array("imdb_code" => $argv[3]),
		      array('$set' =>array("renting_students" => $studentid)));
  echo "rented\n";
  $db->movies->update(array("imdb_code" => $argv[3]),
		      array('$set' =>array("stock"=> --$stock)));
}
?>