<?php
require('fonction3.php');
require('fonction4.php');
function add_movies($db, $argv, $tmp)
{
  $c = new MongoClient();
  $db = $c->db_etna;
  $collection = $db->createCollection('movies');
  $rando = rand(0, 5);
  $doc = array(
	       "imdb_code" => $tmp[1],
	       "title" => $tmp[5],
	       "year" => $tmp[11],
	       "genres" => $tmp[12],
	       "directors" =>  $tmp[7],
	       "rate" => $tmp[9],
	       "link" => $tmp[15],
	       "stock" => $rando,
	       "renting_students" => array(), 
	       );
  $collection->insert($doc);
}

function movies_storing($argv, $db)
{
  $comp = 0;
  if (($handle = fopen("movies.csv", "r")) !== FALSE) {
    $firstline = fgetcsv($handle, 1000, ",");
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
      {
	$num = count($data);
	for ($c=1; $c<$num; $c +=16)
	  $tmp[$c] = $data[$c];
	for ($c=5; $c < $num; $c+=16)
	  $tmp[$c] = $data[$c];
	for ($c=11; $c < $num; $c +=16)
	  $tmp[$c] = $data[$c];
	for ($c=12; $c < $num; $c +=16)
	  $tmp[$c] = explode(", ", $data[$c]);
	for ($c=7; $c < $num; $c +=16)
	  $tmp[$c] = $data[$c];
	for ($c=9; $c < $num; $c +=16)
	  $tmp[$c] = $data[$c];
	for ($c=15; $c < $num; $c +=16)
	  $tmp[$c] = $data[$c];
	
	$comp++;
	add_movies($db,$argv,$tmp);
      }
  }
  echo $comp."  movies successfully stored !\n";
  fclose($handle);
}
function show_movies($db, $argv, $argc)
{
  if ($argc == 2)
    {
      $i=0;
      $recup = $db->movies->find();
      $recup->sort(array("title" => 1));
      foreach ($recup as $value)
	{
	  echo "Title : ". $value['title'] . "\n";
	  echo "year  : ". $value['year']. "\n";
	  $genress = implode(", ", $value['genres']);
	  echo "genre  : ". $genress. "\n";
	  echo "Directors  : ". $value['directors']. "\n";
	  echo "imdb_code  : ". $value['imdb_code']. "\n";
	  echo "=============================================\n\n";
	  $i++;
	}
      echo "*". $i."*\n";
    }
  else{
    if (strtolower($argv[2]) == "year")
      year($db, $argv, $argc);
    else if (strtolower($argv[2]) == "genre")
      genre($db, $argv, $argc);
    else if (strtolower($argv[2]) == "rate")
      rate($db, $argv, $argc);
    else if (strtolower($argv[2]) == "desc")
      desc($argv,$db, $argc);
    else
      echo " Error\nargument ex: (genre, year, rate, desc)\n";
  }
}
function desc($argv, $db, $argc)
{
  if ($argv[2] ==  "desc")
    {
      $recup = $db->movies->find();
      $recup->sort(array("title" => -1));
      $comp = 0;
      foreach ($recup as $value)
	{
	  echo "title : ". $value['title'] . "\n";
	  echo "year  : ". $value['year']. "\n";
	  $genress = implode(", ", $value['genres']);
	  echo "genre  : ". $genress. "\n";
	  echo "Directors  : ". $value['directors']. "\n";
	  echo "imdb_code  : ". $value['imdb_code']. "\n";
	  echo "=============================================\n\n";
	  $comp++;
	}
      echo "*$comp*\n"."film trouves\n";
    }
  else {
    echo "vous devez entrer desc pour l'ordre decroissant\n";
  }
}

function year($db, $argv, $argc)
{
  if ($argc == 4){
    $recup = $db->movies->find(array("year" => $argv[3]));
    $comp = 0;
    foreach ($recup as $value){
      echo "title : ". $value['title'] . "\n";
      echo "year  : ". $value['year']. "\n";
      $genress = implode(", ", $value['genres']);
      echo "genre  : ". $genress. "\n";
      echo "Directors  : ". $value['directors']. "\n";
      echo "imdb_code  : ". $value['imdb_code']. "\n";
      echo "=============================================\n\n";
      $comp++;
    }
    echo "*$comp*\n"."film trouves\n";
  }
  else{
    echo "vous devez entrer une annee";
  }
}

?>