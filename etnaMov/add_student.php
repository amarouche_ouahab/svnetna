<?php

function nom(){

  echo "Name ?\n";
  $name = readline();
  while (!preg_match('/\s|\w/', $name))
    {
      echo "Name ?\n";
      $name = readline();
    }
  return ($name);
}
function age()
{
  echo "Age ?\n";
  $age = readline();
  while (!preg_match('/^[0-9]{2}+$/', $age))
    {
      echo "error age\n";
      echo "Age ?\n";
      $age = readline();
    }
  return ($age);
}
function email()
{
  echo "Email ?\n";
  $Email = readline();
  while (!preg_match('/^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-z]{2,4}/', $Email))
    {
      echo "error email\n";
      echo "Email ?\n";
      $Email = readline();
    }
  return ($Email);
}

function phone()
{
  echo "Phone number ?\n";
  $phone = readline();
  while (!preg_match('/^0[1-68][0-9]{8}/', $phone))
    {
      echo "Error phone number !\n";
      echo "Phone number ?\n";
      $phone = readline();
    }
  return ($phone);
}
