#!/usr/bin/env ruby                                                            

require "csv"

s = ARGV[0]

CSV.foreach(ARGV[0]) do |row|
  if ARGV[1]== row[1] && row[0] == "download"
    puts "From #{Time.at(row[2].to_i).to_time} to #{Time.at(row[3].to_i).to_time}: #{row[4].to_i/(1024*1024)} Mbytes"
  end
end
