/*
** parser.c for my_slack in /home/verpilc/Documents/my_slack
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Mon Feb 19 22:18:27 2018 VERPILLAT Corentin
** Last update Mon Feb 19 22:18:29 2018 VERPILLAT Corentin
*/

#include "client/client.h"

t_parser *parser_command_client(char *rl, int sock, char *tmp, int length)
{
    t_parser *parser;

    if ((parser = init_parser(tmp)) != NULL)
    {
        if (parser->length == length)
            write_server(sock, rl);
        else if (parser->length > length)
            my_putstr_color("red", "Too much argument\n");
        else if (parser->length < length)
            my_putstr_color("red", "Not enough argument\n");
    }
    return (parser);
}

t_parser *parser_command_server(char *tmp)
{
    t_parser *parser;

    if ((parser = init_parser(tmp)) == NULL)
    {
        return (NULL);
    }
    return (parser);
}