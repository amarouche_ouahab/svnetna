/*
** main.c for my_slack in /home/verpilc/Documents/my_slack
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Wed Feb 14 11:21:32 2018 VERPILLAT Corentin
** Last update Wed Feb 14 11:21:50 2018 VERPILLAT Corentin
*/

#include "client.h"

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        my_putstr_color("red", "Usage : /client [address] [pseudo]\n");
        return (EXIT_FAILURE);
    }
    if (argc == 2 && argv[2] == NULL)
    {
        my_putstr_color("red", "Usage : /client [address] [pseudo]\n");
        return (EXIT_FAILURE);
    }

    client(argv[1], argv[2]);

    return (EXIT_SUCCESS);
}

int client(const char *address, const char *name)
{
    int sock;
    char buffer[BUFFER_SIZE];
    fd_set rdfs;
    char *rl;

    rl = "abc";
    if ((sock = init_connection(address)) == ERROR)
        return (ERROR);
    if (write_server(sock, name) == ERROR)
        return (ERROR);
    while (my_strcmp(rl, "/quit") != 0)
    {
        if (descriptor_and_select(&rdfs, sock) == ERROR)
            return (ERROR);
        if (FD_ISSET(STDIN_FILENO, &rdfs))
            get_action(sock, &rl);
        else if (FD_ISSET(sock, &rdfs))
        {
            if (server_connect(sock, buffer) == ERROR)
                break;
        }
    }
    end_connection(sock);
    return (1);
}