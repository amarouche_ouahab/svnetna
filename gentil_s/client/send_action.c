/*
** send_action.c for my_slack in /home/verpilc/Documents/my_slack/client
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Mon Feb 19 20:41:38 2018 VERPILLAT Corentin
** Last update Mon Feb 19 20:41:40 2018 VERPILLAT Corentin
*/

#include "client.h"

void get_action(int sock, char **rl)
{
    if ((*rl = readline()) == NULL)
    {
        my_putstr_color("red", "Command is stuck\n");
    }
    else
    {
        if (*rl[0] == '/')
            play_fnct(*rl, sock);
        else
            write_server(sock, *rl);
    }
}

void play_fnct(char *rl, int sock)
{
    int i;
    int found;
    char *tmp;
    t_parser *parser;

    i = 0;
    found = 0;
    if ((tmp = my_strdup(rl)) == NULL)
	{
	  my_putstr_color("red", "Parsage error\n");
	}
    delete_first_and_delim(tmp, FALSE);
    while (command_tab[i].cha != NULL && found != TRUE)
    {
        if (my_strcmp_delim(tmp, command_tab[i].cha) == 0)
        {
            parser = command_tab[i].fnct_client(rl, sock, tmp, command_tab[i].nb_element);
            free_parser(parser);
            found = 1;
        }
        i += 1;
    }
    if (found == 0 && my_strcmp_delim(tmp, "quit") != 0)
        my_putstr_color("red", "This command doesn't exist\n");
}