/*
** put_color.c for my_ftl in /home/verpilc/Documents/my_ftp
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Fri Nov 10 15:39:20 2017 VERPILLAT Corentin
** Last update Fri Nov 10 15:42:24 2017 VERPILLAT Corentin
*/

#include "common.h"

void	my_putstr_color(const char *color, const char *str)
{
  int	i;

  i = 0;
  while (g_color[i].color != NULL && (my_strcmp(g_color[i].color, color) != 0))
    i++;
  if (g_color[i].color == NULL)
    {
      my_putstr(str);
      return ;
    }
  my_putstr(g_color[i].unicode);
  my_putstr(str);
  my_putstr("\033[0m");
}

void	my_put_nbr(int n)
{
  if (n == -2147483648)
    {
      my_putstr("-2147483648");
      return ;
    }
  if (n < 0)
    {
      my_putchar('-');
      n = -n;
    }
  if (n > 9)
    {
      my_put_nbr(n / 10);
    }
  my_putchar((n % 10) + '0');
}

void	my_put_nbr_color(const char *color, const int n)
{
  int	i;

  i = 0;
  while (g_color[i].color != NULL && (my_strcmp(g_color[i].color, color) != 0))
    i++;
  if (g_color[i].color == NULL)
    {
      my_put_nbr(n);
      return ;
    }
  my_putstr(g_color[i].unicode);
  my_put_nbr(n);
  my_putstr("\033[0m");
}