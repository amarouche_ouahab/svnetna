/*
** aff_funct.c for my_slack in /home/verpilc/Documents/my_slack/server
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Mon Feb 19 01:04:44 2018 VERPILLAT Corentin
** Last update Mon Feb 19 01:04:46 2018 VERPILLAT Corentin
*/

#include "server.h"
#include "client.h"

void client_join_server(t_client *client, t_listc *p_clients, int if_connect)
{
    char message[BUFFER_SIZE];

    message[0] = 0;
    if (if_connect == TRUE)
        write_client(client->sock, "OK;You're connected");
    else
        write_client(client->sock, "OK;You join the chanel");
    my_strncpy(message, client->name, BUFFER_SIZE - 1);
    my_strncat(message, " join the chanel", sizeof message - my_strlen(message) - 1);
    send_message_to_all_clients(p_clients, client, message, TRUE);
}

void aff_info_recv(t_listc *p_clients, t_client *sender, const char *message, t_parser *parser)
{
    my_putstr_color("blue", "Nb clients: ");
    my_put_nbr_color("blue", p_clients->length);
    my_putstr("\n");
    my_putstr_color("blue", "Function: ");
    my_putstr_color("blue", parser->first);
    my_putstr("\n");
    my_putstr_color("blue", "Name: ");
    my_putstr_color("blue", sender->name);
    my_putstr("\n");
    my_putstr_color("blue", "Message: ");
    my_putstr_color("blue", message);
    my_putstr("\n");
}