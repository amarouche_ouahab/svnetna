/*
** check_client.c for my_slack in /home/verpilc/Documents/my_slack/server
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Sat Feb 17 01:54:32 2018 VERPILLAT Corentin
** Last update Sat Feb 17 01:54:34 2018 VERPILLAT Corentin
*/

#include "server.h"
#include "client.h"

int check_if_name_exist(t_listc *p_clients, char *name)
{
    t_client *temp;

    if (p_clients != NULL)
    {
        temp = p_clients->p_first;
        while (temp != NULL)
        {
            if (my_strcmp(temp->name, name) == 0)
            {
                return (ERROR);
            }
            temp = temp->p_next;
        }
    }
    return (0);
}