/*
** client.c for my_slack in /home/verpilc/Documents/my_slack/server
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Sat Feb 17 01:23:56 2018 VERPILLAT Corentin
** Last update Sat Feb 17 01:23:58 2018 VERPILLAT Corentin
*/

#include "server.h"
#include "client.h"

int write_client(int sock, const char *buffer)
{
    if (send(sock, buffer, my_strlen(buffer), 0) < 0)
    {
        my_putstr_color("red", "Failure during communication\n");
        return (ERROR);
    }
    return (0);
}

int read_client(int sock, char *buffer)
{
    int n = 0;

    if ((n = recv(sock, buffer, BUFFER_SIZE - 1, 0)) < 0)
    {
        my_putstr_color("red", "Failure during communication\n");
        n = 0;
    }
    buffer[n] = 0;
    return (n);
}

void send_message_to_all_clients(t_listc *p_clients, t_client *sender, const char *buffer, char from_server)
{
    char message[BUFFER_SIZE];
    char buf[BUFFER_SIZE];

    message[0] = 0;
    my_strncat(buf, buffer, my_strlen(buffer));
    if (p_clients != NULL && sender != NULL)
    {
        t_client *temp = p_clients->p_first;
        while (temp != NULL)
        {
            if (sender->sock != temp->sock && (my_strcmp(sender->room, temp->room) == 0))
            {
                if (from_server == 0)
                {
                    my_strncpy(message, sender->name, BUFFER_SIZE - 1);
                    my_strncat(message, " : ", 4);
                    my_strncat(message, buffer, my_strlen(buffer));
                }
                else
                    my_strncpy(message, (void *)buffer, BUFFER_SIZE - 1);
                write_client(temp->sock, message);
            }
            temp = temp->p_next;
        }
    }
}

void send_message_to_client(t_client *client, t_client *sender, const char *buffer, char from_server)
{
    char message[BUFFER_SIZE];
    
    message[0] = 0;
    if (client != NULL && sender != NULL)
    {
        if (from_server == 0)
        {
            my_strncpy(message, sender->name, BUFFER_SIZE - 1);
            my_strncat(message, " : ", my_strlen(buffer));
        }
        my_strncat(message, buffer, my_strlen(buffer));
        write_client(client->sock, message);
    }
}