/*
** identification.c for slack.c in /home/verpilc/Documents/my_slack/server
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Sun Feb 18 19:05:20 2018 VERPILLAT Corentin
** Last update Sun Feb 18 19:05:22 2018 VERPILLAT Corentin
*/

#include "server.h"
#include "client.h"

void client_logout(t_listc *p_clients, t_client *temp, char *buffer)
{
    close(temp->sock);
    my_strncpy(buffer, "OK;", BUFFER_SIZE - 1);
    my_strncat(buffer, temp->name, BUFFER_SIZE - my_strlen(buffer) - 1);
    delete_client(p_clients, temp);
    my_strncat(buffer, " disconnected", BUFFER_SIZE - my_strlen(buffer) - 1);
    send_message_to_all_clients(p_clients, temp, buffer, 1);
}

t_client *client_connect(t_listc *p_clients, fd_set *rdfs, int sock, char *buffer, int *max)
{
    int client_sock;
    t_client *new_client;

    if ((client_sock = read_message(sock, buffer)) == ERROR)
    {
        return (NULL);
    }
    (*max) = client_sock > (*max) ? client_sock : (*max);
    if (check_if_name_exist(p_clients, buffer) == 0)
    {
        FD_SET(client_sock, &(*rdfs));
        new_client = add_client(p_clients, buffer, client_sock);
    }
    else
    {
        client_refused(client_sock, "KO;Username already in use");
        (*max) = client_sock == (*max) ? (*max - 1) : (*max);
        return (NULL);
    }
    return (new_client);
}

void play_fnct(t_listc *p_clients, const char *rl, t_client *sender)
{
    int i;
    char *tmp;
    t_parser *parser;
    int found;

    i = 0;
    found = FALSE;
    if ((tmp = my_strdup(rl)) == NULL)
        my_putstr_color("red", "Parsage error\n");
    delete_first_and_delim(tmp, FALSE);
    while (command_tab[i].cha != NULL && found != TRUE)
    {
        if (my_strcmp_delim(tmp, command_tab[i].cha) == 0)
        {
            found = TRUE;
            parser = command_tab[i].fnct_server(tmp);
            if (parser != NULL)
            {
                exec_tab[i].fnct_server(p_clients, rl, sender, parser);
                free_parser(parser);
            }
        }
        i += 1;
    }
}

void client_message(t_listc *p_clients, t_client *sender, const char *buffer)
{
    if (buffer[0] == '/')
        play_fnct(p_clients, buffer, sender);
    else
        send_message_to_all_clients(p_clients, sender, buffer, 0);
}

int client_refused(int client_sock, char *message)
{
    write_client(client_sock, message);
    close(client_sock);
    return (0);
}