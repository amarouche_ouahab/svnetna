/*
** end_client.c for my_slack in /home/verpilc/Documents/my_slack/server
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Sat Feb 17 01:27:06 2018 VERPILLAT Corentin
** Last update Sat Feb 17 01:27:09 2018 VERPILLAT Corentin
*/

#include "server.h"
#include "client.h"

void end_connection(int sock)
{
   close(sock);
}