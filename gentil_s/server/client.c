/*
** client.c for my_slack in /home/verpilc/Documents/my_slack/server
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Sat Feb 17 10:06:09 2018 VERPILLAT Corentin
** Last update Sat Feb 17 10:06:11 2018 VERPILLAT Corentin
*/

#include "server.h"
#include "client.h"

t_listc *create_clients(void)
{
    t_listc *p_clients;

    if ((p_clients = malloc(sizeof *p_clients)) != NULL)
    {
        p_clients->length = 0;
        p_clients->p_first = NULL;
        p_clients->p_last = NULL;
    }
    return (p_clients);
}

t_client *add_client(t_listc *p_clients, char *name, int sock)
{
    t_client *p_new;

    if (p_clients != NULL)
    {
        if ((p_new = malloc(sizeof *p_new)) != NULL)
        {
            if ((p_new->name = my_strdup(name)) == NULL)
            {
                return (NULL);
            }
            if ((p_new->room = my_strdup(room_tab[0].room)) == NULL)
            {
                return (NULL);
            }
            p_new->sock = sock;
            p_new->p_next = NULL;
            if (p_clients->p_last == NULL)
            {
                p_new->p_prev = NULL;
                p_clients->p_first = p_new;
                p_clients->p_last = p_new;
            }
            else
            {
                p_clients->p_last->p_next = p_new;
                p_new->p_prev = p_clients->p_last;
                p_clients->p_last = p_new;
            }
            p_clients->length++;
        }
    }
    return (p_new);
}

void delete_client(t_listc *p_clients, t_client *client)
{
    t_client *temp;
    int del;

    if (p_clients != NULL && client != NULL)
    {
        if (p_clients->length == 1)
        {
            p_clients->p_first = NULL;
            p_clients->p_last = NULL;
            p_clients->length -= 1;
        }
        else
        {
            del = 0;
            temp = p_clients->p_first;
            while (temp != NULL && !del)
            {
                del = del_condition(p_clients, client, &temp);
            }
            free(temp->name);
            free(temp);
        }
    }
}

int del_condition(t_listc *p_clients, t_client *client, t_client **temp)
{
    if ((*temp) == client)
    {
        if ((*temp)->p_next == NULL)
        {
            p_clients->p_last = (*temp)->p_prev;
            p_clients->p_last->p_next = NULL;
        }
        else if ((*temp)->p_prev == NULL)
        {
            p_clients->p_first = (*temp)->p_next;
            p_clients->p_first->p_prev = NULL;
        }
        else
        {
            (*temp)->p_next->p_prev = (*temp)->p_prev;
            (*temp)->p_prev->p_next = (*temp)->p_next;
        }
        p_clients->length -= 1;
        return (1);
    }
    else
        (*temp) = (*temp)->p_next;
    return (0);
}

void remove_clients(t_listc **p_clients)
{
    if (*p_clients != NULL)
    {
        t_client *temp = (*p_clients)->p_first;
        while (temp != NULL)
        {
            t_client *to_del = temp;
            close(temp->sock);
            temp = temp->p_next;
            free(to_del);
        }
        free(*p_clients);
        *p_clients = NULL;
    }
}