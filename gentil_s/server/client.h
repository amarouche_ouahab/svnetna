/*
** ftl.h for my_ftl in /home/verpilc/Documents/my_ftp
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Mon Nov  6 14:29:06 2017 VERPILLAT Corentin
** Last update Wed Feb 14 11:30:23 2018 VERPILLAT Corentin
*/

#ifndef __CLIENT_H__
#define __CLIENT_H__

typedef struct s_client t_client;
struct s_client
{
  int sock;
  char *name;
  char *room;
  t_client *p_next;
  t_client *p_prev;
};

typedef struct s_listc t_listc;
struct s_listc
{
  size_t length;
  t_client *p_first;
  t_client *p_last;
};

t_listc *create_clients(void);
t_client *add_client(t_listc *p_clients, char *name, int sock);
void delete_client(t_listc *p_clients, t_client *client);
int del_condition(t_listc *p_clients, t_client *client, t_client **temp);
void remove_clients(t_listc **p_clients);

int check_if_name_exist(t_listc *p_clients, char *name);
int new_client(fd_set *rdfs, int sock, char *buffer, int *max, t_listc *p_clients);
void client_logout(t_listc *p_clients, t_client *temp, char *buffer);
void client_message(t_listc *p_clients, t_client *sender, const char *buffer);
t_client *client_connect(t_listc *p_clients, fd_set *rdfs, int sock, char *buffer, int *max);
int client_refused(int client_sock, char *message);
void client_join_server(t_client *client, t_listc *p_clients, int if_connect);

#endif
