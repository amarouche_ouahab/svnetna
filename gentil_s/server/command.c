/*
** command.c for my_slack in /home/verpilc/Documents/my_slack/server
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Tue Feb 20 19:18:08 2018 VERPILLAT Corentin
** Last update Tue Feb 20 19:18:09 2018 VERPILLAT Corentin
*/

#include "server.h"
#include "client.h"

int commands_list(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser)
{
    int i;
    char message_to_send[BUFFER_SIZE];

    i = 0;
    aff_info_recv(p_clients, sender, message, parser);
    my_strncpy(message_to_send, "OK;", BUFFER_SIZE - 1);
    while (exec_tab[i].cha != NULL)
    {
        my_strncat(message_to_send, exec_tab[i].description, my_strlen(exec_tab[i].description));
        my_strncat(message_to_send, "\n", 1);
        i += 1;
    }
    write_client(sender->sock, message_to_send);
    return (0);
}

int clients_list(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser)
{
    char message_to_send[BUFFER_SIZE];
    t_client *temp;

    aff_info_recv(p_clients, sender, message, parser);
    my_strncpy(message_to_send, "OK;", BUFFER_SIZE - 1);
    temp = p_clients->p_first;
    while (temp != NULL)
    {
        if (my_strcmp(temp->name, sender->name) != 0)
        {
            my_strncat(message_to_send, temp->room, BUFFER_SIZE);
            my_strncat(message_to_send, " : ", 4);
            my_strncat(message_to_send, temp->name, BUFFER_SIZE);
            my_strncat(message_to_send, "\n", 1);
        }
        temp = temp->p_next;
    }
    write_client(sender->sock, message_to_send);
    return (0);
}

int direct_message(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser)
{
    t_client *temp;
    int found;

    found = FALSE;
    aff_info_recv(p_clients, sender, message, parser);
    temp = p_clients->p_first;
    while (temp != NULL && found != TRUE)
    {
        if (my_strcmp(temp->name, sender->name) != 0)
        {
            if (my_strcmp(temp->name, parser->second) == 0)
            {
                found = TRUE;
                send_message_to_client(temp, sender, "message private from ", TRUE);
                send_message_to_client(temp, sender, parser->third, FALSE);
            }
        }
        temp = temp->p_next;
    }
    if (found == FALSE)
        write_client(sender->sock, "KO;Client doesn't exist");
    return (0);
}

int join(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser)
{
    int found;
    int i;

    found = FALSE;
    i = 0;
    aff_info_recv(p_clients, sender, message, parser);
    while (room_tab[i].room != NULL && found != TRUE)
    {
        if (my_strcmp_delim(room_tab[i].room, parser->second) == 0)
        {
            found = TRUE;
            send_message_to_all_clients(p_clients, sender, "quit the chanel", FALSE);
            free(sender->room);
            if ((sender->room = my_strdup(room_tab[i].room)) == NULL)
                return (ERROR);
            else
                send_message_to_all_clients(p_clients, sender, "join the chanel", FALSE);
        }
        i += 1;
    }
    if (found == FALSE)
        write_client(sender->sock, "KO;Chanel doesn't exist");
    return (0);
}

int chanels_list(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser)
{
    int i;
    char message_to_send[BUFFER_SIZE];

    i = 0;
    aff_info_recv(p_clients, sender, message, parser);
    my_strncpy(message_to_send, "OK;", BUFFER_SIZE - 1);
    while (room_tab[i].room != NULL)
    {
        my_strncat(message_to_send, room_tab[i].room, my_strlen(room_tab[i].room));
        my_strncat(message_to_send, "\n", 1);
        i += 1;
    }
    write_client(sender->sock, message_to_send);
    return (0);
}