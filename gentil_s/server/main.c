/*
** main.c for my_slack in /home/verpilc/Documents/my_slack
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Wed Feb 14 11:21:32 2018 VERPILLAT Corentin
** Last update Wed Feb 14 11:21:50 2018 VERPILLAT Corentin
*/

#include "server.h"
#include "client.h"

int main()
{
    int sock;
    char buffer[BUFFER_SIZE];
    int max;
    t_listc *p_clients;

    sock = init_connection();
    p_clients = create_clients();
    max = sock;
    server(sock, buffer, max, p_clients);
    return (EXIT_SUCCESS);
}

int server(int sock, char *buffer, int max, t_listc *p_clients)
{
    fd_set rdfs;

    while (TRUE)
    {
        create_descriptor(p_clients, &rdfs, sock);
        if (select(max + 1, &rdfs, NULL, NULL, NULL) == -1)
        {
            my_putstr_color("red", "Failure during exchange\n");
            return (ERROR);
        }
        if (FD_ISSET(STDIN_FILENO, &rdfs))
            break;
        else if (FD_ISSET(sock, &rdfs))
        {
            if (new_client(&rdfs, sock, buffer, &max, p_clients) == ERROR)
                continue;
        }
        else
            if (message_recv(p_clients, &rdfs, buffer, &max) == 2)  
                continue;
    }
    remove_clients(&p_clients);
    end_connection(sock);
    return (0);
}

void create_descriptor(t_listc *p_clients, fd_set *rdfs, int sock)
{
    FD_ZERO(&(*rdfs));
    FD_SET(STDIN_FILENO, &(*rdfs));
    FD_SET(sock, &(*rdfs));
    if (p_clients != NULL)
    {
        t_client *temp = p_clients->p_first;
        while (temp != NULL)
        {
            FD_SET(temp->sock, &(*rdfs));
            temp = temp->p_next;
        }
    }
}

int new_client(fd_set *rdfs, int sock, char *buffer, int *max, t_listc *p_clients)
{
    t_client *client;
    int client_sock;

    if (p_clients->length < MAX_CLIENTS)
    {
        if ((client = client_connect(p_clients, &(*rdfs), sock, buffer, &(*max))) != NULL)
        {
            client_join_server(client, p_clients, TRUE);
            return (0);
        }
    }
    else
    {
        if ((client_sock = read_message(sock, buffer)) == ERROR)
            return (ERROR);
        client_refused(client_sock, "KO;Server is full");
    }
    return (ERROR);
}

int message_recv(t_listc *p_clients, fd_set *rdfs, char *buffer, int *max)
{
    int c;

    if (p_clients != NULL)
    {
        t_client *temp = p_clients->p_first;
        while (temp != NULL)
        {
            if (FD_ISSET(temp->sock, &(*rdfs)))
            {
                if ((c = read_client(temp->sock, buffer)) == 0)
                {
                    (*max) = temp->sock == (*max) ? (*max - 1) : (*max);
                    client_logout(p_clients, temp, buffer);
                    return (2);
                }
                else
                    client_message(p_clients, temp, buffer);
                break;
            }
            temp = temp->p_next;
        }
    }
    return (0);
}