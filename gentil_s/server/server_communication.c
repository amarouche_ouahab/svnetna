/*
** server_communication.c for my_slack in /home/verpilc/Documents/my_slack/server
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Sun Feb 18 23:31:33 2018 VERPILLAT Corentin
** Last update Sun Feb 18 23:31:34 2018 VERPILLAT Corentin
*/

#include "server.h"
#include "client.h"

void server_speak_to_client(t_client *client, char *message)
{
    if (client != NULL)
    {
        write_client(client->sock, message);
    }
}

void server_speak_to_clients(t_listc *p_clients, t_client *sender, char *message)
{
    if (p_clients != NULL && sender != NULL)
    {
        t_client *temp = p_clients->p_first;
        while (temp != NULL)
        {
            if (sender->sock != temp->sock)
            {
                write_client(temp->sock, message);
            }
            temp = temp->p_next;
        }
    }
}