/*
** parser_condition.c for my_slack in /home/verpilc/Documents/my_slack
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Tue Feb 20 01:11:58 2018 VERPILLAT Corentin
** Last update Tue Feb 20 01:11:59 2018 VERPILLAT Corentin
*/

#include "common.h"

int if_parser_found(int *j, int *i, char *line)
{
    *j += 1;
    if (*j > 3)
    {
        return (ERROR);
    }
    while (line[*i] != '\0' && line[*i] == PARSER)
    {
        *i += 1;
    }
    return (0);
}

void free_parser(t_parser *parser)
{
    free(parser->first);
    free(parser->second);
    free(parser->third);
    free(parser);
}

t_parser *init_parser(char *line)
{
    t_parser *parser;
    char *part_message;

    if ((parser = malloc(sizeof(parser) * (sizeof(parser->second)) * (sizeof(parser->third)) * (sizeof(parser->first)))) != NULL)
    {
        part_message = get_specific_word(line, 1, &parser->length);
        if ((parser->first = my_strdup(part_message)) == NULL)
            return (NULL);
        free(part_message);
        part_message = get_specific_word(line, 2, &parser->length);
        if ((parser->second = my_strdup(part_message)) == NULL)
            return (NULL);
        free(part_message);
        part_message = get_specific_word(line, 3, &parser->length);
        if ((parser->third = my_strdup(part_message)) == NULL)
            return (NULL);
        free(part_message);
    }
    else
    {
        return (NULL);
    }
    return (parser);
}

char *get_specific_word(char *str, int j, int *length)
{
    int i;
    int l;
    int k;
    char *message;

    i = 0;
    l = 0;
    k = 1;
    if ((message = malloc((my_strlen(str)) * sizeof(char))) == NULL)
        return (NULL);
    while (str[i] != '\0')
    {
        if (str[i] == PARSER)
        {
            if (if_parser_found(&k, &i, str) == ERROR)
                return (NULL);
        }
        else
        {
            if (k == j)
            {
                message[l] = str[i];
                l += 1;
            }
            i += 1;
        }
        message[l] = '\0';
    }
    *length = k;
    return (message);
}