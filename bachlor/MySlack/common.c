/*
** common.c for my_slack in /home/verpilc/Documents/my_slack
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Tue Feb 20 17:49:56 2018 VERPILLAT Corentin
** Last update Tue Feb 20 17:49:57 2018 VERPILLAT Corentin
*/

#include "common.h"

int write_server(int sock, const char *buffer)
{
    if (send(sock, buffer, my_strlen(buffer), 0) < 0)
    {
        my_putstr_color("red", "Failure during communication\n");
        return (ERROR);
    }
    return (0);
}