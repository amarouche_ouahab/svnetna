/*
** server.c for my_slack in /home/verpilc/Documents/my_slack/server
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Sat Feb 17 01:35:19 2018 VERPILLAT Corentin
** Last update Sat Feb 17 01:35:21 2018 VERPILLAT Corentin
*/

#include "server.h"
#include "client.h"

int init_connection(void)
{
   int sock;
   SOCKADDR_IN sin;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == ERROR)
    {
        my_putstr_color("red", "Error with the socket\n");
        return (ERROR);
    }
    sin.sin_port = htons(PORT);
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = inet_addr(ADDR);
    if(bind(sock,(SOCKADDR *) &sin, sizeof sin) == ERROR)
    {
        my_putstr_color("red", "Already in use\n");
        return (ERROR);
    }
    if(listen(sock, MAX_CLIENTS) == ERROR)
    {
        my_putstr_color("red", "Error during the listenning\n");
        return (ERROR);
    }
    return (sock);
}

int read_message(int sock, char *buffer)
{
    SOCKADDR_IN csin;
    uint sin_size;
    int client_sock;

    sin_size = sizeof csin;
    client_sock = accept(sock, (SOCKADDR *)&csin, &sin_size);
    if (client_sock == ERROR)
    {
        my_putstr_color("red", "Connection refused\n");
        return (ERROR);
    }
    if (read_client(client_sock, buffer) == -1)
    {
        return (ERROR);
    }
    return (client_sock);
}