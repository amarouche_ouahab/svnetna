/*
** ftl.h for my_ftl in /home/verpilc/Documents/my_ftp
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Mon Nov  6 14:29:06 2017 VERPILLAT Corentin
** Last update Wed Feb 14 11:30:23 2018 VERPILLAT Corentin
*/

#ifndef __SERVER_H__
#define __SERVER_H__

#include "../common.h"
#include "client.h"

void aff_info_recv(t_listc *p_clients, t_client *sender, const char *message, t_parser *parser);

int server(int sock, char *buffer, int max, t_listc *p_clients);
int init_connection(void);
void end_connection(int sock);

void create_descriptor(t_listc *p_clients, fd_set *rdfs, int sock);
int read_client(int sock, char *buffer);
int write_client(int sock, const char *buffer);
int message_recv(t_listc *p_clients, fd_set *rdfs, char *buffer, int *max);

int read_message(int sock, char *buffer);
void play_fnct(t_listc *p_clients, const char *buffer, t_client *sender);

void send_message_to_all_clients(t_listc *p_clients, t_client *sender, const char *buffer, char from_server);
void send_message_to_client(t_client *client, t_client *sender, const char *buffer, char from_server);
void server_speak_to_clients(t_listc *p_clients, t_client *sender, char *message);
void server_speak_to_client(t_client *client, char *message);

int commands_list(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser);
int clients_list(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser);
int direct_message(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser);
int join(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser);
int chanels_list(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser);

typedef struct s_client_exec t_client_exec;
struct s_client_exec
{
  char *cha;
  int (*fnct_server)(t_listc *p_clients, const char *message, t_client *sender, t_parser *parser);
  int nb_element;
  char *description;
};

static const t_client_exec exec_tab[] =
    {
        {"commands_list", &commands_list, 1, "/commands_list: get list of commands"},
        {"clients_list", &clients_list, 1, "/clients_list: get list of clients"},
        {"join", &join, 2, "/join;chanel_name: join a chanel"},
        {"direct", &direct_message, 3, "/direct;user;message: send a private message"},
        {"chanels_list", &chanels_list, 1, "/chanels_list: get list of chanel"},
        {NULL, NULL, 0, NULL}};


#endif
