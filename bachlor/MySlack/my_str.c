/*
** my_str.c for my_slack in /home/verpilc/Documents/my_slack
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Sat Feb 17 00:36:08 2018 VERPILLAT Corentin
** Last update Sat Feb 17 00:36:10 2018 VERPILLAT Corentin
*/

#include "common.h"

char *my_strncpy(char *str, char *src, int n)
{
  int i;

  i = 0;
  while (i < n && src[i] != '\0')
  {
    str[i] = src[i];
    i++;
  }
  while (i <= n)
  {
    str[i] = '\0';
    i++;
  }
  return (str);
}

void my_puts(const char *str)
{
  my_putstr(str);
  my_putstr("\n");
}

char *my_strncat(char *str, const char *src, int nb)
{
  int length;
  int i;

  length = my_strlen(str);
  i = 0;
  while (i < nb && src[i])
  {
    str[length + i] = src[i];
    i += 1;
  }
  str[length + i] = '\0';
  return (str);
}

char *delete_first_and_delim(char *str, int if_stop_delim)
{
  int i;

  i = 0;
  while (str[i] != '\0')
  {
    if (if_stop_delim == TRUE)
    {
      if (str[i + 1] == PARSER)
      {
        str[i] = '\0';
        return (str);
      }
      str[i] = str[i + 1];
    }
    else
    {
      str[i] = str[i + 1];
    }
    i += 1;
  }
  return (str);
}

int			my_strcmp_delim(const char *s1, const char *s2)
{
  int			i;

  if (s1 == NULL || s2 == NULL)
    return (-2);
  i = 0;
  while (s1[i] != '\0' && s1[i] != PARSER)
    {
      if (s1[i] > s2[i])
	return (1);
      else if (s1[i] < s2[i])
	return (-1);
      i++;
    }
  if (s2[i] != '\0')
    return (-1);
  return (0);
}