/*
** common.h for my_slack in /home/verpilc/Documents/my_slack
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Wed Feb 14 13:54:45 2018 VERPILLAT Corentin
** Last update Wed Feb 14 13:54:46 2018 VERPILLAT Corentin
*/

#ifndef __SLACK_H__
#define __SLACK_H_

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

#define ERROR -1
#define MAX_CLIENTS 100
#define BUFFER_SIZE 1024
#define PORT 12345
#define ADDR "0.0.0.0"
#define TRUE 1
#define FALSE 0
#define PARSER ';'

typedef struct sockaddr_in SOCKADDR_IN;

typedef struct sockaddr SOCKADDR;

typedef struct in_addr IN_ADDR;

typedef struct s_color t_color;
struct s_color
{
  char *color;
  char *unicode;
};

static const t_color g_color[] =
    {
        {"clear", "\033[H\033[2J"},
        {"red", "\033[31m"},
        {"green", "\033[32m"},
        {"yellow", "\033[33m"},
        {"blue", "\033[34m"},
        {"magenta", "\033[35m"},
        {"cyan", "\033[36m"},
        {NULL, NULL}};

void my_put_nbr(int n);
void my_put_nbr_color(const char *color, const int n);
void my_putchar(const char c);
int my_strlen(const char *str);
void my_putstr(const char *str);
int my_strcmp(const char *s1, const char *s2);
char *my_strdup(const char *str);
void my_putstr_color(const char *color, const char *str);
void my_puts(const char *str);
char *my_strncat(char *str, const char *src, int nb);
char *my_strncpy(char *dest, char *src, int n);
char *delete_first_and_delim(char *str, int if_stop_delim);
int my_strcmp_delim(const char *s1, const char *s2);

typedef struct s_parser t_parser;
struct s_parser
{
  int length;
  char *first;
  char *second;
  char *third;
};

t_parser *parser_command_client(char *rl, int sock, char *tmp, int length);
t_parser *parser_command_server(char *tmp);
int if_parser_found(int *j, int *i, char *line);
char *get_specific_word(char *str, int j, int *length);
t_parser *init_parser(char *line);
void free_parser(t_parser *parser);

typedef struct s_client_command t_client_command;
struct s_client_command
{
  char *cha;
  t_parser *(*fnct_client)(char *, int, char *, int);
  t_parser *(*fnct_server)(char *);
  int nb_element;
};

static const t_client_command command_tab[] =
    {
        {"commands_list", &parser_command_client, &parser_command_server, 1},
        {"clients_list", &parser_command_client, &parser_command_server, 1},
        {"join", &parser_command_client, &parser_command_server, 2},
        {"direct", &parser_command_client, &parser_command_server, 3},
        {"chanels_list", &parser_command_client, &parser_command_server, 1},
        {NULL, NULL, NULL, 0}};

typedef struct s_client_room t_client_room;
struct s_client_room
{
  char *room;
};

static const t_client_room room_tab[] =
    {
        {"General"},
        {"Vikings"},
        {"Casa de papel"},
        {"The good place"},
        {NULL}};


#endif
