/*
** ftl.h for my_ftl in /home/verpilc/Documents/my_ftp
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Mon Nov  6 14:29:06 2017 VERPILLAT Corentin
** Last update Wed Feb 14 11:30:23 2018 VERPILLAT Corentin
*/

#ifndef __CLIENT_H__
#define __CLIENT_H__

#include "../common.h"

int client(const char *address, const char *name);
int init_connection(const char *address);
void end_connection(int sock);
int read_server(int sock, char *buffer);
int write_server(int sock, const char *buffer);
int descriptor_and_select(fd_set *rdfs, int sock);
void get_action(int sock, char **rl);
char *readline(void);
int server_connect(int sock, char *buffer);
void play_fnct(char *rl, int sock);

#endif
