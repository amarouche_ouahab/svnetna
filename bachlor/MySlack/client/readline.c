/*
** readline.c for my_slack in /home/verpilc/Documents/my_slack/client
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Mon Feb 19 20:46:32 2018 VERPILLAT Corentin
** Last update Mon Feb 19 20:46:40 2018 VERPILLAT Corentin
*/

#include  "../common.h"

char		*readline(void)
{
  ssize_t	ret;
  char		*buff;

  if ((buff = malloc((50 + 1) * sizeof(char))) == NULL)
    return (NULL);
  if ((ret = read(0, buff, 50)) > 1)
    {
      buff[ret - 1] = '\0';
      return (buff);
    }
  free(buff);
  return (NULL);
}