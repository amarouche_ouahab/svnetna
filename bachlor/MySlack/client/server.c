/*
** server.c for my_slack in /home/verpilc/Documents/my_slack/client
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Mon Feb 19 21:22:14 2018 VERPILLAT Corentin
** Last update Mon Feb 19 21:22:18 2018 VERPILLAT Corentin
*/

#include "client.h"

int server_connect(int sock, char *buffer)
{
    int n;

    n = read_server(sock, buffer);
    if (n == 0)
    {
        my_putstr_color("red", "Server disconnected \n");
        return (ERROR);
    }
    my_puts(buffer);
    return (0);
}