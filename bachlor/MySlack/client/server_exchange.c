/*
** server_exchange.c for my_slack in /home/verpilc/Documents/my_slack/client
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Mon Feb 19 20:36:35 2018 VERPILLAT Corentin
** Last update Mon Feb 19 20:36:36 2018 VERPILLAT Corentin
*/

#include "client.h"

int init_connection(const char *address)
{
    int sock;
    SOCKADDR_IN sin;
    struct hostent *hostinfo;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == ERROR)
    {
        my_putstr_color("red", "Error with the socket\n");
        return (ERROR);
    }
    hostinfo = gethostbyname(address);
    if (hostinfo == NULL)
    {
        my_putstr_color("red", "Unknown host\n");
        return (ERROR);
    }
    sin.sin_addr = *(IN_ADDR *)hostinfo->h_addr;
    sin.sin_port = htons(PORT);
    sin.sin_family = AF_INET;
    if (connect(sock, (SOCKADDR *)&sin, sizeof(SOCKADDR)) == ERROR)
    {
        my_putstr_color("red", "Server isn't responding\n");
        return (ERROR);
    }
    return (sock);
}

void end_connection(int sock)
{
    close(sock);
}

int read_server(int sock, char *buffer)
{
    int n = 0;

    if ((n = recv(sock, buffer, BUFFER_SIZE - 1, 0)) < 0)
    {
        my_putstr_color("red", "Failure during communication\n");
        return (ERROR);
    }
    buffer[n] = 0;
    return n;
}

int descriptor_and_select(fd_set *rdfs, int sock)
{
    FD_ZERO(&(*rdfs));
    FD_SET(STDIN_FILENO, &(*rdfs));
    FD_SET(sock, &(*rdfs));
    if (select(sock + 1, &(*rdfs), NULL, NULL, NULL) == -1)
    {
        my_putstr_color("red", "Failure during exchange\n");
        return (ERROR);
    }
    return (0);
}