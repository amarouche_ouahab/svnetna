import React  from 'react';
import  {Header}  from 'react-native-elements';
import {createStackNavigator, TabNavigator} from 'react-navigation';
import { View, StatusBar, Title, Button, Left, Right, Body, Icon} from 'react-native';
import UploadScreen from './Pages/Upload.js';
import GraphScreen from './Pages/Graph.js';

const Tabs = TabNavigator({
  Upload: { screen: UploadScreen }, //Screens
  Graph: { screen: GraphScreen },
  },
  {
    tabBarPosition: "bottom",
    initialRouteName: 'Upload',
    tabBarOptions:{
      showIcon:true,
      pressColor:"#FF0245",
      style:{
        backgroundColor:"#a2273c",
        borderTopWidth:1,
        borderColor:"#3f101c"
      }
    }
  },
);

export default class App extends React.Component {

  render() {
    return (
      <View style={{flex:1}}>
       <Header
          centerComponent={{ text: 'APP Meteo', style: { color: '#fff' } }}
        />
        <StatusBar hidden={true}/>
        <Tabs/>
      </View>
    );
  }
}


{/* /*import React from 'react';
import { StyleSheet, Text, View } from 'react-native';



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
 */}