package com.penelope.dao.factory;

import com.penelope.dao.IDao;
import com.penelope.dao.IPersonneDao;
import com.penelope.dao.impl.json.JsonContactDaoImpl;
import com.penelope.dao.impl.json.JsonGroupeDaoImpl;
import com.penelope.dao.impl.json.JsonPersonneDaoImpl;
import com.penelope.dao.impl.json.JsonRoleDaoImpl;
import com.penelope.entities.Personne;
import com.penelope.observer.MyObserver;
import com.penelope.observer.MySubjectObserver;
import com.penelope.utils.Constants;

public class JsonDaoFactory extends AbstractDaoFactory {

    private IPersonneDao personneDao = null;
    private IDao contactDao = null;
    private IDao roleDao = null;
    private IDao groupeDao = null;
    private static MySubjectObserver mySubjectObserver;

    private JsonDaoFactory(MySubjectObserver subject) {
        mySubjectObserver = subject;
    }

    private static class JsonDaoFactoryHolder {
        private static JsonDaoFactory instance = new JsonDaoFactory(mySubjectObserver);
    }

    static JsonDaoFactory getInstance(MySubjectObserver subject) {
        mySubjectObserver = subject;
        return JsonDaoFactoryHolder.instance;
    }

    @Override
    public IDao getPersonneDao()
    {
        return JsonPersonneDaoImpl.getInstance(mySubjectObserver);
    }

    @Override
    public IDao getContactDao() {
        mySubjectObserver.addObserveur((MyObserver) this.getPersonneDao());
        return JsonContactDaoImpl.getInstance(mySubjectObserver);
    }

    @Override
    public IDao getRoleDao() {
        return JsonRoleDaoImpl.getInstance(mySubjectObserver);
    }

    @Override
    public IDao getGroupeDao() {
        mySubjectObserver.addObserveur((MyObserver) this.getPersonneDao());
        return JsonGroupeDaoImpl.getInstance(mySubjectObserver);
    }

}
