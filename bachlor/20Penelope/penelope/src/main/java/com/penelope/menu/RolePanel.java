package com.penelope.menu;

import com.penelope.dao.IDao;
import com.penelope.dao.factory.AbstractDaoFactory;
import com.penelope.entities.Contact;
import com.penelope.entities.Groupe;
import com.penelope.entities.Personne;
import com.penelope.entities.Role;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

public class RolePanel {
    private AbstractDaoFactory jsonFac = AbstractDaoFactory.getDaoFactory(AbstractDaoFactory.FactoryType.JSON_DAO);
    private IDao roleDao = jsonFac.getRoleDao();
    private ObservableList<Role> roleFx =  FXCollections.observableArrayList(roleDao.findAll());

    private static class RolePanelHolder
    {
        private final static RolePanel instance = new RolePanel();
    }

    public static RolePanel getInstance()
    {
        return RolePanel.RolePanelHolder.instance;
    }

    public StackPane RoleList(){
        StackPane roleList = new  StackPane();
        TableView<Role> roles = new TableView<Role>();

        TableColumn identifiant = new TableColumn("Identifiant");
        identifiant.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Role, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Role, String> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getidentifiant());
            }
        });
        TableColumn description= new TableColumn("Description");
        description.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Role, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Role, String> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getdescription());
            }
        });
        roles.getColumns().addAll(identifiant,description);
        roles.setItems(getList(roleFx));
        roleList.getChildren().add(roles);
        roles.setRowFactory(new Callback<TableView<Role>, TableRow<Role>>() {
            @Override
            public TableRow<Role> call(TableView<Role> param) {
                TableRow<Role> row = new TableRow<>();
                row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent event) {
                        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                            Role rowData = row.getItem();

                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Supprimer un role");
                            alert.setHeaderText("Voulez vous supprimer ce role ?");
                            alert.setContentText(rowData.getidentifiant() );
                            Optional<ButtonType> option = alert.showAndWait();
                            if (option.get() == null) {
//                                label.setText("No selection!");
                            } else if (option.get() == ButtonType.OK) {
                                roleDao.delete(rowData.getId());
                                roleFx.remove(rowData);
                                roleList.getChildren().remove(roles);
                                roleList.getChildren().add(RoleList());
                            } else if (option.get() == ButtonType.CANCEL) {
//                                label.setText("Cancelled!");
                            } else {
//                                label.setText("-");
                            }
//                            ro.getChildren().remove(users);
//                            UserList.getChildren().add(DetailUser(rowData.getId()));
                        }
                    }
                });
                return row ;
            }
        });
        return roleList;
    }

    public StackPane CreateRole(){
        StackPane CreateRole = new  StackPane();
        Label label = new Label();
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(120, 100, 100, 120));
        grid.setVgap(10);
        grid.setHgap(10);

        TextField ident = new TextField();
        TextField desc = new TextField();

        ident.setPromptText("Identifiant");
        desc.setPromptText("Description");

        GridPane.setConstraints(ident, 0, 0);
        grid.getChildren().add(ident);

        GridPane.setConstraints(desc, 0, 2);
        grid.getChildren().add(desc);
        Button submit = new Button("Submit");
        GridPane.setConstraints(submit, 1, 2);
        grid.getChildren().add(submit);
        GridPane.setConstraints(label, 1, 3);
        grid.getChildren().add(label);

        StackPane r = new StackPane();
        r.setPadding(new Insets(5, 1, 2, 1));
        r.setBackground((new Background(new BackgroundFill(Color.rgb(40, 40, 40), null, new Insets(100, 100, 100, 100)))));

        r.getChildren().add(grid);
        CreateRole.getChildren().add(r);
        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if ((ident.getText() != null && !ident.getText().isEmpty())
                && (desc.getText() != null && !desc.getText().isEmpty()) ) {
                    Role groupe = new Role(ident.getText(),desc.getText());
                    roleDao.create(groupe);
                    roleFx.add(groupe);
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Creation");
                    alert.setHeaderText("Results:");
                    alert.setContentText("Le role " + ident.getText()+ " a bien été crée");
                    alert.showAndWait();
                } else {
                    label.setText("Veuillez remplir tout les champs !");
                    label.setTextFill(Color.WHEAT);
                }
            }
        });
        return CreateRole;
    }

    public <T>  ObservableList getList( ObservableList<T> per){
        HashSet<T> h = new HashSet<T>(per);
        per.clear();
        per.addAll(h);
        return per;
    }

}

