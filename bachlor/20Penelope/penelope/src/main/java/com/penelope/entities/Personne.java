package com.penelope.entities;

import org.json.simple.JSONObject;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Personne implements IUser<Personne> {

    private Integer id;
    private String firstname;
    private String lastname;
    private String civilite;
    private Integer role;
    private String road;
    private String city;
    private String zipCode;
    private List groupe;
    //    private Set<Integer> contacts;
    private List contacts;

    public Personne(String firstname, String lastname, String civilite, Integer role, String road, String city, String zipCode, List groupe, List contacts) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.civilite = civilite;
        this.role = role;
        this.road = road;
        this.city = city;
        this.zipCode = zipCode;
        this.groupe = groupe;
        this.contacts = contacts;

    }

    public Personne(Integer id, String firstname, String lastname, String civilite, Integer role, String road, String city, String zipCode, List groupe, List contacts) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.civilite = civilite;
        this.role = role;
        this.road = road;
        this.city = city;
        this.zipCode = zipCode;
        this.groupe = groupe;
        this.contacts = contacts;
    }

    public Personne() {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne personne = (Personne) o;
        return Objects.equals(getId(), personne.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public List getGroupe() {
        return groupe;
    }

    public void setGroupe(List groupe) {
        this.groupe = groupe;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public JSONObject toJsonObject() {
        JSONObject obj = new JSONObject();
        obj.put("id", this.id);
        obj.put("firstname", this.firstname);
        obj.put("lastname", this.lastname);
        obj.put("civilite", this.civilite);
        obj.put("role", this.role);
        obj.put("road", this.road);
        obj.put("city", this.city);
        obj.put("zipCode", this.zipCode);
        obj.put("groupe", this.groupe);
        obj.put("contacts", this.contacts);

        return obj;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"firstname\":\"" + firstname + '\"' +
                ", \"lastname\":\"" + lastname + '\"' +
                ", \"civilite\":\"" + civilite + '\"' +
                ", \"role\":" + role +
                ", \"road\":\"" + road + '\"' +
                ", \"city\":\"" + city + '\"' +
                ", \"zipCode\":\"" + zipCode + '\"' +
                ", \"groupe\":" + groupe  +
//                ", \"contacts\":" + contacts.toString() +
                ", \"contacts\":" + contacts +
                '}';
    }

    @Override
    public boolean add(IUser obj) {
        try {
            if (obj instanceof Contact) {
                this.contacts.add(((Contact) obj).getId());
            } else if (obj instanceof Groupe) {
                this.groupe.add(((Groupe) obj).getId());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public boolean remove(IUser obj) {
        try {
            if (obj instanceof Contact) {
                for (int i = 0; i < this.contacts.size(); i++) {
                    if (((Long) this.contacts.get(i)).intValue() == ((Contact) obj).getId()) {
                        this.contacts.remove(i);
                    }
                }
            } else if (obj instanceof Groupe) {
                for (int i = 0; i < this.groupe.size(); i++) {
                    if (((Long) this.groupe.get(i)).intValue() == ((Groupe) obj).getId()) {
                        this.groupe.remove(i);
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }

        return true;
    }

    public List getContacts() {
        return contacts;
    }

    public void setContacts(List contacts) {
        this.contacts = contacts;
    }
}
