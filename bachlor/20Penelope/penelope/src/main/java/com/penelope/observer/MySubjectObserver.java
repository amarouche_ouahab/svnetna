/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.penelope.observer;

import com.penelope.entities.IUser;
import com.penelope.entities.Personne;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author elhad
 */
public class MySubjectObserver {

    private List<MyObserver> observers = new ArrayList<MyObserver>();
    private int state;
    private IUser obj;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
        notifyAllObservers();
    }

    public IUser getObject() {
        return obj;
    }

    public void setObject(IUser obj) {
        this.obj = obj;
    }

    public void addObserveur(MyObserver observer) {
        observers.add(observer);
    }

    public void notifyAllObservers() {
        for (MyObserver observer : observers) {
            observer.updateSource();
        }
    }
}
