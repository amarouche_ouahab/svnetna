package com.penelope.dao.impl.json;

import com.penelope.entities.Contact;
import com.penelope.entities.Personne;
import com.penelope.observer.MySubjectObserver;
import com.penelope.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class JsonContactDaoImpl extends AbstractJsonDao<Contact> {

    private static MySubjectObserver subject = null;
    private List<Contact> contacts = new ArrayList<>();

    public JsonContactDaoImpl(MySubjectObserver subject) {
        super(Contact.class, Constants.CONTACT_ARGS, subject, Constants.CONTACTS_JSON_PATH_FILE);
        JsonContactDaoImpl.subject = subject;
    }

    private static class JsonContactDaoImplHolder {
        private static JsonContactDaoImpl instance = new JsonContactDaoImpl(subject);
    }

    public static JsonContactDaoImpl getInstance(MySubjectObserver subject) {
        JsonContactDaoImpl.subject = subject;
        return JsonContactDaoImplHolder.instance;
    }

    @Override
    public Contact findById(int id) {
        Contact contact = super.findById(id);
        return (Contact) contact;
    }

    @Override
    public boolean delete(Contact contact) {
        return false;
    }

    @Override
    public Contact create(Contact obj) {
        super.create(obj);
        // this.sendNotification(Constants.STATE_CREATE_JSON, obj);

        return obj;
    }

    @Override
    public Contact update(Contact contact) {
        return super.update(contact);
    }

    @Override
    public boolean delete(Integer id) {
        boolean result = super.delete(id);
        if (result) {
            this.sendNotification(Constants.STATE_DELETE_IUSER, new Contact(id));
        }

        return result;
    }


    @Override
    public List<Contact> findAll() {
        this.contacts = super.findAll();
        return this.contacts;
    }

    public void sendNotification(int state, Contact contact) {
        subject.setObject(contact);
        subject.setState(state);
    }
}
