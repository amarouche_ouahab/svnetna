package com.penelope.utils;

import java.util.List;
import java.util.Set;

public class Constants {

    public static String PERSONNES_CSV_PATH_FILE = "persons.csv";
    public static String PERSONNES_JSON_PATH_FILE = "persons.json";
    public static String CONTACTS_JSON_PATH_FILE = "contacts.json";
    public static String ROLES_JSON_PATH_FILE = "roles.json";
    public static String GROUPE_JSON_PATH_FILE = "groupe.json";
    public static String CSV_HEADER = "idPersonne;Prenom;Nom;Poids;Taille;Rue;Ville;Code Postal";


    // Constante des identifications de notifications
    public static final int STATE_CREATE_JSON = 1;
    public static final int STATE_DELETE_JSON = 2;
    public static final int STATE_CREATE_XML = 3;
    public static final int STATE_DELETE_XML = 4;
    public static final int STATE_CREATE_CSV = 5;
    public static final int STATE_DELETE_CSV = 6;
    public static final int STATE_DELETE_IUSER = 42;

    public static final Class[] PERSON_ARGS = new Class[]{Integer.class, String.class, String.class, String.class, Integer.class, String.class, String.class, String.class,List.class, List.class};
    public static final Class[] CONTACT_ARGS = new Class[]{Integer.class, String.class, String.class, String.class};
    public static final Class[] ROLE_ARGS = new Class[]{Integer.class, String.class, String.class};
    public static final Class[] GROUPE_ARGS = new Class[]{Integer.class, String.class, String.class, List.class};

}
