package com.penelope.menu;

import com.penelope.dao.IDao;
import com.penelope.dao.factory.AbstractDaoFactory;
import com.penelope.entities.Contact;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Callback;

import java.util.List;
import java.util.Optional;


public class ContactPanel {

//    private ContactPanel() {
//    }
    private AbstractDaoFactory jsonFac = AbstractDaoFactory.getDaoFactory(AbstractDaoFactory.FactoryType.JSON_DAO);
    private IDao contactDao = jsonFac.getContactDao();
    private ObservableList<Contact> contactsFx =  FXCollections.observableArrayList(contactDao.findAll());

    private static class ContactPanelHolder
    {
        private final static ContactPanel instance = new ContactPanel();
    }

    public static ContactPanel getInstance()
    {
        return ContactPanel.ContactPanelHolder.instance;
    }

    public StackPane ContactList(){
        StackPane contactList = new StackPane();
        TableView<Contact> contacts = new TableView<Contact>();

        TableColumn lastname = new TableColumn("Nom");
        lastname.setCellValueFactory(new PropertyValueFactory<Contact,String>("name"));
        TableColumn email= new TableColumn("Email");
        email.setCellValueFactory(new PropertyValueFactory<Contact,String>("email"));
        TableColumn phone= new TableColumn("Phone");
        phone.setCellValueFactory(new PropertyValueFactory<Contact,String>("phone"));
        contacts.getColumns().addAll(lastname,email,phone);
        contacts.setItems(contactsFx);
        contactList.getChildren().add(contacts);
        contacts.setRowFactory(new Callback<TableView<Contact>, TableRow<Contact>>() {
            @Override
            public TableRow<Contact> call(TableView<Contact> param) {
                TableRow<Contact> row = new TableRow<>();
                row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent event) {
                        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                            Contact rowData = row.getItem();
//                            System.out.println(rowData.getId());
//                            System.out.println(rowData);
                            contactList.getChildren().remove(contacts);
                            contactList.getChildren().add(DetailContact(rowData.getId()));
                        }
                    }
                });

                return row ;
            }
        });
        return contactList;
    }

    public StackPane CreateContact(){
        StackPane CreateContact = new  StackPane();
        Label label = new Label();
        GridPane grid = new GridPane();
        ComboBox roles = new ComboBox();
        grid.setPadding(new Insets(120, 100, 100, 120));
        grid.setVgap(10);
        grid.setHgap(10);

        ObservableList<String> rolesList = FXCollections.observableArrayList("admin","rien");
        roles.setPromptText("Selectionnez votre role");
        roles.setItems(rolesList);
        TextField lastname = new TextField();
        TextField email= new TextField();
        TextField phone = new TextField();

        lastname.setPromptText("Nom");
        email.setPromptText("email");
        phone.setPromptText("Numero Tel");

        GridPane.setConstraints(lastname, 0, 0);
        grid.getChildren().add(lastname);
        GridPane.setConstraints(email, 0, 2);
        grid.getChildren().add(email);
        GridPane.setConstraints(phone, 0, 3);
        grid.getChildren().add(phone);
        Button submit = new Button("Submit");
        GridPane.setConstraints(submit, 1, 2);
        grid.getChildren().add(submit);
        GridPane.setConstraints(label, 1, 3);
        grid.getChildren().add(label);
//        GridPane.setConstraints(roles, 0, 4);
//        grid.getChildren().add(roles);
        StackPane r = new StackPane();
        r.setPadding(new Insets(5, 1, 2, 1));
        r.setBackground((new Background(new BackgroundFill(Color.rgb(40, 40, 40), null, new Insets(100, 100, 100, 100)))));

        r.getChildren().add(grid);
        CreateContact.getChildren().add(r);
        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if ((lastname.getText() != null && !lastname.getText().isEmpty())
                        && phone.getText() != null && !phone.getText().isEmpty()
                        && email.getText() != null && !email.getText().isEmpty()
                        ) {
                    Contact contact = new Contact(lastname.getText(),email.getText(),phone.getText());
                    contactDao.create(contact);
                    contactsFx.add(contact);
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Creation");
                    alert.setHeaderText("Results:");
                    alert.setContentText("Le contact " + lastname.getText()+ " a bien été crée");
                    alert.showAndWait();
                } else {
                    label.setText("Veuillez remplir tout les champs !");
                    label.setTextFill(Color.WHEAT);

                }
            }
        });
        return CreateContact;
    }

    public StackPane DetailContact(int ID){
        StackPane DetailContact = new  StackPane();
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 2, 10));
//        grid.setPadding(new Insets(100, 100, 20, 200));
        grid.setVgap(10);
        grid.setHgap(10);

        Label label = new Label();
        Label lastname = new Label();
        Label email= new Label();
        Label phone= new Label();
//        List<Contact> contacts = contactDao.findAll();
//        Contact contactSelected = null;
//        for(Contact contact1: contacts) {
//            if(ID == contact1.getId()){
//                contactSelected = contact1;
//                break;
//            }
//        }
        Contact contact = getContact(ID);
        lastname.setText("Nom : " + contact.getName());
        lastname.setTextFill(Color.WHEAT);
        email.setText("email : " + contact.getEmail());
        email.setTextFill(Color.WHEAT);
        phone.setText("Phone : " + contact.getPhone());
        phone.setTextFill(Color.WHEAT);

        GridPane.setConstraints(lastname, 0, 0);
        grid.getChildren().add(lastname);
        GridPane.setConstraints(email, 0, 1);
        grid.getChildren().add(email);
        GridPane.setConstraints(phone, 0, 2);
        grid.getChildren().add(phone);

        Button retour = new Button("retour");
        GridPane.setConstraints(retour, 0, 5);
//        Button update = new Button("Modifier");
//        GridPane.setConstraints(update, 1, 5);
        Button delet = new Button("Supprimer");
        GridPane.setConstraints(delet, 2, 5);
        GridPane.setConstraints(label, 3, 5);
        grid.getChildren().add(label);
//        GridPane.setConstraints(label, 1, 3);
        grid.getChildren().add(retour);
//        grid.getChildren().add(update);
        grid.getChildren().add(delet);
        StackPane r = new StackPane();
        r.setPadding(new Insets(5, 1, 2, 1));
        r.setBackground((new Background(new BackgroundFill(Color.rgb(40, 40, 40), null, new Insets(10, 150, 330, 10)))));
        r.getChildren().add(grid);
        DetailContact.getChildren().add(r);
        retour.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                DetailContact.getChildren().remove(grid);
                DetailContact.getChildren().add(ContactList());
            }
        });
        delet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Supprimer un utilisteur");
                alert.setHeaderText("Voulez vous supprimer cet utilisateur?");
//                alert.setContentText(contact.getFirstname() + " " + contact.getLastname());
                Optional<ButtonType> option = alert.showAndWait();
                if (option.get() == null) {
                    label.setText("No selection!");
                } else if (option.get() == ButtonType.OK) {
                    contactDao.delete(contact.getId());
                    contactsFx.remove(contact);
                    DetailContact.getChildren().remove(grid);
                    DetailContact.getChildren().add(ContactList());
                } else if (option.get() == ButtonType.CANCEL) {
                    label.setText("Cancelled!");
                } else {
                    label.setText("-");
                }
            }
        });
//        retour.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent e) {
//                DetailContact.getChildren().remove(grid);
////                DetailUser.getChildren().add(UpdateUser());
//            }
//        });
        return DetailContact;
    }

    public Contact getContact(Integer ID){
        List<Contact>  contacts =  contactDao.findAll();
        Contact contactSelected = null;
        for(Contact contact1: contacts) {
            if(ID == contact1.getId()){
                contactSelected = contact1;
                break;
            }
        }
        return contactSelected;
    }
}
