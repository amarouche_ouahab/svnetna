package com.penelope.dao;

import com.penelope.entities.Personne;

import java.util.List;

public interface IPersonneDao {

    public List<Personne> findAll();

    public Personne findById(int id);

    public int generateIdNewPersonne();

    public Personne create(Personne p);

    public boolean delete(Personne p);

    public boolean delete(Integer id);

    void sendNotification(int state, Personne personne);

    public void setSendNotification(boolean sendNotification);

}
