package com.penelope.menu;

import com.penelope.dao.IPersonneDao;
import com.penelope.dao.factory.AbstractDaoFactory;
import com.penelope.menu.factory.PanelFactory;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Stage;

import java.io.File;

public class MenuFx extends Application {

    private Stage primaryStage;
    //    public ContactPanel contactInstance = ContactPanel.getInstance();
    public RolePanel roleInstance = new RolePanel();

    private PanelFactory panelFactory;
    private ContactPanel contactInstance;
    private GroupePanel groupeInstance;
    private UserPanel usersInstance;

    public MenuFx() {
        panelFactory = PanelFactory.getInstance();
        contactInstance = panelFactory.getContactPanel();
        groupeInstance = panelFactory.getGroupePanel();
        usersInstance = panelFactory.getUserPanel();
        roleInstance = panelFactory.getRolePanel();
    }
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        initRootLayout();
    }

    public void initRootLayout() {
        StackPane root =  new StackPane();
        root.setBackground(home());
//        root.setMargin(btn, new Insets(-10, 100, 10, 10));
        root.getChildren().addAll(bar());


//        root.getChildren().add(t);
//        root.getChildren().add(text);
        Scene scene = new Scene(root, 800, 650);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public  BorderPane bar(){
        BorderPane bar = new BorderPane();

        MenuBar menuBar = new MenuBar();
        Menu utilisateurs = new Menu("Utilisateurs");
        Menu contacts = new Menu("Contacts");
        Menu roles = new Menu("Roles");
        Menu groupes = new Menu("Groupes");

        MenuItem utilisateursList = new MenuItem("List");
        MenuItem createUtilisateur = new MenuItem("Create");
//        MenuItem exitItem = new MenuItem("Delete");

        MenuItem contactList = new MenuItem("List");
        MenuItem contactCreate = new MenuItem("Create");

        MenuItem roleList = new MenuItem("List");
        MenuItem roleCreate = new MenuItem("Create");

        MenuItem groupeList = new MenuItem("List");
        MenuItem groupeCreate = new MenuItem("Create");
        
        utilisateursList.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                bar.setCenter(usersInstance.UserList());
            }
        });
        createUtilisateur.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                bar.setCenter(usersInstance.CreateUser());
            }
        });
        contactList.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                bar.setCenter(contactInstance.ContactList());
            }
        });
        contactCreate.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                bar.setCenter(contactInstance.CreateContact());
            }
        });
        roleList.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                bar.setCenter(roleInstance.RoleList());
            }
        });
        roleCreate.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                bar.setCenter(roleInstance.CreateRole());
            }
        });

        groupeList.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                bar.setCenter(groupeInstance.GroupeList());
            }
        });
        groupeCreate.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                bar.setCenter(groupeInstance.CreateGroupe());
            }
        });

        utilisateurs.getItems().addAll(utilisateursList, createUtilisateur);
        contacts.getItems().addAll(contactList, contactCreate);
        groupes.getItems().addAll(groupeList, groupeCreate);
        roles.getItems().addAll(roleList, roleCreate);

        // Add Menus to the MenuBar
        menuBar.getMenus().addAll(utilisateurs, contacts, roles,groupes);
        bar.setTop(menuBar);
//        bar.setPadding(new Insets(10, 10, 2, 10));
//        root.getCenter(users.UserStackPanel());
        return bar;
    }
    public Background home() {
        File imgUrl = new File("src/main/resources/img/background.jpg");
        ImagePattern image = new ImagePattern(new Image(imgUrl.toURI().toString()));
        Background home = new Background(new BackgroundFill(image, CornerRadii.EMPTY, Insets.EMPTY));

        return home;
    }
}
