package com.penelope.dao.impl.json;

import com.penelope.entities.Contact;
import com.penelope.entities.Personne;
import com.penelope.entities.Groupe;
import com.penelope.observer.MySubjectObserver;
import com.penelope.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class JsonGroupeDaoImpl extends AbstractJsonDao<Groupe> {

    private static MySubjectObserver subject = null;
    private List<Groupe> groupes = new ArrayList<>();

    public JsonGroupeDaoImpl(MySubjectObserver subject) {
        super(Groupe.class, Constants.GROUPE_ARGS, subject, Constants.GROUPE_JSON_PATH_FILE);
        JsonGroupeDaoImpl.subject = subject;
    }

    private static class JsonGroupeDaoImplHolder {
        private static JsonGroupeDaoImpl instance = new JsonGroupeDaoImpl(subject);
    }

    public static JsonGroupeDaoImpl getInstance(MySubjectObserver subject) {
        JsonGroupeDaoImpl.subject = subject;
        return JsonGroupeDaoImpl.JsonGroupeDaoImplHolder.instance;
    }

    @Override
    public Groupe findById(int id) {
        Groupe groupe = super.findById(id);
        return (Groupe) groupe;
    }

    @Override
    public boolean delete(Groupe groupe) {
        return false;
    }

    @Override
    public Groupe create(Groupe obj) {
        super.create(obj);
        // this.sendNotification(Constants.STATE_CREATE_JSON, obj);

        return obj;
    }

    @Override
    public Groupe update(Groupe groupe) {
        return super.update(groupe);
    }

    @Override
    public boolean delete(Integer id) {
        boolean result = super.delete(id);
        if (result) {
            this.sendNotification(Constants.STATE_DELETE_IUSER, new Groupe(id));
        }

        return result;
    }


    @Override
    public List<Groupe> findAll() {
        this.groupes = super.findAll();
        return this.groupes;
    }

    public void sendNotification(int state, Groupe groupe) {
        subject.setObject(groupe);
        subject.setState(state);
    }
}
