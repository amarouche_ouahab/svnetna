package com.penelope.menu.factory;

import com.penelope.dao.IDao;
import com.penelope.dao.IPersonneDao;
import com.penelope.dao.factory.AbstractDaoFactory;
import com.penelope.dao.impl.json.JsonContactDaoImpl;
import com.penelope.dao.impl.json.JsonGroupeDaoImpl;
import com.penelope.dao.impl.json.JsonPersonneDaoImpl;
import com.penelope.dao.impl.json.JsonRoleDaoImpl;
import com.penelope.menu.ContactPanel;
import com.penelope.menu.GroupePanel;
import com.penelope.menu.RolePanel;
import com.penelope.menu.UserPanel;
import com.penelope.observer.MySubjectObserver;
import com.penelope.utils.Constants;

public class PanelFactory {

    private PanelFactory() {
    }

    private static class PanelFactoryHolder {
        private static PanelFactory instance = new PanelFactory();
    }

    public static PanelFactory getInstance() {
        return PanelFactoryHolder.instance;
    }

    public UserPanel getUserPanel() {
        return UserPanel.getInstance();
    }

    public ContactPanel getContactPanel() {
        return ContactPanel.getInstance();
    }

    public GroupePanel getGroupePanel() {
        return GroupePanel.getInstance();
    }

    public RolePanel getRolePanel() {
        return RolePanel.getInstance();
    }

}
