package com.penelope.dao.factory;

import com.penelope.dao.IDao;
import com.penelope.dao.impl.csv.CsvPersonneDaoImpl;
import com.penelope.observer.MySubjectObserver;
import com.penelope.utils.Constants;

public class CsvDaoFactory extends AbstractDaoFactory {

    private static MySubjectObserver observable;
    private IDao personneDao = null;
    //private final MySubjectObserver mySubjectObserver;

    public CsvDaoFactory(MySubjectObserver observable) {
        this.personneDao = CsvPersonneDaoImpl.getInstance(observable);
        this.observable = observable;
    }

    private static class CsvDaoFactoryHolder {
        private static CsvDaoFactory instance = new CsvDaoFactory(observable);
    }

    public static CsvDaoFactory getInstance(MySubjectObserver subject) {
        observable = subject;
        return CsvDaoFactoryHolder.instance;
    }

    @Override
    public IDao getPersonneDao() {
        return personneDao;
    }

    @Override
    public IDao getContactDao() {
        return null;
    }

    @Override
    public IDao getRoleDao() {
        return null;
    }

    @Override
    public IDao getGroupeDao() {
        return null;
    }
}
