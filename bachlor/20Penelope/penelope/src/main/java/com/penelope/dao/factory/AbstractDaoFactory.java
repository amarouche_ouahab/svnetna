package com.penelope.dao.factory;

import com.penelope.dao.IDao;
import com.penelope.dao.IPersonneDao;
import com.penelope.observer.MyObserver;
import com.penelope.observer.MySubjectObserver;

public abstract class AbstractDaoFactory {

    public abstract IDao getPersonneDao();
    public abstract IDao getContactDao();
    public abstract IDao getRoleDao();
    public abstract IDao getGroupeDao();

    public enum FactoryType {

        MANUAL_DAO, CSV_DAO, JSON_DAO, SQL_DAO
    }

    public static AbstractDaoFactory getDaoFactory(FactoryType type) {
        MySubjectObserver observable = new MySubjectObserver();
        AbstractDaoFactory abstractDaoFactory = null;
        switch (type) {
//            case CSV_DAO:
//                observable.addObserveur(JsonDaoFactory.getInstance(observable));
//                abstractDaoFactory = CsvDaoFactory.getInstance(observable);
//                break;
            case JSON_DAO:
                // observable.addObserveur(CsvDaoFactory.getInstance(observable));
                abstractDaoFactory = JsonDaoFactory.getInstance(observable);
                break;
        }
        return abstractDaoFactory;
    }
}
