/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.penelope.dao.impl.csv;

import com.penelope.dao.IDao;
import com.penelope.observer.MySubjectObserver;
import com.penelope.utils.Constants;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @param <T>
 * @author ElHadji
 */
public abstract class AbstractCsvDao<T> implements IDao<T> {

    protected List<T> data = null;
    private String csvPathFile;
    private Class<T> myClass;
    private Class[] args;
    private MySubjectObserver mySubject = null;
    private Map<Integer, String> header = null;


    public AbstractCsvDao(Class<T> myClass, Class[] args, MySubjectObserver mySubject, String csvPathFile) {
        this.myClass = myClass;
        this.mySubject = mySubject;
        this.csvPathFile = csvPathFile;
        this.args = args;
    }

    @Override
    public List<T> findAll() {
        this.data = new ArrayList<>();
        try (Reader reader = new FileReader(this.csvPathFile)) {
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                    .withFirstRecordAsHeader()
                    .withTrim()
                    .withDelimiter(';'));
            List<CSVRecord> records = csvParser.getRecords();
            for (CSVRecord record : records) {
                Constructor constructor = this.myClass.getConstructor(args);
                Object[] formattedRecord = this.getFormattedRecord(record);
                if (formattedRecord != null) {
                    Object obj = constructor.newInstance(formattedRecord);
                    this.data.add((T) obj);
                }
            }
        } catch (IOException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            ex.getStackTrace();
        }

        return this.data;
    }

    @Override
    public T findById(int id) {
        try {
            Method method = this.myClass.getMethod("getId");
            for (T elt : data) {
                if ((int) method.invoke(elt) == id) {
                    return elt;
                }
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public T create(T obj) {
        try (FileWriter fr = new FileWriter(new File(this.csvPathFile), true)) {
            Method setIdFunc = this.myClass.getMethod("setId", Integer.class);
            Method getIdFunc = this.myClass.getMethod("getId");
            Method getFirstnameFunc = this.myClass.getMethod("getFirstname");
            Method getLastnameFunc = this.myClass.getMethod("getLastname");
//            Method getWeightFunc = this.myClass.getMethod("getWeight");
//            Method getHeightFunc = this.myClass.getMethod("getHeight");
            Method getRoadFunc = this.myClass.getMethod("getRoad");
            Method getCityFunc = this.myClass.getMethod("getCity");
            Method getZipCodeFunc = this.myClass.getMethod("getZipCode");
            setIdFunc.invoke(obj, this.generateIdNewPersonne());
//            fr.write("\n" + getIdFunc.invoke(obj) + "; " + getFirstnameFunc.invoke(obj) + "; " + getLastnameFunc.invoke(obj) + "; " + getWeightFunc.invoke(obj) + "; " +
//                    getHeightFunc.invoke(obj) + "; " + getRoadFunc.invoke(obj) + "; " + getCityFunc.invoke(obj) + "; " + getZipCodeFunc.invoke(obj));
            fr.write("\n" + getIdFunc.invoke(obj) + "; " + getFirstnameFunc.invoke(obj) + "; " + getLastnameFunc.invoke(obj) + "; "  + getRoadFunc.invoke(obj) + "; " + getCityFunc.invoke(obj) + "; " + getZipCodeFunc.invoke(obj));
            this.data.add(obj);
        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return obj;
    }

    @Override
    public boolean delete(T t) {
        Boolean found = false;
        HashMap<Integer, String> header = this.getHeaderStr();
        if (header.size() < Constants.CSV_HEADER.length() + 1) { // +1 pour le champ id
            return false;
        }
        try (
                BufferedWriter writer = Files.newBufferedWriter(Paths.get(this.csvPathFile));
                CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                        .withFirstRecordAsHeader()
                        .withDelimiter(';')
                        .withTrim()
                )
        ) {
            Method getIdFunc = this.myClass.getMethod("getId");
            Method getFirstnameFunc = this.myClass.getMethod("getFirstname");
            Method getLastnameFunc = this.myClass.getMethod("getLastname");
//            Method getWeightFunc = this.myClass.getMethod("getWeight");
//            Method getHeightFunc = this.myClass.getMethod("getHeight");
            Method getRoadFunc = this.myClass.getMethod("getRoad");
            Method getCityFunc = this.myClass.getMethod("getCity");
            Method getZipCodeFunc = this.myClass.getMethod("getZipCode");
            csvPrinter.printRecord(header.get(0), header.get(1), header.get(2), header.get(3), header.get(4), header.get(5), header.get(6), header.get(7));
            for (T obj : this.data) {
                if (getIdFunc.invoke(obj).equals(getIdFunc.invoke(t))) {
                    found = true;
                    continue;
                }
//                csvPrinter.printRecord(getIdFunc.invoke(obj), getFirstnameFunc.invoke(obj), getLastnameFunc.invoke(obj), getWeightFunc.invoke(obj), getHeightFunc.invoke(obj), getRoadFunc.invoke(obj), getCityFunc.invoke(obj), getZipCodeFunc.invoke(obj));
                csvPrinter.printRecord(getIdFunc.invoke(obj), getFirstnameFunc.invoke(obj), getLastnameFunc.invoke(obj), getRoadFunc.invoke(obj), getCityFunc.invoke(obj), getZipCodeFunc.invoke(obj));
            }
        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            ex.getStackTrace();
        }

        return found;
    }

    private Object[] getFormattedRecord(CSVRecord record) {
        ArrayList<Object> values = new ArrayList<>();

        if (record.size() < Constants.CSV_HEADER.length() + 1) {
            return null;
        }

        for (Field field : this.myClass.getDeclaredFields()) {
            if (record.get(field.getName()) == null) {
                continue;
            }
            if (Integer.class.equals(field.getType())) {
                values.add(Integer.parseInt(record.get(field.getName())));
            } else if (Double.class.equals(field.getType())) {
                values.add(Double.parseDouble(record.get(field.getName())));
            } else {
                values.add(record.get(field.getName()));
            }
        }

        if (values.size() < Constants.CSV_HEADER.length() + 1) {
            return null;
        }

        return values.toArray();
    }

    private int generateIdNewPersonne() {
        int max = 0;
        int tmp = 0;
        try {
            Method method = this.myClass.getMethod("getId");
            for (T obj : this.data) {
                tmp = (int) method.invoke(obj);
                if (tmp > max) {
                    max = tmp;
                }
            }

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return tmp + 1;
    }

    private HashMap<Integer, String> getHeaderStr() {
        if (this.header != null && this.header.size() > 0) {
            return (HashMap<Integer, String>) this.header;
        }
        this.header = new HashMap<>();
        try (Reader reader = new FileReader(this.csvPathFile)) {
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                    .withTrim()
                    .withDelimiter(';'));
            for (CSVRecord csvRecord : csvParser) {
                for (int i = 0; i < csvRecord.size(); i++) {
                    this.header.put(i, csvRecord.get(i));
                }
                break;
            }
        } catch (IOException ex) {
            ex.getStackTrace();
        }

        return (HashMap<Integer, String>) this.header;
    }
}
