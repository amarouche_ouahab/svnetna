package com.penelope.menu;

import com.penelope.dao.IDao;
import com.penelope.dao.IPersonneDao;
import com.penelope.dao.factory.AbstractDaoFactory;
import com.penelope.entities.Groupe;
import com.penelope.entities.Personne;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

public class GroupePanel {
    private AbstractDaoFactory jsonFac = AbstractDaoFactory.getDaoFactory(AbstractDaoFactory.FactoryType.JSON_DAO);
    private IDao groupeDao = jsonFac.getGroupeDao();
//    private IDao personneDao = jsonFac.getPersonneDao();
    private ObservableList<Groupe> groupeFx =  FXCollections.observableArrayList(groupeDao.findAll());


    private static class GroupePanelHolder
    {
        private final static GroupePanel instance = new GroupePanel();
    }

    public static GroupePanel getInstance()
    {
        return GroupePanel.GroupePanelHolder.instance;
    }

    public StackPane GroupeList(){
        StackPane groupeList = new StackPane();
        TableView<Groupe> groupes = new TableView<Groupe>();
        TableColumn lastname = new TableColumn("Nom");
        lastname.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Groupe, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Groupe, String> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getName());
            }
        });
        TableColumn description= new TableColumn("Description");
        description.setCellValueFactory(new PropertyValueFactory<Groupe,String >("description"));

        groupes.getColumns().addAll(lastname,description);
        groupes.setItems(groupeFx);
        groupeList.getChildren().add(groupes);

        groupes.setRowFactory(new Callback<TableView<Groupe>, TableRow<Groupe>>() {
            @Override
            public TableRow<Groupe> call(TableView<Groupe> param) {
                TableRow<Groupe> row = new TableRow<>();
                row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent event) {
                        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                            Groupe rowData = row.getItem();
                            groupeList.getChildren().remove(groupes);
                            groupeList.getChildren().add(DetailGroupe(rowData.getId()));
                        }
                    }
                });

                return row ;
            }
        });
        return groupeList;
    }
    public StackPane DetailGroupe(int ID){
        StackPane DetailGroupe = new  StackPane();

        List<Groupe> groupea = groupeDao.findAll();
        Groupe personneSelected = null;
        for(Groupe personne1: groupea) {
            if(ID == personne1.getId()){
                personneSelected = personne1;
                break;
            }
        }
        Groupe groupe = personneSelected;
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 2, 10));
        grid.setVgap(10);
        grid.setHgap(10);
        Label label = new Label();
        Label nome = new Label();
        Label desc = new Label();
        Label mombres= new Label();
//        ObservableList<Personne> personneFx =  getListUsers(FXCollections.observableArrayList(this.personneBygroupe(groupe.getMembres())));
//
//        TableView<Personne> personnes = tablePersonne(groupe,personneFx);
//        personnes.setPadding(new Insets(10, 10, 2, 10));
//        Label titleGp = new Label("Membre du groupe");
        label.setFont(new Font("Arial", 40));
//        titleGp.setTextFill(Color.CYAN);
        nome.setText("Nom du groupe: " + groupe.getName());
        nome.setTextFill(Color.WHEAT);
        desc.setText("Description du groupe: " + groupe.getDescription());
        desc.setTextFill(Color.WHEAT);
        mombres.setText("Nombre de personnes dans le groupe: " + groupe.getMembres().size());
        mombres.setTextFill(Color.WHEAT);
        GridPane.setConstraints(nome, 0, 1);
        grid.getChildren().add(nome);
        GridPane.setConstraints(desc, 0, 2);
        grid.getChildren().add(desc);
        GridPane.setConstraints(mombres, 0, 3);
        grid.getChildren().add(mombres);
        Button retour = new Button("retour");
        GridPane.setConstraints(retour, 0, 5);
        Button delet = new Button("Supprimer");
        GridPane.setConstraints(delet, 2, 5);
        GridPane.setConstraints(label, 3, 5);
        grid.getChildren().add(label);

//        GridPane.setConstraints(personnes, 0, 7);
//        grid.getChildren().add(personnes);
        grid.getChildren().add(retour);
        grid.getChildren().add(delet);
        StackPane r = new StackPane();
        r.setPadding(new Insets(5, 1, 2, 1));
        r.setBackground((new Background(new BackgroundFill(Color.rgb(40, 40, 40), null, new Insets(10, 150, 330, 10)))));
        r.getChildren().add(grid);
        DetailGroupe.getChildren().add(r);
        retour.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                DetailGroupe.getChildren().remove(grid);
                DetailGroupe.getChildren().add(GroupeList());
            }
        });
        delet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Supprimer ce groupe");
                alert.setHeaderText("Voulez vous supprimer ce groupe?");
//                alert.setContentText(groupe.getFirstname() + " " + groupe.getLastname());
                Optional<ButtonType> option = alert.showAndWait();
                if (option.get() == null) {
                    label.setText("No selection!");
                } else if (option.get() == ButtonType.OK) {
                    groupeDao.delete(groupe.getId());
                    groupeFx.remove(groupe);
                    DetailGroupe.getChildren().remove(grid);
                    DetailGroupe.getChildren().add(GroupeList());
                } else if (option.get() == ButtonType.CANCEL) {
                    label.setText("Cancelled!");
                } else {
                    label.setText("-");
                }
            }
        });
        retour.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {

                DetailGroupe.getChildren().remove(grid);
                DetailGroupe.getChildren().add(GroupeList());
            }
        });
        return DetailGroupe;
    }

    public TableView<Personne> tablePersonne(Groupe groupe,  ObservableList<Personne> personneFx){
//        ObservableList<Personne> personneFx =  FXCollections.observableArrayList(this.personneBygroupe(groupe.getMembres()));
        TableView<Personne> personnes = new TableView<Personne>();
        TableColumn lastname = new TableColumn("Nom");
        lastname.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Personne, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Personne, String> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getFirstname());
            }
        });
        personnes.getColumns().addAll(lastname);
        personnes.setItems(personneFx);
        return personnes;
    }

//    public List<Personne> personneBygroupe(List ids){
//        List<Personne> perslist = new ArrayList<>();
//        for (Personne p: (List<Personne>) personneDao.findAll()) {
//            for (Object id : ids){
//                if(((Long)id).intValue() == p.getId()){
//                    perslist.add(p);
//                }
//            }
//        }
//        return perslist;
//    }
//
//    public  ObservableList<Personne> getListUsers( ObservableList<Personne> per){
//        HashSet<Personne> h = new HashSet<Personne>(per);
//        per.clear();
//        per.addAll(h);
//        return per;
//    }

    public StackPane CreateGroupe(){
        StackPane CreateGroupe = new  StackPane();
        Label label = new Label();
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(120, 100, 100, 120));
        grid.setVgap(10);
        grid.setHgap(10);

        TextField name = new TextField();
        TextField desc= new TextField();

        name.setPromptText("Nom");
        desc.setPromptText("Description");

        GridPane.setConstraints(name, 0, 0);
        grid.getChildren().add(name);

        GridPane.setConstraints(desc, 0, 2);
        grid.getChildren().add(desc);
        Button submit = new Button("Submit");
        GridPane.setConstraints(submit, 1, 2);
        grid.getChildren().add(submit);
        GridPane.setConstraints(label, 1, 3);
        grid.getChildren().add(label);

        StackPane r = new StackPane();
        r.setPadding(new Insets(5, 1, 2, 1));
        r.setBackground((new Background(new BackgroundFill(Color.rgb(40, 40, 40), null, new Insets(100, 100, 100, 100)))));

        r.getChildren().add(grid);
        CreateGroupe.getChildren().add(r);
        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if ((name.getText() != null && !name.getText().isEmpty())) {
                    List nb = new ArrayList();
                    Groupe groupe = new Groupe(name.getText(),desc.getText(),nb);
                    groupeDao.create(groupe);
                    groupeFx.add(groupe);
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Creation");
                    alert.setHeaderText("Results:");
                    alert.setContentText("Le groupe " + name.getText()+ " a bien été crée");
                    alert.showAndWait();
                } else {
                    label.setText("Veuillez remplir tout les champs !");
                    label.setTextFill(Color.WHEAT);

                }
            }
        });
        return CreateGroupe;
    }

}
