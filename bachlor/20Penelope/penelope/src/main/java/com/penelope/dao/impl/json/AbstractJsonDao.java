/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.penelope.dao.impl.json;

import com.penelope.dao.IDao;
import com.penelope.entities.Personne;
import com.penelope.observer.MyObserver;
import com.penelope.observer.MySubjectObserver;
import com.penelope.utils.Constants;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public abstract class AbstractJsonDao<T> extends MyObserver implements IDao<T> {

    private List<T> data = new ArrayList<>();
    private String jsonPathFile = null;
    private Class<T> myClass = null;
    private Class[] args;
    private MySubjectObserver subject = null;

    public AbstractJsonDao(Class<T> myClass, Class[] args, MySubjectObserver subject, String jsonPathFile) {
        this.myClass = myClass;
        this.subject = subject;
        this.jsonPathFile = jsonPathFile;
        this.args = args;
    }

    @Override
    public List<T> findAll() {
        JSONParser parser = new JSONParser();
        List<T> jsonData = new ArrayList<>();
        try {
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(this.jsonPathFile));
            JSONArray data = (JSONArray) jsonObject.get("data");
            Iterator<Object> iterator = data.iterator();
            while (iterator.hasNext()) {
                JSONObject jsonEntity = (JSONObject) iterator.next();

                Constructor constructor = this.myClass.getConstructor(args);
                Object[] formattedJsonObj = this.getFormattedObject(jsonEntity);
                if (formattedJsonObj != null) {
                    Object obj = constructor.newInstance(formattedJsonObj);
                    jsonData.add((T) obj);
                }
            }

        } catch (IOException | ParseException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return jsonData;
    }

    @Override
    public T findById(int id) {
        try {
            Method method = this.myClass.getMethod("getId");
            for (T elt : this.findAll()) {
                if ((int) method.invoke(elt) == id) {
                    return elt;
                }
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public T create(T obj) {
        try {
            JSONObject data = new JSONObject();
            JSONParser parser = new JSONParser();
            Method setIdFunc = this.myClass.getMethod("setId", Integer.class);
            Method toJsonObjFunc = this.myClass.getMethod("toJsonObject");
            setIdFunc.invoke(obj, this.generateNewId());

            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(this.jsonPathFile));
            JSONArray jsonArray = (JSONArray) jsonObject.get("data");
            jsonArray.add(toJsonObjFunc.invoke(obj));
            data.put("data", jsonArray);
            FileWriter file = new FileWriter(this.jsonPathFile);
            file.write(data.toJSONString());
            file.flush();
            file.close();
        } catch (IOException | ParseException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return obj;
    }

    @Override
    public boolean delete(Integer id) {
        boolean found = false;
        JSONParser parser = new JSONParser();
        JSONObject data = new JSONObject();
        List<T> dataList = new ArrayList<>();

        try {
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(this.jsonPathFile));
            JSONArray jsonArray = (JSONArray) jsonObject.get("data");
            Iterator<Object> iterator = jsonArray.iterator();
            while (iterator.hasNext()) {
                JSONObject jsonEntity = (JSONObject) iterator.next();
                if ((Long) jsonEntity.get("id") == id.longValue()) {
                    found = true;
                    continue;
                }
                Constructor constructor = this.myClass.getConstructor(args);
                Object[] formattedJsonObj = this.getFormattedObject(jsonEntity);
                if (formattedJsonObj != null) {
                    Object obj = constructor.newInstance(formattedJsonObj);
                    dataList.add((T) obj);
                }
            }
            this.saveJson(dataList);
        } catch (IOException | ParseException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            ex.getStackTrace();
        }

        return found;
    }

    @Override
    public T update(T obj) {
        Method method = null;
        List<T> newList = new ArrayList<>();
        List<T> jsonData;
        try {
            method = this.myClass.getMethod("getId");
            jsonData = this.findAll();

            for (int i = 0; i < jsonData.size(); i++) {
                if ((int) method.invoke(jsonData.get(i)) == (int) method.invoke(obj)) {
                    newList.add(obj);
                } else {
                    newList.add(jsonData.get(i));
                }
            }
            this.saveJson(newList);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return obj;
    }

    private Object[] getFormattedObject(JSONObject obj) {
        ArrayList<Object> values = new ArrayList<>();

        if (obj == null || obj.size() < this.args.length) {
            return null;
        }

        for (Field field : this.myClass.getDeclaredFields()) {
            if (obj.get(field.getName()) == null) {
                continue;
            }
            if (Integer.class.equals(field.getType())) {
                values.add(Math.toIntExact((Long) obj.get(field.getName())));
            } else if (Double.class.equals(field.getType())) {
                values.add((Double) obj.get(field.getName()));
            } else if (Set.class.equals(field.getType())) {
                values.add(this.JsonArrayToSet((JSONArray) obj.get(field.getName())));
            }
            else {
                values.add(obj.get(field.getName()));
            }
        }

        if (values.size() < this.args.length) {
            return null;
        }

        return values.toArray();
    }

    private Set<Integer> JsonArrayToSet(JSONArray jsonArray) {
        Set<Integer> set = new HashSet<>();
        if (jsonArray != null) {
            int len = jsonArray.size();
            for (int i=0;i<len;i++){
                set.add(((Long) jsonArray.get(i)).intValue());
            }
        }
        return set;
    }

    private int generateNewId() {
        int max = 0;
        int tmp = 0;
        try {
            Method method = this.myClass.getMethod("getId");
            for (T obj : this.findAll()) {
                tmp = (int) method.invoke(obj);
                if (tmp > max) {
                    max = tmp;
                }
            }

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return tmp + 1;
    }

    @Override
    public void updateSource() {
    }

    protected void saveJson(List<T> data) {
        FileWriter file = null;
        JSONObject jsonData = new JSONObject();
        try {
            file = new FileWriter(this.jsonPathFile);
            jsonData.put("data", data);
            file.write(jsonData.toJSONString());
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

