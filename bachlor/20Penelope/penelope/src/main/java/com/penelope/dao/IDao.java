/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.penelope.dao;

import java.util.List;

/**
 *
 * @author Nouri
 */
public interface IDao<T> {

    public List<T> findAll();

    public T findById(int id);

    public T create(T t);

    public T update(T t);

    public boolean delete(T t);

    boolean delete(Integer id);
}
