package com.penelope.dao.impl.json;

import com.penelope.dao.IPersonneDao;
import com.penelope.entities.Personne;
import com.penelope.observer.MySubjectObserver;
import com.penelope.utils.Constants;

import java.util.*;
import java.util.stream.Collectors;

public class JsonPersonneDaoImpl extends AbstractJsonDao<Personne> implements IPersonneDao {

    private final String personnesJsonPathFile = Constants.PERSONNES_JSON_PATH_FILE;
    private static MySubjectObserver subject = null;
    private boolean sendNotification = true;
    private List<Personne> personnes = new ArrayList<>();

    public JsonPersonneDaoImpl(MySubjectObserver subject) {
        super(Personne.class, Constants.PERSON_ARGS, subject, Constants.PERSONNES_JSON_PATH_FILE);
        JsonPersonneDaoImpl.subject = subject;
    }

    private static class JsonPersonneDaoImplHolder {
        private static JsonPersonneDaoImpl instance = new JsonPersonneDaoImpl(subject);
    }

    public static JsonPersonneDaoImpl getInstance(MySubjectObserver subject) {
        JsonPersonneDaoImpl.subject = subject;
        return JsonPersonneDaoImplHolder.instance;
    }

    @Override
    public Personne findById(int idPersonne) {
        return super.findById(idPersonne);
    }

    @Override
    public Personne create(Personne obj) {
        super.create(obj);
        this.sendNotification(Constants.STATE_CREATE_JSON, obj);

        return obj;
    }

    @Override
    public Personne update(Personne personne) {
        return super.update(personne);
    }

    @Override
    public boolean delete(Personne personne) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        boolean result = super.delete(id);
        if (result) {
//            this.sendNotification(Constants.STATE_DELETE_CSV, id);
        }

        return result;
    }

    @Override
    public int generateIdNewPersonne() {
        List<Personne> personnes = this.findAll().stream().sorted(Comparator.comparing(Personne::getId)).collect(Collectors.toList());
        return personnes.get(personnes.size() - 1).getId() + 1;
    }

    @Override
    public List<Personne> findAll() {
        this.personnes = super.findAll();
        return this.personnes;
    }

    @Override
    public void sendNotification(int state, Personne personne) {
        if (subject.getState() == 0 || subject.getState() == state) {
            subject.setState(state);
        }
    }

    @Override
    public void setSendNotification(boolean sendNotification) {
        this.sendNotification = sendNotification;
    }

    @Override
    public void updateSource() {
        int state = subject.getState();
        System.out.println(state);
        switch (state) {
            case Constants.STATE_DELETE_IUSER:
                List<Personne> personnes = this.findAll();
                for (Personne personne : personnes) {
                    personne.remove(this.subject.getObject());
                }
                super.saveJson(personnes);
                break;
        }
    }

}
