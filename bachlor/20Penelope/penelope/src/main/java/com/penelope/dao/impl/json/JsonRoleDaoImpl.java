package com.penelope.dao.impl.json;

import com.penelope.entities.Role;
import com.penelope.entities.Personne;
import com.penelope.observer.MySubjectObserver;
import com.penelope.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class JsonRoleDaoImpl extends AbstractJsonDao<Role> {

    private static MySubjectObserver subject = null;
    private List<Role> roles = new ArrayList<>();

    public JsonRoleDaoImpl(MySubjectObserver subject) {
        super(Role.class, Constants.ROLE_ARGS, subject, Constants.ROLES_JSON_PATH_FILE);
        JsonRoleDaoImpl.subject = subject;
    }

    private static class JsonRoleDaoImplHolder {
        private static JsonRoleDaoImpl instance = new JsonRoleDaoImpl(subject);
    }

    public static JsonRoleDaoImpl getInstance(MySubjectObserver subject) {
        JsonRoleDaoImpl.subject = subject;
        return JsonRoleDaoImpl.JsonRoleDaoImplHolder.instance;
    }

    @Override
    public Role findById(int id) {
        Role role = super.findById(id);
        return (Role) role;
    }

    @Override
    public boolean delete(Role role) {
        return false;
    }

    @Override
    public Role create(Role obj) {
        super.create(obj);
        // this.sendNotification(Constants.STATE_CREATE_JSON, obj);

        return obj;
    }

    @Override
    public Role update(Role role) {
        return super.update(role);
    }

    @Override
    public boolean delete(Integer id) {
        boolean result = super.delete(id);
        if (result) {
//            this.sendNotification(Constants.STATE_DELETE_CSV, id);
        }

        return result;
    }


    @Override
    public List<Role> findAll() {
        this.roles = super.findAll();
        return this.roles;
    }
}
