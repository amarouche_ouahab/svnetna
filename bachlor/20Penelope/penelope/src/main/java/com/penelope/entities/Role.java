package com.penelope.entities;

import org.json.simple.JSONObject;

import java.util.Objects;

public class Role implements IUser<Role> {


    private Integer id;
    private String identifiant;
    private String description;


    public Role(Integer id, String identifiant, String description) {
        this.id = id;
        this.identifiant = identifiant;
        this.description = description;
    }

    public Role(String identifiant, String description) {
        this.identifiant = identifiant;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return Objects.equals(getId(), role.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getidentifiant(), getdescription());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getidentifiant() {
        return identifiant;
    }

    public void setidentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }



    public JSONObject toJsonObject() {
        JSONObject obj = new JSONObject();
        obj.put("id", this.id);
        obj.put("identifiant", this.identifiant);
        obj.put("description", this.description);

        return obj;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"identifiant\":\"" + identifiant + '\"' +
                ", \"description\":\"" + description + '\"' +
                '}';
    }

    @Override
    public boolean add(IUser obj) {
        return false;
    }

    @Override
    public boolean remove(IUser obj) {
        return false;
    }
}
