package com.penelope.dao.impl.csv;

import com.penelope.dao.IPersonneDao;
import com.penelope.entities.Personne;
import com.penelope.observer.MySubjectObserver;
import com.penelope.utils.Constants;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CsvPersonneDaoImpl extends AbstractCsvDao<Personne> implements IPersonneDao {

    private final String personnesCsvPathFile = Constants.PERSONNES_CSV_PATH_FILE;
    private static MySubjectObserver subject = null;
    private boolean sendNotification = true;
    private List<Personne> personnes = new ArrayList<>();

    private CsvPersonneDaoImpl(MySubjectObserver subject) {
        super(Personne.class, Constants.PERSON_ARGS, subject, Constants.PERSONNES_CSV_PATH_FILE);
        this.findAll();
        CsvPersonneDaoImpl.subject = subject;
    }

    private static class CsvPersonneDaoImplHolder {
        private static CsvPersonneDaoImpl instance = new CsvPersonneDaoImpl(subject);
    }

    public static CsvPersonneDaoImpl getInstance(MySubjectObserver subject) {
        CsvPersonneDaoImpl.subject = subject;
        return CsvPersonneDaoImplHolder.instance;
    }

    @Override
    public Personne findById(int idPersonne) {
        return super.findById(idPersonne);
    }

    @Override
    public Personne create(Personne obj) {
        super.create(obj);
        this.sendNotification(Constants.STATE_CREATE_CSV, obj);

        return obj;
    }

    @Override
    public Personne update(Personne personne) {
        return null;
    }

    @Override
    public boolean delete(Personne person) {
        boolean result = super.delete(person);
        if (result) {
            this.sendNotification(Constants.STATE_DELETE_CSV, person);
        }

        return result;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public List<Personne> findAll() {
        this.personnes = super.findAll();
        return this.personnes;
    }

    @Override
    public int generateIdNewPersonne() {
        this.personnes = this.personnes.stream().sorted(Comparator.comparing(Personne::getId)).collect(Collectors.toList());
        return this.personnes.get(this.personnes.size() - 1).getId() + 1;
    }

    @Override
    public void sendNotification(int state, Personne personne) {
        if (subject.getState() == 0 || subject.getState() == state) {
            subject.setState(state);
        }
    }

    @Override
    public void setSendNotification(boolean sendNotification) {
        this.sendNotification = sendNotification;
    }
}
