package com.penelope.menu;

import com.penelope.dao.IDao;
import com.penelope.dao.IPersonneDao;
import com.penelope.dao.factory.AbstractDaoFactory;
import com.penelope.entities.*;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.lang.reflect.Field;
import java.util.*;

public class UserPanel{

    private UserPanel() {
    }
    AbstractDaoFactory jsonFac = AbstractDaoFactory.getDaoFactory(AbstractDaoFactory.FactoryType.JSON_DAO);
    IDao personneDao = jsonFac.getPersonneDao();
    IDao roleDao = jsonFac.getRoleDao();
    IDao groupeDao = jsonFac.getGroupeDao();
    IDao contactDao = jsonFac.getContactDao();
    ObservableList<Personne> personnesFx = FXCollections.observableArrayList(personneDao.findAll());

    private static class UserPanelHolder
    {
        private final static UserPanel instance = new UserPanel();
    }

    public static UserPanel getInstance()
    {
        return UserPanelHolder.instance;
    }

    public StackPane UserList(){
        final StackPane UserList = new  StackPane();
        TableView<Personne> users = new TableView<Personne>();

        TableColumn lastname = new TableColumn("Nom");
        lastname.setCellValueFactory(new PropertyValueFactory<Personne,String>("lastname"));
        TableColumn id = new TableColumn("id");
        id.setCellValueFactory(new PropertyValueFactory<Personne,String>("id"));
        TableColumn firstname= new TableColumn("Prenom");
        firstname.setCellValueFactory(new PropertyValueFactory<Personne,String>("firstname"));

        users.getColumns().addAll(id,lastname,firstname);
        users.setItems(personnesFx);
//        System.out.println(personnesFx.size());
        UserList.getChildren().add(users);

        users.setRowFactory(new Callback<TableView<Personne>, TableRow<Personne>>() {
            @Override
            public TableRow<Personne> call(TableView<Personne> param) {
                TableRow<Personne> row = new TableRow<>();
                row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent event) {
                        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                            Personne rowData = row.getItem();
                            UserList.getChildren().remove(users);
                            UserList.getChildren().add(DetailUser(rowData.getId(),personneDao.findAll()));
                        }
                    }
                });
                return row ;
            }
        });
        return UserList;
    }

    public StackPane CreateUser(){
        StackPane CreateUser = new  StackPane();
        Label label = new Label();
        GridPane grid = new GridPane();
        ComboBox roles = new ComboBox();
        ComboBox civilite = new ComboBox();
        ComboBox groupes = new ComboBox();
        grid.setPadding(new Insets(120, 100, 100, 120));
        grid.setVgap(10);
        grid.setHgap(10);
        ObservableList<Role> rolesList = FXCollections.observableArrayList(roleDao.findAll());
        ObservableList<Groupe> groupesList= FXCollections.observableArrayList(groupeDao.findAll());

        ObservableList<String> civiliteList = FXCollections.observableArrayList("M","Mm");
        civilite.setPromptText("Indiquez votre sexe");
        civilite.setItems(civiliteList);

        roles.setPromptText("Selectionnez votre Role");
        roles.setConverter(new StringConverter<Role>() {
            @Override
            public String toString(Role user) {
                if (user== null){
                    return null;
                } else {
                    return user.getidentifiant();
                }
            }
            @Override
            public Role fromString(String id) {
                return null;
            }
        });
        roles.setItems(getList(rolesList));

        groupes.setPromptText("Choisissez un groupe ");
        groupes.setConverter(new StringConverter<Groupe>() {
            @Override
            public String toString(Groupe user) {
                if (user== null){
                    return null;
                } else {
                    return user.getName();
                }
            }
            @Override
            public Groupe fromString(String id) {
                return null;
            }
        });
        groupes.setItems(getList(groupesList));

        TextField  firstname = new TextField();
        TextField lastname = new TextField();
        TextField road= new TextField();
        TextField city = new TextField();
        Button submit = new Button("Submit");

        lastname.setPromptText("Nom");
        firstname.setPromptText("Prenom");
        road.setPromptText("rue");
        city.setPromptText("ville");

        GridPane.setConstraints(lastname, 0, 0);
        grid.getChildren().add(lastname);
        GridPane.setConstraints(firstname, 0, 1);
        grid.getChildren().add(firstname);
        GridPane.setConstraints(road, 0, 2);
        grid.getChildren().add(road);
        GridPane.setConstraints(city, 0, 3);
        grid.getChildren().add(city);
        GridPane.setConstraints(submit, 1, 2);
        grid.getChildren().add(submit);
        GridPane.setConstraints(label, 1, 3);
        grid.getChildren().add(label);
        GridPane.setConstraints(roles, 0, 4);
        grid.getChildren().add(roles);
        GridPane.setConstraints(civilite, 0, 5);
        grid.getChildren().add(civilite);
        GridPane.setConstraints(groupes, 0, 6);
        grid.getChildren().add(groupes);
        StackPane r = new StackPane();
        r.setPadding(new Insets(5, 1, 2, 1));
        r.setBackground((new Background(new BackgroundFill(Color.rgb(40, 40, 40), null, new Insets(100, 100, 100, 100)))));
        r.getChildren().add(grid);
        CreateUser.getChildren().add(r);
        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Role role = (Role) roles.getSelectionModel().getSelectedItem();
                Groupe gp =(Groupe) groupes.getSelectionModel().getSelectedItem();
                String civil =  (String) civilite.getSelectionModel().getSelectedItem();
                List groupePersonne = new ArrayList();
                if ((lastname.getText() != null && !lastname.getText().isEmpty())
                        && (role != null)
                        && (civil != null && !civil.isEmpty())
                        && (gp != null)
                        ) {
                    groupePersonne.add(gp.getId());
                    Personne personne = new Personne(firstname.getText() , lastname.getText(),
                            civil,role.getId(), road.getText(), city.getText(), "53000", groupePersonne, new ArrayList());
                    personneDao.create(personne);
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Creation");
                    alert.setHeaderText("Results:");
                    alert.setContentText("L'utilisateur " + lastname.getText()+ " a bien été crée");
                    alert.showAndWait();
                    personnesFx.add(personne);
                } else {
                    label.setText("Veuillez remplir tout les champs !");
                    label.setTextFill(Color.WHEAT);

                }
            }
        });
        return CreateUser;
    }

    public StackPane DetailUser(int ID,  List<Personne> personnes){
        StackPane DetailUser = new  StackPane();
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 2, 10));

        grid.setVgap(10);
        grid.setHgap(10);

        Label label = new Label();
        Label firstname = new Label();
        Label lastname = new Label();
        Label road = new Label();
        Label city= new Label();
        Label zipcCode= new Label();
        Label role= new Label();

        Label titleGp = new Label("Liste des groupes");
        label.setFont(new Font("Arial", 40));
        titleGp.setTextFill(Color.CYAN);
        Label titleContact = new Label("Liste des contacts");
        titleContact.setTextFill(Color.CYAN);
        label.setFont(new Font("Arial", 40));


//        List<Personne> personnes = personneDao.findAll();
        Personne personneSelected = null;
        for(Personne personne1: personnes) {
            if(ID == personne1.getId()){
                personneSelected = personne1;
                break;
            }
        }
        Personne personne = personneSelected;
        ObservableList<Groupe> groupeFx =  getList(FXCollections.observableArrayList(this.groupeByPers(personne.getGroupe())));
        TableView<Groupe> groupes = tableGroupe(groupeFx);
        List<String> mainList = new ArrayList<String>();
        mainList.addAll(personne.getContacts());

        ObservableList<Contact> contactsFx = getList(FXCollections.observableArrayList(this.Pers((List)personne.getContacts())));
        TableView<Contact> contacts = tableContact(contactsFx);
//        System.out.println(contactsFx.size());

        Role rolefindById = (Role) roleDao.findById(personne.getRole());
        firstname.setText("Prenom : " + personne.getFirstname());
        firstname.setTextFill(Color.WHEAT);
        lastname.setText("Nom : " + personne.getLastname());
        lastname.setTextFill(Color.WHEAT);
        road.setText("adresse : " + personne.getRoad());
        road.setTextFill(Color.WHEAT);
        city.setText("Ville : " + personne.getCity());
        city.setTextFill(Color.WHEAT);
        if(rolefindById == null){
            role.setText("Role: aucun role");
        }
        else {
            role.setText("Role: " + rolefindById.getidentifiant());
        }

        role.setTextFill(Color.WHEAT);
        GridPane.setConstraints(firstname, 0, 0);
        grid.getChildren().add(firstname);
        GridPane.setConstraints(lastname, 0, 1);
        grid.getChildren().add(lastname);
        GridPane.setConstraints(road, 0, 2);
        grid.getChildren().add(road);
        GridPane.setConstraints(city, 0, 3);
        grid.getChildren().add(city);
        GridPane.setConstraints(role, 0, 4);
        grid.getChildren().add(role);
        GridPane.setConstraints(titleGp, 0, 9);
        grid.getChildren().add(titleGp);
        GridPane.setConstraints(titleContact, 1, 9);
        grid.getChildren().add(titleContact);
        GridPane.setConstraints(groupes, 0, 10);
        grid.getChildren().add(groupes);
        GridPane.setConstraints(contacts, 1, 10);
        grid.getChildren().add(contacts);

        Button retour = new Button("retour");
        GridPane.setConstraints(retour, 0, 5);
//        Button update = new Button("Modifier");
//        GridPane.setConstraints(update, 1, 5);
        Button addContact = new Button("Ajouter un contact");
        GridPane.setConstraints(addContact, 0, 6);
        Button addGroupe = new Button("Ajouter un groupe");
        GridPane.setConstraints(addGroupe, 0, 8);

        Button delet = new Button("Supprimer");
        GridPane.setConstraints(delet, 1, 6);
        GridPane.setConstraints(label, 3, 5);
        grid.getChildren().add(label);
        grid.getChildren().add(retour);
        grid.getChildren().add(addContact);
        grid.getChildren().add(addGroupe);
//        grid.getChildren().add(update);
        grid.getChildren().add(delet);

        StackPane r = new StackPane();
        r.setPadding(new Insets(5, 1, 2, 1));
        r.setBackground((new Background(new BackgroundFill(Color.rgb(40, 40, 40), null, new Insets(10, 10, 330, 10)))));
        r.getChildren().add(grid);
        DetailUser.getChildren().add(r);

        retour.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                DetailUser.getChildren().remove(grid);
                DetailUser.getChildren().add(UserList());
            }
        });
        delet.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Supprimer un utilisteur");
                alert.setHeaderText("Voulez vous supprimer cet utilisateur?");
                alert.setContentText(personne.getFirstname() + " " + personne.getLastname());
                Optional<ButtonType> option = alert.showAndWait();
                if (option.get() == null) {
                    label.setText("No selection!");
                } else if (option.get() == ButtonType.OK) {
                    personneDao.delete(personne.getId());
                    personnesFx.remove(personne);
                    DetailUser.getChildren().remove(grid);
                    DetailUser.getChildren().add(UserList());
                } else if (option.get() == ButtonType.CANCEL) {
                    label.setText("Cancelled!");
                } else {
                    label.setText("-");
                }
            }
        });
//        update.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent e) {
//                DetailUser.getChildren().remove(r);
//                DetailUser.getChildren().add(CreateUser(ID));
//            }
//        });
        addContact.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                DetailUser.getChildren().remove(r);
                DetailUser.getChildren().add(AddContact(ID, personne, FXCollections.observableArrayList(contactDao.findAll()),Contact.class));
            }
        });

        addGroupe.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                DetailUser.getChildren().remove(r);
                DetailUser.getChildren().add(AddContact(ID, personne, FXCollections.observableArrayList(groupeDao.findAll()), Groupe.class ));
            }
        });
        return DetailUser;
    }


    public StackPane AddContact(Integer ID, Personne personne,ObservableList ListFx, Class myClass){

        StackPane addContact = new StackPane();
        Button retour = new Button("retour");
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(10);
        grid.setHgap(10);
        ObservableList<Contact> contactsFx = ListFx;// FXCollections.observableArrayList(contactDao.findAll());
        for (Field field : myClass.getDeclaredFields()) {
            if(field.getName().equals("name")){

            }
            System.out.println(field.getName());
        }

        TableView<IUser> contacts = new TableView<IUser>();
        TableColumn lastname = new TableColumn("Nom");
        lastname.setCellValueFactory(new PropertyValueFactory<IUser,String>("name"));
        TableColumn email= new TableColumn("Email");
        email.setCellValueFactory(new PropertyValueFactory<IUser,String>("email"));
        TableColumn phone= new TableColumn("Phone");
        phone.setCellValueFactory(new PropertyValueFactory<IUser,String>("phone"));
        contacts.getColumns().addAll(lastname, email, phone);
        contacts.setItems(getList(contactsFx));
        GridPane.setConstraints(retour, 0, 0);
        GridPane.setConstraints(contacts, 0, 1);

        grid.getChildren().addAll(contacts,retour);
        addContact.getChildren().addAll(grid);
        contacts.setRowFactory(new Callback<TableView<IUser>, TableRow<IUser>>() {
            @Override
            public TableRow<IUser> call(TableView<IUser> param) {
                TableRow<IUser> row = new TableRow<>();
                row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent event) {
                        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                            IUser rowData = row.getItem();
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                            alert.setTitle("Ajouter un Contact");
                            alert.setHeaderText("Voulez vous ajouter ce contact ?");
                            Optional<ButtonType> option = alert.showAndWait();
                            if (option.get() == null) {
//                                label.setText("No selection!");
                            } else if (option.get() == ButtonType.OK) {
                                    personne.add(rowData);
                                    personneDao.update(personne);

                            } else if (option.get() == ButtonType.CANCEL) {
//                                label.setText("Cancelled!");
                            } else {
                            }
//
                        }
                    }
                });
                return row ;
            }
        });
        retour.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                addContact.getChildren().remove(grid);
                addContact.getChildren().add(DetailUser(ID, personneDao.findAll()));
            }
        });

        return addContact;
    }

    public String roleNull(Integer idRole){
//        String text = null;
        Role rolefindById = (Role) roleDao.findById(idRole);
        if(rolefindById == null){
           return " aucun role";
        }
        else {
           return  rolefindById.getidentifiant();
        }
    }

//    public  GridPane createOrUpdatePanel(){
////        StackPane CreateUser = new  StackPane();
//        Label label = new Label();
//        GridPane grid = new GridPane();
//        ComboBox roles = new ComboBox();
//        ComboBox civilite = new ComboBox();
//        ComboBox groupes = new ComboBox();
//        grid.setPadding(new Insets(120, 100, 100, 120));
//        grid.setVgap(10);
//        grid.setHgap(10);
//        ObservableList<Role> rolesList = FXCollections.observableArrayList(roleDao.findAll());
//        ObservableList<Groupe> groupesList= FXCollections.observableArrayList(groupeDao.findAll());
//
//        ObservableList<String> civiliteList = FXCollections.observableArrayList("M","Mm");
//        civilite.setPromptText("Indiquez votre sexe");
//        civilite.setItems(civiliteList);
//
//        roles.setPromptText("Selectionnez votre Role");
//        roles.setConverter(new StringConverter<Role>() {
//            @Override
//            public String toString(Role user) {
//                if (user== null){
//                    return null;
//                } else {
//                    return user.getidentifiant();
//                }
//            }
//            @Override
//            public Role fromString(String id) {
//                return null;
//            }
//        });
//        roles.setItems(getList(rolesList));
//
//        groupes.setPromptText("Choisissez un groupe ");
//        groupes.setConverter(new StringConverter<Groupe>() {
//            @Override
//            public String toString(Groupe user) {
//                if (user== null){
//                    return null;
//                } else {
//                    return user.getName();
//                }
//            }
//            @Override
//            public Groupe fromString(String id) {
//                return null;
//            }
//        });
//        groupes.setItems(getList(groupesList));
//
//        TextField  firstname = new TextField();
//        TextField lastname = new TextField();
//        TextField road= new TextField();
//        TextField city = new TextField();
////        Button submit = new Button("Submit");
//
//        lastname.setPromptText("Nom");
//        firstname.setPromptText("Prenom");
//        road.setPromptText("rue");
//        city.setPromptText("ville");
//
//        GridPane.setConstraints(lastname, 0, 0);
//        grid.getChildren().add(lastname);
//        GridPane.setConstraints(firstname, 0, 1);
//        grid.getChildren().add(firstname);
//        GridPane.setConstraints(road, 0, 2);
//        grid.getChildren().add(road);
//        GridPane.setConstraints(city, 0, 3);
//        grid.getChildren().add(city);
////        GridPane.setConstraints(submit, 1, 2);
////        grid.getChildren().add(submit);
//        GridPane.setConstraints(label, 1, 3);
//        grid.getChildren().add(label);
//        GridPane.setConstraints(roles, 0, 4);
//        grid.getChildren().add(roles);
//        GridPane.setConstraints(civilite, 0, 5);
//        grid.getChildren().add(civilite);
//        GridPane.setConstraints(groupes, 0, 6);
//        grid.getChildren().add(groupes);
////        StackPane r = new StackPane();
////        r.setPadding(new Insets(5, 1, 2, 1));
////        r.setBackground((new Background(new BackgroundFill(Color.rgb(40, 40, 40), null, new Insets(100, 100, 100, 100)))));
////        r.getChildren().add(grid);
////        CreateUser.getChildren().add(r);
//
//        return CreateUser;
//    }
//    public StackPane UpdateUser(){
//        StackPane updateUser = new  StackPane();
//        Label label = new Label();
//        GridPane grid = new GridPane();
//        ComboBox roles = new ComboBox();
//        ComboBox civilite = new ComboBox();
//        ComboBox groupes = new ComboBox();
//        grid.setPadding(new Insets(120, 100, 100, 120));
//        grid.setVgap(10);
//        grid.setHgap(10);
//        ObservableList<Role> rolesList = FXCollections.observableArrayList(roleDao.findAll());
//        ObservableList<Groupe> groupesList= FXCollections.observableArrayList(groupeDao.findAll());
//
//        ObservableList<String> civiliteList = FXCollections.observableArrayList("M","Mm");
//        civilite.setPromptText("Indiquez votre sexe");
//        civilite.setItems(civiliteList);
//
//        roles.setPromptText("Selectionnez votre Role");
//        roles.setConverter(new StringConverter<Role>() {
//            @Override
//            public String toString(Role user) {
//                if (user== null){
//                    return null;
//                } else {
//                    return user.getidentifiant();
//                }
//            }
//            @Override
//            public Role fromString(String id) {
//                return null;
//            }
//        });
//        roles.setItems(getList(rolesList));
//
//        groupes.setPromptText("Choisissez un groupe ");
//        groupes.setConverter(new StringConverter<Groupe>() {
//            @Override
//            public String toString(Groupe user) {
//                if (user== null){
//                    return null;
//                } else {
//                    return user.getName();
//                }
//            }
//            @Override
//            public Groupe fromString(String id) {
//                return null;
//            }
//        });
//        groupes.setItems(getList(groupesList));
//
//        TextField  firstname = new TextField();
//        TextField lastname = new TextField();
//        TextField road= new TextField();
//        TextField city = new TextField();
//        Button submit = new Button("Submit");
//
//        lastname.setPromptText("Nom");
//        firstname.setPromptText("Prenom");
//        road.setPromptText("rue");
//        city.setPromptText("ville");
//
//        GridPane.setConstraints(lastname, 0, 0);
//        grid.getChildren().add(lastname);
//        GridPane.setConstraints(firstname, 0, 1);
//        grid.getChildren().add(firstname);
//        GridPane.setConstraints(road, 0, 2);
//        grid.getChildren().add(road);
//        GridPane.setConstraints(city, 0, 3);
//        grid.getChildren().add(city);
//        GridPane.setConstraints(submit, 1, 2);
//        grid.getChildren().add(submit);
//        GridPane.setConstraints(label, 1, 3);
//        grid.getChildren().add(label);
//        GridPane.setConstraints(roles, 0, 4);
//        grid.getChildren().add(roles);
//        GridPane.setConstraints(civilite, 0, 5);
//        grid.getChildren().add(civilite);
//        GridPane.setConstraints(groupes, 0, 6);
//        grid.getChildren().add(groupes);
//
//        StackPane r = new StackPane();
//        r.setPadding(new Insets(5, 1, 2, 1));
//        r.setBackground((new Background(new BackgroundFill(Color.rgb(40, 40, 40), null, new Insets(100, 100, 100, 100)))));
//
//        r.getChildren().add(grid);
//        CreateUser.getChildren().add(r);
//        submit.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent e) {
//                Role role = (Role) roles.getSelectionModel().getSelectedItem();
//                Groupe gp =(Groupe) groupes.getSelectionModel().getSelectedItem();
//                String civil =  (String) civilite.getSelectionModel().getSelectedItem();
//                List groupePersonne = new ArrayList();
//                if ((lastname.getText() != null && !lastname.getText().isEmpty())
//                        && (role != null)
//                        && (civil != null && !civil.isEmpty())
//                        && (gp != null)
//                        ) {
//                    groupePersonne.add(gp.getId());
//                    Personne personne = new Personne(firstname.getText() , lastname.getText(),
//                            civil,role.getId(), road.getText(), city.getText(), "53000", groupePersonne);
//                    personneDao.create(personne);
//                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
//                    alert.setTitle("Creation");
//                    alert.setHeaderText("Results:");
//                    alert.setContentText("L'utilisateur " + lastname.getText()+ " a bien été crée");
////                    alert.setContentText("L'utilisateur " + role + " a bien été crée");
//                    alert.showAndWait();
//                    personnesFx.add(personne);
//                } else {
//                    label.setText("Veuillez remplir tout les champs !");
//                    label.setTextFill(Color.WHEAT);
//
//                }
//            }
//        });
//        return updateUser;
//    }
////
//    public  ObservableList<Groupe> getListGroupe( ObservableList<Groupe> per){
//        HashSet<Groupe> h = new HashSet<Groupe>(per);
//        per.clear();
//        per.addAll(h);
//        return per;
//    }
    public List<Groupe> groupeByPers(List ids){
        List<Groupe> perslist = new ArrayList<>();
        for (Object p:  groupeDao.findAll()) {
            for (Object id : ids){
                Groupe gp = (Groupe) p;
//                System.out.println(gp);
                if(((Long)id).intValue() == gp.getId()){
                    perslist.add(gp);
                }
            }
        }
        return perslist;
    }
    public List<Contact> Pers(List ids){
        List<Contact> perslist = new ArrayList<>();
        for (Object p:  contactDao.findAll()) {
            for (Object id : ids){
                Contact gp = (Contact) p;
//                System.out.println(gp);
                if(((Long)id).intValue() == gp.getId()){
                    perslist.add(gp);
                }
            }
        }
        return perslist;
    }
//   public List<Contact> contactByPers(List ids){
//        List<Contact> perslist = new ArrayList<>();
//        for (Object p:  contactDao.findAll()) {
//            for (Object id : ids){
//                Contact gp = (Contact) p;
//                if(((Long)id).intValue() == gp.getId()){
//                    perslist.add(gp);
//                }
//            }
//        }
//        return perslist;
//    }

    public <T>  ObservableList getList( ObservableList<T> per){
        HashSet<T> h = new HashSet<T>(per);
        per.clear();
        per.addAll(h);
        return per;
    }

    public TableView<Groupe> tableGroupe(ObservableList<Groupe> groupeFx){
        TableView<Groupe> groupes = new TableView<Groupe>();
        TableColumn name = new TableColumn("Nom");
        TableColumn desc = new TableColumn("Description");
        name.setPrefWidth(200);
        desc.setPrefWidth(200);
        name.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Groupe, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Groupe, String> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getName());
            }
        });

        desc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Groupe, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Groupe, String> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getDescription());
            }
        });
        groupes.getColumns().addAll(name,desc);
        groupes.setItems(groupeFx);
        return groupes;
    }


    public TableView<Contact> tableContact(ObservableList<Contact> contactFx){
        TableView<Contact> contacts = new TableView<Contact>();
        TableColumn name = new TableColumn("Nom");
        TableColumn email = new TableColumn("Email");
        TableColumn phone = new TableColumn("Phone");
        name.setPrefWidth(135);
        email.setPrefWidth(135);
        phone.setPrefWidth(135);
        name.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Contact, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Contact, String> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getName());
            }
        });

        email.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Contact, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Contact, String> p) {
                return new ReadOnlyObjectWrapper(p.getValue().getEmail());
            }
        });
        contacts.getColumns().addAll(name,email,phone);
        contacts.setItems(contactFx);
        return contacts;
    }
//    private void addButtonToTable() {
//        TableColumn<Personne, Void> colBtn = new TableColumn("Button Column");
//
//        Callback<TableColumn<Personne, Void>, TableCell<Personne, Void>> cellFactory = new Callback<TableColumn<Personne, Void>, TableCell<Personne, Void>>() {
//            @Override
//            public TableCell<Personne, Void> call(final TableColumn<Personne, Void> param) {
//                final TableCell<Personne, Void> cell = new TableCell<Personne, Void>() {
//                    private final Button btn = new Button("Action");
//                    {
//                        btn.setOnAction((ActionEvent event) -> {
//                            Personne data = getTableView().getItems().get(getIndex());
//                            System.out.println("selectedData: " + data);
//                        });
//                    }
////                    @Override
////                    public void updateItem(Void item, boolean empty) {
////                        super.updateItem(item, empty);
////                        if (empty) {
////                            setGraphic(null);
////                        } else {
////                            setGraphic(btn);
////                        }
////                    }
//                };
//                return cell;
//            }
//        };
//
//        colBtn.setCellFactory(cellFactory);
//
//        users.getColumns().add(colBtn);
//
//    }
}
