require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("bluebird");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var mysql = __webpack_require__(22);
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "etna_crowding"
});
con.connect(function (err) {
    if (err) {
        console.log('error when connecting to db:', err);
    }
});
exports.default = con;


/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(6);


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(7);
const server_1 = __webpack_require__(9);
const routes_1 = __webpack_require__(18);
const port = parseInt(process.env.PORT);
exports.default = new server_1.default()
    .router(routes_1.default)
    .listen(port);


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = __webpack_require__(8);
dotenv.config();


/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("dotenv");

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {
Object.defineProperty(exports, "__esModule", { value: true });
const express = __webpack_require__(3);
const path = __webpack_require__(4);
const bodyParser = __webpack_require__(10);
const http = __webpack_require__(11);
const os = __webpack_require__(12);
const cookieParser = __webpack_require__(13);
const swagger_1 = __webpack_require__(14);
const logger_1 = __webpack_require__(16);
const app = express();
class ExpressServer {
    constructor() {
        const root = path.normalize(__dirname + '/../..');
        app.set('appPath', root + 'client');
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(cookieParser(process.env.SESSION_SECRET));
        app.use(express.static(`${root}/public`));
    }
    router(routes) {
        swagger_1.default(app, routes);
        return this;
    }
    listen(port = parseInt(process.env.PORT)) {
        const welcome = port => () => logger_1.default.info(`up and running in ${"development" || 'development'} @: ${os.hostname()} on port: ${port}}`);
        http.createServer(app).listen(port, welcome(port));
        return app;
    }
}
exports.default = ExpressServer;

/* WEBPACK VAR INJECTION */}.call(exports, "server/common"))

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("os");

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("cookie-parser");

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {
Object.defineProperty(exports, "__esModule", { value: true });
const middleware = __webpack_require__(15);
const path = __webpack_require__(4);
function default_1(app, routes) {
    middleware(path.join(__dirname, 'Api.yaml'), app, function (err, middleware) {
        // Enable Express' case-sensitive and strict options
        // (so "/entities", "/Entities", and "/Entities/" are all different)
        app.enable('case sensitive routing');
        app.enable('strict routing');
        app.use(middleware.metadata());
        app.use(middleware.files(app, {
            apiPath: process.env.SWAGGER_API_SPEC,
        }));
        app.use(middleware.parseRequest({
            // Configure the cookie parser to use secure cookies
            cookie: {
                secret: process.env.SESSION_SECRET
            },
            // Don't allow JSON content over 100kb (default is 1mb)
            json: {
                limit: process.env.REQUEST_LIMIT
            }
        }));
        // These two middleware don't have any options (yet)
        app.use(middleware.CORS(), middleware.validateRequest());
        // Error handler to display the validation error as HTML
        app.use(function (err, req, res, next) {
            res.status(err.status);
            res.send('<h1>' + err.status + ' Error</h1>' +
                '<pre>' + err.message + '</pre>');
        });
        routes(app);
    });
}
exports.default = default_1;

/* WEBPACK VAR INJECTION */}.call(exports, "server/common/swagger"))

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("swagger-express-middleware");

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const pino = __webpack_require__(17);
const l = pino({
    name: process.env.APP_ID,
    level: process.env.LOG_LEVEL
});
exports.default = l;


/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = require("pino");

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// import examplesRouter from './api/controllers/examples/router'
const route_1 = __webpack_require__(19);
function routes(app) {
    app.use('/api/', route_1.default);
    app.use(function (req, res, next) {
        res.status(404).json({ code: 404, message: 'error' });
    });
}
exports.default = routes;
;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const express = __webpack_require__(3);
const domainsController_1 = __webpack_require__(20);
const PostController_1 = __webpack_require__(23);
const PostDomainController_1 = __webpack_require__(25);
const PutController_1 = __webpack_require__(27);
const DeleteController_1 = __webpack_require__(29);
exports.default = express.Router()
    .post('/domains/:name/translations.:type', PostController_1.default.transPost)
    .post('/domains.:type', PostDomainController_1.default.domainPost)
    .put('/domains/:name/translations/:id.:type', PutController_1.default.transPut)
    .delete('/domains/:name/translations/:id.:type', DeleteController_1.default.transDelete)
    // .get('/', domainsController.all)
    .get('/domains.:type', domainsController_1.default.all)
    .get('/domains/:name.:type', domainsController_1.default.getName)
    .get('/domains/:name/translations.:type', domainsController_1.default.getTrans);


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const domains_service_1 = __webpack_require__(21);
;
class DomainsController {
    all(req, res) {
        if (req.params.type === 'json') {
            domains_service_1.default.all().then(r => {
                if (r)
                    res.json(r);
                else
                    res.status(404).json({ code: 404, message: 'error' });
            });
        }
        else
            res.status(400).json({ code: 400, message: 'error', datas: [] });
    }
    getName(req, res) {
        if (req.params.type === 'json') {
            let authorization = null;
            if (req.headers.authorization) {
                authorization = req.headers.authorization;
            }
            domains_service_1.default.getName(req.params.name, authorization).then(r => {
                if (r && r.code !== 404) {
                    res.json(r);
                }
                else
                    res.status(404).json({ code: 404, message: 'error' });
            });
        }
        else
            res.status(400).json({ code: 400, message: 'error', datas: [] });
    }
    getTrans(req, res) {
        if (req.params.type === 'json') {
            let code = null;
            if (req.query.code) {
                code = req.query.code;
            }
            domains_service_1.default.getTrans(req.params.name, code).then(r => {
                if (r && r.code !== 404) {
                    res.json(r);
                }
                else
                    res.status(404).json({ code: 404, message: 'error' });
            });
        }
        else
            res.status(400).json({ code: 400, message: 'error', datas: [] });
    }
}
exports.DomainsController = DomainsController;
exports.default = new DomainsController();


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Promise = __webpack_require__(0);
const db_connect_1 = __webpack_require__(1);
let id = 0;
;
let domain;
class DomainsService {
    all() {
        return new Promise(function (resolve, reject) {
            let db = db_connect_1.default.query("select id, slug, name, description from domain;", function (err, result) {
                if (err)
                    throw err;
                resolve({ code: 200, message: 'success', datas: result });
            });
        });
    }
    getName(name, authorization) {
        return new Promise(function (resolve, reject) {
            let mailerRequest = "SELECT domain.id, slug, name,description, GROUP_CONCAT(domain_lang.lang_id) as langs, GROUP_CONCAT(domain_lang.domain_id) as domain_langs, user.id as user_id,user.username, created_at FROM domain INNER JOIN domain_lang INNER JOIN user ON domain.user_id = user.id  WHERE slug = '" + name + "'  GROUP BY domain.id;";
            selectByautho(name, authorization).then(selBy => {
                let db = db_connect_1.default.query(mailerRequest, function (err, result) {
                    if (err)
                        throw err;
                    if (resultName(result, selBy).length > 0) {
                        resolve({ code: 404, message: 'error' });
                    }
                    else {
                        resolve({ code: 200, message: 'success', datas: resultName(result, selBy) });
                    }
                });
            });
        });
    }
    getTrans(name, code) {
        return new Promise(function (resolve, reject) {
            let Request;
            //SELECT id, code, (SELECT GROUP_CONCAT(CONCAT(lang_id, '.', trans)) FROM translation_to_lang WHERE translation_id = translation.id ) as trans FROM translation WHERE code LIKE '%reg%'
            if (code !== null) {
                Request = "SELECT id, code, (SELECT GROUP_CONCAT(CONCAT(lang_id, '.', trans)) FROM translation_to_lang WHERE translation_id = translation.id ) as trans FROM translation WHERE code LIKE '%" + code + "%';";
            }
            else {
                Request = "SELECT id, code, (SELECT GROUP_CONCAT(CONCAT(lang_id, '.', trans)) FROM translation_to_lang WHERE translation_id = translation.id ) as trans FROM translation WHERE domain_id = (SELECT id FROM domain WHERE slug = '" + name + "');";
            }
            let db = db_connect_1.default.query(Request, function (err, result) {
                // console.log(result)
                if (err)
                    throw err;
                disp().then(r => {
                    let lansgDom = [];
                    let domain = [];
                    if (r) {
                        r.forEach(element => {
                            lansgDom.push(element["lang_id"]);
                        });
                        getDomainName().then(t => {
                            t.forEach(ele => {
                                domain.push(ele["slug"]);
                            });
                            if (domain.indexOf(name) === -1) {
                                resolve({ code: 404, message: 'error', datas: [] });
                            }
                            else
                                resolve({ code: 200, message: 'success', datas: resultTrans(result, lansgDom) });
                        });
                    }
                });
            });
        });
    }
}
function selectByautho(domain, autho) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT * FROM `user` WHERE id = (SELECT user_id FROM `domain` WHERE slug = '" + domain + "')  AND password ='" + autho + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function resultTrans(result, lansgDom) {
    let LangDomains;
    let allResult = [];
    // console.log(result)
    result.forEach(element => {
        var obj = {};
        let LangTrans = element['trans'].split(",");
        let t = [];
        LangTrans.forEach(e => {
            let one = e.split(".");
            let mykey = one[0];
            lansgDom.forEach(langs => {
                if (langs === mykey) {
                    obj[mykey] = one[1];
                }
                if (Object.keys(obj).indexOf(langs) === -1) {
                    obj[langs] = element['code'];
                }
            });
        });
        let r = {
            "trans": obj,
            "id": element['id'],
            "code": element['code'],
        };
        allResult.push(r);
    });
    return allResult;
}
function resultName(result, selBy) {
    let allResult = [];
    result.forEach(element => {
        let arrayLang = [];
        let creator = {};
        if (selBy.length > 0) {
            creator = {
                "id": element['user_id'],
                "username": element['username'],
                "email": selBy[0].email
            };
        }
        else {
            creator = {
                "id": element['user_id'],
                "username": element['username'],
            };
        }
        // console.log(element)
        let domainLang = element['domain_langs'].split(",");
        for (let k in domainLang) {
            if (domainLang[k] == element['id']) {
                arrayLang.push(element['langs'].split(",")[k]);
            }
        }
        let r = {
            "langs": arrayLang,
            "id": element['id'],
            "slug": element['slug'],
            "name": element['name'],
            "description": element['description'],
            "creator": creator,
            "created_at": element['created_at'],
        };
        allResult.push(r);
    });
    if (allResult[0] == null) {
        return ["vide"];
    }
    return allResult[0];
}
function disp() {
    {
        return new Promise(function (resolve, reject) {
            let LangDomains;
            let db = db_connect_1.default.query("SELECT domain_id, lang_id FROM `domain_lang`", function (err, result) {
                if (err)
                    throw err;
                LangDomains = result;
                resolve(LangDomains);
            });
        });
    }
}
function getDomainName() {
    {
        return new Promise(function (resolve, reject) {
            let Domains;
            let db = db_connect_1.default.query("SELECT name, slug, id, user_id FROM `domain`", function (err, result) {
                if (err)
                    throw err;
                Domains = result;
                resolve(Domains);
            });
        });
    }
}
exports.default = new DomainsService();


/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = require("mysql");

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const post_service_1 = __webpack_require__(24);
class PostController {
    transPost(req, res) {
        // if(req.params.type === 'json'){
        post_service_1.default.transPost(req.body.code, req.body.trans, req.params.name, req.headers.authorization).then((r) => {
            // if(req.body.code == null)
            // {
            //     console.log("cool")
            // }
            // else {
            //     console.log(req.body.code)
            // }
            if (r) {
                if (r.code === 401)
                    res.status(401).json({ code: 401, message: 'error authorization' });
                else if (r.code === 403)
                    res.status(403).json({ code: 403, message: 'error domain' });
                else if (r.code === 400)
                    res.status(400).json(r);
                else if (req.params.type !== 'json')
                    res.status(400).json({ code: 400, message: 'error', datas: [] });
                else
                    res.status(201).json(r);
            }
        });
        // }
    }
}
exports.PostController = PostController;
exports.default = new PostController();


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Promise = __webpack_require__(0);
var _ = __webpack_require__(2);
const db_connect_1 = __webpack_require__(1);
class PostService {
    transPost(code, trans, name, authorization) {
        return new Promise(function (resolve, reject) {
            let form;
            getAuthoUser(authorization).then(AuthoUser => {
                if (_.isEmpty(AuthoUser))
                    resolve({ code: 401 });
                else {
                    getDomain(name).then(Domain => {
                        if (_.isEmpty(Domain))
                            resolve({ code: 403 });
                        else
                            getDomainByUser(name, AuthoUser).then(na => {
                                if (_.isEmpty(na))
                                    resolve({ code: 403 });
                                else if (code === '' || trans == null || code === 'undefined' || code == null)
                                    resolve({ code: 400, message: 'error', datas: "code or trans is empty" });
                                else if (emptyElem(trans) === 1)
                                    resolve({ code: 400, message: 'error', datas: "code or trans is empty" });
                                else
                                    // resolve(code)
                                    getDomainLang(trans).then(DomainLang => {
                                        // console.log(DomainLang)
                                        if (DomainLang[0]) {
                                            let lang = { "lang": " lang no found " + DomainLang };
                                            form = { "form": lang };
                                            resolve({ code: 400, message: 'error', datas: form });
                                        }
                                        else
                                            getCode(code).then(c => {
                                                if (!_.isEmpty(c)) {
                                                    let lang = { "code": code + " alredy exist" };
                                                    form = { "form": lang };
                                                    resolve({ code: 400, message: 'error' + code, datas: form });
                                                }
                                                else
                                                    getDomLangs(Domain[0].id).then(DomLangs => {
                                                        insertTrans(code, Domain[0].id).then(insertT => {
                                                            insertTransLang(code, trans, DomLangs, insertT).then(transLang => {
                                                                resolve({ code: 201, message: 'success', datas: transLang });
                                                            });
                                                        });
                                                    });
                                            });
                                    });
                            });
                    });
                }
            });
        });
    }
}
function insertTrans(code, domainId) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("INSERT INTO `translation` (domain_id, code) VALUES ('" + domainId + "','" + code + "')", function (err, result) {
            if (err)
                throw err;
            resolve(result.insertId);
        });
    });
}
function insertTransLang(code, trans, langsDomain, insertIdTrans) {
    return new Promise(function (resolve, reject) {
        let obj;
        let t;
        // console.log(responsPost(code, trans, langsDomain, insertIdTrans))
        langsDomain.forEach(el => {
            if (Object.keys(trans).indexOf(el) === -1) {
                trans[el] = code;
            }
        });
        let lang = Object.keys(trans);
        let trad = Object.values(trans);
        for (let k in lang) {
            let db = db_connect_1.default.query("INSERT INTO `translation_to_lang` (`translation_id`, `lang_id`,`trans`) VALUES ('" + insertIdTrans + "','" + lang[k] + "','" + trad[k] + "' ) ", function (err, result) {
                if (err)
                    throw err;
            });
        }
        ;
        resolve(responsPost(code, lang, trad, insertIdTrans));
    });
}
function responsPost(code, lang, trad, insertIdTrans) {
    let tabTrans = {};
    for (let k in lang) {
        tabTrans[lang[k]] = trad[k];
    }
    let data = {
        "trans": tabTrans,
        "code": code,
        "id": insertIdTrans,
    };
    return data;
    // console.log(data)
}
function getDomainByUser(domain, autho_id) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT * FROM `domain` WHERE user_id ='" + autho_id[0].id + "' AND slug = '" + domain + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function getCode(code) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT  code FROM `translation` WHERE code ='" + code + "'", function (err, result) {
            if (err)
                throw err;
            // if(result[0].code != "undefined")
            //     resolve(false)
            // else
            resolve(result);
        });
    });
}
function getDomainLang(trans) {
    return new Promise(function (resolve, reject) {
        let tab = Object.keys(trans);
        let resultLang = [];
        let r = tab.join("','");
        let db = db_connect_1.default.query("SELECT lang_id FROM `domain_lang` WHERE lang_id IN ('" + r + "')", function (err, result) {
            if (err)
                throw err;
            result.forEach(element => {
                resultLang.push(element.lang_id);
            });
            resolve(arr_diff(resultLang, tab));
        });
    });
}
function getAuthoUser(autho) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT id FROM `user` WHERE password ='" + autho + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function getDomain(name) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT user_id, name, id FROM `domain` WHERE slug ='" + name + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function getDomLangs(domainId) {
    return new Promise(function (resolve, reject) {
        let langs = [];
        let db = db_connect_1.default.query("SELECT lang_id FROM `domain_lang` WHERE domain_id ='" + domainId + "'", function (err, result) {
            if (err)
                throw err;
            result.forEach(element => {
                langs.push(element.lang_id);
            });
            resolve(langs);
        });
    });
}
function arr_diff(a1, a2) {
    var a = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }
    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        }
        else {
            a[a2[i]] = true;
        }
    }
    for (var k in a) {
        diff.push(k);
    }
    return diff;
}
function emptyElem(trans) {
    let val = 0;
    let tab = Object.values(trans);
    tab.forEach(element => {
        if (element === '')
            val = 1;
    });
    return val;
}
// function emptyElem(obj) {
//     for(var key in obj) {
//         if(obj.hasOwnProperty(key))
//             return false;
//     }
//     return true;
// }
exports.default = new PostService();


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const postDomain_service_1 = __webpack_require__(26);
class PostDomainController {
    domainPost(req, res) {
        postDomain_service_1.default.domainPost(req.headers.authorization, req.body.name, req.body.description, req.body.lang).then((r) => {
            if (r) {
                if (r.code === 401)
                    res.status(401).json({ code: 401, message: 'error authorization' });
                else if (r.code === 400)
                    res.status(400).json(r);
                else
                    res.status(200).json(r);
            }
        });
    }
}
exports.PostDomainController = PostDomainController;
exports.default = new PostDomainController();


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Promise = __webpack_require__(0);
var _ = __webpack_require__(2);
const db_connect_1 = __webpack_require__(1);
class DomainPostService {
    domainPost(authorization, name, description, lang) {
        return new Promise(function (resolve, reject) {
            let form;
            getAuthoUser(authorization).then(AuthoUser => {
                if (_.isEmpty(AuthoUser))
                    resolve({ code: 401 });
                else {
                    if (name === '' || description == null || lang == null)
                        resolve({ code: 400, message: 'error', datas: "one of these items is empty (name, description, lang)" });
                    else
                        getLang().then(langDb => {
                            console.log(LangExist(lang, langDb));
                            if (LangExist(lang, langDb) === 1) {
                                resolve({ code: 400, message: 'error', datas: "Lang Don't exist" });
                            }
                            else
                                uniqueSlug(name).then(unikSlu => {
                                    addDomain(name, unikSlu, description, lang, AuthoUser[0].id).then(addDo => {
                                        getDateAndId(unikSlu).then(date => {
                                            // console.log(result(name,unikSlu, description, lang,  AuthoUser[0], date))
                                            // resolve("ok")
                                            resolve({ code: 201, message: "success", datas: result(name, unikSlu, description, lang, AuthoUser[0], date) });
                                        });
                                    });
                                });
                            // resolve("ok")
                        });
                }
            });
        });
    }
}
function addDomain(name, slug, description, lang, user_id) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("INSERT INTO `domain` (`name`, `description`, `user_id`, `slug`,`created_at`) VALUES ('" + name + "','" + description + "', '" + user_id + "', '" + slug + "', NOW());", function (err, result) {
            if (err)
                throw err;
            for (let k in lang) {
                let dbs = db_connect_1.default.query("INSERT INTO `domain_lang`  (`domain_id`, `lang_id`) VALUES ('" + result.insertId + "', '" + lang[k] + "') ", function (err, results) {
                    resolve(results);
                });
            }
        });
        // resolve("ok")
    });
}
function result(name, slug, descr, lang, auth, domain) {
    let data = {
        "langs": lang,
        "id": domain.id,
        "slug": slug,
        "name": name,
        "description": descr,
        "creator": {
            "id": auth.id,
            "username": auth.username,
            "email": auth.email
        },
        "created_at": domain.created_at
    };
    return data;
}
function getDateAndId(slug) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT  id, created_at FROM `domain` WHERE slug = '" + slug + "'", function (err, result) {
            resolve(result[0]);
        });
    });
}
function uniqueSlug(name) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT name, slug FROM `domain` WHERE slug = '" + name + "'", function (err, result) {
            if (err)
                throw err;
            if (result[0]) {
                let db = db_connect_1.default.query("SELECT COUNT(*) AS slugCount FROM `domain` WHERE slug = '" + name + "'", function (err, results) {
                    if (err)
                        throw err;
                    let slug = name + '_' + uniqueId();
                    resolve(slug);
                });
            }
            else
                resolve(name);
        });
    });
}
function getAuthoUser(autho) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT * FROM `user` WHERE password ='" + autho + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function getLang() {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT code FROM `lang`", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function uniqueId() {
    return 'id-' + Math.random().toString(36).substr(2, 6);
}
;
function LangExist(lang, langDb) {
    let x = 0;
    let ta = [];
    for (let k in langDb) {
        ta.push(langDb[k].code);
    }
    for (let e in lang) {
        if (ta.indexOf(lang[e]) == -1) {
            x = 1;
        }
        else
            x = 0;
    }
    return x;
}
exports.default = new DomainPostService();


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const put_service_1 = __webpack_require__(28);
class PutController {
    transPut(req, res) {
        // // if(req.params.type === 'json'){
        console.log('updating-', req.body);
        put_service_1.default.transPut(req.params.id, req.body.trans, req.params.name, req.headers.authorization).then((r) => {
            if (r) {
                if (r.code === 401)
                    res.status(401).json({ code: 401, message: 'error authorization' });
                else if (r.code === 403)
                    res.status(403).json({ code: 403, message: 'error domain' });
                else if (r.code === 400)
                    res.status(400).json(r);
                else
                    res.status(200).json(r);
            }
        });
    }
}
exports.PutController = PutController;
exports.default = new PutController();


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Promise = __webpack_require__(0);
var _ = __webpack_require__(2);
const db_connect_1 = __webpack_require__(1);
class PutService {
    transPut(id, trans, name, authorization) {
        return new Promise(function (resolve, reject) {
            let form;
            getAuthoUser(authorization).then(AuthoUser => {
                if (_.isEmpty(AuthoUser))
                    resolve({ code: 401 });
                else {
                    // getDomain(name).then(Domain => {
                    //     if(_.isEmpty(Domain))
                    //         resolve({code: 403})
                    // else
                    getDomainByUser(name, AuthoUser).then(na => {
                        if (_.isEmpty(na))
                            resolve({ code: 403 });
                        else 
                        // console.log(emptyElem(trans))
                        if (emptyElem(trans))
                            resolve({ code: 400, message: 'error', datas: "trans is empty" });
                        else {
                            getDomainLang(trans).then(DomainLang => {
                                // console.log(DomainLang)
                                if (DomainLang[0]) {
                                    let lang = { "lang": " lang no found " + DomainLang };
                                    form = { "form": lang };
                                    resolve({ code: 400, message: 'error', datas: form });
                                }
                                else {
                                    getIdTransExist(id).then(idTransExist => {
                                        if (_.isEmpty(idTransExist)) {
                                            let lang = { "id": "id no found " + id };
                                            form = { "form": lang };
                                            resolve({ code: 400, message: 'error', datas: form });
                                        }
                                        else
                                            putLang(id, trans).then(put => {
                                                resolve({ code: 200, message: 'success', datas: put });
                                            });
                                    });
                                }
                            });
                        }
                    });
                    //})
                }
            });
        });
    }
}
function putLang(id, trans) {
    return new Promise(function (resolve, reject) {
        let lang = Object.keys(trans);
        let val = Object.values(trans);
        for (let k in lang) {
            //UPDATE `translation` SET `code` = 'waffdfso' WHERE `translation`.`id` = 145;
            let db = db_connect_1.default.query("UPDATE `translation_to_lang` SET `trans` = '" + val[k] + "' WHERE `translation_to_lang`.`translation_id` = " + id + " AND `translation_to_lang`.`lang_id` = '" + lang[k] + "'", function (err, result) {
                if (err)
                    throw err;
            });
        }
        result(id).then(e => {
            resolve(e);
        });
    });
}
function result(id) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT id, code, (SELECT GROUP_CONCAT(CONCAT(lang_id, '.', trans)) FROM translation_to_lang WHERE translation_id = translation.id ) as trans FROM translation WHERE id ='" + id + "'", function (err, result) {
            if (err)
                throw err;
            var obj = {};
            let resu = {};
            let trad = result[0].trans.split(",");
            trad.forEach(e => {
                let one = e.split(".");
                let mykey = one[0];
                let val = one[1];
                obj[mykey] = val;
            });
            resu = {
                "trans": obj,
                "id": result[0].id,
                "code": result[0].code
            };
            resolve(resu);
        });
    });
}
function getAuthoUser(autho) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT id FROM `user` WHERE password ='" + autho + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function getDomain(name) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT user_id, name, id FROM `domain` WHERE slug ='" + name + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function getDomainByUser(domain, autho_id) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT * FROM `domain` WHERE user_id ='" + autho_id[0].id + "' AND slug = '" + domain + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function getDomainLang(trans) {
    return new Promise(function (resolve, reject) {
        let tab = Object.keys(trans);
        let resultLang = [];
        let r = tab.join("','");
        let db = db_connect_1.default.query("SELECT lang_id FROM `domain_lang` WHERE lang_id IN ('" + r + "')", function (err, result) {
            if (err)
                throw err;
            result.forEach(element => {
                resultLang.push(element.lang_id);
            });
            resolve(arr_diff(resultLang, tab));
        });
    });
}
function getIdTransExist(id) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT * FROM `translation` WHERE id ='" + id + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function arr_diff(a1, a2) {
    var a = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }
    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        }
        else {
            a[a2[i]] = true;
        }
    }
    for (var k in a) {
        diff.push(k);
    }
    return diff;
}
function emptyElem(trans) {
    let val = false;
    if (trans === undefined || trans === null || trans === "") {
        console.log(trans, "1");
        val = true;
    }
    else {
        let tab = Object.values(trans);
        //let key = Object.keys(trans)
        console.log(tab, "4", trans);
        tab.forEach(element => {
            console.log(element, "3");
            if (element === '' || element === 'undefined' || element === 'null' || element === undefined || element === null) {
                console.log(trans, "2");
                val = true;
            }
        });
        // tab.forEach(element => {
        //     if(element === '')
        //          resolve({ code: 400, message: 'error', datas: "trans is empty"})
        // });
    }
    return val;
}
exports.default = new PutService();


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const delete_service_1 = __webpack_require__(30);
class PostController {
    transDelete(req, res) {
        delete_service_1.default.transDelete(req.params.id, req.params.name, req.headers.authorization).then((r) => {
            if (r) {
                if (r.code === 401)
                    res.status(401).json({ code: 401, message: 'error authorization' });
                else if (r.code === 403)
                    res.status(403).json({ code: 403, message: 'error domain' });
                else if (r.code === 400)
                    res.status(400).json(r);
                else if (req.params.type !== 'json')
                    res.status(400).json({ code: 400, message: 'error', datas: [] });
                else
                    res.status(200).json(r);
            }
        });
    }
}
exports.PostController = PostController;
exports.default = new PostController();


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Promise = __webpack_require__(0);
const db_connect_1 = __webpack_require__(1);
var _ = __webpack_require__(2);
class DeleteService {
    transDelete(id, domain, authorization) {
        return new Promise(function (resolve, reject) {
            getAuthoUser(authorization).then(AuthoUser => {
                if (_.isEmpty(AuthoUser))
                    resolve({ code: 401 });
                else {
                    getDomainByUser(domain, AuthoUser).then(na => {
                        if (_.isEmpty(na))
                            resolve({ code: 403 });
                        else
                            getIdByDomainId(id, domain).then(IdByDomainId => {
                                if (_.isEmpty(IdByDomainId)) {
                                    resolve({ code: 400, message: "error", datas: "id no found" });
                                }
                                else
                                    DeleteTrans(id).then(delTrans => {
                                        resolve(delTrans);
                                    });
                            });
                    });
                }
            });
        });
    }
}
function DeleteTrans(id) {
    return new Promise(function (resolve, reject) {
        // let db = connect.query("DELETE  FROM `translation` WHERE id ='"+id+"'", function (err, result) {
        //     if (err) throw err;
        // })
        let dbs = db_connect_1.default.query("DELETE FROM `translation_to_lang` WHERE translation_id ='" + id + "'", function (err, result) {
            if (err)
                throw err;
            let db = db_connect_1.default.query("DELETE  FROM `translation` WHERE id ='" + id + "'", function (err, result) {
                if (err)
                    throw err;
            });
        });
        resolve({ code: 200,
            message: "success",
            datas: {
                id: id
            } });
    });
}
function getIdByDomainId(id, domain) {
    return new Promise(function (resolve, reject) {
        getDomain(domain).then(gDomain => {
            console.log(gDomain);
            let db = db_connect_1.default.query("SELECT id FROM `translation` WHERE id ='" + id + "' AND domain_id ='" + gDomain[0].id + "'", function (err, result) {
                if (err)
                    throw err;
                resolve(result);
            });
            // resolve(gDomain)
        });
    });
}
function getAuthoUser(autho) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT id FROM `user` WHERE password ='" + autho + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function getDomain(domain) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT user_id, name, id FROM `domain` WHERE name ='" + domain + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
function getDomainByUser(domain, autho_id) {
    return new Promise(function (resolve, reject) {
        let db = db_connect_1.default.query("SELECT * FROM `domain` WHERE user_id ='" + autho_id[0].id + "' AND name = '" + domain + "'", function (err, result) {
            if (err)
                throw err;
            resolve(result);
        });
    });
}
exports.default = new DeleteService();


/***/ })
/******/ ]);
//# sourceMappingURL=main.map