import * as Promise from 'bluebird';
var _ = require('lodash');
import connect from '../../common/db_connect';
import L from '../../common/logger';
import { realpathSync } from 'fs';

class DomainPostService {
    domainPost(authorization, name, description, lang){
        return new Promise (function(resolve, reject){
            let form:{}
            getAuthoUser(authorization).then(AuthoUser => {
                if(_.isEmpty(AuthoUser))
                    resolve({code: 401})
                else{
                    if(name === '' || description == null || lang == null)
                            resolve({ code: 400, message: 'error', datas: "one of these items is empty (name, description, lang)"})
                    else
                        getLang().then(langDb => {
                            console.log(LangExist(lang,langDb)) 
                            if(LangExist(lang,langDb) === 1){
                                resolve({ code: 400, message: 'error', datas: "Lang Don't exist"})
                            }
                            else
                                uniqueSlug(name).then(unikSlu =>{
                                    addDomain(name, unikSlu, description, lang, AuthoUser[0].id).then(addDo =>{
                                        getDateAndId(unikSlu).then(date =>{
                                            // console.log(result(name,unikSlu, description, lang,  AuthoUser[0], date))
                                            // resolve("ok")
                                            resolve({code:201, message: "success", datas: result(name,unikSlu, description, lang,  AuthoUser[0], date)})
                                        })
                                    })
                                }) 

                                    // resolve("ok")
                        })
                }
            })    
        })
    }
}

function addDomain(name, slug, description, lang, user_id){
    return new Promise (function(resolve,reject){
        let db = connect.query("INSERT INTO `domain` (`name`, `description`, `user_id`, `slug`,`created_at`) VALUES ('"+name+"','"+description+"', '"+user_id+"', '"+slug+"', NOW());", function (err, result) { 
            if (err) throw err;
            for(let k in lang) {
                let dbs = connect.query("INSERT INTO `domain_lang`  (`domain_id`, `lang_id`) VALUES ('"+result.insertId+"', '"+lang[k]+"') ", function (err, results){
                    resolve(results)
                })
            }
            
        })
        // resolve("ok")
    })
}

function result(name,slug, descr, lang, auth, domain){
    let data = {
        "langs": lang,
		"id": domain.id,
		"slug": slug,
		"name": name,
		"description":descr,
		"creator": {
			"id":auth.id,
			"username": auth.username,
			"email":auth.email
		},
		"created_at": domain.created_at

    }
return data
}

function getDateAndId(slug)
{
    return new Promise (function(resolve,reject){
        let db = connect.query("SELECT  id, created_at FROM `domain` WHERE slug = '"+slug+"'", function (err, result) {
            resolve(result[0])
        })
    })
}
function uniqueSlug(name){
    return new Promise (function(resolve,reject){
        let db = connect.query("SELECT name, slug FROM `domain` WHERE slug = '"+name+"'", function (err, result) {
            if (err) throw err;
            if(result[0]){
                let db = connect.query("SELECT COUNT(*) AS slugCount FROM `domain` WHERE slug = '"+name+"'", function (err, results) {
                    if (err) throw err;
                    let slug = name+'_'+ uniqueId()
                    resolve(slug)
                })
            }
            else
                resolve(name)
        })
    })
}

function getAuthoUser(autho){// tranPost et domainPost
    return new Promise (function(resolve,reject){
        let db = connect.query("SELECT * FROM `user` WHERE password ='"+autho+"'", function (err, result) {
            if (err) throw err;
                resolve(result)
        })
    })
}

function getLang(){
    return new Promise (function(resolve,reject){
        let db = connect.query("SELECT code FROM `lang`", function (err, result) {
            if (err) throw err;
                resolve(result)
        })
    })
}

function uniqueId () {
    return 'id-' + Math.random().toString(36).substr(2, 6);
};

function LangExist(lang,langDb){
    let x = 0
    let ta = []
        for(let k in langDb)
        {
            ta.push(langDb[k].code)
        }
        for(let e in lang)
        {
            if(ta.indexOf(lang[e]) == -1){
                x = 1;
            }
            else
                x = 0
        }
    return x
}

export default new DomainPostService();