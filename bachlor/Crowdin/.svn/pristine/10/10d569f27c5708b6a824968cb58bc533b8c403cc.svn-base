<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Upload
 *
 * @ORM\Table(name="upload")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UploadRepository")
 */
class Upload
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\langues",)
     * @ORM\JoinColumn(name="langue_id", referencedColumnName="id")
     */
    private $langues;
    
    /**
     * @var string
     *
     * @ORM\Column(name="origin_lang", type="string", length=255, nullable=true)
     */
    private $origin_lang;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="iduser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
     
    private $upload;
    

    /**
     * @var string
     *
     * @ORM\Column(name="fileUpload", type="string", length=255)
     */
    private $fileUpload;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateadd", type="datetime", nullable=true)
     */
    private $dateadd;

    /**
     * @var integer
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\KeysFile", mappedBy="idFile",cascade={"persist"})
     */
    private $idfileup;

   


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Upload
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set fileUpload
     *
     * @param string $fileUpload
     *
     * @return Upload
     */
    public function setFileUpload($fileUpload)
    {
        $this->fileUpload = $fileUpload;

        return $this;
    }

    /**
     * Get fileUpload
     *
     * @return string
     */
    public function getFileUpload()
    {
        return $this->fileUpload;
    }



    /**
     * Set upload
     *
     * @param \AppBundle\Entity\User $upload
     *
     * @return Upload
     */
    public function setUpload(\AppBundle\Entity\User $upload = null)
    {
        $this->upload = $upload;

        return $this;
    }

    /**
     * Get upload
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpload()
    {
        return $this->upload;
    }

    /**
     * Set langues
     *
     * @param string $langues
     *
     * @return Upload
     */
    public function setLangues($langues)
    {
        $this->langues = $langues;

        return $this;
    }

    /**
     * Get langues
     *
     * @return string
     */
    public function getLangues()
    {
        return $this->langues;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idfileup = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add idfileup
     *
     * @param \AppBundle\Entity\KeysFile $idfileup
     *
     * @return Upload
     */
    public function addIdfileup(\AppBundle\Entity\KeysFile $idfileup)
    {
        $this->idfileup[] = $idfileup;

        return $this;
    }

    /**
     * Remove idfileup
     *
     * @param \AppBundle\Entity\KeysFile $idfileup
     */
    public function removeIdfileup(\AppBundle\Entity\KeysFile $idfileup)
    {
        $this->idfileup->removeElement($idfileup);
    }

    /**
     * Get idfileup
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdfileup()
    {
        return $this->idfileup;
    }


    /**
     * Set dateadd
     *
     * @param \DateTime $dateadd
     *
     * @return Upload
     */
    public function setDateadd($dateadd)
    {
        $this->dateadd = $dateadd;

        return $this;
    }

    /**
     * Get dateadd
     *
     * @return \DateTime
     */
    public function getDateadd()
    {
        return $this->dateadd;
    }

    /**
     * Set originLang
     *
     * @param string $originLang
     *
     * @return Upload
     */
    public function setOriginLang($originLang)
    {
        $this->origin_lang = $originLang;

        return $this;
    }

    /**
     * Get originLang
     *
     * @return string
     */
    public function getOriginLang()
    {
        return $this->origin_lang;
    }

    /**
     * Add langue
     *
     * @param \AppBundle\Entity\langues $langue
     *
     * @return Upload
     */
    public function addLangue(\AppBundle\Entity\langues $langue)
    {
        $this->langues[] = $langue;

        return $this;
    }

    /**
     * Remove langue
     *
     * @param \AppBundle\Entity\langues $langue
     */
    public function removeLangue(\AppBundle\Entity\langues $langue)
    {
        $this->langues->removeElement($langue);
    }

    /**
     * Add originLang
     *
     * @param \AppBundle\Entity\langues $originLang
     *
     * @return Upload
     */
    public function addOriginLang(\AppBundle\Entity\langues $originLang)
    {
        $this->origin_lang[] = $originLang;

        return $this;
    }

    /**
     * Remove originLang
     *
     * @param \AppBundle\Entity\langues $originLang
     */
    public function removeOriginLang(\AppBundle\Entity\langues $originLang)
    {
        $this->origin_lang->removeElement($originLang);
    }
}
