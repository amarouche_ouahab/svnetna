<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_b29b484c895e6e962f57d19dc1dee2a52b6c7b96b0f7517a4d20ca894df23801 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_79938fbf0f2a975b0d3e055f643cf678804b62382bdf06ab63ad6bf666b748b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79938fbf0f2a975b0d3e055f643cf678804b62382bdf06ab63ad6bf666b748b0->enter($__internal_79938fbf0f2a975b0d3e055f643cf678804b62382bdf06ab63ad6bf666b748b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_a5a0d2cc6677c93c7be138d8c90e140b03c5fe7c56f9970c12851fd791ecd6af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5a0d2cc6677c93c7be138d8c90e140b03c5fe7c56f9970c12851fd791ecd6af->enter($__internal_a5a0d2cc6677c93c7be138d8c90e140b03c5fe7c56f9970c12851fd791ecd6af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_79938fbf0f2a975b0d3e055f643cf678804b62382bdf06ab63ad6bf666b748b0->leave($__internal_79938fbf0f2a975b0d3e055f643cf678804b62382bdf06ab63ad6bf666b748b0_prof);

        
        $__internal_a5a0d2cc6677c93c7be138d8c90e140b03c5fe7c56f9970c12851fd791ecd6af->leave($__internal_a5a0d2cc6677c93c7be138d8c90e140b03c5fe7c56f9970c12851fd791ecd6af_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/chevron-right.svg");
    }
}
