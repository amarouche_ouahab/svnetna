<?php

/* FOSUserBundle:ChangePassword:change_password_content.html.twig */
class __TwigTemplate_8ed71e5b4982277db723a882883c5b6b66f33d01e025bbbd4e8923a822ae1851 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e97457b309dcc694da0daafb067fe234af1212b2ad4c30ebeb797a370002d384 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e97457b309dcc694da0daafb067fe234af1212b2ad4c30ebeb797a370002d384->enter($__internal_e97457b309dcc694da0daafb067fe234af1212b2ad4c30ebeb797a370002d384_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password_content.html.twig"));

        $__internal_939cce66659d1f90df4ce0cc6aa22f470786f288e051ecc59db18191b72290d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_939cce66659d1f90df4ce0cc6aa22f470786f288e051ecc59db18191b72290d7->enter($__internal_939cce66659d1f90df4ce0cc6aa22f470786f288e051ecc59db18191b72290d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password_content.html.twig"));

        // line 2
        echo "
<center>
    <div class = \"container\" style=\"max-width: 500px;margin-top: 100px;\">
        <h3 class=\"form-signin-heading\">Changer votre mot de passe</h3>
        <hr class=\"colorgraph\"><br><br>
        ";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_change_password"), "attr" => array("class" => "fos_user_change_password")));
        echo "
            ";
        // line 8
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            <hr class=\"colorgraph\"><br><br>
            <div>
                <input type=\"submit\" class=\"btn btn-lg btn-success btn-block\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("change_password.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
            </div>
        ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
        </div>
</center>
";
        
        $__internal_e97457b309dcc694da0daafb067fe234af1212b2ad4c30ebeb797a370002d384->leave($__internal_e97457b309dcc694da0daafb067fe234af1212b2ad4c30ebeb797a370002d384_prof);

        
        $__internal_939cce66659d1f90df4ce0cc6aa22f470786f288e051ecc59db18191b72290d7->leave($__internal_939cce66659d1f90df4ce0cc6aa22f470786f288e051ecc59db18191b72290d7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 13,  42 => 11,  36 => 8,  32 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<center>
    <div class = \"container\" style=\"max-width: 500px;margin-top: 100px;\">
        <h3 class=\"form-signin-heading\">Changer votre mot de passe</h3>
        <hr class=\"colorgraph\"><br><br>
        {{ form_start(form, { 'action': path('fos_user_change_password'), 'attr': { 'class': 'fos_user_change_password' } }) }}
            {{ form_widget(form) }}
            <hr class=\"colorgraph\"><br><br>
            <div>
                <input type=\"submit\" class=\"btn btn-lg btn-success btn-block\" value=\"{{ 'change_password.submit'|trans }}\" />
            </div>
        {{ form_end(form) }}
        </div>
</center>
", "FOSUserBundle:ChangePassword:change_password_content.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password_content.html.twig");
    }
}
