<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_52ad76c648a9958bdc7653adca52cc45ccc18f3cbc293e8bccfbfa4bfdf24e16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d81060b38aa3dfdfad093a25f00d874061726ee184c89835d618aa7996ba9d81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d81060b38aa3dfdfad093a25f00d874061726ee184c89835d618aa7996ba9d81->enter($__internal_d81060b38aa3dfdfad093a25f00d874061726ee184c89835d618aa7996ba9d81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_f17a28e8c4b7a975967f81956ea3588898ecab7e38a929bc325bf4e039a58eee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f17a28e8c4b7a975967f81956ea3588898ecab7e38a929bc325bf4e039a58eee->enter($__internal_f17a28e8c4b7a975967f81956ea3588898ecab7e38a929bc325bf4e039a58eee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_d81060b38aa3dfdfad093a25f00d874061726ee184c89835d618aa7996ba9d81->leave($__internal_d81060b38aa3dfdfad093a25f00d874061726ee184c89835d618aa7996ba9d81_prof);

        
        $__internal_f17a28e8c4b7a975967f81956ea3588898ecab7e38a929bc325bf4e039a58eee->leave($__internal_f17a28e8c4b7a975967f81956ea3588898ecab7e38a929bc325bf4e039a58eee_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
