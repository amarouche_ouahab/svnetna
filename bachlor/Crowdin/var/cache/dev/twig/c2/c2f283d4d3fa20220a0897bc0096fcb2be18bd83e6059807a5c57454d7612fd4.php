<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_2b0bb57bf5dc5d3822e5fd77977fc7509645e5f124ab3b35e5fa0b53a8485de0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "@FOSUser/layout.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_081493994e0905e25c98a016ff0c6747eaa2bcb7d13ce36a0745036aff6f395a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_081493994e0905e25c98a016ff0c6747eaa2bcb7d13ce36a0745036aff6f395a->enter($__internal_081493994e0905e25c98a016ff0c6747eaa2bcb7d13ce36a0745036aff6f395a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_fc3a5f12fec58ade040d6f33e72a599427d5a7f6493e4fa9e63d4807f4d403fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc3a5f12fec58ade040d6f33e72a599427d5a7f6493e4fa9e63d4807f4d403fe->enter($__internal_fc3a5f12fec58ade040d6f33e72a599427d5a7f6493e4fa9e63d4807f4d403fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_081493994e0905e25c98a016ff0c6747eaa2bcb7d13ce36a0745036aff6f395a->leave($__internal_081493994e0905e25c98a016ff0c6747eaa2bcb7d13ce36a0745036aff6f395a_prof);

        
        $__internal_fc3a5f12fec58ade040d6f33e72a599427d5a7f6493e4fa9e63d4807f4d403fe->leave($__internal_fc3a5f12fec58ade040d6f33e72a599427d5a7f6493e4fa9e63d4807f4d403fe_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_a9136daf7f61774d3a3b1692d697b258bb6a56c558630ba791da545a51b63fc9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9136daf7f61774d3a3b1692d697b258bb6a56c558630ba791da545a51b63fc9->enter($__internal_a9136daf7f61774d3a3b1692d697b258bb6a56c558630ba791da545a51b63fc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_5988dc99bcf3d1edd907c44c935cd665f903cfdd9d610f620d017fea9b705cf7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5988dc99bcf3d1edd907c44c935cd665f903cfdd9d610f620d017fea9b705cf7->enter($__internal_5988dc99bcf3d1edd907c44c935cd665f903cfdd9d610f620d017fea9b705cf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "
";
        // line 15
        echo "
";
        // line 16
        if ($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 17
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 18
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 19
                    echo "            <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                ";
                    // line 20
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
            </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 25
        echo "
<div style=\"width:100%;height:100%;\">
    ";
        // line 27
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 29
        echo "</div>

";
        
        $__internal_5988dc99bcf3d1edd907c44c935cd665f903cfdd9d610f620d017fea9b705cf7->leave($__internal_5988dc99bcf3d1edd907c44c935cd665f903cfdd9d610f620d017fea9b705cf7_prof);

        
        $__internal_a9136daf7f61774d3a3b1692d697b258bb6a56c558630ba791da545a51b63fc9->leave($__internal_a9136daf7f61774d3a3b1692d697b258bb6a56c558630ba791da545a51b63fc9_prof);

    }

    // line 27
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_8e68ca6dbe225d8c7819ccc5b43533060468086fef6517ea834f482d2b7418f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e68ca6dbe225d8c7819ccc5b43533060468086fef6517ea834f482d2b7418f9->enter($__internal_8e68ca6dbe225d8c7819ccc5b43533060468086fef6517ea834f482d2b7418f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_8e82da26aaedbc42477c05f70cc18bee2c5aa523f13ea97845bf4c5bb756fada = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e82da26aaedbc42477c05f70cc18bee2c5aa523f13ea97845bf4c5bb756fada->enter($__internal_8e82da26aaedbc42477c05f70cc18bee2c5aa523f13ea97845bf4c5bb756fada_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 28
        echo "    ";
        
        $__internal_8e82da26aaedbc42477c05f70cc18bee2c5aa523f13ea97845bf4c5bb756fada->leave($__internal_8e82da26aaedbc42477c05f70cc18bee2c5aa523f13ea97845bf4c5bb756fada_prof);

        
        $__internal_8e68ca6dbe225d8c7819ccc5b43533060468086fef6517ea834f482d2b7418f9->leave($__internal_8e68ca6dbe225d8c7819ccc5b43533060468086fef6517ea834f482d2b7418f9_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 28,  107 => 27,  95 => 29,  93 => 27,  89 => 25,  82 => 23,  73 => 20,  68 => 19,  63 => 18,  58 => 17,  56 => 16,  53 => 15,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

{% block content %}

{#<div>#}
{#    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}#}
{#        {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |#}
{#        <a href=\"{{ path('fos_user_security_logout') }}\">#}
{#            {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}#}
{#        </a>#}
{#    {% else %}#}
{#        <a href=\"{{ path('fos_user_security_login') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>#}
{#    {% endif %}#}
{#</div>#}

{% if app.request.hasPreviousSession %}
    {% for type, messages in app.session.flashbag.all() %}
        {% for message in messages %}
            <div class=\"flash-{{ type }}\">
                {{ message }}
            </div>
        {% endfor %}
    {% endfor %}
{% endif %}

<div style=\"width:100%;height:100%;\">
    {% block fos_user_content %}
    {% endblock fos_user_content %}
</div>

{% endblock %}", "@FOSUser/layout.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/layout.html.twig");
    }
}
