<?php

/* @Framework/Form/button_label.html.php */
class __TwigTemplate_28efc14b25a90ce22b6b28e0083b96f6ad11ba5c296cc6bf8489f34d6da8f69c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_740a45cdf38be0e8977230eabcedfda6fbf77ad2a0966b04f7e37470355302ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_740a45cdf38be0e8977230eabcedfda6fbf77ad2a0966b04f7e37470355302ab->enter($__internal_740a45cdf38be0e8977230eabcedfda6fbf77ad2a0966b04f7e37470355302ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_label.html.php"));

        $__internal_f1b013c7d96765fffd9289c9aa606749108acc31ae98b1d7022d6ba141a126a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f1b013c7d96765fffd9289c9aa606749108acc31ae98b1d7022d6ba141a126a5->enter($__internal_f1b013c7d96765fffd9289c9aa606749108acc31ae98b1d7022d6ba141a126a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_label.html.php"));

        
        $__internal_740a45cdf38be0e8977230eabcedfda6fbf77ad2a0966b04f7e37470355302ab->leave($__internal_740a45cdf38be0e8977230eabcedfda6fbf77ad2a0966b04f7e37470355302ab_prof);

        
        $__internal_f1b013c7d96765fffd9289c9aa606749108acc31ae98b1d7022d6ba141a126a5->leave($__internal_f1b013c7d96765fffd9289c9aa606749108acc31ae98b1d7022d6ba141a126a5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_label.html.php";
    }

    public function getDebugInfo()
    {
        return array ();
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/button_label.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_label.html.php");
    }
}
