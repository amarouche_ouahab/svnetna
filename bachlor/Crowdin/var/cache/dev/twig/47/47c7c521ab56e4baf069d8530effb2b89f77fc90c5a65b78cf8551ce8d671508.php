<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_af889d545eabaee974ca97a2921d7f1f5f87d85e2abaaa2900458f1a5129844b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1132ff263dc98a1ac650ea86400f39cf558e03f57cab39df6d4cd7a5afdad4f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1132ff263dc98a1ac650ea86400f39cf558e03f57cab39df6d4cd7a5afdad4f9->enter($__internal_1132ff263dc98a1ac650ea86400f39cf558e03f57cab39df6d4cd7a5afdad4f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_07dd063a5903346293986190cf38d01ef1b9913f6de30b1e44bc8eabcb479765 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07dd063a5903346293986190cf38d01ef1b9913f6de30b1e44bc8eabcb479765->enter($__internal_07dd063a5903346293986190cf38d01ef1b9913f6de30b1e44bc8eabcb479765_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_1132ff263dc98a1ac650ea86400f39cf558e03f57cab39df6d4cd7a5afdad4f9->leave($__internal_1132ff263dc98a1ac650ea86400f39cf558e03f57cab39df6d4cd7a5afdad4f9_prof);

        
        $__internal_07dd063a5903346293986190cf38d01ef1b9913f6de30b1e44bc8eabcb479765->leave($__internal_07dd063a5903346293986190cf38d01ef1b9913f6de30b1e44bc8eabcb479765_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
