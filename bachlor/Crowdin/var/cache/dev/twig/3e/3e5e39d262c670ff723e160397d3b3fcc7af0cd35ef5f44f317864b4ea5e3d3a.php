<?php

/* ::base.html.twig */
class __TwigTemplate_70f2ea4d9ba6befff2419e33e81e4263a6a39804101f8049eb9b3bfd1f26a548 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e9a2e7acbec0d61c8a509b562787af8bebb7baac7fe711f2f71511dddb7d313 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e9a2e7acbec0d61c8a509b562787af8bebb7baac7fe711f2f71511dddb7d313->enter($__internal_8e9a2e7acbec0d61c8a509b562787af8bebb7baac7fe711f2f71511dddb7d313_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_7b3c9ab02b88eb6db7b96191438197ff299a6d739ce5456ab81f187d0b6c6c0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b3c9ab02b88eb6db7b96191438197ff299a6d739ce5456ab81f187d0b6c6c0c->enter($__internal_7b3c9ab02b88eb6db7b96191438197ff299a6d739ce5456ab81f187d0b6c6c0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 12
        echo "
";
        // line 14
        echo "
";
        // line 19
        echo "
";
        // line 26
        echo "
";
        // line 28
        echo "    
";
        // line 41
        echo "
";
        // line 45
        echo "
";
        // line 54
        echo "
<!DOCTYPE html>
<html>

<head>

\t<meta charset=\"utf-8\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>";
        // line 64
        $this->displayBlock('title', $context, $blocks);
        echo "</title>


\t<link rel=\"stylesheet\" href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/demo.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/header-login-signup.css"), "html", null, true);
        echo "\">
\t<link href='http://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
\t    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css\" integrity=\"sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy\" crossorigin=\"anonymous\">

 ";
        // line 73
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 74
        echo "</head>

<body>

<header class=\"header-login-signup\">

\t<div class=\"header-limiter\">

\t\t<h1><a href=\"";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_show");
        echo "\">Crow<span>din</span></a></h1>

 ";
        // line 84
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 85
            echo "        ";
            // line 86
            echo "        <nav>
\t\t\t<a href=";
            // line 87
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_show");
            echo ">Home</a>
\t\t\t<a href=";
            // line 88
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("showFile");
            echo ">Voir tout les fichers</a>
\t\t\t<a href=";
            // line 89
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("showMyFile");
            echo ">Voir mes fichers</a>
\t\t\t<a href=";
            // line 90
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_product_new");
            echo ">Ajouter un fichier</a>
\t\t</nav>
\t\t
\t\t<ul>
\t\t\t
\t\t\t<li><a href=\"";
            // line 95
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">Se deconnecter</a></li>
\t\t</ul>
    ";
        } else {
            // line 98
            echo "        <ul>
\t\t\t<li><a href=\"";
            // line 99
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">Login</a></li>
\t\t\t<li><a href=\"";
            // line 100
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
            echo "\">Sign up</a></li>
\t\t</ul>

    ";
        }
        // line 104
        echo "
\t</div>

</header>

<!-- The content of your page would go here. -->
<div class=\"row\" style=\"margin-left:  1%;margin-top:  5%;margin-right:  1%;margin-bottom: 15%;\">
 ";
        // line 111
        $this->displayBlock('content', $context, $blocks);
        // line 113
        echo "</div>
";
        // line 115
        echo "
";
        // line 117
        echo "
";
        // line 120
        echo "
";
        // line 130
        echo "
";
        // line 132
        echo "


<!-- Demo ads. Please ignore and remove. -->
<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>
";
        // line 138
        echo "
</body>

</html>
";
        
        $__internal_8e9a2e7acbec0d61c8a509b562787af8bebb7baac7fe711f2f71511dddb7d313->leave($__internal_8e9a2e7acbec0d61c8a509b562787af8bebb7baac7fe711f2f71511dddb7d313_prof);

        
        $__internal_7b3c9ab02b88eb6db7b96191438197ff299a6d739ce5456ab81f187d0b6c6c0c->leave($__internal_7b3c9ab02b88eb6db7b96191438197ff299a6d739ce5456ab81f187d0b6c6c0c_prof);

    }

    // line 64
    public function block_title($context, array $blocks = array())
    {
        $__internal_b5b5b325f0f3479024ae65007009646848619ca8d7488cd49c36aa9861ab2d9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b5b5b325f0f3479024ae65007009646848619ca8d7488cd49c36aa9861ab2d9a->enter($__internal_b5b5b325f0f3479024ae65007009646848619ca8d7488cd49c36aa9861ab2d9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_efd18bb545b9e0c38f38cb4935ecd6b549dcf0e054e71c158c2ba89b46347f5d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efd18bb545b9e0c38f38cb4935ecd6b549dcf0e054e71c158c2ba89b46347f5d->enter($__internal_efd18bb545b9e0c38f38cb4935ecd6b549dcf0e054e71c158c2ba89b46347f5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "My crowdin";
        
        $__internal_efd18bb545b9e0c38f38cb4935ecd6b549dcf0e054e71c158c2ba89b46347f5d->leave($__internal_efd18bb545b9e0c38f38cb4935ecd6b549dcf0e054e71c158c2ba89b46347f5d_prof);

        
        $__internal_b5b5b325f0f3479024ae65007009646848619ca8d7488cd49c36aa9861ab2d9a->leave($__internal_b5b5b325f0f3479024ae65007009646848619ca8d7488cd49c36aa9861ab2d9a_prof);

    }

    // line 73
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_aa814401801f4662afb69e9223cdba45a0960404be5dc554e556ba94e2c7ed55 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa814401801f4662afb69e9223cdba45a0960404be5dc554e556ba94e2c7ed55->enter($__internal_aa814401801f4662afb69e9223cdba45a0960404be5dc554e556ba94e2c7ed55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_138efda875be3de6af530ff0ebbef15f5868ce3c203df7344836dcb581658c56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_138efda875be3de6af530ff0ebbef15f5868ce3c203df7344836dcb581658c56->enter($__internal_138efda875be3de6af530ff0ebbef15f5868ce3c203df7344836dcb581658c56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_138efda875be3de6af530ff0ebbef15f5868ce3c203df7344836dcb581658c56->leave($__internal_138efda875be3de6af530ff0ebbef15f5868ce3c203df7344836dcb581658c56_prof);

        
        $__internal_aa814401801f4662afb69e9223cdba45a0960404be5dc554e556ba94e2c7ed55->leave($__internal_aa814401801f4662afb69e9223cdba45a0960404be5dc554e556ba94e2c7ed55_prof);

    }

    // line 111
    public function block_content($context, array $blocks = array())
    {
        $__internal_002620e0a0c8ce0a50172346db9d7f2e44b9a266dcffec60ae4184fa5b133862 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_002620e0a0c8ce0a50172346db9d7f2e44b9a266dcffec60ae4184fa5b133862->enter($__internal_002620e0a0c8ce0a50172346db9d7f2e44b9a266dcffec60ae4184fa5b133862_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_1b9f745ed9fad014d18add28522609e2a37233aeca6bd21ba3c4c3cfd0293c99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b9f745ed9fad014d18add28522609e2a37233aeca6bd21ba3c4c3cfd0293c99->enter($__internal_1b9f745ed9fad014d18add28522609e2a37233aeca6bd21ba3c4c3cfd0293c99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 112
        echo " ";
        
        $__internal_1b9f745ed9fad014d18add28522609e2a37233aeca6bd21ba3c4c3cfd0293c99->leave($__internal_1b9f745ed9fad014d18add28522609e2a37233aeca6bd21ba3c4c3cfd0293c99_prof);

        
        $__internal_002620e0a0c8ce0a50172346db9d7f2e44b9a266dcffec60ae4184fa5b133862->leave($__internal_002620e0a0c8ce0a50172346db9d7f2e44b9a266dcffec60ae4184fa5b133862_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 112,  225 => 111,  208 => 73,  190 => 64,  176 => 138,  169 => 132,  166 => 130,  163 => 120,  160 => 117,  157 => 115,  154 => 113,  152 => 111,  143 => 104,  136 => 100,  132 => 99,  129 => 98,  123 => 95,  115 => 90,  111 => 89,  107 => 88,  103 => 87,  100 => 86,  98 => 85,  96 => 84,  91 => 82,  81 => 74,  79 => 73,  71 => 68,  67 => 67,  61 => 64,  49 => 54,  46 => 45,  43 => 41,  40 => 28,  37 => 26,  34 => 19,  31 => 14,  28 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#<!DOCTYPE html>#}
{#<html lang=\"en\">#}
{#<head>#}
{#    <meta charset=\"utf-8\">#}
{#    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">#}
{#    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">#}
{#    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->#}
{#    <meta name=\"description\" content=\"\">#}
{#    <meta name=\"author\" content=\"\">#}
{#    <link rel=\"icon\" href=\"favicon.ico\">  #}
{#    {% block stylesheets %}{% endblock %}#}

{#    <title>{% block title %}My crowdin{% endblock %}</title>#}

{#    <!-- Latest compiled and minified CSS -->#}
{#    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">#}
{#    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css\" integrity=\"sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy\" crossorigin=\"anonymous\">#}
{#    <!-- Latest compiled and minified JavaScript -->#}

{#    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->#}
{#    <!--[if lt IE 9]>#}
{#    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>#}
{#    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>#}
{#    <![endif]-->#}
{#</head>#}

{#<body>#}
    
{#<div class=\"d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow\">#}
{#    <h5 href=\"{{ path('homepage') }}\" class=\"my-0 mr-md-auto font-weight-normal\"><a class=\"p-2 text-dark\" href=\"#\">My crowdin</a></h5>#}
{#    <nav class=\"my-2 my-md-0 mr-md-3\">#}
{#    <a class=\"p-2 text-dark\" href=\"#\">Traduire</a>#}
{#    <a class=\"p-2 text-dark\" href=\"#\">Voir les traductions</a>#}
{#    </nav>#}
{#    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}#}
{#        <a class=\"btn btn-outline-primary\" href=\"{{ path('fos_user_security_logout') }}\">Se deconnecter</a>#}
{#    {% else %}#}
{#        <a class=\"btn btn-outline-primary\" href=\"{{ path('fos_user_security_login') }}\">Se connecter</a>#}
{#    {% endif %}#}
{#</div>#}

{#<div class=\"container\">#}
{#    {% block content %}{% endblock %}#}
{#</div>#}

{#<!-- Bootstrap core JavaScript#}
{#================================================== -->#}
{#<!-- Placed at the end of the document so the pages load faster -->#}
{#<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>#}
{#<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js\"></script>#}
{#{% block javascripts %}{% endblock %}#}
{#</body>#}
{#</html>#}

<!DOCTYPE html>
<html>

<head>

\t<meta charset=\"utf-8\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>{% block title %}My crowdin{% endblock %}</title>


\t<link rel=\"stylesheet\" href=\"{{ asset('assets/demo.css') }}\">
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/header-login-signup.css') }}\">
\t<link href='http://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
\t    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css\" integrity=\"sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy\" crossorigin=\"anonymous\">

 {% block stylesheets %}{% endblock %}
</head>

<body>

<header class=\"header-login-signup\">

\t<div class=\"header-limiter\">

\t\t<h1><a href=\"{{ path('fos_user_profile_show') }}\">Crow<span>din</span></a></h1>

 {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
        {#<a class=\"btn btn-outline-primary\" href=\"{{ path('fos_user_security_logout') }}\">Se deconnecter</a>#}
        <nav>
\t\t\t<a href={{ path('fos_user_profile_show')}}>Home</a>
\t\t\t<a href={{ path('showFile')}}>Voir tout les fichers</a>
\t\t\t<a href={{ path('showMyFile')}}>Voir mes fichers</a>
\t\t\t<a href={{ path('app_product_new')}}>Ajouter un fichier</a>
\t\t</nav>
\t\t
\t\t<ul>
\t\t\t
\t\t\t<li><a href=\"{{ path('fos_user_security_logout') }}\">Se deconnecter</a></li>
\t\t</ul>
    {% else %}
        <ul>
\t\t\t<li><a href=\"{{ path('fos_user_security_login') }}\">Login</a></li>
\t\t\t<li><a href=\"{{ path('fos_user_registration_register') }}\">Sign up</a></li>
\t\t</ul>

    {% endif %}

\t</div>

</header>

<!-- The content of your page would go here. -->
<div class=\"row\" style=\"margin-left:  1%;margin-top:  5%;margin-right:  1%;margin-bottom: 15%;\">
 {% block content %}
 {% endblock %}
</div>
{#<div class=\"menu\">#}

{#\t<img src=\"assets/demo-arrow.png\" alt=\"arrow\" height=\"120\">#}

{#\t<h1>Freebie: 7 Responsive Header Templates</h1>#}
{#\t<h2><a href=\"http://tutorialzine.com/2015/02/freebie-7-responsive-header-templates/\">Download</a></h2>#}

{#\t<ul>#}
{#\t\t<li><a href=\"index.html\">Basic</a></li>#}
{#\t\t<li><a href=\"header-basic-light.html\">Basic Light</a></li>#}
{#\t\t<li><a href=\"header-fixed.html\">Fixed</a></li>#}
{#\t\t<li><a href=\"header-login-signup.html\" class=\"active\">Login/Sign up</a></li>#}
{#\t\t<li><a href=\"header-search.html\">Search</a></li>#}
{#\t\t<li><a href=\"header-second-bar.html\">Second Bar</a></li>#}
{#\t\t<li><a href=\"header-user-dropdown.html\">User Dropdown</a></li>#}
{#\t</ul>#}

{#</div>#}



<!-- Demo ads. Please ignore and remove. -->
<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>
{#<script src=\"http://cdn.tutorialzine.com/misc/enhance/v3.js\"></script>#}

</body>

</html>
", "::base.html.twig", "/home/ubuntu/workspace/app/Resources/views/base.html.twig");
    }
}
