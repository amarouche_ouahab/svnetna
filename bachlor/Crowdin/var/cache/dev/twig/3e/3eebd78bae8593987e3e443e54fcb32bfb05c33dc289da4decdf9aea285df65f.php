<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_5bd81a8259cde2f18a497093a26686350bceb6e454738704d5db1e480feec32c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87c61a947fce2cf6832efc1cd5c8ea2f93c052ec9965ad508ee987f85556377f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87c61a947fce2cf6832efc1cd5c8ea2f93c052ec9965ad508ee987f85556377f->enter($__internal_87c61a947fce2cf6832efc1cd5c8ea2f93c052ec9965ad508ee987f85556377f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_ae8255ec660a5de3fa634c4a2ec5c96e63da565709b4ef477631b1c056960e45 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae8255ec660a5de3fa634c4a2ec5c96e63da565709b4ef477631b1c056960e45->enter($__internal_ae8255ec660a5de3fa634c4a2ec5c96e63da565709b4ef477631b1c056960e45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_87c61a947fce2cf6832efc1cd5c8ea2f93c052ec9965ad508ee987f85556377f->leave($__internal_87c61a947fce2cf6832efc1cd5c8ea2f93c052ec9965ad508ee987f85556377f_prof);

        
        $__internal_ae8255ec660a5de3fa634c4a2ec5c96e63da565709b4ef477631b1c056960e45->leave($__internal_ae8255ec660a5de3fa634c4a2ec5c96e63da565709b4ef477631b1c056960e45_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_dbdff7f26dfecdd9964d93cf4b6815c8a6b01c068f871e587fadf32bdb04df43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dbdff7f26dfecdd9964d93cf4b6815c8a6b01c068f871e587fadf32bdb04df43->enter($__internal_dbdff7f26dfecdd9964d93cf4b6815c8a6b01c068f871e587fadf32bdb04df43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_f10483bb948dd445bbf4c2edd4f787046e1782e5f713ce98cecc31fdd67eef17 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f10483bb948dd445bbf4c2edd4f787046e1782e5f713ce98cecc31fdd67eef17->enter($__internal_f10483bb948dd445bbf4c2edd4f787046e1782e5f713ce98cecc31fdd67eef17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_f10483bb948dd445bbf4c2edd4f787046e1782e5f713ce98cecc31fdd67eef17->leave($__internal_f10483bb948dd445bbf4c2edd4f787046e1782e5f713ce98cecc31fdd67eef17_prof);

        
        $__internal_dbdff7f26dfecdd9964d93cf4b6815c8a6b01c068f871e587fadf32bdb04df43->leave($__internal_dbdff7f26dfecdd9964d93cf4b6815c8a6b01c068f871e587fadf32bdb04df43_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
