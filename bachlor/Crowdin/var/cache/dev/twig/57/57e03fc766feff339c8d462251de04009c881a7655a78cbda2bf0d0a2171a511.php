<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_5af560a7e229c5af2173dab813e8489ed0c5b921dea03bc0ce77c3d417946164 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83fa23f7fed7cba5c95da72501264e849941edcff215c06a9489f9f917e5c43c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_83fa23f7fed7cba5c95da72501264e849941edcff215c06a9489f9f917e5c43c->enter($__internal_83fa23f7fed7cba5c95da72501264e849941edcff215c06a9489f9f917e5c43c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        $__internal_efb50f83cf958e9518983c75bb0440ac00d2e9983dd85cf3a8c015faa3b69315 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efb50f83cf958e9518983c75bb0440ac00d2e9983dd85cf3a8c015faa3b69315->enter($__internal_efb50f83cf958e9518983c75bb0440ac00d2e9983dd85cf3a8c015faa3b69315_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        // line 2
        echo "
";
        // line 15
        echo "
";
        // line 17
        echo "<div class=\"col-12 col-md-3\"></div>
<div class=\"col-12 col-md-1\"></div>
    <div class=\"col-12 col-md-4\">
        <div class=\"panel panel-success\">
            <div class=\"panel-heading\" style=\"font-size:20px;\"><center>Mon Profil</center></div>
            <div class=\"panel-body\">
                <center>
                <p style=\"font-size:15px;\">Bienvenue sur votre profil ";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        echo "
                </br>Email:  ";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "email", array()), "html", null, true);
        echo "</br>
                
            </p>
                <a class=\"btn btn-outline-primary\" href=\"";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_edit");
        echo "\">Modifier mon profile</a>
                <a class=\"btn btn-outline-primary\" href=\"";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_change_password");
        echo "\">Modifier mon Mot de passe</a>
            </div></br></br></center>
                 <center><a class=\"btn btn-outline-warning btn-lg\" style=\"width: 50%;\" href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_product_new");
        echo "\">upload</a>
                 </br></br>
                 <a class=\"btn btn-outline-warning btn-lg\" style=\"width: 50%;\" href=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("showMyFile");
        echo "\">Voir mes fichiers</a>
                 </br></br>
            </center>
        </div>
    </div>
</div>";
        
        $__internal_83fa23f7fed7cba5c95da72501264e849941edcff215c06a9489f9f917e5c43c->leave($__internal_83fa23f7fed7cba5c95da72501264e849941edcff215c06a9489f9f917e5c43c_prof);

        
        $__internal_efb50f83cf958e9518983c75bb0440ac00d2e9983dd85cf3a8c015faa3b69315->leave($__internal_efb50f83cf958e9518983c75bb0440ac00d2e9983dd85cf3a8c015faa3b69315_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 33,  59 => 31,  54 => 29,  50 => 28,  44 => 25,  40 => 24,  31 => 17,  28 => 15,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

{#<div class=\"d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow\">#}
{#    <h5 href=\"{{ path('homepage') }}\" class=\"my-0 mr-md-auto font-weight-normal\"><a class=\"p-2 text-dark\" href=\"#\">My crowdin</a></h5>#}
{#    <nav class=\"my-2 my-md-0 mr-md-3\">#}
{#    <a class=\"p-2 text-dark\" href=\"#\">Traduire</a>#}
{#    <a class=\"p-2 text-dark\" href=\"#\">Voir les traductions</a>#}
{#    </nav>#}
{#    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}#}
{#        <a class=\"btn btn-outline-primary\" href=\"{{ path('fos_user_security_logout') }}\">Se deconnecter</a>#}
{#    {% else %}#}
{#        <a class=\"btn btn-outline-primary\" href=\"{{ path('fos_user_security_login') }}\">Se connecter</a>#}
{#    {% endif %}#}
{#</div>#}

{#<a class=\"btn btn-outline-primary\" href=\"{{ path('addNewFile') }}\">Ajouter un fichier</a>#}
<div class=\"col-12 col-md-3\"></div>
<div class=\"col-12 col-md-1\"></div>
    <div class=\"col-12 col-md-4\">
        <div class=\"panel panel-success\">
            <div class=\"panel-heading\" style=\"font-size:20px;\"><center>Mon Profil</center></div>
            <div class=\"panel-body\">
                <center>
                <p style=\"font-size:15px;\">Bienvenue sur votre profil {{ app.user.username }}
                </br>Email:  {{ app.user.email }}</br>
                
            </p>
                <a class=\"btn btn-outline-primary\" href=\"{{ path('fos_user_profile_edit') }}\">Modifier mon profile</a>
                <a class=\"btn btn-outline-primary\" href=\"{{ path('fos_user_change_password') }}\">Modifier mon Mot de passe</a>
            </div></br></br></center>
                 <center><a class=\"btn btn-outline-warning btn-lg\" style=\"width: 50%;\" href=\"{{ path('app_product_new') }}\">upload</a>
                 </br></br>
                 <a class=\"btn btn-outline-warning btn-lg\" style=\"width: 50%;\" href=\"{{ path('showMyFile') }}\">Voir mes fichiers</a>
                 </br></br>
            </center>
        </div>
    </div>
</div>", "FOSUserBundle:Profile:show_content.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show_content.html.twig");
    }
}
