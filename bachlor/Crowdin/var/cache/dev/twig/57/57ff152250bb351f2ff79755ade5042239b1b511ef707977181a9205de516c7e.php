<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_0a72278be842879d97b9411d9db2ae8c82666febe557bd49cffee8cf63d87dfb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b94d00a3682fbaffb557e0cd914c17274d35f1f343c8aa14c10a11acb8e8d5a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b94d00a3682fbaffb557e0cd914c17274d35f1f343c8aa14c10a11acb8e8d5a1->enter($__internal_b94d00a3682fbaffb557e0cd914c17274d35f1f343c8aa14c10a11acb8e8d5a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_9bcce745278ea8493dfe614f39d605fb84051286e4c2e9370fb864b8f4d6803d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9bcce745278ea8493dfe614f39d605fb84051286e4c2e9370fb864b8f4d6803d->enter($__internal_9bcce745278ea8493dfe614f39d605fb84051286e4c2e9370fb864b8f4d6803d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_b94d00a3682fbaffb557e0cd914c17274d35f1f343c8aa14c10a11acb8e8d5a1->leave($__internal_b94d00a3682fbaffb557e0cd914c17274d35f1f343c8aa14c10a11acb8e8d5a1_prof);

        
        $__internal_9bcce745278ea8493dfe614f39d605fb84051286e4c2e9370fb864b8f4d6803d->leave($__internal_9bcce745278ea8493dfe614f39d605fb84051286e4c2e9370fb864b8f4d6803d_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_d413ebfb31cb458ee567b3bd44a87e960adab78bcadee38b5c46106736bb71df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d413ebfb31cb458ee567b3bd44a87e960adab78bcadee38b5c46106736bb71df->enter($__internal_d413ebfb31cb458ee567b3bd44a87e960adab78bcadee38b5c46106736bb71df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_84db7cec52836d0d9ced6b6a2de8711ee28de8a0967b18025f5c92a5e4a7dbc1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84db7cec52836d0d9ced6b6a2de8711ee28de8a0967b18025f5c92a5e4a7dbc1->enter($__internal_84db7cec52836d0d9ced6b6a2de8711ee28de8a0967b18025f5c92a5e4a7dbc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_84db7cec52836d0d9ced6b6a2de8711ee28de8a0967b18025f5c92a5e4a7dbc1->leave($__internal_84db7cec52836d0d9ced6b6a2de8711ee28de8a0967b18025f5c92a5e4a7dbc1_prof);

        
        $__internal_d413ebfb31cb458ee567b3bd44a87e960adab78bcadee38b5c46106736bb71df->leave($__internal_d413ebfb31cb458ee567b3bd44a87e960adab78bcadee38b5c46106736bb71df_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_6302f5e3c416b4a9874e2056937354620c8f10b09e186248db451470c56cfaae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6302f5e3c416b4a9874e2056937354620c8f10b09e186248db451470c56cfaae->enter($__internal_6302f5e3c416b4a9874e2056937354620c8f10b09e186248db451470c56cfaae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_28571a091827001668e898bab51089be03f15f35d861843cb488ea894e0b4fc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28571a091827001668e898bab51089be03f15f35d861843cb488ea894e0b4fc8->enter($__internal_28571a091827001668e898bab51089be03f15f35d861843cb488ea894e0b4fc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_28571a091827001668e898bab51089be03f15f35d861843cb488ea894e0b4fc8->leave($__internal_28571a091827001668e898bab51089be03f15f35d861843cb488ea894e0b4fc8_prof);

        
        $__internal_6302f5e3c416b4a9874e2056937354620c8f10b09e186248db451470c56cfaae->leave($__internal_6302f5e3c416b4a9874e2056937354620c8f10b09e186248db451470c56cfaae_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_fb362641eb325635fd763d1da28518485a35ce18b7d9d55eab538f7a4eb257d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb362641eb325635fd763d1da28518485a35ce18b7d9d55eab538f7a4eb257d1->enter($__internal_fb362641eb325635fd763d1da28518485a35ce18b7d9d55eab538f7a4eb257d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_ebfd9b683e5ee46fe908ec9a4e4acaa36a36a60ef47ce2f5dcbb54a22cded8a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ebfd9b683e5ee46fe908ec9a4e4acaa36a36a60ef47ce2f5dcbb54a22cded8a9->enter($__internal_ebfd9b683e5ee46fe908ec9a4e4acaa36a36a60ef47ce2f5dcbb54a22cded8a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_ebfd9b683e5ee46fe908ec9a4e4acaa36a36a60ef47ce2f5dcbb54a22cded8a9->leave($__internal_ebfd9b683e5ee46fe908ec9a4e4acaa36a36a60ef47ce2f5dcbb54a22cded8a9_prof);

        
        $__internal_fb362641eb325635fd763d1da28518485a35ce18b7d9d55eab538f7a4eb257d1->leave($__internal_fb362641eb325635fd763d1da28518485a35ce18b7d9d55eab538f7a4eb257d1_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
