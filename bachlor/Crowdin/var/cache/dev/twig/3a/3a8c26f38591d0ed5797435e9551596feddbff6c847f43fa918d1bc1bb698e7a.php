<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_d8a072e0c5a3bf8bf49b144272bbd0338501d7ac4427ef2df0fec672434660f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b433df8003bb39fc5757e59cdbb5ec98d4cb72d80df67865396a6e4e1e0ca5fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b433df8003bb39fc5757e59cdbb5ec98d4cb72d80df67865396a6e4e1e0ca5fe->enter($__internal_b433df8003bb39fc5757e59cdbb5ec98d4cb72d80df67865396a6e4e1e0ca5fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_054368eb707538050ad29f886a21c737bf194352b214b9dca480ade9152ab9c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_054368eb707538050ad29f886a21c737bf194352b214b9dca480ade9152ab9c0->enter($__internal_054368eb707538050ad29f886a21c737bf194352b214b9dca480ade9152ab9c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_b433df8003bb39fc5757e59cdbb5ec98d4cb72d80df67865396a6e4e1e0ca5fe->leave($__internal_b433df8003bb39fc5757e59cdbb5ec98d4cb72d80df67865396a6e4e1e0ca5fe_prof);

        
        $__internal_054368eb707538050ad29f886a21c737bf194352b214b9dca480ade9152ab9c0->leave($__internal_054368eb707538050ad29f886a21c737bf194352b214b9dca480ade9152ab9c0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
