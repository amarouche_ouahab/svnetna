<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_cabc05a51f4adde61756c081b45d2ebe99cb6996522d64cbd1c1f51947768601 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c30ace0e0a829ba900ee563cd0fdfa5916cca131806e11fb5b3266985c22b35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c30ace0e0a829ba900ee563cd0fdfa5916cca131806e11fb5b3266985c22b35->enter($__internal_0c30ace0e0a829ba900ee563cd0fdfa5916cca131806e11fb5b3266985c22b35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_9c444fc3a54379ea755c5dde896b7a2f00270961193c5e761d37266608489240 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c444fc3a54379ea755c5dde896b7a2f00270961193c5e761d37266608489240->enter($__internal_9c444fc3a54379ea755c5dde896b7a2f00270961193c5e761d37266608489240_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_0c30ace0e0a829ba900ee563cd0fdfa5916cca131806e11fb5b3266985c22b35->leave($__internal_0c30ace0e0a829ba900ee563cd0fdfa5916cca131806e11fb5b3266985c22b35_prof);

        
        $__internal_9c444fc3a54379ea755c5dde896b7a2f00270961193c5e761d37266608489240->leave($__internal_9c444fc3a54379ea755c5dde896b7a2f00270961193c5e761d37266608489240_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
