<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_dd7144d71c9113c0d7502bc393816e5493eae9d9489021428907dc7189fa399a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d76e59062d119ea90ac2361cbcc800175c0793cf3cac7d630b365eef16ffb3dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d76e59062d119ea90ac2361cbcc800175c0793cf3cac7d630b365eef16ffb3dc->enter($__internal_d76e59062d119ea90ac2361cbcc800175c0793cf3cac7d630b365eef16ffb3dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_54f6ba5445722ef72aa243787f617c8e26998674b9f92311f4d8a8c391c2f8b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_54f6ba5445722ef72aa243787f617c8e26998674b9f92311f4d8a8c391c2f8b4->enter($__internal_54f6ba5445722ef72aa243787f617c8e26998674b9f92311f4d8a8c391c2f8b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form); ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form); ?>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_d76e59062d119ea90ac2361cbcc800175c0793cf3cac7d630b365eef16ffb3dc->leave($__internal_d76e59062d119ea90ac2361cbcc800175c0793cf3cac7d630b365eef16ffb3dc_prof);

        
        $__internal_54f6ba5445722ef72aa243787f617c8e26998674b9f92311f4d8a8c391c2f8b4->leave($__internal_54f6ba5445722ef72aa243787f617c8e26998674b9f92311f4d8a8c391c2f8b4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form); ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form); ?>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}
