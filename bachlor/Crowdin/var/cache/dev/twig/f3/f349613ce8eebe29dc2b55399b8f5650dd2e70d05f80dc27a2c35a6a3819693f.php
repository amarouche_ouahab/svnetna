<?php

/* WebProfilerBundle:Collector:exception.css.twig */
class __TwigTemplate_f488227bc8c4ec1f865cb7df715ad99eb36d5675f45bdc9473769783ba3fbfe4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d9543227fcda19f31b8984d2d0dfb4c5e4608d7635d58c6e06341876544b580d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d9543227fcda19f31b8984d2d0dfb4c5e4608d7635d58c6e06341876544b580d->enter($__internal_d9543227fcda19f31b8984d2d0dfb4c5e4608d7635d58c6e06341876544b580d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        $__internal_a7c4386a760e2b0d48461b389da36cf4c8a8da72ee969fdb4c862dfa1cc495d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7c4386a760e2b0d48461b389da36cf4c8a8da72ee969fdb4c862dfa1cc495d1->enter($__internal_a7c4386a760e2b0d48461b389da36cf4c8a8da72ee969fdb4c862dfa1cc495d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
";
        
        $__internal_d9543227fcda19f31b8984d2d0dfb4c5e4608d7635d58c6e06341876544b580d->leave($__internal_d9543227fcda19f31b8984d2d0dfb4c5e4608d7635d58c6e06341876544b580d_prof);

        
        $__internal_a7c4386a760e2b0d48461b389da36cf4c8a8da72ee969fdb4c862dfa1cc495d1->leave($__internal_a7c4386a760e2b0d48461b389da36cf4c8a8da72ee969fdb4c862dfa1cc495d1_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/exception.css.twig') }}

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
", "WebProfilerBundle:Collector:exception.css.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.css.twig");
    }
}
