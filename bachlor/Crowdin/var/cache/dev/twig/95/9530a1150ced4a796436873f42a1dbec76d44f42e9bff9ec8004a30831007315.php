<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_454420bf5d207588adcc7d50ccb775ad1f0570ccd67d68e083c1ad0df1fb7c29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2fb5c8f3b774da53762af9fa6e265548716a450b6dcda0fb841142030aa1c9c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2fb5c8f3b774da53762af9fa6e265548716a450b6dcda0fb841142030aa1c9c4->enter($__internal_2fb5c8f3b774da53762af9fa6e265548716a450b6dcda0fb841142030aa1c9c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        $__internal_7c5522f7ac4a038bd887a709fda127bd78dbb6eecf463ef2159168a6b736add2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c5522f7ac4a038bd887a709fda127bd78dbb6eecf463ef2159168a6b736add2->enter($__internal_7c5522f7ac4a038bd887a709fda127bd78dbb6eecf463ef2159168a6b736add2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_2fb5c8f3b774da53762af9fa6e265548716a450b6dcda0fb841142030aa1c9c4->leave($__internal_2fb5c8f3b774da53762af9fa6e265548716a450b6dcda0fb841142030aa1c9c4_prof);

        
        $__internal_7c5522f7ac4a038bd887a709fda127bd78dbb6eecf463ef2159168a6b736add2->leave($__internal_7c5522f7ac4a038bd887a709fda127bd78dbb6eecf463ef2159168a6b736add2_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.js.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
