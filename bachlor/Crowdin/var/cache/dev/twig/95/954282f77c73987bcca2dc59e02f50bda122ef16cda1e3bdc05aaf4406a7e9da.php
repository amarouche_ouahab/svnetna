<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_1ca2dc6a762554e1e5fd898ae85a2a8cdb04205a1e2f28b431259b305771ecd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d0f13ee5eb8ecec3333da3596370785ffcacc6662d774975497a2f1e7696afe3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0f13ee5eb8ecec3333da3596370785ffcacc6662d774975497a2f1e7696afe3->enter($__internal_d0f13ee5eb8ecec3333da3596370785ffcacc6662d774975497a2f1e7696afe3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_19f0f5dacb2ab71a7a41e480eecf99c91ddc97204a9bcf84ae7f0900a09acda8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19f0f5dacb2ab71a7a41e480eecf99c91ddc97204a9bcf84ae7f0900a09acda8->enter($__internal_19f0f5dacb2ab71a7a41e480eecf99c91ddc97204a9bcf84ae7f0900a09acda8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d0f13ee5eb8ecec3333da3596370785ffcacc6662d774975497a2f1e7696afe3->leave($__internal_d0f13ee5eb8ecec3333da3596370785ffcacc6662d774975497a2f1e7696afe3_prof);

        
        $__internal_19f0f5dacb2ab71a7a41e480eecf99c91ddc97204a9bcf84ae7f0900a09acda8->leave($__internal_19f0f5dacb2ab71a7a41e480eecf99c91ddc97204a9bcf84ae7f0900a09acda8_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4d4250dc7678930adc0a6446708e1553e3cdc213d8750b19d5baeb74592597f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d4250dc7678930adc0a6446708e1553e3cdc213d8750b19d5baeb74592597f8->enter($__internal_4d4250dc7678930adc0a6446708e1553e3cdc213d8750b19d5baeb74592597f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_a5cb61b199cc8b173c6db6c8e44b2a67a5671ec566df8e88f3293a198b233a03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5cb61b199cc8b173c6db6c8e44b2a67a5671ec566df8e88f3293a198b233a03->enter($__internal_a5cb61b199cc8b173c6db6c8e44b2a67a5671ec566df8e88f3293a198b233a03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_a5cb61b199cc8b173c6db6c8e44b2a67a5671ec566df8e88f3293a198b233a03->leave($__internal_a5cb61b199cc8b173c6db6c8e44b2a67a5671ec566df8e88f3293a198b233a03_prof);

        
        $__internal_4d4250dc7678930adc0a6446708e1553e3cdc213d8750b19d5baeb74592597f8->leave($__internal_4d4250dc7678930adc0a6446708e1553e3cdc213d8750b19d5baeb74592597f8_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
