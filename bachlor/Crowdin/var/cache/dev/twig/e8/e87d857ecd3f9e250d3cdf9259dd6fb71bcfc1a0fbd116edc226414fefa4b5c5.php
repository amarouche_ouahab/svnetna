<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_d5a804dd6f036b028687fa19e1bd183de75002a69cafb5189cc526188f38b9cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9fa4979667cac9081c02c9f2c084ddf216a2af1185b5ab3eb5c0311be64f1cb4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9fa4979667cac9081c02c9f2c084ddf216a2af1185b5ab3eb5c0311be64f1cb4->enter($__internal_9fa4979667cac9081c02c9f2c084ddf216a2af1185b5ab3eb5c0311be64f1cb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_4212f23982fad99272a8b6f0aed7e4f629145f5296061811ac43540fd5d2f556 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4212f23982fad99272a8b6f0aed7e4f629145f5296061811ac43540fd5d2f556->enter($__internal_4212f23982fad99272a8b6f0aed7e4f629145f5296061811ac43540fd5d2f556_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9fa4979667cac9081c02c9f2c084ddf216a2af1185b5ab3eb5c0311be64f1cb4->leave($__internal_9fa4979667cac9081c02c9f2c084ddf216a2af1185b5ab3eb5c0311be64f1cb4_prof);

        
        $__internal_4212f23982fad99272a8b6f0aed7e4f629145f5296061811ac43540fd5d2f556->leave($__internal_4212f23982fad99272a8b6f0aed7e4f629145f5296061811ac43540fd5d2f556_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_935634e47464b399ebffca210ed9980b22aec4694e03ec8472a56b7e7a5b362c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_935634e47464b399ebffca210ed9980b22aec4694e03ec8472a56b7e7a5b362c->enter($__internal_935634e47464b399ebffca210ed9980b22aec4694e03ec8472a56b7e7a5b362c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_d4e7bcd1837c89085e8598091d6f61e174f7cf12dcdb1fded03161afff8d86ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4e7bcd1837c89085e8598091d6f61e174f7cf12dcdb1fded03161afff8d86ab->enter($__internal_d4e7bcd1837c89085e8598091d6f61e174f7cf12dcdb1fded03161afff8d86ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_d4e7bcd1837c89085e8598091d6f61e174f7cf12dcdb1fded03161afff8d86ab->leave($__internal_d4e7bcd1837c89085e8598091d6f61e174f7cf12dcdb1fded03161afff8d86ab_prof);

        
        $__internal_935634e47464b399ebffca210ed9980b22aec4694e03ec8472a56b7e7a5b362c->leave($__internal_935634e47464b399ebffca210ed9980b22aec4694e03ec8472a56b7e7a5b362c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
