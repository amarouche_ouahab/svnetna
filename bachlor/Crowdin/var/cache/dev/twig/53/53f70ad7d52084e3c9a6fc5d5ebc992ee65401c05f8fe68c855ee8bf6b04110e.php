<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_c1f0757f2c8086917b9c1371f1cf88af9658397bab73f2365f12d1a5293833ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b626c3e74df581fa4c377d726b68b23b19fa161c364bc6a22a1f4035029fc99e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b626c3e74df581fa4c377d726b68b23b19fa161c364bc6a22a1f4035029fc99e->enter($__internal_b626c3e74df581fa4c377d726b68b23b19fa161c364bc6a22a1f4035029fc99e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        $__internal_d0ec2b363e5d9687f6e24a4c5fc1cdc287580038d505a9f5c72fb5a3a4515e4d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d0ec2b363e5d9687f6e24a4c5fc1cdc287580038d505a9f5c72fb5a3a4515e4d->enter($__internal_d0ec2b363e5d9687f6e24a4c5fc1cdc287580038d505a9f5c72fb5a3a4515e4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_b626c3e74df581fa4c377d726b68b23b19fa161c364bc6a22a1f4035029fc99e->leave($__internal_b626c3e74df581fa4c377d726b68b23b19fa161c364bc6a22a1f4035029fc99e_prof);

        
        $__internal_d0ec2b363e5d9687f6e24a4c5fc1cdc287580038d505a9f5c72fb5a3a4515e4d->leave($__internal_d0ec2b363e5d9687f6e24a4c5fc1cdc287580038d505a9f5c72fb5a3a4515e4d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/form_widget_simple.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_simple.html.php");
    }
}
