<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_2f8f2499aa3c855571913f9d8c0d2a7ae1ebd5ad7e2fc89af69b2e5e95fb02b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9bc0205c0df5b3256287e48173514bd94603f2e1e16752b5210ba3a178b0d64d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9bc0205c0df5b3256287e48173514bd94603f2e1e16752b5210ba3a178b0d64d->enter($__internal_9bc0205c0df5b3256287e48173514bd94603f2e1e16752b5210ba3a178b0d64d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_c353099e856ea1caa18c52ad4ec5a3269c29bbead4461f8f77c3f8216fcc0fd6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c353099e856ea1caa18c52ad4ec5a3269c29bbead4461f8f77c3f8216fcc0fd6->enter($__internal_c353099e856ea1caa18c52ad4ec5a3269c29bbead4461f8f77c3f8216fcc0fd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_9bc0205c0df5b3256287e48173514bd94603f2e1e16752b5210ba3a178b0d64d->leave($__internal_9bc0205c0df5b3256287e48173514bd94603f2e1e16752b5210ba3a178b0d64d_prof);

        
        $__internal_c353099e856ea1caa18c52ad4ec5a3269c29bbead4461f8f77c3f8216fcc0fd6->leave($__internal_c353099e856ea1caa18c52ad4ec5a3269c29bbead4461f8f77c3f8216fcc0fd6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_attributes.html.php");
    }
}
