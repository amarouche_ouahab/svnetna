<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_7d02df8ba603d40a4bfac8c3df20f283bfe699e540976f56733a273b4ea2db25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8395c2ca28b9a827c67fd574b277404bf0f88a108d88511f661b3d0f8891fcb0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8395c2ca28b9a827c67fd574b277404bf0f88a108d88511f661b3d0f8891fcb0->enter($__internal_8395c2ca28b9a827c67fd574b277404bf0f88a108d88511f661b3d0f8891fcb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        $__internal_79bbe2249da3a959e84def5bd2f6eddb3daea77abb4f335b55251051b1a2236a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79bbe2249da3a959e84def5bd2f6eddb3daea77abb4f335b55251051b1a2236a->enter($__internal_79bbe2249da3a959e84def5bd2f6eddb3daea77abb4f335b55251051b1a2236a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["group"] ?? $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_8395c2ca28b9a827c67fd574b277404bf0f88a108d88511f661b3d0f8891fcb0->leave($__internal_8395c2ca28b9a827c67fd574b277404bf0f88a108d88511f661b3d0f8891fcb0_prof);

        
        $__internal_79bbe2249da3a959e84def5bd2f6eddb3daea77abb4f335b55251051b1a2236a->leave($__internal_79bbe2249da3a959e84def5bd2f6eddb3daea77abb4f335b55251051b1a2236a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "FOSUserBundle:Group:show_content.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show_content.html.twig");
    }
}
