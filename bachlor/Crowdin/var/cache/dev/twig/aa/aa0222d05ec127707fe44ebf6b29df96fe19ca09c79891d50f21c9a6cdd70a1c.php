<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_eb40c19189fe9e230c63a802f04636cee721b5791ec34dc1161bc42e53de8d73 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_454e7bfc9c9fec1a754f5e3b14ee82cfeb9a35dd29bef14dacdb2594c797c333 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_454e7bfc9c9fec1a754f5e3b14ee82cfeb9a35dd29bef14dacdb2594c797c333->enter($__internal_454e7bfc9c9fec1a754f5e3b14ee82cfeb9a35dd29bef14dacdb2594c797c333_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_30734ec6a0ea64a6c3e1b5ce668f1cae1cd5dcf418a629553fe64d83c43710a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30734ec6a0ea64a6c3e1b5ce668f1cae1cd5dcf418a629553fe64d83c43710a1->enter($__internal_30734ec6a0ea64a6c3e1b5ce668f1cae1cd5dcf418a629553fe64d83c43710a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_454e7bfc9c9fec1a754f5e3b14ee82cfeb9a35dd29bef14dacdb2594c797c333->leave($__internal_454e7bfc9c9fec1a754f5e3b14ee82cfeb9a35dd29bef14dacdb2594c797c333_prof);

        
        $__internal_30734ec6a0ea64a6c3e1b5ce668f1cae1cd5dcf418a629553fe64d83c43710a1->leave($__internal_30734ec6a0ea64a6c3e1b5ce668f1cae1cd5dcf418a629553fe64d83c43710a1_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_34e4fae805e328cd11b3793023568285563e42ddd0790ee8e0b5b50fdb34e6dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34e4fae805e328cd11b3793023568285563e42ddd0790ee8e0b5b50fdb34e6dc->enter($__internal_34e4fae805e328cd11b3793023568285563e42ddd0790ee8e0b5b50fdb34e6dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_0414e418e8ff4f9e439b4efd1986bd6df63b84794483eee2f428ac0dba2178d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0414e418e8ff4f9e439b4efd1986bd6df63b84794483eee2f428ac0dba2178d7->enter($__internal_0414e418e8ff4f9e439b4efd1986bd6df63b84794483eee2f428ac0dba2178d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_0414e418e8ff4f9e439b4efd1986bd6df63b84794483eee2f428ac0dba2178d7->leave($__internal_0414e418e8ff4f9e439b4efd1986bd6df63b84794483eee2f428ac0dba2178d7_prof);

        
        $__internal_34e4fae805e328cd11b3793023568285563e42ddd0790ee8e0b5b50fdb34e6dc->leave($__internal_34e4fae805e328cd11b3793023568285563e42ddd0790ee8e0b5b50fdb34e6dc_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
