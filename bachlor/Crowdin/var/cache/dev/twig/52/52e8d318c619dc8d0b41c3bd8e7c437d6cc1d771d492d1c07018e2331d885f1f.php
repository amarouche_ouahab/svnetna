<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_62b5f07a84b2f46ebe4d4f975dc098357105d6285dbbc1f605b3b8c40d59836e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_379179969e3fc8e3add8ba618fab81eec426d4c5313167293ab8d63992d572b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_379179969e3fc8e3add8ba618fab81eec426d4c5313167293ab8d63992d572b2->enter($__internal_379179969e3fc8e3add8ba618fab81eec426d4c5313167293ab8d63992d572b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_f95d4fde44cec274defaa432742516e88cb484a7d4095cb28552e0b67fbc697a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f95d4fde44cec274defaa432742516e88cb484a7d4095cb28552e0b67fbc697a->enter($__internal_f95d4fde44cec274defaa432742516e88cb484a7d4095cb28552e0b67fbc697a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_379179969e3fc8e3add8ba618fab81eec426d4c5313167293ab8d63992d572b2->leave($__internal_379179969e3fc8e3add8ba618fab81eec426d4c5313167293ab8d63992d572b2_prof);

        
        $__internal_f95d4fde44cec274defaa432742516e88cb484a7d4095cb28552e0b67fbc697a->leave($__internal_f95d4fde44cec274defaa432742516e88cb484a7d4095cb28552e0b67fbc697a_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_1efc88c18dd545594a44dd5cf48b073d10674579f1059b3e75982742f27c12cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1efc88c18dd545594a44dd5cf48b073d10674579f1059b3e75982742f27c12cd->enter($__internal_1efc88c18dd545594a44dd5cf48b073d10674579f1059b3e75982742f27c12cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_7b324c9b822dea24df185f0aafefa2a154f56adf74491fcaacad98cf0afdb27f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b324c9b822dea24df185f0aafefa2a154f56adf74491fcaacad98cf0afdb27f->enter($__internal_7b324c9b822dea24df185f0aafefa2a154f56adf74491fcaacad98cf0afdb27f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_7b324c9b822dea24df185f0aafefa2a154f56adf74491fcaacad98cf0afdb27f->leave($__internal_7b324c9b822dea24df185f0aafefa2a154f56adf74491fcaacad98cf0afdb27f_prof);

        
        $__internal_1efc88c18dd545594a44dd5cf48b073d10674579f1059b3e75982742f27c12cd->leave($__internal_1efc88c18dd545594a44dd5cf48b073d10674579f1059b3e75982742f27c12cd_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_cc6ceb7cafca725c922100192921cb10fb7b330a3ff1b820cfa71dd9c4b375f3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc6ceb7cafca725c922100192921cb10fb7b330a3ff1b820cfa71dd9c4b375f3->enter($__internal_cc6ceb7cafca725c922100192921cb10fb7b330a3ff1b820cfa71dd9c4b375f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_5a4ab56b121f038e43f55e5e139e76dd08324831e897219aca178906046c3f85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a4ab56b121f038e43f55e5e139e76dd08324831e897219aca178906046c3f85->enter($__internal_5a4ab56b121f038e43f55e5e139e76dd08324831e897219aca178906046c3f85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_5a4ab56b121f038e43f55e5e139e76dd08324831e897219aca178906046c3f85->leave($__internal_5a4ab56b121f038e43f55e5e139e76dd08324831e897219aca178906046c3f85_prof);

        
        $__internal_cc6ceb7cafca725c922100192921cb10fb7b330a3ff1b820cfa71dd9c4b375f3->leave($__internal_cc6ceb7cafca725c922100192921cb10fb7b330a3ff1b820cfa71dd9c4b375f3_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
