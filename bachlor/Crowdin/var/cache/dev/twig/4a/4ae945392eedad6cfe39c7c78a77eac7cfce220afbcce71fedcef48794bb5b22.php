<?php

/* @Framework/Form/form_start.html.php */
class __TwigTemplate_54e9a25f8558b93bea32314b42f28062214c9fc39496802df9aee83aa8d86a3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c1c9c2d8f880dd6bb26f279fa2f12ddd8fc85b58da0d01f7ee1b2918a0e240f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c1c9c2d8f880dd6bb26f279fa2f12ddd8fc85b58da0d01f7ee1b2918a0e240f->enter($__internal_2c1c9c2d8f880dd6bb26f279fa2f12ddd8fc85b58da0d01f7ee1b2918a0e240f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_start.html.php"));

        $__internal_7ba0f30eb93649272a3344dca1bcf19e581e590b7e4bcbcf8392d6a4516dffc6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ba0f30eb93649272a3344dca1bcf19e581e590b7e4bcbcf8392d6a4516dffc6->enter($__internal_7ba0f30eb93649272a3344dca1bcf19e581e590b7e4bcbcf8392d6a4516dffc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_start.html.php"));

        // line 1
        echo "<?php \$method = strtoupper(\$method) ?>
<?php \$form_method = \$method === 'GET' || \$method === 'POST' ? \$method : 'POST' ?>
<form name=\"<?php echo \$name ?>\" method=\"<?php echo strtolower(\$form_method) ?>\"<?php if (\$action !== ''): ?> action=\"<?php echo \$action ?>\"<?php endif ?><?php foreach (\$attr as \$k => \$v) { printf(' %s=\"%s\"', \$view->escape(\$k), \$view->escape(\$v)); } ?><?php if (\$multipart): ?> enctype=\"multipart/form-data\"<?php endif ?>>
<?php if (\$form_method !== \$method): ?>
    <input type=\"hidden\" name=\"_method\" value=\"<?php echo \$method ?>\" />
<?php endif ?>
";
        
        $__internal_2c1c9c2d8f880dd6bb26f279fa2f12ddd8fc85b58da0d01f7ee1b2918a0e240f->leave($__internal_2c1c9c2d8f880dd6bb26f279fa2f12ddd8fc85b58da0d01f7ee1b2918a0e240f_prof);

        
        $__internal_7ba0f30eb93649272a3344dca1bcf19e581e590b7e4bcbcf8392d6a4516dffc6->leave($__internal_7ba0f30eb93649272a3344dca1bcf19e581e590b7e4bcbcf8392d6a4516dffc6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_start.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php \$method = strtoupper(\$method) ?>
<?php \$form_method = \$method === 'GET' || \$method === 'POST' ? \$method : 'POST' ?>
<form name=\"<?php echo \$name ?>\" method=\"<?php echo strtolower(\$form_method) ?>\"<?php if (\$action !== ''): ?> action=\"<?php echo \$action ?>\"<?php endif ?><?php foreach (\$attr as \$k => \$v) { printf(' %s=\"%s\"', \$view->escape(\$k), \$view->escape(\$v)); } ?><?php if (\$multipart): ?> enctype=\"multipart/form-data\"<?php endif ?>>
<?php if (\$form_method !== \$method): ?>
    <input type=\"hidden\" name=\"_method\" value=\"<?php echo \$method ?>\" />
<?php endif ?>
", "@Framework/Form/form_start.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_start.html.php");
    }
}
