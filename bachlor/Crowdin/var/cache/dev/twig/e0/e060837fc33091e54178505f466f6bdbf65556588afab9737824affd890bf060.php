<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_d2707ee3304d9145bc05c33462dc650cf0433754c685845192ec1b1620fce78f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_30e9eb80d97deb8e8925ceefdba5b6bea7e525ac454608d6e9777114d643b6db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30e9eb80d97deb8e8925ceefdba5b6bea7e525ac454608d6e9777114d643b6db->enter($__internal_30e9eb80d97deb8e8925ceefdba5b6bea7e525ac454608d6e9777114d643b6db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_b9fe77d0af418ecf28703442b7d82f6eaef95ab10dcb1c4f32ddab6cace39887 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9fe77d0af418ecf28703442b7d82f6eaef95ab10dcb1c4f32ddab6cace39887->enter($__internal_b9fe77d0af418ecf28703442b7d82f6eaef95ab10dcb1c4f32ddab6cace39887_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_30e9eb80d97deb8e8925ceefdba5b6bea7e525ac454608d6e9777114d643b6db->leave($__internal_30e9eb80d97deb8e8925ceefdba5b6bea7e525ac454608d6e9777114d643b6db_prof);

        
        $__internal_b9fe77d0af418ecf28703442b7d82f6eaef95ab10dcb1c4f32ddab6cace39887->leave($__internal_b9fe77d0af418ecf28703442b7d82f6eaef95ab10dcb1c4f32ddab6cace39887_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}
