<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_304397361907c1b885fe0207c9a1e59995f6f188804aa2ad2b2a5942f2d85a23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_44f9feb8e78e8d9d257021786f4b1405b9a760ccbcbb46ece4dd89c010c34c8f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44f9feb8e78e8d9d257021786f4b1405b9a760ccbcbb46ece4dd89c010c34c8f->enter($__internal_44f9feb8e78e8d9d257021786f4b1405b9a760ccbcbb46ece4dd89c010c34c8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_2b0164ad92f78da93281eeae9ca8871ae258be28eee34ae28583c6950961c0b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2b0164ad92f78da93281eeae9ca8871ae258be28eee34ae28583c6950961c0b8->enter($__internal_2b0164ad92f78da93281eeae9ca8871ae258be28eee34ae28583c6950961c0b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_44f9feb8e78e8d9d257021786f4b1405b9a760ccbcbb46ece4dd89c010c34c8f->leave($__internal_44f9feb8e78e8d9d257021786f4b1405b9a760ccbcbb46ece4dd89c010c34c8f_prof);

        
        $__internal_2b0164ad92f78da93281eeae9ca8871ae258be28eee34ae28583c6950961c0b8->leave($__internal_2b0164ad92f78da93281eeae9ca8871ae258be28eee34ae28583c6950961c0b8_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_4177f647618d42d4910e7dd01a448ff0e30cb606584abc14b6ac7e90e4d848a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4177f647618d42d4910e7dd01a448ff0e30cb606584abc14b6ac7e90e4d848a7->enter($__internal_4177f647618d42d4910e7dd01a448ff0e30cb606584abc14b6ac7e90e4d848a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_37794a4347b98ab166b924d0e359d09b4e6c4314a2ec1d648b02b14c7467f758 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37794a4347b98ab166b924d0e359d09b4e6c4314a2ec1d648b02b14c7467f758->enter($__internal_37794a4347b98ab166b924d0e359d09b4e6c4314a2ec1d648b02b14c7467f758_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_37794a4347b98ab166b924d0e359d09b4e6c4314a2ec1d648b02b14c7467f758->leave($__internal_37794a4347b98ab166b924d0e359d09b4e6c4314a2ec1d648b02b14c7467f758_prof);

        
        $__internal_4177f647618d42d4910e7dd01a448ff0e30cb606584abc14b6ac7e90e4d848a7->leave($__internal_4177f647618d42d4910e7dd01a448ff0e30cb606584abc14b6ac7e90e4d848a7_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_eaf3bd0b9ac7a9ff78f118e01e333b05decac96fc4c4d87c556867448984ef92 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eaf3bd0b9ac7a9ff78f118e01e333b05decac96fc4c4d87c556867448984ef92->enter($__internal_eaf3bd0b9ac7a9ff78f118e01e333b05decac96fc4c4d87c556867448984ef92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_b239227437a157335c8b5882e7d0caee64bcd352f0c93f2f6ff6df8327037264 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b239227437a157335c8b5882e7d0caee64bcd352f0c93f2f6ff6df8327037264->enter($__internal_b239227437a157335c8b5882e7d0caee64bcd352f0c93f2f6ff6df8327037264_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_b239227437a157335c8b5882e7d0caee64bcd352f0c93f2f6ff6df8327037264->leave($__internal_b239227437a157335c8b5882e7d0caee64bcd352f0c93f2f6ff6df8327037264_prof);

        
        $__internal_eaf3bd0b9ac7a9ff78f118e01e333b05decac96fc4c4d87c556867448984ef92->leave($__internal_eaf3bd0b9ac7a9ff78f118e01e333b05decac96fc4c4d87c556867448984ef92_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
