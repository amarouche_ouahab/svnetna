<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_9fcd256a4222bb8607b7fc5823e87c1d0f3406873fcc67be8725196c0049aa84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dfcd2c1ae6d4ff0a6f37fa20729c9d3fd6df68073d6911df574e2d1b8d522762 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dfcd2c1ae6d4ff0a6f37fa20729c9d3fd6df68073d6911df574e2d1b8d522762->enter($__internal_dfcd2c1ae6d4ff0a6f37fa20729c9d3fd6df68073d6911df574e2d1b8d522762_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_51052c6b39bb35916b16e7a4e843797408fa29df1c027e3472f6a4b0d2705f97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51052c6b39bb35916b16e7a4e843797408fa29df1c027e3472f6a4b0d2705f97->enter($__internal_51052c6b39bb35916b16e7a4e843797408fa29df1c027e3472f6a4b0d2705f97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_dfcd2c1ae6d4ff0a6f37fa20729c9d3fd6df68073d6911df574e2d1b8d522762->leave($__internal_dfcd2c1ae6d4ff0a6f37fa20729c9d3fd6df68073d6911df574e2d1b8d522762_prof);

        
        $__internal_51052c6b39bb35916b16e7a4e843797408fa29df1c027e3472f6a4b0d2705f97->leave($__internal_51052c6b39bb35916b16e7a4e843797408fa29df1c027e3472f6a4b0d2705f97_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
