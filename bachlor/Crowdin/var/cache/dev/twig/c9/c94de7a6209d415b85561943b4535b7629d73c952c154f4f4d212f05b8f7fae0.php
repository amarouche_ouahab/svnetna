<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_9433201755788dd11962cf779cbda83058b6dbda5344320ea26f256aa85ca550 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f4c1543e0b3b1989cde7773813101195abec5fae56bce84d63deaf5b339d0312 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f4c1543e0b3b1989cde7773813101195abec5fae56bce84d63deaf5b339d0312->enter($__internal_f4c1543e0b3b1989cde7773813101195abec5fae56bce84d63deaf5b339d0312_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_e4c5773f0a001e8d5cf09a26a9c9b1f991c5359b7c4f43248ffa0e3921561b50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e4c5773f0a001e8d5cf09a26a9c9b1f991c5359b7c4f43248ffa0e3921561b50->enter($__internal_e4c5773f0a001e8d5cf09a26a9c9b1f991c5359b7c4f43248ffa0e3921561b50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_f4c1543e0b3b1989cde7773813101195abec5fae56bce84d63deaf5b339d0312->leave($__internal_f4c1543e0b3b1989cde7773813101195abec5fae56bce84d63deaf5b339d0312_prof);

        
        $__internal_e4c5773f0a001e8d5cf09a26a9c9b1f991c5359b7c4f43248ffa0e3921561b50->leave($__internal_e4c5773f0a001e8d5cf09a26a9c9b1f991c5359b7c4f43248ffa0e3921561b50_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
