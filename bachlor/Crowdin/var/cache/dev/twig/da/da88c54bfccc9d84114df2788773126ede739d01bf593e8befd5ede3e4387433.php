<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_bcacbd06ab1a4d61c3f944a0aa04ff4574e47e46a50102e1229b69e4d6ff4177 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_148e30540d63d94f6c239225c70e1005811aa7e46e7fedf7840dcc1f2f994f7e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_148e30540d63d94f6c239225c70e1005811aa7e46e7fedf7840dcc1f2f994f7e->enter($__internal_148e30540d63d94f6c239225c70e1005811aa7e46e7fedf7840dcc1f2f994f7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_068989277592b0c8fa7c8725a6c3310d2ac721a97b93e65560e534afa70be276 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_068989277592b0c8fa7c8725a6c3310d2ac721a97b93e65560e534afa70be276->enter($__internal_068989277592b0c8fa7c8725a6c3310d2ac721a97b93e65560e534afa70be276_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_148e30540d63d94f6c239225c70e1005811aa7e46e7fedf7840dcc1f2f994f7e->leave($__internal_148e30540d63d94f6c239225c70e1005811aa7e46e7fedf7840dcc1f2f994f7e_prof);

        
        $__internal_068989277592b0c8fa7c8725a6c3310d2ac721a97b93e65560e534afa70be276->leave($__internal_068989277592b0c8fa7c8725a6c3310d2ac721a97b93e65560e534afa70be276_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
