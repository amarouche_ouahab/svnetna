<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_19272a72dea1c8b5f6ad9f327fe9668de57f2d94a987e7c7888132206b0df5fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bdad9a82050be61e2e73a4938ac1908cac11845cc3978ac505ea65ee6fc0719c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bdad9a82050be61e2e73a4938ac1908cac11845cc3978ac505ea65ee6fc0719c->enter($__internal_bdad9a82050be61e2e73a4938ac1908cac11845cc3978ac505ea65ee6fc0719c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_2e3a97f666e68f427b83a74cae324ae42558dd111d0218c1b8bb02714da0d316 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e3a97f666e68f427b83a74cae324ae42558dd111d0218c1b8bb02714da0d316->enter($__internal_2e3a97f666e68f427b83a74cae324ae42558dd111d0218c1b8bb02714da0d316_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_bdad9a82050be61e2e73a4938ac1908cac11845cc3978ac505ea65ee6fc0719c->leave($__internal_bdad9a82050be61e2e73a4938ac1908cac11845cc3978ac505ea65ee6fc0719c_prof);

        
        $__internal_2e3a97f666e68f427b83a74cae324ae42558dd111d0218c1b8bb02714da0d316->leave($__internal_2e3a97f666e68f427b83a74cae324ae42558dd111d0218c1b8bb02714da0d316_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
