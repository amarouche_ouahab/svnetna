<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_638ca8c133e197214d1315e9fe73a87e9b2174eb7d93cb270ede3b8fa0fdffb4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_316116241f95c013b2ba129902bc7acc818c8e5848ce79d972a2ef23b242b8b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_316116241f95c013b2ba129902bc7acc818c8e5848ce79d972a2ef23b242b8b1->enter($__internal_316116241f95c013b2ba129902bc7acc818c8e5848ce79d972a2ef23b242b8b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_e7471e014b8e7155fbfd6c428d03be4b661c486f9d14bf078b32f7e182eeff5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7471e014b8e7155fbfd6c428d03be4b661c486f9d14bf078b32f7e182eeff5c->enter($__internal_e7471e014b8e7155fbfd6c428d03be4b661c486f9d14bf078b32f7e182eeff5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_316116241f95c013b2ba129902bc7acc818c8e5848ce79d972a2ef23b242b8b1->leave($__internal_316116241f95c013b2ba129902bc7acc818c8e5848ce79d972a2ef23b242b8b1_prof);

        
        $__internal_e7471e014b8e7155fbfd6c428d03be4b661c486f9d14bf078b32f7e182eeff5c->leave($__internal_e7471e014b8e7155fbfd6c428d03be4b661c486f9d14bf078b32f7e182eeff5c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
