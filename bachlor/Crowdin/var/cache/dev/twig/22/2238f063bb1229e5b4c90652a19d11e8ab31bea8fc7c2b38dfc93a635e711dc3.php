<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_f6f0452f9db8aeb3b08a49d84b8e668abf58d9f5205b38d8faf888dffe174f74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_be9f2cac4fc4bf8c87c2eba5fee9953904bd52e36acc17e616fd49c9143e879a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_be9f2cac4fc4bf8c87c2eba5fee9953904bd52e36acc17e616fd49c9143e879a->enter($__internal_be9f2cac4fc4bf8c87c2eba5fee9953904bd52e36acc17e616fd49c9143e879a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        $__internal_788b1e79b83cec2fe7b5ed7c400acbf4abe8b26fa4b0188c819c3b29ab06af02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_788b1e79b83cec2fe7b5ed7c400acbf4abe8b26fa4b0188c819c3b29ab06af02->enter($__internal_788b1e79b83cec2fe7b5ed7c400acbf4abe8b26fa4b0188c819c3b29ab06af02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_be9f2cac4fc4bf8c87c2eba5fee9953904bd52e36acc17e616fd49c9143e879a->leave($__internal_be9f2cac4fc4bf8c87c2eba5fee9953904bd52e36acc17e616fd49c9143e879a_prof);

        
        $__internal_788b1e79b83cec2fe7b5ed7c400acbf4abe8b26fa4b0188c819c3b29ab06af02->leave($__internal_788b1e79b83cec2fe7b5ed7c400acbf4abe8b26fa4b0188c819c3b29ab06af02_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_container_attributes.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_container_attributes.html.php");
    }
}
