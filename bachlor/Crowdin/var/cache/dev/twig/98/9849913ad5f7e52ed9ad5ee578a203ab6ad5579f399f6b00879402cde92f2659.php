<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_67210b46eb6ea29047a65b9b0be637713427b54db75c36dff6e9e82ed4165885 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a726ec9deb38623c64bc11b8c0a0e3c7339983d268aca9624b0d3a5f2b379aaa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a726ec9deb38623c64bc11b8c0a0e3c7339983d268aca9624b0d3a5f2b379aaa->enter($__internal_a726ec9deb38623c64bc11b8c0a0e3c7339983d268aca9624b0d3a5f2b379aaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_44d6c77ab086634ee8f41aae0bf3aa873a79bb33e3008b4c50a468fe4aff620f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_44d6c77ab086634ee8f41aae0bf3aa873a79bb33e3008b4c50a468fe4aff620f->enter($__internal_44d6c77ab086634ee8f41aae0bf3aa873a79bb33e3008b4c50a468fe4aff620f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a726ec9deb38623c64bc11b8c0a0e3c7339983d268aca9624b0d3a5f2b379aaa->leave($__internal_a726ec9deb38623c64bc11b8c0a0e3c7339983d268aca9624b0d3a5f2b379aaa_prof);

        
        $__internal_44d6c77ab086634ee8f41aae0bf3aa873a79bb33e3008b4c50a468fe4aff620f->leave($__internal_44d6c77ab086634ee8f41aae0bf3aa873a79bb33e3008b4c50a468fe4aff620f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_02cba226286cd128fecf8d70dfa717854c6a106f66cfa610194427859e1f63bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_02cba226286cd128fecf8d70dfa717854c6a106f66cfa610194427859e1f63bf->enter($__internal_02cba226286cd128fecf8d70dfa717854c6a106f66cfa610194427859e1f63bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_e0bdbbd68f1ce72256e559244ba5dcf0ad2562cdb8964f490d4f3363e10fc6c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0bdbbd68f1ce72256e559244ba5dcf0ad2562cdb8964f490d4f3363e10fc6c4->enter($__internal_e0bdbbd68f1ce72256e559244ba5dcf0ad2562cdb8964f490d4f3363e10fc6c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_e0bdbbd68f1ce72256e559244ba5dcf0ad2562cdb8964f490d4f3363e10fc6c4->leave($__internal_e0bdbbd68f1ce72256e559244ba5dcf0ad2562cdb8964f490d4f3363e10fc6c4_prof);

        
        $__internal_02cba226286cd128fecf8d70dfa717854c6a106f66cfa610194427859e1f63bf->leave($__internal_02cba226286cd128fecf8d70dfa717854c6a106f66cfa610194427859e1f63bf_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_c1581d8099bcbfa896d223af91ba0742de3285d5763f3ea0aad0d387b038597d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c1581d8099bcbfa896d223af91ba0742de3285d5763f3ea0aad0d387b038597d->enter($__internal_c1581d8099bcbfa896d223af91ba0742de3285d5763f3ea0aad0d387b038597d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_58e744f982190f5ed4d299edd873491520a4fbb60a742334d893cef636a22078 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58e744f982190f5ed4d299edd873491520a4fbb60a742334d893cef636a22078->enter($__internal_58e744f982190f5ed4d299edd873491520a4fbb60a742334d893cef636a22078_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_58e744f982190f5ed4d299edd873491520a4fbb60a742334d893cef636a22078->leave($__internal_58e744f982190f5ed4d299edd873491520a4fbb60a742334d893cef636a22078_prof);

        
        $__internal_c1581d8099bcbfa896d223af91ba0742de3285d5763f3ea0aad0d387b038597d->leave($__internal_c1581d8099bcbfa896d223af91ba0742de3285d5763f3ea0aad0d387b038597d_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
