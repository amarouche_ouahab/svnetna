<?php

/* @Framework/Form/button_attributes.html.php */
class __TwigTemplate_f65c607b023c0488ef36b0ae6d3115ee9129d7754c65f149e52f24b3ce71ea0f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ddb2bc595fc28fbe27caace519689392e2fd8973e704de419e74af93b6bac588 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ddb2bc595fc28fbe27caace519689392e2fd8973e704de419e74af93b6bac588->enter($__internal_ddb2bc595fc28fbe27caace519689392e2fd8973e704de419e74af93b6bac588_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        $__internal_9d53cce57f42849265e638fe8aff92795ab0ef9680a256261c20978d0922103e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d53cce57f42849265e638fe8aff92795ab0ef9680a256261c20978d0922103e->enter($__internal_9d53cce57f42849265e638fe8aff92795ab0ef9680a256261c20978d0922103e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_ddb2bc595fc28fbe27caace519689392e2fd8973e704de419e74af93b6bac588->leave($__internal_ddb2bc595fc28fbe27caace519689392e2fd8973e704de419e74af93b6bac588_prof);

        
        $__internal_9d53cce57f42849265e638fe8aff92795ab0ef9680a256261c20978d0922103e->leave($__internal_9d53cce57f42849265e638fe8aff92795ab0ef9680a256261c20978d0922103e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/button_attributes.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_attributes.html.php");
    }
}
