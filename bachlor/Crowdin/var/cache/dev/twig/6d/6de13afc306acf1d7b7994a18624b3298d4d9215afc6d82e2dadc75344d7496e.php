<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_03781dc186d2f27a78f92039ad19b37308465c9e810db01e2fac92a52253d3ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d8611ac4af44e3957ccb898573818ea75b865af49614471ac781c89a29643f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d8611ac4af44e3957ccb898573818ea75b865af49614471ac781c89a29643f6->enter($__internal_4d8611ac4af44e3957ccb898573818ea75b865af49614471ac781c89a29643f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_56cce2a4ab94df272ec744aa97f45591a9d2c3957c48760ec608b476823c9f19 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56cce2a4ab94df272ec744aa97f45591a9d2c3957c48760ec608b476823c9f19->enter($__internal_56cce2a4ab94df272ec744aa97f45591a9d2c3957c48760ec608b476823c9f19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4d8611ac4af44e3957ccb898573818ea75b865af49614471ac781c89a29643f6->leave($__internal_4d8611ac4af44e3957ccb898573818ea75b865af49614471ac781c89a29643f6_prof);

        
        $__internal_56cce2a4ab94df272ec744aa97f45591a9d2c3957c48760ec608b476823c9f19->leave($__internal_56cce2a4ab94df272ec744aa97f45591a9d2c3957c48760ec608b476823c9f19_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_26a0c68490ce41b9c0a7a19e3b9e211dd47c4209e3846b3f341fdf3323c11e9b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26a0c68490ce41b9c0a7a19e3b9e211dd47c4209e3846b3f341fdf3323c11e9b->enter($__internal_26a0c68490ce41b9c0a7a19e3b9e211dd47c4209e3846b3f341fdf3323c11e9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_633837b974ee518d7756478a3e555e95f0dfb07cfe2a77f5c9fdb1dd3491ae0b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_633837b974ee518d7756478a3e555e95f0dfb07cfe2a77f5c9fdb1dd3491ae0b->enter($__internal_633837b974ee518d7756478a3e555e95f0dfb07cfe2a77f5c9fdb1dd3491ae0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_633837b974ee518d7756478a3e555e95f0dfb07cfe2a77f5c9fdb1dd3491ae0b->leave($__internal_633837b974ee518d7756478a3e555e95f0dfb07cfe2a77f5c9fdb1dd3491ae0b_prof);

        
        $__internal_26a0c68490ce41b9c0a7a19e3b9e211dd47c4209e3846b3f341fdf3323c11e9b->leave($__internal_26a0c68490ce41b9c0a7a19e3b9e211dd47c4209e3846b3f341fdf3323c11e9b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
