<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_5d5d251903e84e40c4f25e0029c90ac8d91df4e831adac7ddf57cfcdb564ccd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0cf82b6cb801b640c0488b86f3beae812a86ef53d58ea14956f9246aca6aad5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0cf82b6cb801b640c0488b86f3beae812a86ef53d58ea14956f9246aca6aad5c->enter($__internal_0cf82b6cb801b640c0488b86f3beae812a86ef53d58ea14956f9246aca6aad5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_17b0b81eff43221f3fbe01404b680299f80028e2f3de783220fd60ee70fd4b4c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17b0b81eff43221f3fbe01404b680299f80028e2f3de783220fd60ee70fd4b4c->enter($__internal_17b0b81eff43221f3fbe01404b680299f80028e2f3de783220fd60ee70fd4b4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0cf82b6cb801b640c0488b86f3beae812a86ef53d58ea14956f9246aca6aad5c->leave($__internal_0cf82b6cb801b640c0488b86f3beae812a86ef53d58ea14956f9246aca6aad5c_prof);

        
        $__internal_17b0b81eff43221f3fbe01404b680299f80028e2f3de783220fd60ee70fd4b4c->leave($__internal_17b0b81eff43221f3fbe01404b680299f80028e2f3de783220fd60ee70fd4b4c_prof);

    }

    // line 6
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0d4d40b05afa8cf4d60633813741159e24cf938d3e34d2244a160dd12290c2f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d4d40b05afa8cf4d60633813741159e24cf938d3e34d2244a160dd12290c2f5->enter($__internal_0d4d40b05afa8cf4d60633813741159e24cf938d3e34d2244a160dd12290c2f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_5fcf584526fd56c4e0c6dbae68cc9003f1a3bc81c84bebff187696f6d1d1eea9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5fcf584526fd56c4e0c6dbae68cc9003f1a3bc81c84bebff187696f6d1d1eea9->enter($__internal_5fcf584526fd56c4e0c6dbae68cc9003f1a3bc81c84bebff187696f6d1d1eea9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 7
        echo "<div class=\"col-12 col-md-2\"></div>
<div class=\"col-12 col-md-8\">
<div class=\"panel panel-success\">
    <div class=\"panel-heading\" style=\"font-size:20px;\"><center>Compte Activité</center></div>
    <div class=\"panel-body\">
    <center><p  style=\"font-size:15px;\">";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 13
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 14
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, ($context["targetUrl"] ?? $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        // line 15
        echo "</br>
    <a class=\"btn btn-outline-secondary\" href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_show");
        echo "\">Acceder a mon Profil</a>
    </center>
    </div>
    </div>
</div>
";
        
        $__internal_5fcf584526fd56c4e0c6dbae68cc9003f1a3bc81c84bebff187696f6d1d1eea9->leave($__internal_5fcf584526fd56c4e0c6dbae68cc9003f1a3bc81c84bebff187696f6d1d1eea9_prof);

        
        $__internal_0d4d40b05afa8cf4d60633813741159e24cf938d3e34d2244a160dd12290c2f5->leave($__internal_0d4d40b05afa8cf4d60633813741159e24cf938d3e34d2244a160dd12290c2f5_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 16,  70 => 15,  62 => 14,  60 => 13,  56 => 12,  49 => 7,  40 => 6,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}


{% block fos_user_content %}
<div class=\"col-12 col-md-2\"></div>
<div class=\"col-12 col-md-8\">
<div class=\"panel panel-success\">
    <div class=\"panel-heading\" style=\"font-size:20px;\"><center>Compte Activité</center></div>
    <div class=\"panel-body\">
    <center><p  style=\"font-size:15px;\">{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}</br>
    <a class=\"btn btn-outline-secondary\" href=\"{{ path('fos_user_profile_show') }}\">Acceder a mon Profil</a>
    </center>
    </div>
    </div>
</div>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
