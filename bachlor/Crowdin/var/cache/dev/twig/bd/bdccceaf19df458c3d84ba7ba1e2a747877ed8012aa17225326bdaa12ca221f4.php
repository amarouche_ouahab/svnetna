<?php

/* :translate:usersTranslate.html.twig */
class __TwigTemplate_b025e5c619f70ce26bf587fce0b9a4216492d0a249399308e882acc4227f50b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", ":translate:usersTranslate.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9bce402fae427c14862a76e247f807b2dc87351c5b3ebf21e62d29dac4c9afd0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9bce402fae427c14862a76e247f807b2dc87351c5b3ebf21e62d29dac4c9afd0->enter($__internal_9bce402fae427c14862a76e247f807b2dc87351c5b3ebf21e62d29dac4c9afd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":translate:usersTranslate.html.twig"));

        $__internal_fbcfaae4c24a8f38581062697b892320389bcf3d93d9e6fe5735468a58f46353 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fbcfaae4c24a8f38581062697b892320389bcf3d93d9e6fe5735468a58f46353->enter($__internal_fbcfaae4c24a8f38581062697b892320389bcf3d93d9e6fe5735468a58f46353_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":translate:usersTranslate.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9bce402fae427c14862a76e247f807b2dc87351c5b3ebf21e62d29dac4c9afd0->leave($__internal_9bce402fae427c14862a76e247f807b2dc87351c5b3ebf21e62d29dac4c9afd0_prof);

        
        $__internal_fbcfaae4c24a8f38581062697b892320389bcf3d93d9e6fe5735468a58f46353->leave($__internal_fbcfaae4c24a8f38581062697b892320389bcf3d93d9e6fe5735468a58f46353_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_7ffc77795e953061f0efedc1d49dffa6f107697648870b44eb4df5e6ff260c29 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ffc77795e953061f0efedc1d49dffa6f107697648870b44eb4df5e6ff260c29->enter($__internal_7ffc77795e953061f0efedc1d49dffa6f107697648870b44eb4df5e6ff260c29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_e9da5aa9108e79dc03fc6ce7cb90b70b2c8163f3183adc190759a677de639745 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e9da5aa9108e79dc03fc6ce7cb90b70b2c8163f3183adc190759a677de639745->enter($__internal_e9da5aa9108e79dc03fc6ce7cb90b70b2c8163f3183adc190759a677de639745_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "      <div class=\"col-12 col-md-2 border border-primar rounded\">
            <center><h3>Detail du ficher</h3></center><br/>
            <b>Ajouter par: </b> ";
        // line 5
        echo twig_escape_filter($this->env, ($context["user"] ?? $this->getContext($context, "user")), "html", null, true);
        echo "<br/>
            <b> nom: </b>";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute(($context["detail"] ?? $this->getContext($context, "detail")), "name", array()), "html", null, true);
        echo "<br/>
            <b> Date d'ajout: </b>";
        // line 7
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["detail"] ?? $this->getContext($context, "detail")), "dateadd", array()), "Y-m-d H:i:s", "Europe/Paris"), "html", null, true);
        echo "<br/>
            ";
        // line 8
        echo "</br></br>
            <center><a  href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailfile", array("id" => ($context["id"] ?? $this->getContext($context, "id")))), "html", null, true);
        echo "\"  class=\"btn btn-lg btn-info\">Acceder aux details du fichier</a>
           </center> </br>
    </div>
    <div class=\"col-12 col-md-1\"></div>
        <div class=\"col-12 col-md-4\" style=\"padding: 0 !important;\">
            <table class=\"table table-bordered\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\">Valeurs</th>
                </tr>
              </thead>
              <tbody>
                  ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["table"] ?? $this->getContext($context, "table")));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 23
            echo "                <tr>
                  <th>";
            // line 24
            if ($this->getAttribute($context["key"], 0, array(), "array", true, true)) {
                // line 25
                echo "                       ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 0, array(), "array"), "html", null, true);
                echo "
                    ";
            }
            // line 26
            echo "</th>
                  <td>
                    ";
            // line 28
            if ($this->getAttribute($context["key"], 2, array(), "array", true, true)) {
                // line 29
                echo "                       ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 2, array(), "array"), "html", null, true);
                echo "
                    ";
            }
            // line 31
            echo "                    </td>

                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                 
              </tbody>
            </table>
        </div>
        ";
        // line 40
        echo "        <div class=\"\" style=\"position:absolute;margin-left: 57%;width: 16%;\">
           <table class=\"table table-bordered\">
              <thead>
                <tr>
                  <th scope=\"col\">Valeurs traduite</th>
                </tr>
              </thead>
              <tbody>
            ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["values"] ?? $this->getContext($context, "values")));
        foreach ($context['_seq'] as $context["_key"] => $context["val"]) {
            // line 49
            echo "                <tr>
                  <td>";
            // line 50
            if (array_key_exists("val", $context)) {
                // line 51
                echo "                       <center>";
                echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                echo "</center>
                    ";
            }
            // line 52
            echo "</td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['val'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "            </tbody>
            </table>
        </div>
    </div>

";
        
        $__internal_e9da5aa9108e79dc03fc6ce7cb90b70b2c8163f3183adc190759a677de639745->leave($__internal_e9da5aa9108e79dc03fc6ce7cb90b70b2c8163f3183adc190759a677de639745_prof);

        
        $__internal_7ffc77795e953061f0efedc1d49dffa6f107697648870b44eb4df5e6ff260c29->leave($__internal_7ffc77795e953061f0efedc1d49dffa6f107697648870b44eb4df5e6ff260c29_prof);

    }

    public function getTemplateName()
    {
        return ":translate:usersTranslate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 55,  151 => 52,  145 => 51,  143 => 50,  140 => 49,  136 => 48,  126 => 40,  120 => 35,  111 => 31,  105 => 29,  103 => 28,  99 => 26,  93 => 25,  91 => 24,  88 => 23,  84 => 22,  68 => 9,  65 => 8,  61 => 7,  57 => 6,  53 => 5,  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block content %}
      <div class=\"col-12 col-md-2 border border-primar rounded\">
            <center><h3>Detail du ficher</h3></center><br/>
            <b>Ajouter par: </b> {{user}}<br/>
            <b> nom: </b>{{detail.name}}<br/>
            <b> Date d'ajout: </b>{{detail.dateadd|date(\"Y-m-d H:i:s\", \"Europe/Paris\")}}<br/>
            {#<b>Langue du fichier: </b>{{ detail.langues}}<br/>#}</br></br>
            <center><a  href=\"{{ path('detailfile',{'id': id})}}\"  class=\"btn btn-lg btn-info\">Acceder aux details du fichier</a>
           </center> </br>
    </div>
    <div class=\"col-12 col-md-1\"></div>
        <div class=\"col-12 col-md-4\" style=\"padding: 0 !important;\">
            <table class=\"table table-bordered\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\">Valeurs</th>
                </tr>
              </thead>
              <tbody>
                  {% for key in table %}
                <tr>
                  <th>{% if  key[0] is defined %}
                       {{key[0]}}
                    {% endif %}</th>
                  <td>
                    {% if  key[2] is defined %}
                       {{key[2]}}
                    {% endif %}
                    </td>

                </tr>
                {% endfor %}
                 
              </tbody>
            </table>
        </div>
        {#<div class=\"col-12 col-md-1\"></div>#}
        <div class=\"\" style=\"position:absolute;margin-left: 57%;width: 16%;\">
           <table class=\"table table-bordered\">
              <thead>
                <tr>
                  <th scope=\"col\">Valeurs traduite</th>
                </tr>
              </thead>
              <tbody>
            {% for val in values %}
                <tr>
                  <td>{% if  val is defined %}
                       <center>{{val}}</center>
                    {% endif %}</td>
                </tr>
                {% endfor %}
            </tbody>
            </table>
        </div>
    </div>

{% endblock %}
", ":translate:usersTranslate.html.twig", "/home/ubuntu/workspace/app/Resources/views/translate/usersTranslate.html.twig");
    }
}
