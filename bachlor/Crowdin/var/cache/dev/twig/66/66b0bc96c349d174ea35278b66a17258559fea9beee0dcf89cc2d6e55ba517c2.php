<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_126d6f108d564b9d9b8cdd700afd57f9c002cda8220541c4ceb44537068e5775 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8639b2b2fd7977a608316d70ede0ebd2a0cf887488f5d824da6a04bfd4a95191 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8639b2b2fd7977a608316d70ede0ebd2a0cf887488f5d824da6a04bfd4a95191->enter($__internal_8639b2b2fd7977a608316d70ede0ebd2a0cf887488f5d824da6a04bfd4a95191_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_d88bad41e622c31ae706e12a1a99896ee90a95e9a25dc0d56f242201a4d79e65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d88bad41e622c31ae706e12a1a99896ee90a95e9a25dc0d56f242201a4d79e65->enter($__internal_d88bad41e622c31ae706e12a1a99896ee90a95e9a25dc0d56f242201a4d79e65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_8639b2b2fd7977a608316d70ede0ebd2a0cf887488f5d824da6a04bfd4a95191->leave($__internal_8639b2b2fd7977a608316d70ede0ebd2a0cf887488f5d824da6a04bfd4a95191_prof);

        
        $__internal_d88bad41e622c31ae706e12a1a99896ee90a95e9a25dc0d56f242201a4d79e65->leave($__internal_d88bad41e622c31ae706e12a1a99896ee90a95e9a25dc0d56f242201a4d79e65_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
