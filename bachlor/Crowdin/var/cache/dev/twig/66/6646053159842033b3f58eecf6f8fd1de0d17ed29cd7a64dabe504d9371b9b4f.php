<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_59bdd88dc2e89c8040d54734fb569b93c442e92df2c77aa5d9e43626b95cbb34 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7287fb21fb5b405afad5ca8aaa5d98f86806ec922f9d8a426c3524bdc0c39f9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7287fb21fb5b405afad5ca8aaa5d98f86806ec922f9d8a426c3524bdc0c39f9e->enter($__internal_7287fb21fb5b405afad5ca8aaa5d98f86806ec922f9d8a426c3524bdc0c39f9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $__internal_36f1df6dc52788dadcc4e13764e98088b742e54639b7923feeac048aeb8516c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36f1df6dc52788dadcc4e13764e98088b742e54639b7923feeac048aeb8516c2->enter($__internal_36f1df6dc52788dadcc4e13764e98088b742e54639b7923feeac048aeb8516c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7287fb21fb5b405afad5ca8aaa5d98f86806ec922f9d8a426c3524bdc0c39f9e->leave($__internal_7287fb21fb5b405afad5ca8aaa5d98f86806ec922f9d8a426c3524bdc0c39f9e_prof);

        
        $__internal_36f1df6dc52788dadcc4e13764e98088b742e54639b7923feeac048aeb8516c2->leave($__internal_36f1df6dc52788dadcc4e13764e98088b742e54639b7923feeac048aeb8516c2_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_cb4534ae30603fcc0a7ee9e691ebb70863a6cf332c6548d453c17bb67dd54851 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb4534ae30603fcc0a7ee9e691ebb70863a6cf332c6548d453c17bb67dd54851->enter($__internal_cb4534ae30603fcc0a7ee9e691ebb70863a6cf332c6548d453c17bb67dd54851_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b01aa9c83f1606dd4a5222584c7b203acf4f90177bf2a9777526f2414f3f6f6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b01aa9c83f1606dd4a5222584c7b203acf4f90177bf2a9777526f2414f3f6f6a->enter($__internal_b01aa9c83f1606dd4a5222584c7b203acf4f90177bf2a9777526f2414f3f6f6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_b01aa9c83f1606dd4a5222584c7b203acf4f90177bf2a9777526f2414f3f6f6a->leave($__internal_b01aa9c83f1606dd4a5222584c7b203acf4f90177bf2a9777526f2414f3f6f6a_prof);

        
        $__internal_cb4534ae30603fcc0a7ee9e691ebb70863a6cf332c6548d453c17bb67dd54851->leave($__internal_cb4534ae30603fcc0a7ee9e691ebb70863a6cf332c6548d453c17bb67dd54851_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "FOSUserBundle:Security:login.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
