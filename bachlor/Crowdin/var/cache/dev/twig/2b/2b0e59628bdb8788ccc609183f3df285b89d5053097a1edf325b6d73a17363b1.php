<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_ccf06df31639b7c9b2857bbf3c5e82ca65ad8846bad8b03705b504d691c84a56 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_77980e7d0eade5eaa6a33cae460280d73874508b484f9c2ab2293afa1df82bf6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_77980e7d0eade5eaa6a33cae460280d73874508b484f9c2ab2293afa1df82bf6->enter($__internal_77980e7d0eade5eaa6a33cae460280d73874508b484f9c2ab2293afa1df82bf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $__internal_2bfb565d696e4049b4f83db4eb87585cf5cf4fa64ed305fe6c9f1252fdcf32f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2bfb565d696e4049b4f83db4eb87585cf5cf4fa64ed305fe6c9f1252fdcf32f2->enter($__internal_2bfb565d696e4049b4f83db4eb87585cf5cf4fa64ed305fe6c9f1252fdcf32f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_77980e7d0eade5eaa6a33cae460280d73874508b484f9c2ab2293afa1df82bf6->leave($__internal_77980e7d0eade5eaa6a33cae460280d73874508b484f9c2ab2293afa1df82bf6_prof);

        
        $__internal_2bfb565d696e4049b4f83db4eb87585cf5cf4fa64ed305fe6c9f1252fdcf32f2->leave($__internal_2bfb565d696e4049b4f83db4eb87585cf5cf4fa64ed305fe6c9f1252fdcf32f2_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6a987c87884a445ec233225193943d29af1d9dc7e0f9b1615a2211e79023abe6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a987c87884a445ec233225193943d29af1d9dc7e0f9b1615a2211e79023abe6->enter($__internal_6a987c87884a445ec233225193943d29af1d9dc7e0f9b1615a2211e79023abe6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_45dc9cdeb651a735f428a6e681f4e8b5fcb15a966bc2b8dd588d28e559532bf3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_45dc9cdeb651a735f428a6e681f4e8b5fcb15a966bc2b8dd588d28e559532bf3->enter($__internal_45dc9cdeb651a735f428a6e681f4e8b5fcb15a966bc2b8dd588d28e559532bf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_45dc9cdeb651a735f428a6e681f4e8b5fcb15a966bc2b8dd588d28e559532bf3->leave($__internal_45dc9cdeb651a735f428a6e681f4e8b5fcb15a966bc2b8dd588d28e559532bf3_prof);

        
        $__internal_6a987c87884a445ec233225193943d29af1d9dc7e0f9b1615a2211e79023abe6->leave($__internal_6a987c87884a445ec233225193943d29af1d9dc7e0f9b1615a2211e79023abe6_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_a2eb6d547d80bd490df9ad16718dafa3c4beba6e8ed24746a483664315b522f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2eb6d547d80bd490df9ad16718dafa3c4beba6e8ed24746a483664315b522f7->enter($__internal_a2eb6d547d80bd490df9ad16718dafa3c4beba6e8ed24746a483664315b522f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_f6cd3c74b2e9f161e2da845c4dd91953a4dadd0d5d4e7565392893648025f2c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6cd3c74b2e9f161e2da845c4dd91953a4dadd0d5d4e7565392893648025f2c3->enter($__internal_f6cd3c74b2e9f161e2da845c4dd91953a4dadd0d5d4e7565392893648025f2c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_f6cd3c74b2e9f161e2da845c4dd91953a4dadd0d5d4e7565392893648025f2c3->leave($__internal_f6cd3c74b2e9f161e2da845c4dd91953a4dadd0d5d4e7565392893648025f2c3_prof);

        
        $__internal_a2eb6d547d80bd490df9ad16718dafa3c4beba6e8ed24746a483664315b522f7->leave($__internal_a2eb6d547d80bd490df9ad16718dafa3c4beba6e8ed24746a483664315b522f7_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_2640de4b924c464dc7928a479680b8e8dee743db38357a0779d69affc9d1b1ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2640de4b924c464dc7928a479680b8e8dee743db38357a0779d69affc9d1b1ca->enter($__internal_2640de4b924c464dc7928a479680b8e8dee743db38357a0779d69affc9d1b1ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3304523c09fa244430ead8dc104534a02230ac9dd86a36e2f6689aeaf2a773b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3304523c09fa244430ead8dc104534a02230ac9dd86a36e2f6689aeaf2a773b2->enter($__internal_3304523c09fa244430ead8dc104534a02230ac9dd86a36e2f6689aeaf2a773b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 141)->display($context);
        
        $__internal_3304523c09fa244430ead8dc104534a02230ac9dd86a36e2f6689aeaf2a773b2->leave($__internal_3304523c09fa244430ead8dc104534a02230ac9dd86a36e2f6689aeaf2a773b2_prof);

        
        $__internal_2640de4b924c464dc7928a479680b8e8dee743db38357a0779d69affc9d1b1ca->leave($__internal_2640de4b924c464dc7928a479680b8e8dee743db38357a0779d69affc9d1b1ca_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
