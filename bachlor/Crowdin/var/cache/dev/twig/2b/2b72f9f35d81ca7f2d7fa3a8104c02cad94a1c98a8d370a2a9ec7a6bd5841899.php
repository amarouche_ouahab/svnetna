<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_6bdc415026468751b0fd3b8ea4bb5b8b9e992dbfca85e1c86f20068b91fbe660 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d36aa77591ee01f4f62d8a37f242830f892c5d14f076e874bda43253f8c941f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d36aa77591ee01f4f62d8a37f242830f892c5d14f076e874bda43253f8c941f->enter($__internal_8d36aa77591ee01f4f62d8a37f242830f892c5d14f076e874bda43253f8c941f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_df15989d784ac4510d0a2a725a75242b6d4d3a9421b2f7beb8040e354305d3a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df15989d784ac4510d0a2a725a75242b6d4d3a9421b2f7beb8040e354305d3a7->enter($__internal_df15989d784ac4510d0a2a725a75242b6d4d3a9421b2f7beb8040e354305d3a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8d36aa77591ee01f4f62d8a37f242830f892c5d14f076e874bda43253f8c941f->leave($__internal_8d36aa77591ee01f4f62d8a37f242830f892c5d14f076e874bda43253f8c941f_prof);

        
        $__internal_df15989d784ac4510d0a2a725a75242b6d4d3a9421b2f7beb8040e354305d3a7->leave($__internal_df15989d784ac4510d0a2a725a75242b6d4d3a9421b2f7beb8040e354305d3a7_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_9bce4cb06452cb9ef514ed21240f6db86c79f018293420ad70d5c66f32154fc0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9bce4cb06452cb9ef514ed21240f6db86c79f018293420ad70d5c66f32154fc0->enter($__internal_9bce4cb06452cb9ef514ed21240f6db86c79f018293420ad70d5c66f32154fc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_7448ee2b36f0bb72f45f6f129c2bb8c7eaa5004f03d0db1b85c9fe975d3ce257 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7448ee2b36f0bb72f45f6f129c2bb8c7eaa5004f03d0db1b85c9fe975d3ce257->enter($__internal_7448ee2b36f0bb72f45f6f129c2bb8c7eaa5004f03d0db1b85c9fe975d3ce257_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_7448ee2b36f0bb72f45f6f129c2bb8c7eaa5004f03d0db1b85c9fe975d3ce257->leave($__internal_7448ee2b36f0bb72f45f6f129c2bb8c7eaa5004f03d0db1b85c9fe975d3ce257_prof);

        
        $__internal_9bce4cb06452cb9ef514ed21240f6db86c79f018293420ad70d5c66f32154fc0->leave($__internal_9bce4cb06452cb9ef514ed21240f6db86c79f018293420ad70d5c66f32154fc0_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_7c2fe0d55ac6436dbe8662fc721ffbbf57cd75e32c0e128fa560480f2b353a57 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c2fe0d55ac6436dbe8662fc721ffbbf57cd75e32c0e128fa560480f2b353a57->enter($__internal_7c2fe0d55ac6436dbe8662fc721ffbbf57cd75e32c0e128fa560480f2b353a57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_65011f982d598758d18256a70f85a8144d2e9a59835f3be8cd03a2d3613f3686 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65011f982d598758d18256a70f85a8144d2e9a59835f3be8cd03a2d3613f3686->enter($__internal_65011f982d598758d18256a70f85a8144d2e9a59835f3be8cd03a2d3613f3686_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_65011f982d598758d18256a70f85a8144d2e9a59835f3be8cd03a2d3613f3686->leave($__internal_65011f982d598758d18256a70f85a8144d2e9a59835f3be8cd03a2d3613f3686_prof);

        
        $__internal_7c2fe0d55ac6436dbe8662fc721ffbbf57cd75e32c0e128fa560480f2b353a57->leave($__internal_7c2fe0d55ac6436dbe8662fc721ffbbf57cd75e32c0e128fa560480f2b353a57_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_262f10b183c154fe3fb38f34dd6b5b92f6be779b2d2021dc0a17b6ba1df46bd3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_262f10b183c154fe3fb38f34dd6b5b92f6be779b2d2021dc0a17b6ba1df46bd3->enter($__internal_262f10b183c154fe3fb38f34dd6b5b92f6be779b2d2021dc0a17b6ba1df46bd3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_3a0540483ebe7c837e76bf5342fd516661b7694f147ef77613fcedc227898d47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a0540483ebe7c837e76bf5342fd516661b7694f147ef77613fcedc227898d47->enter($__internal_3a0540483ebe7c837e76bf5342fd516661b7694f147ef77613fcedc227898d47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_3a0540483ebe7c837e76bf5342fd516661b7694f147ef77613fcedc227898d47->leave($__internal_3a0540483ebe7c837e76bf5342fd516661b7694f147ef77613fcedc227898d47_prof);

        
        $__internal_262f10b183c154fe3fb38f34dd6b5b92f6be779b2d2021dc0a17b6ba1df46bd3->leave($__internal_262f10b183c154fe3fb38f34dd6b5b92f6be779b2d2021dc0a17b6ba1df46bd3_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
