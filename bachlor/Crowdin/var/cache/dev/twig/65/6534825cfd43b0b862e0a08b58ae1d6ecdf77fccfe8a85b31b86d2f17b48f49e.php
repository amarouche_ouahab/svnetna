<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_8ebd332e3a254704278c400cbf5ee4b34bed56e5da51e300078366e241c542af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d87f3495e5e1f8dbc4e6c5229c8b5c27378061dcaae05c375face8a53971e507 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d87f3495e5e1f8dbc4e6c5229c8b5c27378061dcaae05c375face8a53971e507->enter($__internal_d87f3495e5e1f8dbc4e6c5229c8b5c27378061dcaae05c375face8a53971e507_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_28052df4501adf69f4ce3949d7b691026e15ecd23550fc092a106a47501b8df4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28052df4501adf69f4ce3949d7b691026e15ecd23550fc092a106a47501b8df4->enter($__internal_28052df4501adf69f4ce3949d7b691026e15ecd23550fc092a106a47501b8df4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_d87f3495e5e1f8dbc4e6c5229c8b5c27378061dcaae05c375face8a53971e507->leave($__internal_d87f3495e5e1f8dbc4e6c5229c8b5c27378061dcaae05c375face8a53971e507_prof);

        
        $__internal_28052df4501adf69f4ce3949d7b691026e15ecd23550fc092a106a47501b8df4->leave($__internal_28052df4501adf69f4ce3949d7b691026e15ecd23550fc092a106a47501b8df4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
