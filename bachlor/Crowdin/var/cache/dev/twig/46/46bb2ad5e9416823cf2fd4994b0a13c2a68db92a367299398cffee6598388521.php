<?php

/* FOSUserBundle:Profile:edit_content.html.twig */
class __TwigTemplate_363ce43948fa962e38e190ab2fa5619df6464258ac53cb0c4ddcc41c5e673d99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7997e3cb75005468c8953b356428a43a94cfc1e287ef1f6d4377d27fb011a142 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7997e3cb75005468c8953b356428a43a94cfc1e287ef1f6d4377d27fb011a142->enter($__internal_7997e3cb75005468c8953b356428a43a94cfc1e287ef1f6d4377d27fb011a142_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit_content.html.twig"));

        $__internal_f6d6afa36dee1715392e65f60469e87711d7aab7b5ec28969c865c1a2bf76399 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6d6afa36dee1715392e65f60469e87711d7aab7b5ec28969c865c1a2bf76399->enter($__internal_f6d6afa36dee1715392e65f60469e87711d7aab7b5ec28969c865c1a2bf76399_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit_content.html.twig"));

        // line 2
        echo "<center>
     <div class = \"container\" style=\"max-width: 500px;margin-top: 100px;\">
        <h3 class=\"form-signin-heading\">Modifier mon ptofile</h3>
        <hr class=\"colorgraph\"><br><br>
        ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_edit"), "attr" => array("class" => "fos_user_profile_edit")));
        echo "
            ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            <hr class=\"colorgraph\"><br><br>
            <div>
                <input type=\"submit\" class=\"btn btn-lg btn-success btn-block\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.edit.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
            </div>
        ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    </div>
</center>";
        
        $__internal_7997e3cb75005468c8953b356428a43a94cfc1e287ef1f6d4377d27fb011a142->leave($__internal_7997e3cb75005468c8953b356428a43a94cfc1e287ef1f6d4377d27fb011a142_prof);

        
        $__internal_f6d6afa36dee1715392e65f60469e87711d7aab7b5ec28969c865c1a2bf76399->leave($__internal_f6d6afa36dee1715392e65f60469e87711d7aab7b5ec28969c865c1a2bf76399_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 12,  41 => 10,  35 => 7,  31 => 6,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
<center>
     <div class = \"container\" style=\"max-width: 500px;margin-top: 100px;\">
        <h3 class=\"form-signin-heading\">Modifier mon ptofile</h3>
        <hr class=\"colorgraph\"><br><br>
        {{ form_start(form, { 'action': path('fos_user_profile_edit'), 'attr': { 'class': 'fos_user_profile_edit' } }) }}
            {{ form_widget(form) }}
            <hr class=\"colorgraph\"><br><br>
            <div>
                <input type=\"submit\" class=\"btn btn-lg btn-success btn-block\" value=\"{{ 'profile.edit.submit'|trans }}\" />
            </div>
        {{ form_end(form) }}
    </div>
</center>", "FOSUserBundle:Profile:edit_content.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit_content.html.twig");
    }
}
