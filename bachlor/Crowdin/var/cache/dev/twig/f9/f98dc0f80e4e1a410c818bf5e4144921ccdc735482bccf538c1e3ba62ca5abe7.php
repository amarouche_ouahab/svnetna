<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_c87710f357776402c9631da4bda3850b78ef8adca6273d8bd36cbe5e2fe827f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_34f178ae1cdebbe4ceeeacb46f10ee92802099f7dce0eb0890c337d84a4b6f3b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34f178ae1cdebbe4ceeeacb46f10ee92802099f7dce0eb0890c337d84a4b6f3b->enter($__internal_34f178ae1cdebbe4ceeeacb46f10ee92802099f7dce0eb0890c337d84a4b6f3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $__internal_e72f35090dbdc6eb5cf328816eb9636cff9ce5cf2c767032cc555569c8dfd1a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e72f35090dbdc6eb5cf328816eb9636cff9ce5cf2c767032cc555569c8dfd1a2->enter($__internal_e72f35090dbdc6eb5cf328816eb9636cff9ce5cf2c767032cc555569c8dfd1a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_34f178ae1cdebbe4ceeeacb46f10ee92802099f7dce0eb0890c337d84a4b6f3b->leave($__internal_34f178ae1cdebbe4ceeeacb46f10ee92802099f7dce0eb0890c337d84a4b6f3b_prof);

        
        $__internal_e72f35090dbdc6eb5cf328816eb9636cff9ce5cf2c767032cc555569c8dfd1a2->leave($__internal_e72f35090dbdc6eb5cf328816eb9636cff9ce5cf2c767032cc555569c8dfd1a2_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_da5b27bae8fbf2e41301a75d884a46a9a20da17cd1bf97c62f71b6bd4131753b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da5b27bae8fbf2e41301a75d884a46a9a20da17cd1bf97c62f71b6bd4131753b->enter($__internal_da5b27bae8fbf2e41301a75d884a46a9a20da17cd1bf97c62f71b6bd4131753b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_9617c5a899d65619eeac70a7fd4ebefb7ddd0379e381bc2fe05334b752e72425 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9617c5a899d65619eeac70a7fd4ebefb7ddd0379e381bc2fe05334b752e72425->enter($__internal_9617c5a899d65619eeac70a7fd4ebefb7ddd0379e381bc2fe05334b752e72425_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_9617c5a899d65619eeac70a7fd4ebefb7ddd0379e381bc2fe05334b752e72425->leave($__internal_9617c5a899d65619eeac70a7fd4ebefb7ddd0379e381bc2fe05334b752e72425_prof);

        
        $__internal_da5b27bae8fbf2e41301a75d884a46a9a20da17cd1bf97c62f71b6bd4131753b->leave($__internal_da5b27bae8fbf2e41301a75d884a46a9a20da17cd1bf97c62f71b6bd4131753b_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_6c28cae317c9d339b77cb308c0ff585a3bca5f5b1cdd977e885335baaf515d3a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c28cae317c9d339b77cb308c0ff585a3bca5f5b1cdd977e885335baaf515d3a->enter($__internal_6c28cae317c9d339b77cb308c0ff585a3bca5f5b1cdd977e885335baaf515d3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_2d3f21cbfe11ae28cf0d3e3ec84e252f7a28967a2ad1d75de3f307fd6ba86621 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d3f21cbfe11ae28cf0d3e3ec84e252f7a28967a2ad1d75de3f307fd6ba86621->enter($__internal_2d3f21cbfe11ae28cf0d3e3ec84e252f7a28967a2ad1d75de3f307fd6ba86621_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_2d3f21cbfe11ae28cf0d3e3ec84e252f7a28967a2ad1d75de3f307fd6ba86621->leave($__internal_2d3f21cbfe11ae28cf0d3e3ec84e252f7a28967a2ad1d75de3f307fd6ba86621_prof);

        
        $__internal_6c28cae317c9d339b77cb308c0ff585a3bca5f5b1cdd977e885335baaf515d3a->leave($__internal_6c28cae317c9d339b77cb308c0ff585a3bca5f5b1cdd977e885335baaf515d3a_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_dbd8f920d563238449304eb46af04cc5d1991a4fbf28e01ba5639697da63a6c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dbd8f920d563238449304eb46af04cc5d1991a4fbf28e01ba5639697da63a6c9->enter($__internal_dbd8f920d563238449304eb46af04cc5d1991a4fbf28e01ba5639697da63a6c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_f7ff7bb8ff5381916e1bbd9c27ac0613da196bde7320c5df4094e31814207aaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7ff7bb8ff5381916e1bbd9c27ac0613da196bde7320c5df4094e31814207aaf->enter($__internal_f7ff7bb8ff5381916e1bbd9c27ac0613da196bde7320c5df4094e31814207aaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_f7ff7bb8ff5381916e1bbd9c27ac0613da196bde7320c5df4094e31814207aaf->leave($__internal_f7ff7bb8ff5381916e1bbd9c27ac0613da196bde7320c5df4094e31814207aaf_prof);

        
        $__internal_dbd8f920d563238449304eb46af04cc5d1991a4fbf28e01ba5639697da63a6c9->leave($__internal_dbd8f920d563238449304eb46af04cc5d1991a4fbf28e01ba5639697da63a6c9_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
