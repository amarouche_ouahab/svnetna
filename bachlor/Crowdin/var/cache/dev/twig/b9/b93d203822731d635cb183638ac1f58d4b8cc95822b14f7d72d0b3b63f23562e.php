<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_b5546d210ace54589f76e104271cd2ded7b60294d28c41bb652e33ae5a0ba20b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01ef26e8fc67783048bd17295777b1d9e9664b271051a873b6c2628a4ede44c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01ef26e8fc67783048bd17295777b1d9e9664b271051a873b6c2628a4ede44c6->enter($__internal_01ef26e8fc67783048bd17295777b1d9e9664b271051a873b6c2628a4ede44c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_f1fc9419f5d8d1468e614cbbdfe4f801c35c375e1152321e7d3e6d9cd479c36e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f1fc9419f5d8d1468e614cbbdfe4f801c35c375e1152321e7d3e6d9cd479c36e->enter($__internal_f1fc9419f5d8d1468e614cbbdfe4f801c35c375e1152321e7d3e6d9cd479c36e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes'); ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form); ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php echo \$view['form']->block(\$form, 'form_rows'); ?>
    <?php echo \$view['form']->rest(\$form); ?>
</table>
";
        
        $__internal_01ef26e8fc67783048bd17295777b1d9e9664b271051a873b6c2628a4ede44c6->leave($__internal_01ef26e8fc67783048bd17295777b1d9e9664b271051a873b6c2628a4ede44c6_prof);

        
        $__internal_f1fc9419f5d8d1468e614cbbdfe4f801c35c375e1152321e7d3e6d9cd479c36e->leave($__internal_f1fc9419f5d8d1468e614cbbdfe4f801c35c375e1152321e7d3e6d9cd479c36e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes'); ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form); ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php echo \$view['form']->block(\$form, 'form_rows'); ?>
    <?php echo \$view['form']->rest(\$form); ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}
