<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_af32942d7046c690edbce5aa81172a2b82cace10d7ff2377632905e8c6c617d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_667a990654f7e350624f371b5aa0d01c68270708ecbe7450593003cc311bdbef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_667a990654f7e350624f371b5aa0d01c68270708ecbe7450593003cc311bdbef->enter($__internal_667a990654f7e350624f371b5aa0d01c68270708ecbe7450593003cc311bdbef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_665abd91de7e8537b6ca7601c398a45d436c1a1e947151f96e1a887d33df6688 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_665abd91de7e8537b6ca7601c398a45d436c1a1e947151f96e1a887d33df6688->enter($__internal_665abd91de7e8537b6ca7601c398a45d436c1a1e947151f96e1a887d33df6688_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_667a990654f7e350624f371b5aa0d01c68270708ecbe7450593003cc311bdbef->leave($__internal_667a990654f7e350624f371b5aa0d01c68270708ecbe7450593003cc311bdbef_prof);

        
        $__internal_665abd91de7e8537b6ca7601c398a45d436c1a1e947151f96e1a887d33df6688->leave($__internal_665abd91de7e8537b6ca7601c398a45d436c1a1e947151f96e1a887d33df6688_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ebd7b4b15e04f07bfe382fbf5c9f162b2f99dccf78d76960e575d89fd0a39dbd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ebd7b4b15e04f07bfe382fbf5c9f162b2f99dccf78d76960e575d89fd0a39dbd->enter($__internal_ebd7b4b15e04f07bfe382fbf5c9f162b2f99dccf78d76960e575d89fd0a39dbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_3fdee79f4111e5c666c8027527c5212bce9d86fcbc5a4debf8fd01572b53569d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3fdee79f4111e5c666c8027527c5212bce9d86fcbc5a4debf8fd01572b53569d->enter($__internal_3fdee79f4111e5c666c8027527c5212bce9d86fcbc5a4debf8fd01572b53569d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_3fdee79f4111e5c666c8027527c5212bce9d86fcbc5a4debf8fd01572b53569d->leave($__internal_3fdee79f4111e5c666c8027527c5212bce9d86fcbc5a4debf8fd01572b53569d_prof);

        
        $__internal_ebd7b4b15e04f07bfe382fbf5c9f162b2f99dccf78d76960e575d89fd0a39dbd->leave($__internal_ebd7b4b15e04f07bfe382fbf5c9f162b2f99dccf78d76960e575d89fd0a39dbd_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}
