<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_d0555d51a72f466643a8c0f2a55114d5ff3162328e6caaea1f015a67807248f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d8eab6ccfdf952cb1554513432f44108f3fcd5ae882562b0769252102a002e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d8eab6ccfdf952cb1554513432f44108f3fcd5ae882562b0769252102a002e9->enter($__internal_5d8eab6ccfdf952cb1554513432f44108f3fcd5ae882562b0769252102a002e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_ef03252210dc1fe4bace09927d1504ddafc8a6cc358cb246ffc9beec66737fed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef03252210dc1fe4bace09927d1504ddafc8a6cc358cb246ffc9beec66737fed->enter($__internal_ef03252210dc1fe4bace09927d1504ddafc8a6cc358cb246ffc9beec66737fed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_5d8eab6ccfdf952cb1554513432f44108f3fcd5ae882562b0769252102a002e9->leave($__internal_5d8eab6ccfdf952cb1554513432f44108f3fcd5ae882562b0769252102a002e9_prof);

        
        $__internal_ef03252210dc1fe4bace09927d1504ddafc8a6cc358cb246ffc9beec66737fed->leave($__internal_ef03252210dc1fe4bace09927d1504ddafc8a6cc358cb246ffc9beec66737fed_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}
