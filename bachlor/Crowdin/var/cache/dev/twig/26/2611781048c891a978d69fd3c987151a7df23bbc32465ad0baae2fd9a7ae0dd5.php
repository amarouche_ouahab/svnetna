<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_349e2de801df98270fecfa0898cbf7019486a3b3a05b2d17750b577b74666353 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22bf6f639cf63ad90dac6096d0e0101a626e6b27d6c9ab312fbdcb4b842d1475 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22bf6f639cf63ad90dac6096d0e0101a626e6b27d6c9ab312fbdcb4b842d1475->enter($__internal_22bf6f639cf63ad90dac6096d0e0101a626e6b27d6c9ab312fbdcb4b842d1475_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_bccb97bb7776eeb679e76c443417d70c5858956c5962f365b32d6dcb77f0fdcc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bccb97bb7776eeb679e76c443417d70c5858956c5962f365b32d6dcb77f0fdcc->enter($__internal_bccb97bb7776eeb679e76c443417d70c5858956c5962f365b32d6dcb77f0fdcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_22bf6f639cf63ad90dac6096d0e0101a626e6b27d6c9ab312fbdcb4b842d1475->leave($__internal_22bf6f639cf63ad90dac6096d0e0101a626e6b27d6c9ab312fbdcb4b842d1475_prof);

        
        $__internal_bccb97bb7776eeb679e76c443417d70c5858956c5962f365b32d6dcb77f0fdcc->leave($__internal_bccb97bb7776eeb679e76c443417d70c5858956c5962f365b32d6dcb77f0fdcc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
