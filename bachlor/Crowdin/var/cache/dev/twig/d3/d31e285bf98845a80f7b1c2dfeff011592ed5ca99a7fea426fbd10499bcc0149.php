<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_afcdb8821998075a7745c0abd753644a8f5f83d79c50d27a6ad49ab66ab0d382 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_23cf6cb61b8c025c1f4a499b8dc7eec0b1bab2d4725266473a6b18d974cb40b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23cf6cb61b8c025c1f4a499b8dc7eec0b1bab2d4725266473a6b18d974cb40b2->enter($__internal_23cf6cb61b8c025c1f4a499b8dc7eec0b1bab2d4725266473a6b18d974cb40b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_8b0134c710be3c5b48fbd02fa51f0237c5f8f0705cd4631961b3f7cff683626e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b0134c710be3c5b48fbd02fa51f0237c5f8f0705cd4631961b3f7cff683626e->enter($__internal_8b0134c710be3c5b48fbd02fa51f0237c5f8f0705cd4631961b3f7cff683626e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_23cf6cb61b8c025c1f4a499b8dc7eec0b1bab2d4725266473a6b18d974cb40b2->leave($__internal_23cf6cb61b8c025c1f4a499b8dc7eec0b1bab2d4725266473a6b18d974cb40b2_prof);

        
        $__internal_8b0134c710be3c5b48fbd02fa51f0237c5f8f0705cd4631961b3f7cff683626e->leave($__internal_8b0134c710be3c5b48fbd02fa51f0237c5f8f0705cd4631961b3f7cff683626e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.rdf.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
