<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_dd80ed8de64ba2ae67d0945c074f25fc1d4907298679a691e0c575249d4da835 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_887279a270734f44c872d6c42f8808c1bd7a06c6c159388b7e908cad0b60469c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_887279a270734f44c872d6c42f8808c1bd7a06c6c159388b7e908cad0b60469c->enter($__internal_887279a270734f44c872d6c42f8808c1bd7a06c6c159388b7e908cad0b60469c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $__internal_dc7d54d1df76423736709f292d790b4fdbc0f288819ffd9fa7e55e5d881ab1a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dc7d54d1df76423736709f292d790b4fdbc0f288819ffd9fa7e55e5d881ab1a8->enter($__internal_dc7d54d1df76423736709f292d790b4fdbc0f288819ffd9fa7e55e5d881ab1a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_887279a270734f44c872d6c42f8808c1bd7a06c6c159388b7e908cad0b60469c->leave($__internal_887279a270734f44c872d6c42f8808c1bd7a06c6c159388b7e908cad0b60469c_prof);

        
        $__internal_dc7d54d1df76423736709f292d790b4fdbc0f288819ffd9fa7e55e5d881ab1a8->leave($__internal_dc7d54d1df76423736709f292d790b4fdbc0f288819ffd9fa7e55e5d881ab1a8_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_781a1070f4a24c26fcd5376a57a9a36406474760e30fce596e37a1ce99bfbbd2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_781a1070f4a24c26fcd5376a57a9a36406474760e30fce596e37a1ce99bfbbd2->enter($__internal_781a1070f4a24c26fcd5376a57a9a36406474760e30fce596e37a1ce99bfbbd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_f61f5840f888aac5fb6230d0eac8c62e8080da0a7f68d9517135e5cfd46ed7e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f61f5840f888aac5fb6230d0eac8c62e8080da0a7f68d9517135e5cfd46ed7e3->enter($__internal_f61f5840f888aac5fb6230d0eac8c62e8080da0a7f68d9517135e5cfd46ed7e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_f61f5840f888aac5fb6230d0eac8c62e8080da0a7f68d9517135e5cfd46ed7e3->leave($__internal_f61f5840f888aac5fb6230d0eac8c62e8080da0a7f68d9517135e5cfd46ed7e3_prof);

        
        $__internal_781a1070f4a24c26fcd5376a57a9a36406474760e30fce596e37a1ce99bfbbd2->leave($__internal_781a1070f4a24c26fcd5376a57a9a36406474760e30fce596e37a1ce99bfbbd2_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}
