<?php

/* :Upload:detailFile.html.twig */
class __TwigTemplate_9228ec5348da99259919f6975addfcd8071340b5236ab3b4568a6af53aceaaa1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", ":Upload:detailFile.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac5a9a80af10ca1c065f8597e6744945bc0911ad591e61992ea72d3177b2e573 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac5a9a80af10ca1c065f8597e6744945bc0911ad591e61992ea72d3177b2e573->enter($__internal_ac5a9a80af10ca1c065f8597e6744945bc0911ad591e61992ea72d3177b2e573_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Upload:detailFile.html.twig"));

        $__internal_5b9ef33415d77bd30e7b4d8eaf2d868aae9273da9e6f1b9267743e4d54723be6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b9ef33415d77bd30e7b4d8eaf2d868aae9273da9e6f1b9267743e4d54723be6->enter($__internal_5b9ef33415d77bd30e7b4d8eaf2d868aae9273da9e6f1b9267743e4d54723be6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Upload:detailFile.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ac5a9a80af10ca1c065f8597e6744945bc0911ad591e61992ea72d3177b2e573->leave($__internal_ac5a9a80af10ca1c065f8597e6744945bc0911ad591e61992ea72d3177b2e573_prof);

        
        $__internal_5b9ef33415d77bd30e7b4d8eaf2d868aae9273da9e6f1b9267743e4d54723be6->leave($__internal_5b9ef33415d77bd30e7b4d8eaf2d868aae9273da9e6f1b9267743e4d54723be6_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_1d425a8b30c83ad4ed0326936ebe1b3479384c4d4ff66693c0688fb3addf1914 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d425a8b30c83ad4ed0326936ebe1b3479384c4d4ff66693c0688fb3addf1914->enter($__internal_1d425a8b30c83ad4ed0326936ebe1b3479384c4d4ff66693c0688fb3addf1914_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_ba5a3613fa4669627592348ff4346faf75ccba0dee60ca16df7fcbd97a400d16 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba5a3613fa4669627592348ff4346faf75ccba0dee60ca16df7fcbd97a400d16->enter($__internal_ba5a3613fa4669627592348ff4346faf75ccba0dee60ca16df7fcbd97a400d16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "
";
        // line 7
        echo "    <div class=\"col-12 col-md-3 border border-primar rounded\">
            <center><h3>Detail du ficher</h3></center><br/>
            <b>nom: </b>";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute(($context["detail"] ?? $this->getContext($context, "detail")), "name", array()), "html", null, true);
        echo "<br/>
            <b> Date d'ajout: </b>";
        // line 10
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["detail"] ?? $this->getContext($context, "detail")), "dateadd", array()), "Y-m-d H:i:s", "Europe/Paris"), "html", null, true);
        echo "<br/>
            ";
        // line 12
        echo "    </div>
        <div class=\"col-12 col-md-6\">
            <table class=\"table table-bordered\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\">valeur</th>
                </tr>
              </thead>
              <tbody>
                  ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["table"] ?? $this->getContext($context, "table")));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 23
            echo "                <tr>
                    <th>";
            // line 24
            if ($this->getAttribute($context["key"], 0, array(), "array", true, true)) {
                // line 25
                echo "                           ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 0, array(), "array"), "html", null, true);
                echo "
                        ";
            }
            // line 27
            echo "                    </th>
                    <td>
                        ";
            // line 29
            if ($this->getAttribute($context["key"], 2, array(), "array", true, true)) {
                // line 30
                echo "                           ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 2, array(), "array"), "html", null, true);
                echo "
                        ";
            }
            // line 32
            echo "                    </td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "              </tbody>
            </table>
        </div>
        <div class=\"col-12 col-md-1\"></div>
        <div class=\"col-12 col-md-2 border border-primar rounded\">
            <center><h3>Traduire ce fichier</h3></center><br/>
             ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["langues"] ?? $this->getContext($context, "langues")));
        foreach ($context['_seq'] as $context["_key"] => $context["lang"]) {
            // line 42
            echo "             <center>
                <a class=\"btn btn-outline-warning btn-lg\" style=\"width: 50%;\" href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("translate", array("langues" => $context["lang"], "id" => $this->getAttribute(($context["detail"] ?? $this->getContext($context, "detail")), "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["lang"], "html", null, true);
            echo "</a></br></br>
            </center>    
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "            ";
        // line 47
        echo "        </div>
    

";
        // line 51
        echo "    <div class=\"col-12 col-md-3 border border-primar rounded\" style=\"height: 440px;margin-top:  2%;\">
            <center><h3>Personnes ayant traduit ce fichier</h3></center><br/>
             ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["users"] ?? $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 54
            echo "                ";
            if ($this->getAttribute($context["user"], 0, array(), "array", true, true)) {
                // line 55
                echo "                Ce fichier à été en ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], 2, array(), "array"), "html", null, true);
                echo " par ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], 0, array(), "array"), "html", null, true);
                echo "
                <div style=\"margin-left: 70%;margin-top: -5%;\">
                    <a class=\"btn btn-outline-primary\" href=\"";
                // line 57
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("showtranslate", array("langues" => $this->getAttribute($context["user"], 2, array(), "array"), "id_file" => $this->getAttribute(($context["detail"] ?? $this->getContext($context, "detail")), "id", array()), "id_user" => $this->getAttribute($context["user"], 1, array(), "array"))), "html", null, true);
                echo "\">
                        Voire la traduction
                    </a>
                </div></br>
                ";
            } else {
                // line 62
                echo "                    <p>il n'y a aucune traduction pour le moment</p>
                ";
            }
            // line 64
            echo "
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "    </div>
";
        
        $__internal_ba5a3613fa4669627592348ff4346faf75ccba0dee60ca16df7fcbd97a400d16->leave($__internal_ba5a3613fa4669627592348ff4346faf75ccba0dee60ca16df7fcbd97a400d16_prof);

        
        $__internal_1d425a8b30c83ad4ed0326936ebe1b3479384c4d4ff66693c0688fb3addf1914->leave($__internal_1d425a8b30c83ad4ed0326936ebe1b3479384c4d4ff66693c0688fb3addf1914_prof);

    }

    public function getTemplateName()
    {
        return ":Upload:detailFile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 66,  175 => 64,  171 => 62,  163 => 57,  155 => 55,  152 => 54,  148 => 53,  144 => 51,  139 => 47,  137 => 46,  126 => 43,  123 => 42,  119 => 41,  111 => 35,  103 => 32,  97 => 30,  95 => 29,  91 => 27,  85 => 25,  83 => 24,  80 => 23,  76 => 22,  64 => 12,  60 => 10,  56 => 9,  52 => 7,  49 => 4,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block content %}
{#<h1>edit files</h1>#}

{#<div class=\"col-12 col-md-1 border border-primar rounded\">d#}
{#</div>#}
    <div class=\"col-12 col-md-3 border border-primar rounded\">
            <center><h3>Detail du ficher</h3></center><br/>
            <b>nom: </b>{{detail.name}}<br/>
            <b> Date d'ajout: </b>{{detail.dateadd|date(\"Y-m-d H:i:s\", \"Europe/Paris\")}}<br/>
            {#<b>Langue du fichier: </b>{{ detail.langues}}<br/>#}
    </div>
        <div class=\"col-12 col-md-6\">
            <table class=\"table table-bordered\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\">valeur</th>
                </tr>
              </thead>
              <tbody>
                  {% for key in table %}
                <tr>
                    <th>{% if  key[0] is defined %}
                           {{key[0]}}
                        {% endif %}
                    </th>
                    <td>
                        {% if  key[2] is defined %}
                           {{key[2]}}
                        {% endif %}
                    </td>
                </tr>
                {% endfor %}
              </tbody>
            </table>
        </div>
        <div class=\"col-12 col-md-1\"></div>
        <div class=\"col-12 col-md-2 border border-primar rounded\">
            <center><h3>Traduire ce fichier</h3></center><br/>
             {% for lang in langues %}
             <center>
                <a class=\"btn btn-outline-warning btn-lg\" style=\"width: 50%;\" href=\"{{ path('translate',{'langues': lang, 'id':detail.id}) }}\">{{lang }}</a></br></br>
            </center>    
            {% endfor %}
            {#>#}
        </div>
    

{#<div class=\"row\" style=\"margin-left:  1%;margin-top:  5%;margin-right:  1%;\">#}
    <div class=\"col-12 col-md-3 border border-primar rounded\" style=\"height: 440px;margin-top:  2%;\">
            <center><h3>Personnes ayant traduit ce fichier</h3></center><br/>
             {% for user in users %}
                {% if  user[0] is defined %}
                Ce fichier à été en {{user[2]}} par {{user[0]}}
                <div style=\"margin-left: 70%;margin-top: -5%;\">
                    <a class=\"btn btn-outline-primary\" href=\"{{ path('showtranslate',{'langues': user[2], 'id_file':detail.id,'id_user':user[1]}) }}\">
                        Voire la traduction
                    </a>
                </div></br>
                {% else %}
                    <p>il n'y a aucune traduction pour le moment</p>
                {% endif %}

            {% endfor %}
    </div>
{#</div>#}
{% endblock %}", ":Upload:detailFile.html.twig", "/home/ubuntu/workspace/app/Resources/views/Upload/detailFile.html.twig");
    }
}
