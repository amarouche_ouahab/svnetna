<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_85d263fda3536fa210be214f32813fa44d4a758fe5b40f3a43c96bb04cdc6be6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32bd61f70f0320489b70a8be2de62962d8ae15659f531003f84df8001a2d8ae9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_32bd61f70f0320489b70a8be2de62962d8ae15659f531003f84df8001a2d8ae9->enter($__internal_32bd61f70f0320489b70a8be2de62962d8ae15659f531003f84df8001a2d8ae9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_5c86c7be8381e67b58c3d2acbbad4e7cc982ff205731e1435a45020082d74e87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c86c7be8381e67b58c3d2acbbad4e7cc982ff205731e1435a45020082d74e87->enter($__internal_5c86c7be8381e67b58c3d2acbbad4e7cc982ff205731e1435a45020082d74e87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_32bd61f70f0320489b70a8be2de62962d8ae15659f531003f84df8001a2d8ae9->leave($__internal_32bd61f70f0320489b70a8be2de62962d8ae15659f531003f84df8001a2d8ae9_prof);

        
        $__internal_5c86c7be8381e67b58c3d2acbbad4e7cc982ff205731e1435a45020082d74e87->leave($__internal_5c86c7be8381e67b58c3d2acbbad4e7cc982ff205731e1435a45020082d74e87_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
