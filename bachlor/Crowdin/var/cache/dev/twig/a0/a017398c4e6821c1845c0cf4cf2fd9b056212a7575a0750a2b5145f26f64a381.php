<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_e9a2f8da40e319aee86a8b78cb8dddc95f97fe67a2045f1bb7e811209ece971a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f20e4caca949a0198d358cb16e5f2386eb67a6667b11dc52f12ca9257feed746 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f20e4caca949a0198d358cb16e5f2386eb67a6667b11dc52f12ca9257feed746->enter($__internal_f20e4caca949a0198d358cb16e5f2386eb67a6667b11dc52f12ca9257feed746_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_3a2a0a076055af46bf2a206dbaf825c35e417b8f645611f7c2a80c48926be771 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a2a0a076055af46bf2a206dbaf825c35e417b8f645611f7c2a80c48926be771->enter($__internal_3a2a0a076055af46bf2a206dbaf825c35e417b8f645611f7c2a80c48926be771_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_f20e4caca949a0198d358cb16e5f2386eb67a6667b11dc52f12ca9257feed746->leave($__internal_f20e4caca949a0198d358cb16e5f2386eb67a6667b11dc52f12ca9257feed746_prof);

        
        $__internal_3a2a0a076055af46bf2a206dbaf825c35e417b8f645611f7c2a80c48926be771->leave($__internal_3a2a0a076055af46bf2a206dbaf825c35e417b8f645611f7c2a80c48926be771_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
