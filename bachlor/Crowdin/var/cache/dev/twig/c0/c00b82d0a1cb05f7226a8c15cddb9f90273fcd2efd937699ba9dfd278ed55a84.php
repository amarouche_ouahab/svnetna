<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_4513df380544d002e3af11879e71043417aa023ca4a4f1a660cd5ca38d03a4e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a4d449203b5debb296bf04bb6c1fb7517c3ad63f06dbb17a26a6ab3c93d3642 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a4d449203b5debb296bf04bb6c1fb7517c3ad63f06dbb17a26a6ab3c93d3642->enter($__internal_0a4d449203b5debb296bf04bb6c1fb7517c3ad63f06dbb17a26a6ab3c93d3642_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_be304d3c3e2aca1bedd67cb71540f4864a6e414d4e2ac8e3c576ac8bdbebc88f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be304d3c3e2aca1bedd67cb71540f4864a6e414d4e2ac8e3c576ac8bdbebc88f->enter($__internal_be304d3c3e2aca1bedd67cb71540f4864a6e414d4e2ac8e3c576ac8bdbebc88f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_0a4d449203b5debb296bf04bb6c1fb7517c3ad63f06dbb17a26a6ab3c93d3642->leave($__internal_0a4d449203b5debb296bf04bb6c1fb7517c3ad63f06dbb17a26a6ab3c93d3642_prof);

        
        $__internal_be304d3c3e2aca1bedd67cb71540f4864a6e414d4e2ac8e3c576ac8bdbebc88f->leave($__internal_be304d3c3e2aca1bedd67cb71540f4864a6e414d4e2ac8e3c576ac8bdbebc88f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
