<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_54b4561a3fe1b5f6d91d31c2b8ae5ef4de0ab84b7815cc6243bda4acbb90bf0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9423ef82154fa94093a8f2ddb031d09928a77233eb7ba1cf810f88e3431c481f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9423ef82154fa94093a8f2ddb031d09928a77233eb7ba1cf810f88e3431c481f->enter($__internal_9423ef82154fa94093a8f2ddb031d09928a77233eb7ba1cf810f88e3431c481f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_27cf782a4771da4689c878fc210738dc2ab19d01844d4c5814fb505fc0403af9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_27cf782a4771da4689c878fc210738dc2ab19d01844d4c5814fb505fc0403af9->enter($__internal_27cf782a4771da4689c878fc210738dc2ab19d01844d4c5814fb505fc0403af9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_9423ef82154fa94093a8f2ddb031d09928a77233eb7ba1cf810f88e3431c481f->leave($__internal_9423ef82154fa94093a8f2ddb031d09928a77233eb7ba1cf810f88e3431c481f_prof);

        
        $__internal_27cf782a4771da4689c878fc210738dc2ab19d01844d4c5814fb505fc0403af9->leave($__internal_27cf782a4771da4689c878fc210738dc2ab19d01844d4c5814fb505fc0403af9_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
