<?php

/* FOSUserBundle:Security:login_content.html.twig */
class __TwigTemplate_8948c62c411c66ef518f07335445d2a2c5aac13140aa84c4e9a0d92b8ed5fc3f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_721374946a55d05d25907a0c52c8614af0313609e875dfd368e3e3dc5c850a96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_721374946a55d05d25907a0c52c8614af0313609e875dfd368e3e3dc5c850a96->enter($__internal_721374946a55d05d25907a0c52c8614af0313609e875dfd368e3e3dc5c850a96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login_content.html.twig"));

        $__internal_183670dbc34bdb9e0c11e4803e9d15ad042da455d2a8236312b8a667992e0fe4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_183670dbc34bdb9e0c11e4803e9d15ad042da455d2a8236312b8a667992e0fe4->enter($__internal_183670dbc34bdb9e0c11e4803e9d15ad042da455d2a8236312b8a667992e0fe4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_721374946a55d05d25907a0c52c8614af0313609e875dfd368e3e3dc5c850a96->leave($__internal_721374946a55d05d25907a0c52c8614af0313609e875dfd368e3e3dc5c850a96_prof);

        
        $__internal_183670dbc34bdb9e0c11e4803e9d15ad042da455d2a8236312b8a667992e0fe4->leave($__internal_183670dbc34bdb9e0c11e4803e9d15ad042da455d2a8236312b8a667992e0fe4_prof);

    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a2a37b0f84faa4443ea11cdbc93ab9a8a6e46ba15e395db4126546867dd75991 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2a37b0f84faa4443ea11cdbc93ab9a8a6e46ba15e395db4126546867dd75991->enter($__internal_a2a37b0f84faa4443ea11cdbc93ab9a8a6e46ba15e395db4126546867dd75991_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_53342628b34071ba1a9efe72c016d07aff15bf9d95e0244c9e75b1b980e717e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53342628b34071ba1a9efe72c016d07aff15bf9d95e0244c9e75b1b980e717e3->enter($__internal_53342628b34071ba1a9efe72c016d07aff15bf9d95e0244c9e75b1b980e717e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 5
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 7
        echo "<center>
    
    <div class = \"container\" style=\"max-width: 500px;margin-top: 150px;\">
    \t<div class=\"wrapper\">
            <form action=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\"  class=\"form-signin\">
                 <h2 class=\"form-signin-heading\">Welcome Back! Please Sign In</h2>
            \t\t\t  <hr class=\"colorgraph\"><br><br><br>
                <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />
            
                <div class=\"form-group\">
                    <label for=\"username\">";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                    <input type=\"text\" id=\"username\" name=\"_username\"  placeholder=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "\" class=\"form-control\" value=\"";
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />
                </div>
            
                <div class=\"form-group\">
                    <label for=\"password\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                    <input type=\"password\" id=\"password\" name=\"_password\" placeholder=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "\" class=\"form-control\" required=\"required\" />
                </div>
            
                <div class=\"checkbox\">
                    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                    <label for=\"remember_me\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                </div>
            
                <input type=\"submit\"
                       class=\"btn btn-lg btn-success btn-block\"
                       id=\"_submit\"
                       name=\"_submit\"
                       value=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
            </form>
            <a  href=\"register/\">register</a>
        </div>
    </div>
<center>
";
        
        $__internal_53342628b34071ba1a9efe72c016d07aff15bf9d95e0244c9e75b1b980e717e3->leave($__internal_53342628b34071ba1a9efe72c016d07aff15bf9d95e0244c9e75b1b980e717e3_prof);

        
        $__internal_a2a37b0f84faa4443ea11cdbc93ab9a8a6e46ba15e395db4126546867dd75991->leave($__internal_a2a37b0f84faa4443ea11cdbc93ab9a8a6e46ba15e395db4126546867dd75991_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  108 => 35,  98 => 28,  90 => 23,  86 => 22,  77 => 18,  73 => 17,  67 => 14,  61 => 11,  55 => 7,  49 => 5,  47 => 4,  29 => 3,  26 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
{% if error %}
    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}
<center>
    
    <div class = \"container\" style=\"max-width: 500px;margin-top: 150px;\">
    \t<div class=\"wrapper\">
            <form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\"  class=\"form-signin\">
                 <h2 class=\"form-signin-heading\">Welcome Back! Please Sign In</h2>
            \t\t\t  <hr class=\"colorgraph\"><br><br><br>
                <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
            
                <div class=\"form-group\">
                    <label for=\"username\">{{ 'security.login.username'|trans }}</label>
                    <input type=\"text\" id=\"username\" name=\"_username\"  placeholder=\"{{ 'security.login.username'|trans }}\" class=\"form-control\" value=\"{{ last_username }}\" required=\"required\" />
                </div>
            
                <div class=\"form-group\">
                    <label for=\"password\">{{ 'security.login.password'|trans }}</label>
                    <input type=\"password\" id=\"password\" name=\"_password\" placeholder=\"{{ 'security.login.password'|trans }}\" class=\"form-control\" required=\"required\" />
                </div>
            
                <div class=\"checkbox\">
                    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                    <label for=\"remember_me\">{{ 'security.login.remember_me'|trans }}</label>
                </div>
            
                <input type=\"submit\"
                       class=\"btn btn-lg btn-success btn-block\"
                       id=\"_submit\"
                       name=\"_submit\"
                       value=\"{{ 'security.login.submit'|trans }}\" />
            </form>
            <a  href=\"register/\">register</a>
        </div>
    </div>
<center>
{% endblock fos_user_content %}", "FOSUserBundle:Security:login_content.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login_content.html.twig");
    }
}
