<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_f2ce3d98f593e88db329a439fd0b009ac98b891744dd37e65c10627df1b09843 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_728ff4e98413c08aa7d0c449b730149808e586b4cdfc2763d4bc956bd958b22c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_728ff4e98413c08aa7d0c449b730149808e586b4cdfc2763d4bc956bd958b22c->enter($__internal_728ff4e98413c08aa7d0c449b730149808e586b4cdfc2763d4bc956bd958b22c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_93c43e777418c51cd4e626659d509d068473f1d3f70bdbba98e7789d3adec817 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93c43e777418c51cd4e626659d509d068473f1d3f70bdbba98e7789d3adec817->enter($__internal_93c43e777418c51cd4e626659d509d068473f1d3f70bdbba98e7789d3adec817_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_728ff4e98413c08aa7d0c449b730149808e586b4cdfc2763d4bc956bd958b22c->leave($__internal_728ff4e98413c08aa7d0c449b730149808e586b4cdfc2763d4bc956bd958b22c_prof);

        
        $__internal_93c43e777418c51cd4e626659d509d068473f1d3f70bdbba98e7789d3adec817->leave($__internal_93c43e777418c51cd4e626659d509d068473f1d3f70bdbba98e7789d3adec817_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_50ee3a6700b2a3a1601c3b41abb7cbdcbeec560c379255fc8d570ed9f6e6648d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50ee3a6700b2a3a1601c3b41abb7cbdcbeec560c379255fc8d570ed9f6e6648d->enter($__internal_50ee3a6700b2a3a1601c3b41abb7cbdcbeec560c379255fc8d570ed9f6e6648d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_8879deed6ce1841d6553f77e58bfdb14de44456a8408243eb7eeab9d1850249a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8879deed6ce1841d6553f77e58bfdb14de44456a8408243eb7eeab9d1850249a->enter($__internal_8879deed6ce1841d6553f77e58bfdb14de44456a8408243eb7eeab9d1850249a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_8879deed6ce1841d6553f77e58bfdb14de44456a8408243eb7eeab9d1850249a->leave($__internal_8879deed6ce1841d6553f77e58bfdb14de44456a8408243eb7eeab9d1850249a_prof);

        
        $__internal_50ee3a6700b2a3a1601c3b41abb7cbdcbeec560c379255fc8d570ed9f6e6648d->leave($__internal_50ee3a6700b2a3a1601c3b41abb7cbdcbeec560c379255fc8d570ed9f6e6648d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
