<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_ed90f5bfe8c1ee145cd6aaba619a64540cddc06caa0dd42a9cbfbe538d54df87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f18c9e59a361fb03a483361b00e512a863a468448dda2dcf53dbf9e26a00b9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f18c9e59a361fb03a483361b00e512a863a468448dda2dcf53dbf9e26a00b9f->enter($__internal_6f18c9e59a361fb03a483361b00e512a863a468448dda2dcf53dbf9e26a00b9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_9d1fb817a9e4a765be381fce59e7ed06f1b816660fc2d90042c792d717d4c4fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d1fb817a9e4a765be381fce59e7ed06f1b816660fc2d90042c792d717d4c4fe->enter($__internal_9d1fb817a9e4a765be381fce59e7ed06f1b816660fc2d90042c792d717d4c4fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6f18c9e59a361fb03a483361b00e512a863a468448dda2dcf53dbf9e26a00b9f->leave($__internal_6f18c9e59a361fb03a483361b00e512a863a468448dda2dcf53dbf9e26a00b9f_prof);

        
        $__internal_9d1fb817a9e4a765be381fce59e7ed06f1b816660fc2d90042c792d717d4c4fe->leave($__internal_9d1fb817a9e4a765be381fce59e7ed06f1b816660fc2d90042c792d717d4c4fe_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9d6d38d8003c4947cca35a062323eb8a9bc43b5156dc70e6bdd8542bbb10bcb9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d6d38d8003c4947cca35a062323eb8a9bc43b5156dc70e6bdd8542bbb10bcb9->enter($__internal_9d6d38d8003c4947cca35a062323eb8a9bc43b5156dc70e6bdd8542bbb10bcb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_217f2c2fba2ac7be35dc590ad302f398e501c11cb5b6bcf0ccb58576d504ae59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_217f2c2fba2ac7be35dc590ad302f398e501c11cb5b6bcf0ccb58576d504ae59->enter($__internal_217f2c2fba2ac7be35dc590ad302f398e501c11cb5b6bcf0ccb58576d504ae59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_217f2c2fba2ac7be35dc590ad302f398e501c11cb5b6bcf0ccb58576d504ae59->leave($__internal_217f2c2fba2ac7be35dc590ad302f398e501c11cb5b6bcf0ccb58576d504ae59_prof);

        
        $__internal_9d6d38d8003c4947cca35a062323eb8a9bc43b5156dc70e6bdd8542bbb10bcb9->leave($__internal_9d6d38d8003c4947cca35a062323eb8a9bc43b5156dc70e6bdd8542bbb10bcb9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
