<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_03ab82daf6987a52864eb3527cd2eb3cfbaaae570d0afc2e608858f68e62eb10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6acfd2a4edcfa4c09425137e0b5ab4359d563f44bddf4de12f8364223ebcdf9c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6acfd2a4edcfa4c09425137e0b5ab4359d563f44bddf4de12f8364223ebcdf9c->enter($__internal_6acfd2a4edcfa4c09425137e0b5ab4359d563f44bddf4de12f8364223ebcdf9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_4cf0d2748354072bd4cfa070f3ecde3b87bcde8dd1569778d2c856580413e5c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4cf0d2748354072bd4cfa070f3ecde3b87bcde8dd1569778d2c856580413e5c2->enter($__internal_4cf0d2748354072bd4cfa070f3ecde3b87bcde8dd1569778d2c856580413e5c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_6acfd2a4edcfa4c09425137e0b5ab4359d563f44bddf4de12f8364223ebcdf9c->leave($__internal_6acfd2a4edcfa4c09425137e0b5ab4359d563f44bddf4de12f8364223ebcdf9c_prof);

        
        $__internal_4cf0d2748354072bd4cfa070f3ecde3b87bcde8dd1569778d2c856580413e5c2->leave($__internal_4cf0d2748354072bd4cfa070f3ecde3b87bcde8dd1569778d2c856580413e5c2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
