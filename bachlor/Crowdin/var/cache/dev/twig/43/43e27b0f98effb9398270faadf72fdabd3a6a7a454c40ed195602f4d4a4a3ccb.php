<?php

/* :Upload:showFiles.html.twig */
class __TwigTemplate_38501530bdddf1be767cd1245ec5d03230d3b81b3a0ee0b0eadb6170edf4a96c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", ":Upload:showFiles.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78f085d4f3f07a3fadea4a77dcc7b13b57f31261dee7ff106aa778530586e3f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78f085d4f3f07a3fadea4a77dcc7b13b57f31261dee7ff106aa778530586e3f7->enter($__internal_78f085d4f3f07a3fadea4a77dcc7b13b57f31261dee7ff106aa778530586e3f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Upload:showFiles.html.twig"));

        $__internal_66743cff9ebe2fe7e1704acae51299e4d876ac372f4544007d6c0dc6c914754b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_66743cff9ebe2fe7e1704acae51299e4d876ac372f4544007d6c0dc6c914754b->enter($__internal_66743cff9ebe2fe7e1704acae51299e4d876ac372f4544007d6c0dc6c914754b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Upload:showFiles.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_78f085d4f3f07a3fadea4a77dcc7b13b57f31261dee7ff106aa778530586e3f7->leave($__internal_78f085d4f3f07a3fadea4a77dcc7b13b57f31261dee7ff106aa778530586e3f7_prof);

        
        $__internal_66743cff9ebe2fe7e1704acae51299e4d876ac372f4544007d6c0dc6c914754b->leave($__internal_66743cff9ebe2fe7e1704acae51299e4d876ac372f4544007d6c0dc6c914754b_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_ec49712da7d8655a6d2de52f6f08cb1920d180e15a2dc4cd85c3492531d5e979 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec49712da7d8655a6d2de52f6f08cb1920d180e15a2dc4cd85c3492531d5e979->enter($__internal_ec49712da7d8655a6d2de52f6f08cb1920d180e15a2dc4cd85c3492531d5e979_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_5d72e43396d95dd3737ca02d3820dfff2890c803c0d4ed8c8aec825a69fcc176 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d72e43396d95dd3737ca02d3820dfff2890c803c0d4ed8c8aec825a69fcc176->enter($__internal_5d72e43396d95dd3737ca02d3820dfff2890c803c0d4ed8c8aec825a69fcc176_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "
<div class=\"col-12 col-md-2\"></div>
<div class=\"col-12 col-md-8\">
<table class=\"table table-bordered\">
  <thead>
    <tr>
      <th scope=\"col\">Nom du fichier</th>
      <th scope=\"col\">Ajouter par</th>
      <th scope=\"col\">Langues du fichier</th>
      <th scope=\"col\">Langue(s) souhaitée</th>
      <th scope=\"col\">Date d'ajout</th>
      <th scope=\"col\">Action</th>
    </tr>
  </thead>
  <tbody>
      ";
        // line 19
        if (twig_test_empty(($context["tab_files"] ?? $this->getContext($context, "tab_files")))) {
            // line 20
            echo "        <tr>
          <th colspan=\"100\"><center> Aucun fichier</center></th>
        </tr>
      ";
        } else {
            // line 24
            echo "         <tr>
           ";
            // line 26
            echo "          ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tab_files"] ?? $this->getContext($context, "tab_files")));
            foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
                // line 27
                echo "            <tr>
              <th>";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["list"], 0, array(), "array"), "html", null, true);
                echo "</th>
              <td>";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["list"], 1, array(), "array"), "html", null, true);
                echo "</td>
              <td>";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["list"], 2, array(), "array"), "html", null, true);
                echo "</td>
              <td>
                ";
                // line 32
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["list"], 3, array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["lang"]) {
                    // line 33
                    echo "                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["lang"], "langue", array()), "html", null, true);
                    echo "</br>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 35
                echo "              </td>
              <td>";
                // line 36
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["list"], 4, array(), "array"), "Y-m-d H:i:s", "Europe/Paris"), "html", null, true);
                echo "</td>
              <td><a  href=\"";
                // line 37
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailfile", array("id" => $this->getAttribute($context["list"], 5, array(), "array"))), "html", null, true);
                echo "\" class=\"btn btn-lg btn-info\"> detail</a>
              ";
                // line 39
                echo "              </td>
            </tr>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "        ";
            // line 43
            echo "        </tr>
      ";
        }
        // line 44
        echo "    
  </tbody>
</table>
</div>


";
        
        $__internal_5d72e43396d95dd3737ca02d3820dfff2890c803c0d4ed8c8aec825a69fcc176->leave($__internal_5d72e43396d95dd3737ca02d3820dfff2890c803c0d4ed8c8aec825a69fcc176_prof);

        
        $__internal_ec49712da7d8655a6d2de52f6f08cb1920d180e15a2dc4cd85c3492531d5e979->leave($__internal_ec49712da7d8655a6d2de52f6f08cb1920d180e15a2dc4cd85c3492531d5e979_prof);

    }

    public function getTemplateName()
    {
        return ":Upload:showFiles.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 44,  132 => 43,  130 => 42,  122 => 39,  118 => 37,  114 => 36,  111 => 35,  102 => 33,  98 => 32,  93 => 30,  89 => 29,  85 => 28,  82 => 27,  77 => 26,  74 => 24,  68 => 20,  66 => 19,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

{% block content %}

<div class=\"col-12 col-md-2\"></div>
<div class=\"col-12 col-md-8\">
<table class=\"table table-bordered\">
  <thead>
    <tr>
      <th scope=\"col\">Nom du fichier</th>
      <th scope=\"col\">Ajouter par</th>
      <th scope=\"col\">Langues du fichier</th>
      <th scope=\"col\">Langue(s) souhaitée</th>
      <th scope=\"col\">Date d'ajout</th>
      <th scope=\"col\">Action</th>
    </tr>
  </thead>
  <tbody>
      {% if  tab_files is empty %}
        <tr>
          <th colspan=\"100\"><center> Aucun fichier</center></th>
        </tr>
      {% else %}
         <tr>
           {#{% for lists in tab_files %}#}
          {% for list in tab_files %}
            <tr>
              <th>{{ list[0]}}</th>
              <td>{{ list[1]}}</td>
              <td>{{ list[2]}}</td>
              <td>
                {% for lang in list[3] %}
                {{ lang.langue }}</br>
                {% endfor %}
              </td>
              <td>{{ list[4]|date(\"Y-m-d H:i:s\", \"Europe/Paris\")}}</td>
              <td><a  href=\"{{ path('detailfile',{'id': list[5]}) }}\" class=\"btn btn-lg btn-info\"> detail</a>
              {#<a  href=\"{{ path('deleteMyFile',{'id': list.id}) }}\" class=\"btn btn-lg btn-danger\"> supprimer</a>#}
              </td>
            </tr>
          {% endfor %}
        {#{% endfor %}#}
        </tr>
      {% endif %}    
  </tbody>
</table>
</div>


{% endblock %}


{#</h1>#}
{#<p> {{id }}</p>#}", ":Upload:showFiles.html.twig", "/home/ubuntu/workspace/app/Resources/views/Upload/showFiles.html.twig");
    }
}
