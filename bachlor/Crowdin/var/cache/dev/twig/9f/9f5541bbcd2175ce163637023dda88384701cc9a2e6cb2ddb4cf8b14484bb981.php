<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_926fbb00248281e39f564c1f0f3685e3462ec1858d60d486dd7366fd7e2a204c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a3854b2e43097cf1a64e9fef71eb09f82d379a5cd506877e7f9c1c23c5bdd5af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3854b2e43097cf1a64e9fef71eb09f82d379a5cd506877e7f9c1c23c5bdd5af->enter($__internal_a3854b2e43097cf1a64e9fef71eb09f82d379a5cd506877e7f9c1c23c5bdd5af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_0720d5ac8f543cfc051e1783833d8227b263547833391eba8ffbb20d1c1a0e0b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0720d5ac8f543cfc051e1783833d8227b263547833391eba8ffbb20d1c1a0e0b->enter($__internal_0720d5ac8f543cfc051e1783833d8227b263547833391eba8ffbb20d1c1a0e0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_a3854b2e43097cf1a64e9fef71eb09f82d379a5cd506877e7f9c1c23c5bdd5af->leave($__internal_a3854b2e43097cf1a64e9fef71eb09f82d379a5cd506877e7f9c1c23c5bdd5af_prof);

        
        $__internal_0720d5ac8f543cfc051e1783833d8227b263547833391eba8ffbb20d1c1a0e0b->leave($__internal_0720d5ac8f543cfc051e1783833d8227b263547833391eba8ffbb20d1c1a0e0b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
