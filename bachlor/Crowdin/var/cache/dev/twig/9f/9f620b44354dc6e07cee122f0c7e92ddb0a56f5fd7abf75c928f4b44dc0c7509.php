<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_fcffe5d8c8ed1cfa381e3dd3a852bd47bd2a45710ab8e26fa9db2821d5b17f03 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fed327a4da7ddd8f5b400ef42c89ba93eda19f0839fa519fd3b8d309e70b2ee2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fed327a4da7ddd8f5b400ef42c89ba93eda19f0839fa519fd3b8d309e70b2ee2->enter($__internal_fed327a4da7ddd8f5b400ef42c89ba93eda19f0839fa519fd3b8d309e70b2ee2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_4658234c70c49dc604d11cb38d589b59700c005c8f1569c0a3867de36cb06baa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4658234c70c49dc604d11cb38d589b59700c005c8f1569c0a3867de36cb06baa->enter($__internal_4658234c70c49dc604d11cb38d589b59700c005c8f1569c0a3867de36cb06baa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_fed327a4da7ddd8f5b400ef42c89ba93eda19f0839fa519fd3b8d309e70b2ee2->leave($__internal_fed327a4da7ddd8f5b400ef42c89ba93eda19f0839fa519fd3b8d309e70b2ee2_prof);

        
        $__internal_4658234c70c49dc604d11cb38d589b59700c005c8f1569c0a3867de36cb06baa->leave($__internal_4658234c70c49dc604d11cb38d589b59700c005c8f1569c0a3867de36cb06baa_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_090ad7dc1a5fbb293fa91eef75b076c80a76c3a457cf810f9333169c2e193fd0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_090ad7dc1a5fbb293fa91eef75b076c80a76c3a457cf810f9333169c2e193fd0->enter($__internal_090ad7dc1a5fbb293fa91eef75b076c80a76c3a457cf810f9333169c2e193fd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_e88e0d6621d3b9a718ae54a87053315f98f8c3a41bf9916fd7fa2a8c3c7f56dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e88e0d6621d3b9a718ae54a87053315f98f8c3a41bf9916fd7fa2a8c3c7f56dd->enter($__internal_e88e0d6621d3b9a718ae54a87053315f98f8c3a41bf9916fd7fa2a8c3c7f56dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_e88e0d6621d3b9a718ae54a87053315f98f8c3a41bf9916fd7fa2a8c3c7f56dd->leave($__internal_e88e0d6621d3b9a718ae54a87053315f98f8c3a41bf9916fd7fa2a8c3c7f56dd_prof);

        
        $__internal_090ad7dc1a5fbb293fa91eef75b076c80a76c3a457cf810f9333169c2e193fd0->leave($__internal_090ad7dc1a5fbb293fa91eef75b076c80a76c3a457cf810f9333169c2e193fd0_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
