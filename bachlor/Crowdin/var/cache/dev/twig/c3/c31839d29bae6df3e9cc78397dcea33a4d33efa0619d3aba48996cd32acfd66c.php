<?php

/* :translate:translateView.html.twig */
class __TwigTemplate_5303d58659b4428cd2bc580b9ac448f7e5a07dd0bdbd7e83de287d0457ddac87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", ":translate:translateView.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46388b14bcff8028902907daa06e634a0f2b9fea7bba6e438df7e1c5b92d6fc2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46388b14bcff8028902907daa06e634a0f2b9fea7bba6e438df7e1c5b92d6fc2->enter($__internal_46388b14bcff8028902907daa06e634a0f2b9fea7bba6e438df7e1c5b92d6fc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":translate:translateView.html.twig"));

        $__internal_1a3a597616383f8043747f03eaee7ac438c8e6984a0f63fbb8fa97927843f55f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a3a597616383f8043747f03eaee7ac438c8e6984a0f63fbb8fa97927843f55f->enter($__internal_1a3a597616383f8043747f03eaee7ac438c8e6984a0f63fbb8fa97927843f55f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":translate:translateView.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_46388b14bcff8028902907daa06e634a0f2b9fea7bba6e438df7e1c5b92d6fc2->leave($__internal_46388b14bcff8028902907daa06e634a0f2b9fea7bba6e438df7e1c5b92d6fc2_prof);

        
        $__internal_1a3a597616383f8043747f03eaee7ac438c8e6984a0f63fbb8fa97927843f55f->leave($__internal_1a3a597616383f8043747f03eaee7ac438c8e6984a0f63fbb8fa97927843f55f_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_da65dd335f16d98d70c7bddffa7cd4c8618a2c3b352dc61a5ce857c91804cca4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da65dd335f16d98d70c7bddffa7cd4c8618a2c3b352dc61a5ce857c91804cca4->enter($__internal_da65dd335f16d98d70c7bddffa7cd4c8618a2c3b352dc61a5ce857c91804cca4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_041c0f27d2d36c048540e7c84b48ecda21202610b6b9aa9525a81d77778b1ea3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_041c0f27d2d36c048540e7c84b48ecda21202610b6b9aa9525a81d77778b1ea3->enter($__internal_041c0f27d2d36c048540e7c84b48ecda21202610b6b9aa9525a81d77778b1ea3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "
    ";
        // line 5
        echo "    ";
        // line 6
        echo "    ";
        // line 7
        echo "    ";
        // line 8
        echo "    ";
        // line 9
        echo "    ";
        // line 10
        echo "    <div class=\"col-12 col-md-1\"></div>
    <div class=\"col-12 col-md-6 border\">
            <table class=\"table\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\">les valeurs</th>
                </tr>
              </thead>
              <tbody>
                  ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["table"] ?? $this->getContext($context, "table")));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 21
            echo "                <tr>
                  <th>";
            // line 22
            if ($this->getAttribute($context["key"], 0, array(), "array", true, true)) {
                // line 23
                echo "                       ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 0, array(), "array"), "html", null, true);
                echo "
                    ";
            }
            // line 24
            echo "</th>
                  <td>
                    ";
            // line 26
            if ($this->getAttribute($context["key"], 2, array(), "array", true, true)) {
                // line 27
                echo "                       ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 2, array(), "array"), "html", null, true);
                echo "
                    ";
            }
            // line 29
            echo "                    </td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "              </tbody>
            </table>
        </div>
        <div class=\"col-12 col-md-1\"></div>
        <div class=\"col-12 col-md-2 border border-primar rounded\">
           <form name=\"input\" action=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("translate", array("langues" => ($context["langues"] ?? $this->getContext($context, "langues")), "id" => ($context["id"] ?? $this->getContext($context, "id")))), "html", null, true);
        echo "\" method=\"post\">
          <table class=\"table\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\"> 
                  </th>
                </tr>
              </thead>
              <tbody>
                  ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["table"] ?? $this->getContext($context, "table")));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 48
            echo "                <tr>
                    <td>
                          <input type=\"text\" name=\"value[";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 1, array(), "array"), "html", null, true);
            echo "]\"/>
                    </td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "              </tbody>
            </table>
             <input type=\"submit\" name=\"submit\" value=\"Submit\">
            </form>
            <div>
          </div>
        </div>
        
  <div class=\"col-12 col-md-3\"></div>   
  <div class=\"col-12 col-md-6\">  

  ";
        // line 65
        if (array_key_exists("success", $context)) {
            // line 66
            echo "  <div class=\"alert alert-success\">
    <strong>Parfait! </strong> ";
            // line 67
            echo twig_escape_filter($this->env, ($context["success"] ?? $this->getContext($context, "success")), "html", null, true);
            echo "
  </div></br></br>
  <center><a  href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailfile", array("id" => ($context["id"] ?? $this->getContext($context, "id")))), "html", null, true);
            echo "\" class=\"btn btn-lg btn-info\">Acceder au fichier</a>
  </center>
  ";
        }
        // line 72
        echo "</div> 
";
        
        $__internal_041c0f27d2d36c048540e7c84b48ecda21202610b6b9aa9525a81d77778b1ea3->leave($__internal_041c0f27d2d36c048540e7c84b48ecda21202610b6b9aa9525a81d77778b1ea3_prof);

        
        $__internal_da65dd335f16d98d70c7bddffa7cd4c8618a2c3b352dc61a5ce857c91804cca4->leave($__internal_da65dd335f16d98d70c7bddffa7cd4c8618a2c3b352dc61a5ce857c91804cca4_prof);

    }

    public function getTemplateName()
    {
        return ":translate:translateView.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 72,  170 => 69,  165 => 67,  162 => 66,  160 => 65,  147 => 54,  137 => 50,  133 => 48,  129 => 47,  116 => 37,  109 => 32,  101 => 29,  95 => 27,  93 => 26,  89 => 24,  83 => 23,  81 => 22,  78 => 21,  74 => 20,  62 => 10,  60 => 9,  58 => 8,  56 => 7,  54 => 6,  52 => 5,  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}
{% block content %}

    {#<div class=\"col-12 col-md-2 border border-primar rounded\">#}
    {#<center><h3>Detail du ficher</h3></center><br/>#}
    {# <b>nom: </b>{{detail.name}}<br/>#}
    {#<b> Date d'ajout: </b>{{detail.dateadd|date(\"Y-m-d H:i:s\", \"Europe/Paris\")}}<br/>#}
    {# <b>Langue du fichier: </b>{{ detail.langues}}<br/>#}
    {#</div>#}
    <div class=\"col-12 col-md-1\"></div>
    <div class=\"col-12 col-md-6 border\">
            <table class=\"table\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\">les valeurs</th>
                </tr>
              </thead>
              <tbody>
                  {% for key in table %}
                <tr>
                  <th>{% if  key[0] is defined %}
                       {{key[0]}}
                    {% endif %}</th>
                  <td>
                    {% if  key[2] is defined %}
                       {{key[2]}}
                    {% endif %}
                    </td>
                </tr>
                {% endfor %}
              </tbody>
            </table>
        </div>
        <div class=\"col-12 col-md-1\"></div>
        <div class=\"col-12 col-md-2 border border-primar rounded\">
           <form name=\"input\" action=\"{{ path('translate', {'langues': langues, 'id': id}) }}\" method=\"post\">
          <table class=\"table\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\"> 
                  </th>
                </tr>
              </thead>
              <tbody>
                  {% for key in table %}
                <tr>
                    <td>
                          <input type=\"text\" name=\"value[{{key[1]}}]\"/>
                    </td>
                </tr>
                {% endfor %}
              </tbody>
            </table>
             <input type=\"submit\" name=\"submit\" value=\"Submit\">
            </form>
            <div>
          </div>
        </div>
        
  <div class=\"col-12 col-md-3\"></div>   
  <div class=\"col-12 col-md-6\">  

  {% if  success is defined %}
  <div class=\"alert alert-success\">
    <strong>Parfait! </strong> {{success}}
  </div></br></br>
  <center><a  href=\"{{ path('detailfile',{'id': id})}}\" class=\"btn btn-lg btn-info\">Acceder au fichier</a>
  </center>
  {% endif %}
</div> 
{% endblock %}", ":translate:translateView.html.twig", "/home/ubuntu/workspace/app/Resources/views/translate/translateView.html.twig");
    }
}
