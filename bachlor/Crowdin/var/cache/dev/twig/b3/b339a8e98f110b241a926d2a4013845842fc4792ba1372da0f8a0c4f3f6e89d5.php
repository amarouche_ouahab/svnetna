<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_9867fd41d7fb74166c596192f250fcb9bbd04f1b21b5974cd52d2caeee7a977d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_addc73fbdecdce0a70f723f3c5d25c0a4b1397a26d0fabfb272ea3942d232010 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_addc73fbdecdce0a70f723f3c5d25c0a4b1397a26d0fabfb272ea3942d232010->enter($__internal_addc73fbdecdce0a70f723f3c5d25c0a4b1397a26d0fabfb272ea3942d232010_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_a21f79ad9ef7b70c78cbeb5462880fad8528ee21eef4f2a9d9e6e72e3c305851 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a21f79ad9ef7b70c78cbeb5462880fad8528ee21eef4f2a9d9e6e72e3c305851->enter($__internal_a21f79ad9ef7b70c78cbeb5462880fad8528ee21eef4f2a9d9e6e72e3c305851_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, ($context["widget"] ?? $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_addc73fbdecdce0a70f723f3c5d25c0a4b1397a26d0fabfb272ea3942d232010->leave($__internal_addc73fbdecdce0a70f723f3c5d25c0a4b1397a26d0fabfb272ea3942d232010_prof);

        
        $__internal_a21f79ad9ef7b70c78cbeb5462880fad8528ee21eef4f2a9d9e6e72e3c305851->leave($__internal_a21f79ad9ef7b70c78cbeb5462880fad8528ee21eef4f2a9d9e6e72e3c305851_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/money_widget.html.php");
    }
}
