<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_95d45569f269284775ac3ce651b5c9a1a3aed8b575d3ff48dc9e6620c9c2df97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ccd805c6c4566c40bb55dcfc8f30873dc1970d0cfd08c4dcb1e07d7717ae81a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ccd805c6c4566c40bb55dcfc8f30873dc1970d0cfd08c4dcb1e07d7717ae81a6->enter($__internal_ccd805c6c4566c40bb55dcfc8f30873dc1970d0cfd08c4dcb1e07d7717ae81a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_0b49e95285ddf5125de6d3785f854138c1b5ed2fd626822424c3e91eacdcd8c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b49e95285ddf5125de6d3785f854138c1b5ed2fd626822424c3e91eacdcd8c9->enter($__internal_0b49e95285ddf5125de6d3785f854138c1b5ed2fd626822424c3e91eacdcd8c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_ccd805c6c4566c40bb55dcfc8f30873dc1970d0cfd08c4dcb1e07d7717ae81a6->leave($__internal_ccd805c6c4566c40bb55dcfc8f30873dc1970d0cfd08c4dcb1e07d7717ae81a6_prof);

        
        $__internal_0b49e95285ddf5125de6d3785f854138c1b5ed2fd626822424c3e91eacdcd8c9->leave($__internal_0b49e95285ddf5125de6d3785f854138c1b5ed2fd626822424c3e91eacdcd8c9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
