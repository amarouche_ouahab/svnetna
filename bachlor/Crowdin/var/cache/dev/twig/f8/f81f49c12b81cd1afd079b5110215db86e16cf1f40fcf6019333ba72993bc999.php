<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_25e70c3831517c9e87996d91de525ba22dee8102131db892f07c07e5b6590319 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd3d6f63b0b5e0cee121cedc8d6f1f0612637740fae0bde8902ec18c107b0283 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd3d6f63b0b5e0cee121cedc8d6f1f0612637740fae0bde8902ec18c107b0283->enter($__internal_cd3d6f63b0b5e0cee121cedc8d6f1f0612637740fae0bde8902ec18c107b0283_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register_content.html.twig"));

        $__internal_fe433251ad5442ec40bfbedf242ca0bc253e1b158754b287da019c9a05ee5909 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe433251ad5442ec40bfbedf242ca0bc253e1b158754b287da019c9a05ee5909->enter($__internal_fe433251ad5442ec40bfbedf242ca0bc253e1b158754b287da019c9a05ee5909_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register_content.html.twig"));

        // line 2
        echo "
<center>
    <div class = \"container\" style=\"max-width: 500px;margin-top: 120px;\">
        <h3 class=\"form-signin-heading\">Welcome Back! Please Sign In</h3>
        <hr class=\"colorgraph\"><br><br>
        ";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
        ";
        // line 8
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <br><br>
        <div>
            <input type=\"submit\"  class=\"btn btn-lg btn-success btn-block\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
        </div>
        ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    </div>
</center>";
        
        $__internal_cd3d6f63b0b5e0cee121cedc8d6f1f0612637740fae0bde8902ec18c107b0283->leave($__internal_cd3d6f63b0b5e0cee121cedc8d6f1f0612637740fae0bde8902ec18c107b0283_prof);

        
        $__internal_fe433251ad5442ec40bfbedf242ca0bc253e1b158754b287da019c9a05ee5909->leave($__internal_fe433251ad5442ec40bfbedf242ca0bc253e1b158754b287da019c9a05ee5909_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 13,  42 => 11,  36 => 8,  32 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<center>
    <div class = \"container\" style=\"max-width: 500px;margin-top: 120px;\">
        <h3 class=\"form-signin-heading\">Welcome Back! Please Sign In</h3>
        <hr class=\"colorgraph\"><br><br>
        {{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}
        {{ form_widget(form) }}
        <br><br>
        <div>
            <input type=\"submit\"  class=\"btn btn-lg btn-success btn-block\" value=\"{{ 'registration.submit'|trans }}\" />
        </div>
        {{ form_end(form) }}
    </div>
</center>", "FOSUserBundle:Registration:register_content.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register_content.html.twig");
    }
}
