<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_6800a6840dc43dc6d4ff9f128a3911935bc27e3942ae8de2491a4846539ceb8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e4b75f3661996f36391932cb2ebf98fe8603178be5dcf0b89880e58d427931cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e4b75f3661996f36391932cb2ebf98fe8603178be5dcf0b89880e58d427931cb->enter($__internal_e4b75f3661996f36391932cb2ebf98fe8603178be5dcf0b89880e58d427931cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_1c10caeed79c9b7c46b0c41534c29a2658b7a515969e21978109a8d96829c6bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c10caeed79c9b7c46b0c41534c29a2658b7a515969e21978109a8d96829c6bc->enter($__internal_1c10caeed79c9b7c46b0c41534c29a2658b7a515969e21978109a8d96829c6bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_e4b75f3661996f36391932cb2ebf98fe8603178be5dcf0b89880e58d427931cb->leave($__internal_e4b75f3661996f36391932cb2ebf98fe8603178be5dcf0b89880e58d427931cb_prof);

        
        $__internal_1c10caeed79c9b7c46b0c41534c29a2658b7a515969e21978109a8d96829c6bc->leave($__internal_1c10caeed79c9b7c46b0c41534c29a2658b7a515969e21978109a8d96829c6bc_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
