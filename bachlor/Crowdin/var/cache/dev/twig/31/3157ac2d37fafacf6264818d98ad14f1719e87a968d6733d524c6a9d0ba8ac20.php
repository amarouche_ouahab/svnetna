<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_15fa605dca1f8dfa058cbb0f42d0d0f799bc7f1b13471a84c75cd0b5ffc6e565 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b5bf2b9b28d9009d19051a89ee174b54b2cfd8d48b00e4b255afa67cde52c9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b5bf2b9b28d9009d19051a89ee174b54b2cfd8d48b00e4b255afa67cde52c9f->enter($__internal_7b5bf2b9b28d9009d19051a89ee174b54b2cfd8d48b00e4b255afa67cde52c9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_60ab9ca6cfaf7d7a9f6f21b9f4789551cb291f9cd7a3c29776c62a54c22be15f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60ab9ca6cfaf7d7a9f6f21b9f4789551cb291f9cd7a3c29776c62a54c22be15f->enter($__internal_60ab9ca6cfaf7d7a9f6f21b9f4789551cb291f9cd7a3c29776c62a54c22be15f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_7b5bf2b9b28d9009d19051a89ee174b54b2cfd8d48b00e4b255afa67cde52c9f->leave($__internal_7b5bf2b9b28d9009d19051a89ee174b54b2cfd8d48b00e4b255afa67cde52c9f_prof);

        
        $__internal_60ab9ca6cfaf7d7a9f6f21b9f4789551cb291f9cd7a3c29776c62a54c22be15f->leave($__internal_60ab9ca6cfaf7d7a9f6f21b9f4789551cb291f9cd7a3c29776c62a54c22be15f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
