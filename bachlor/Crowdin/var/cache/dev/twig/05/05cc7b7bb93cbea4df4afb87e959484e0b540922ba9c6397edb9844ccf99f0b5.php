<?php

/* form_table_layout.html.twig */
class __TwigTemplate_14d93923444e4d7602d17a7c9c873a61087089c12d2619d0713a36002886fcb6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "form_table_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'hidden_row' => array($this, 'block_hidden_row'),
                'form_widget_compound' => array($this, 'block_form_widget_compound'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_74a333931456eacd4d31a457c7b32cc79134ba0d145e98a8f075ab637b3c1ad8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_74a333931456eacd4d31a457c7b32cc79134ba0d145e98a8f075ab637b3c1ad8->enter($__internal_74a333931456eacd4d31a457c7b32cc79134ba0d145e98a8f075ab637b3c1ad8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form_table_layout.html.twig"));

        $__internal_2a1f44146b6a53dab8903a09efeac865dc98d34a0868ac1bb9a0f3f5901acf99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a1f44146b6a53dab8903a09efeac865dc98d34a0868ac1bb9a0f3f5901acf99->enter($__internal_2a1f44146b6a53dab8903a09efeac865dc98d34a0868ac1bb9a0f3f5901acf99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form_table_layout.html.twig"));

        // line 3
        $this->displayBlock('form_row', $context, $blocks);
        // line 15
        $this->displayBlock('button_row', $context, $blocks);
        // line 24
        $this->displayBlock('hidden_row', $context, $blocks);
        // line 32
        $this->displayBlock('form_widget_compound', $context, $blocks);
        
        $__internal_74a333931456eacd4d31a457c7b32cc79134ba0d145e98a8f075ab637b3c1ad8->leave($__internal_74a333931456eacd4d31a457c7b32cc79134ba0d145e98a8f075ab637b3c1ad8_prof);

        
        $__internal_2a1f44146b6a53dab8903a09efeac865dc98d34a0868ac1bb9a0f3f5901acf99->leave($__internal_2a1f44146b6a53dab8903a09efeac865dc98d34a0868ac1bb9a0f3f5901acf99_prof);

    }

    // line 3
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_5dab098fc7ee6d12a56e6a56ccd37d4d2f5883d5747603fc104f084838e98f3e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5dab098fc7ee6d12a56e6a56ccd37d4d2f5883d5747603fc104f084838e98f3e->enter($__internal_5dab098fc7ee6d12a56e6a56ccd37d4d2f5883d5747603fc104f084838e98f3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_5f394814fa7704f4ad4b5e2fcac243555ada14c62297c692cd49e0c8d3670415 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f394814fa7704f4ad4b5e2fcac243555ada14c62297c692cd49e0c8d3670415->enter($__internal_5f394814fa7704f4ad4b5e2fcac243555ada14c62297c692cd49e0c8d3670415_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 4
        echo "<tr>
        <td>";
        // line 6
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 7
        echo "</td>
        <td>";
        // line 9
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 11
        echo "</td>
    </tr>";
        
        $__internal_5f394814fa7704f4ad4b5e2fcac243555ada14c62297c692cd49e0c8d3670415->leave($__internal_5f394814fa7704f4ad4b5e2fcac243555ada14c62297c692cd49e0c8d3670415_prof);

        
        $__internal_5dab098fc7ee6d12a56e6a56ccd37d4d2f5883d5747603fc104f084838e98f3e->leave($__internal_5dab098fc7ee6d12a56e6a56ccd37d4d2f5883d5747603fc104f084838e98f3e_prof);

    }

    // line 15
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_ea04542ebba9c95061728c1edb7e2d7081ac697be5c435428be68821f4f43e2f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea04542ebba9c95061728c1edb7e2d7081ac697be5c435428be68821f4f43e2f->enter($__internal_ea04542ebba9c95061728c1edb7e2d7081ac697be5c435428be68821f4f43e2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_e7295f17541aac75636eaf236d5823ea928550f0f12110ae97f5ac67ae1e9cad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7295f17541aac75636eaf236d5823ea928550f0f12110ae97f5ac67ae1e9cad->enter($__internal_e7295f17541aac75636eaf236d5823ea928550f0f12110ae97f5ac67ae1e9cad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 16
        echo "<tr>
        <td></td>
        <td>";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 20
        echo "</td>
    </tr>";
        
        $__internal_e7295f17541aac75636eaf236d5823ea928550f0f12110ae97f5ac67ae1e9cad->leave($__internal_e7295f17541aac75636eaf236d5823ea928550f0f12110ae97f5ac67ae1e9cad_prof);

        
        $__internal_ea04542ebba9c95061728c1edb7e2d7081ac697be5c435428be68821f4f43e2f->leave($__internal_ea04542ebba9c95061728c1edb7e2d7081ac697be5c435428be68821f4f43e2f_prof);

    }

    // line 24
    public function block_hidden_row($context, array $blocks = array())
    {
        $__internal_f41b0dfa45d3ff368d3a78f766bd1a10d22870733a4e9409283647c8e4d8f941 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f41b0dfa45d3ff368d3a78f766bd1a10d22870733a4e9409283647c8e4d8f941->enter($__internal_f41b0dfa45d3ff368d3a78f766bd1a10d22870733a4e9409283647c8e4d8f941_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        $__internal_0a468e8d0dad83d4d15a1688982283e17cf4f69d81f88a5e8abac7d8e1a07463 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a468e8d0dad83d4d15a1688982283e17cf4f69d81f88a5e8abac7d8e1a07463->enter($__internal_0a468e8d0dad83d4d15a1688982283e17cf4f69d81f88a5e8abac7d8e1a07463_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        // line 25
        echo "<tr style=\"display: none\">
        <td colspan=\"2\">";
        // line 27
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 28
        echo "</td>
    </tr>";
        
        $__internal_0a468e8d0dad83d4d15a1688982283e17cf4f69d81f88a5e8abac7d8e1a07463->leave($__internal_0a468e8d0dad83d4d15a1688982283e17cf4f69d81f88a5e8abac7d8e1a07463_prof);

        
        $__internal_f41b0dfa45d3ff368d3a78f766bd1a10d22870733a4e9409283647c8e4d8f941->leave($__internal_f41b0dfa45d3ff368d3a78f766bd1a10d22870733a4e9409283647c8e4d8f941_prof);

    }

    // line 32
    public function block_form_widget_compound($context, array $blocks = array())
    {
        $__internal_34dbdd1bbf352ec87a3318ece641b4402bfd3514c5e448325d5beef5f79e22b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34dbdd1bbf352ec87a3318ece641b4402bfd3514c5e448325d5beef5f79e22b5->enter($__internal_34dbdd1bbf352ec87a3318ece641b4402bfd3514c5e448325d5beef5f79e22b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        $__internal_64b49dbfb052110315ee1eeeeed269c0fe122b628e613cc00058ee045129192e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64b49dbfb052110315ee1eeeeed269c0fe122b628e613cc00058ee045129192e->enter($__internal_64b49dbfb052110315ee1eeeeed269c0fe122b628e613cc00058ee045129192e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        // line 33
        echo "<table ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">";
        // line 34
        if ((Symfony\Bridge\Twig\Extension\twig_is_root_form(($context["form"] ?? $this->getContext($context, "form"))) && (twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0))) {
            // line 35
            echo "<tr>
            <td colspan=\"2\">";
            // line 37
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
            // line 38
            echo "</td>
        </tr>";
        }
        // line 41
        $this->displayBlock("form_rows", $context, $blocks);
        // line 42
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'rest');
        // line 43
        echo "</table>";
        
        $__internal_64b49dbfb052110315ee1eeeeed269c0fe122b628e613cc00058ee045129192e->leave($__internal_64b49dbfb052110315ee1eeeeed269c0fe122b628e613cc00058ee045129192e_prof);

        
        $__internal_34dbdd1bbf352ec87a3318ece641b4402bfd3514c5e448325d5beef5f79e22b5->leave($__internal_34dbdd1bbf352ec87a3318ece641b4402bfd3514c5e448325d5beef5f79e22b5_prof);

    }

    public function getTemplateName()
    {
        return "form_table_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  168 => 43,  166 => 42,  164 => 41,  160 => 38,  158 => 37,  155 => 35,  153 => 34,  149 => 33,  140 => 32,  129 => 28,  127 => 27,  124 => 25,  115 => 24,  104 => 20,  102 => 19,  98 => 16,  89 => 15,  78 => 11,  76 => 10,  74 => 9,  71 => 7,  69 => 6,  66 => 4,  57 => 3,  47 => 32,  45 => 24,  43 => 15,  41 => 3,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"form_div_layout.html.twig\" %}

{%- block form_row -%}
    <tr>
        <td>
            {{- form_label(form) -}}
        </td>
        <td>
            {{- form_errors(form) -}}
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock form_row -%}

{%- block button_row -%}
    <tr>
        <td></td>
        <td>
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock button_row -%}

{%- block hidden_row -%}
    <tr style=\"display: none\">
        <td colspan=\"2\">
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock hidden_row -%}

{%- block form_widget_compound -%}
    <table {{ block('widget_container_attributes') }}>
        {%- if form is rootform and errors|length > 0 -%}
        <tr>
            <td colspan=\"2\">
                {{- form_errors(form) -}}
            </td>
        </tr>
        {%- endif -%}
        {{- block('form_rows') -}}
        {{- form_rest(form) -}}
    </table>
{%- endblock form_widget_compound -%}
", "form_table_layout.html.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/form_table_layout.html.twig");
    }
}
