<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_f7c439d73aeea9cf66b0f64ae6ea47c1c1e8e566ad8d5f062b25432dce9386bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3971434861394fde5f09c06537419ee7771c0b3cba9f5e43456d4621945a0c6a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3971434861394fde5f09c06537419ee7771c0b3cba9f5e43456d4621945a0c6a->enter($__internal_3971434861394fde5f09c06537419ee7771c0b3cba9f5e43456d4621945a0c6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_b8636a9a520b4ec2ebc1adbdac529dea6aa43cb8f7f04d8577a06c5f21d15a2c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8636a9a520b4ec2ebc1adbdac529dea6aa43cb8f7f04d8577a06c5f21d15a2c->enter($__internal_b8636a9a520b4ec2ebc1adbdac529dea6aa43cb8f7f04d8577a06c5f21d15a2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_3971434861394fde5f09c06537419ee7771c0b3cba9f5e43456d4621945a0c6a->leave($__internal_3971434861394fde5f09c06537419ee7771c0b3cba9f5e43456d4621945a0c6a_prof);

        
        $__internal_b8636a9a520b4ec2ebc1adbdac529dea6aa43cb8f7f04d8577a06c5f21d15a2c->leave($__internal_b8636a9a520b4ec2ebc1adbdac529dea6aa43cb8f7f04d8577a06c5f21d15a2c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.atom.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
