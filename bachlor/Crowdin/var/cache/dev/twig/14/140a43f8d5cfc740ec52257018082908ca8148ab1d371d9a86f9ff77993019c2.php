<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_546e3417c4245744cdf89eeafd3049914faf151e2d21b951528161a08f472951 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e7afc442394526309b8e17477585c73eb9e4463843bf9a5ee08d1d268b61ce78 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7afc442394526309b8e17477585c73eb9e4463843bf9a5ee08d1d268b61ce78->enter($__internal_e7afc442394526309b8e17477585c73eb9e4463843bf9a5ee08d1d268b61ce78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_a7611b2545f429ebb714c7033cadafd7769c254abac485344591afd7f62a52c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7611b2545f429ebb714c7033cadafd7769c254abac485344591afd7f62a52c2->enter($__internal_a7611b2545f429ebb714c7033cadafd7769c254abac485344591afd7f62a52c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_e7afc442394526309b8e17477585c73eb9e4463843bf9a5ee08d1d268b61ce78->leave($__internal_e7afc442394526309b8e17477585c73eb9e4463843bf9a5ee08d1d268b61ce78_prof);

        
        $__internal_a7611b2545f429ebb714c7033cadafd7769c254abac485344591afd7f62a52c2->leave($__internal_a7611b2545f429ebb714c7033cadafd7769c254abac485344591afd7f62a52c2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
