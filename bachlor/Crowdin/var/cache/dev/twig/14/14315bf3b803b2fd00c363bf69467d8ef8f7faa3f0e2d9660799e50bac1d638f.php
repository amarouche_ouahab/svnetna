<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_433a0e3680e62b94df4dfe5afe81fc00f90e47ec8ca3f3ecc0c08a53cfdb9dcd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_794af927e4086c6fdcc4786872d043f11b4ee783771bae296584fefed0a768df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_794af927e4086c6fdcc4786872d043f11b4ee783771bae296584fefed0a768df->enter($__internal_794af927e4086c6fdcc4786872d043f11b4ee783771bae296584fefed0a768df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_745508401d63331b50c5a3d81ebd444362fe4bdb06bbbdb486b8015439bdc1c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_745508401d63331b50c5a3d81ebd444362fe4bdb06bbbdb486b8015439bdc1c2->enter($__internal_745508401d63331b50c5a3d81ebd444362fe4bdb06bbbdb486b8015439bdc1c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_794af927e4086c6fdcc4786872d043f11b4ee783771bae296584fefed0a768df->leave($__internal_794af927e4086c6fdcc4786872d043f11b4ee783771bae296584fefed0a768df_prof);

        
        $__internal_745508401d63331b50c5a3d81ebd444362fe4bdb06bbbdb486b8015439bdc1c2->leave($__internal_745508401d63331b50c5a3d81ebd444362fe4bdb06bbbdb486b8015439bdc1c2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
