<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_3d8a03a1f444dd2eecae034b7d3394a80be29ccf07747649e8f7890c4b9beedf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_671171d9bc2fd26991aa45636d8952c9385caa90a4fceab3491548f16a869a57 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_671171d9bc2fd26991aa45636d8952c9385caa90a4fceab3491548f16a869a57->enter($__internal_671171d9bc2fd26991aa45636d8952c9385caa90a4fceab3491548f16a869a57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_cf6e9a40f0f41f9a0c25573dbf8113d4ad629b2eac21e2c45496895352df0030 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf6e9a40f0f41f9a0c25573dbf8113d4ad629b2eac21e2c45496895352df0030->enter($__internal_cf6e9a40f0f41f9a0c25573dbf8113d4ad629b2eac21e2c45496895352df0030_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_671171d9bc2fd26991aa45636d8952c9385caa90a4fceab3491548f16a869a57->leave($__internal_671171d9bc2fd26991aa45636d8952c9385caa90a4fceab3491548f16a869a57_prof);

        
        $__internal_cf6e9a40f0f41f9a0c25573dbf8113d4ad629b2eac21e2c45496895352df0030->leave($__internal_cf6e9a40f0f41f9a0c25573dbf8113d4ad629b2eac21e2c45496895352df0030_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
