<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_4d7fd10f69a1bfb994ca0ca60b4f238ec8a278e96d5d9c1a10477f7bc2da8789 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0cc7455911d37160f25d34869620f6c1a482b0d24500419fc2157845443bf974 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0cc7455911d37160f25d34869620f6c1a482b0d24500419fc2157845443bf974->enter($__internal_0cc7455911d37160f25d34869620f6c1a482b0d24500419fc2157845443bf974_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_42c6fcd10d0e54321a228ff3747b30b016fa4afe9ec03f3c2e747052dcaf2feb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42c6fcd10d0e54321a228ff3747b30b016fa4afe9ec03f3c2e747052dcaf2feb->enter($__internal_42c6fcd10d0e54321a228ff3747b30b016fa4afe9ec03f3c2e747052dcaf2feb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_0cc7455911d37160f25d34869620f6c1a482b0d24500419fc2157845443bf974->leave($__internal_0cc7455911d37160f25d34869620f6c1a482b0d24500419fc2157845443bf974_prof);

        
        $__internal_42c6fcd10d0e54321a228ff3747b30b016fa4afe9ec03f3c2e747052dcaf2feb->leave($__internal_42c6fcd10d0e54321a228ff3747b30b016fa4afe9ec03f3c2e747052dcaf2feb_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
