<?php

/* :Upload:showMyFiles.html.twig */
class __TwigTemplate_c15e780d21b8d6016ab298a9d098490db966847e2de1b5d06637a4e96f84a211 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", ":Upload:showMyFiles.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0908e2f10a1f718d92afd6e8eb3018eb0a74973136e165e30774ec014d093db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0908e2f10a1f718d92afd6e8eb3018eb0a74973136e165e30774ec014d093db->enter($__internal_e0908e2f10a1f718d92afd6e8eb3018eb0a74973136e165e30774ec014d093db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Upload:showMyFiles.html.twig"));

        $__internal_d3fc23b99925dec096eac19b8132a10a7140c5def8fe9456c820e75d80cdb816 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3fc23b99925dec096eac19b8132a10a7140c5def8fe9456c820e75d80cdb816->enter($__internal_d3fc23b99925dec096eac19b8132a10a7140c5def8fe9456c820e75d80cdb816_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Upload:showMyFiles.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e0908e2f10a1f718d92afd6e8eb3018eb0a74973136e165e30774ec014d093db->leave($__internal_e0908e2f10a1f718d92afd6e8eb3018eb0a74973136e165e30774ec014d093db_prof);

        
        $__internal_d3fc23b99925dec096eac19b8132a10a7140c5def8fe9456c820e75d80cdb816->leave($__internal_d3fc23b99925dec096eac19b8132a10a7140c5def8fe9456c820e75d80cdb816_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_763da0f7f99bd7bb262488f6f3b6424306cfa55a87c8ec481a0f3395c34930c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_763da0f7f99bd7bb262488f6f3b6424306cfa55a87c8ec481a0f3395c34930c0->enter($__internal_763da0f7f99bd7bb262488f6f3b6424306cfa55a87c8ec481a0f3395c34930c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_599c432da6ce4a84d3143ebd0f630ae3076f57deb5a7c3e17990ed15a2de48e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_599c432da6ce4a84d3143ebd0f630ae3076f57deb5a7c3e17990ed15a2de48e8->enter($__internal_599c432da6ce4a84d3143ebd0f630ae3076f57deb5a7c3e17990ed15a2de48e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "
<div class=\"col-12 col-md-2\"></div>
<div class=\"col-12 col-md-8\">
<table class=\"table table-bordered\">
  <thead>
    <tr>
      <th scope=\"col\">Nom du fichier</th>
      <th scope=\"col\">Ajouter par</th>
      <th scope=\"col\">Langues du fichier</th>
      <th scope=\"col\">Langue(s) souhaitée</th>
      <th scope=\"col\">Date d'ajout</th>
      <th scope=\"col\">Action</th>
    </tr>
  </thead>
  <tbody>
      ";
        // line 19
        if (twig_test_empty(($context["lists"] ?? $this->getContext($context, "lists")))) {
            // line 20
            echo "        <tr>
          <th colspan=\"100\"><center> Aucun fichier</center></th>
        </tr>
      ";
        } else {
            // line 24
            echo "         <tr>
          ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["lists"] ?? $this->getContext($context, "lists")));
            foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
                // line 26
                echo "            <tr>
              <th>";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "name", array()), "html", null, true);
                echo "</th>
              <td>";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["list"], "upload", array()), "html", null, true);
                echo "</td>
              <td></td>
              <td>
                ";
                // line 31
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["list"], "langues", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["lang"]) {
                    // line 32
                    echo "                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["lang"], "langue", array()), "html", null, true);
                    echo "</br>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 34
                echo "              </td>
              <td>";
                // line 35
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["list"], "dateadd", array()), "Y-m-d H:i:s", "Europe/Paris"), "html", null, true);
                echo "</td>
              <td><a  href=\"";
                // line 36
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailfile", array("id" => $this->getAttribute($context["list"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-lg btn-info\"> detail</a>
              ";
                // line 38
                echo "              </td>
            </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 41
            echo "        </tr>
      ";
        }
        // line 42
        echo "    
  </tbody>
</table>
</div>
";
        
        $__internal_599c432da6ce4a84d3143ebd0f630ae3076f57deb5a7c3e17990ed15a2de48e8->leave($__internal_599c432da6ce4a84d3143ebd0f630ae3076f57deb5a7c3e17990ed15a2de48e8_prof);

        
        $__internal_763da0f7f99bd7bb262488f6f3b6424306cfa55a87c8ec481a0f3395c34930c0->leave($__internal_763da0f7f99bd7bb262488f6f3b6424306cfa55a87c8ec481a0f3395c34930c0_prof);

    }

    public function getTemplateName()
    {
        return ":Upload:showMyFiles.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 42,  126 => 41,  118 => 38,  114 => 36,  110 => 35,  107 => 34,  98 => 32,  94 => 31,  88 => 28,  84 => 27,  81 => 26,  77 => 25,  74 => 24,  68 => 20,  66 => 19,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

{% block content %}

<div class=\"col-12 col-md-2\"></div>
<div class=\"col-12 col-md-8\">
<table class=\"table table-bordered\">
  <thead>
    <tr>
      <th scope=\"col\">Nom du fichier</th>
      <th scope=\"col\">Ajouter par</th>
      <th scope=\"col\">Langues du fichier</th>
      <th scope=\"col\">Langue(s) souhaitée</th>
      <th scope=\"col\">Date d'ajout</th>
      <th scope=\"col\">Action</th>
    </tr>
  </thead>
  <tbody>
      {% if  lists is empty %}
        <tr>
          <th colspan=\"100\"><center> Aucun fichier</center></th>
        </tr>
      {% else %}
         <tr>
          {% for list in lists %}
            <tr>
              <th>{{ list.name}}</th>
              <td>{{ list.upload}}</td>
              <td></td>
              <td>
                {% for lang in list.langues %}
                {{ lang.langue }}</br>
                {% endfor %}
              </td>
              <td>{{ list.dateadd|date(\"Y-m-d H:i:s\", \"Europe/Paris\")}}</td>
              <td><a  href=\"{{ path('detailfile',{'id': list.id}) }}\" class=\"btn btn-lg btn-info\"> detail</a>
              {#<a  href=\"{{ path('deleteMyFile',{'id': list.id}) }}\" class=\"btn btn-lg btn-danger\"> supprimer</a>#}
              </td>
            </tr>
        {% endfor %}
        </tr>
      {% endif %}    
  </tbody>
</table>
</div>
{% endblock %}


{#</h1>#}
{#<p> {{id }}</p>#}", ":Upload:showMyFiles.html.twig", "/home/ubuntu/workspace/app/Resources/views/Upload/showMyFiles.html.twig");
    }
}
