<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_eeabca540801682a2de1c82373fa058459810df99fcf73ff27473a34020bb26b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2746e053e6f391b9506438f5cef7b3ff7a9b3744d4947db2469c2e240f5d4bd7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2746e053e6f391b9506438f5cef7b3ff7a9b3744d4947db2469c2e240f5d4bd7->enter($__internal_2746e053e6f391b9506438f5cef7b3ff7a9b3744d4947db2469c2e240f5d4bd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_728cdd7149c2afe20abd47d889b425bd16945f97d78eb2dc88f3db2cf850004c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_728cdd7149c2afe20abd47d889b425bd16945f97d78eb2dc88f3db2cf850004c->enter($__internal_728cdd7149c2afe20abd47d889b425bd16945f97d78eb2dc88f3db2cf850004c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_2746e053e6f391b9506438f5cef7b3ff7a9b3744d4947db2469c2e240f5d4bd7->leave($__internal_2746e053e6f391b9506438f5cef7b3ff7a9b3744d4947db2469c2e240f5d4bd7_prof);

        
        $__internal_728cdd7149c2afe20abd47d889b425bd16945f97d78eb2dc88f3db2cf850004c->leave($__internal_728cdd7149c2afe20abd47d889b425bd16945f97d78eb2dc88f3db2cf850004c_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_f794c04669c9ffe2a6b6686f3ce96e64a0d0d8ed3037f3e2c09d6ccba39525bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f794c04669c9ffe2a6b6686f3ce96e64a0d0d8ed3037f3e2c09d6ccba39525bc->enter($__internal_f794c04669c9ffe2a6b6686f3ce96e64a0d0d8ed3037f3e2c09d6ccba39525bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_514182c9c6ca4190eec06ef11d655aeb36a5848716d34bbde59b58f6fb052500 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_514182c9c6ca4190eec06ef11d655aeb36a5848716d34bbde59b58f6fb052500->enter($__internal_514182c9c6ca4190eec06ef11d655aeb36a5848716d34bbde59b58f6fb052500_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_514182c9c6ca4190eec06ef11d655aeb36a5848716d34bbde59b58f6fb052500->leave($__internal_514182c9c6ca4190eec06ef11d655aeb36a5848716d34bbde59b58f6fb052500_prof);

        
        $__internal_f794c04669c9ffe2a6b6686f3ce96e64a0d0d8ed3037f3e2c09d6ccba39525bc->leave($__internal_f794c04669c9ffe2a6b6686f3ce96e64a0d0d8ed3037f3e2c09d6ccba39525bc_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_b80741ec4f93ac2b6eb07a73e14849d1381696f30f0ad5b9b8b241964238e8ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b80741ec4f93ac2b6eb07a73e14849d1381696f30f0ad5b9b8b241964238e8ba->enter($__internal_b80741ec4f93ac2b6eb07a73e14849d1381696f30f0ad5b9b8b241964238e8ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_5249e669b1d6140a6324fa9c07d6aa262cb979af784dea9ed790c071a7c23327 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5249e669b1d6140a6324fa9c07d6aa262cb979af784dea9ed790c071a7c23327->enter($__internal_5249e669b1d6140a6324fa9c07d6aa262cb979af784dea9ed790c071a7c23327_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_5249e669b1d6140a6324fa9c07d6aa262cb979af784dea9ed790c071a7c23327->leave($__internal_5249e669b1d6140a6324fa9c07d6aa262cb979af784dea9ed790c071a7c23327_prof);

        
        $__internal_b80741ec4f93ac2b6eb07a73e14849d1381696f30f0ad5b9b8b241964238e8ba->leave($__internal_b80741ec4f93ac2b6eb07a73e14849d1381696f30f0ad5b9b8b241964238e8ba_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_1d32077cd35062b243b77e6e0b5ed331fb566e03f70385b420de3d003d8c5391 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d32077cd35062b243b77e6e0b5ed331fb566e03f70385b420de3d003d8c5391->enter($__internal_1d32077cd35062b243b77e6e0b5ed331fb566e03f70385b420de3d003d8c5391_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_ed02e8a661b32a36242a279c7e01fcfffd73bba4377f93401581595dcf4fa0b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed02e8a661b32a36242a279c7e01fcfffd73bba4377f93401581595dcf4fa0b2->enter($__internal_ed02e8a661b32a36242a279c7e01fcfffd73bba4377f93401581595dcf4fa0b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_ed02e8a661b32a36242a279c7e01fcfffd73bba4377f93401581595dcf4fa0b2->leave($__internal_ed02e8a661b32a36242a279c7e01fcfffd73bba4377f93401581595dcf4fa0b2_prof);

        
        $__internal_1d32077cd35062b243b77e6e0b5ed331fb566e03f70385b420de3d003d8c5391->leave($__internal_1d32077cd35062b243b77e6e0b5ed331fb566e03f70385b420de3d003d8c5391_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
