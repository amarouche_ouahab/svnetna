<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_c8ea3f8cc595842bc033dd06413db392dfd169decb1573235158ca96d63f9ba5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c8662bec902ac2d1ae7ac7204a5fa633112df7ea437abdf3c8471d23ce02a673 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8662bec902ac2d1ae7ac7204a5fa633112df7ea437abdf3c8471d23ce02a673->enter($__internal_c8662bec902ac2d1ae7ac7204a5fa633112df7ea437abdf3c8471d23ce02a673_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_8708c8fc727a84423161a806722c24880359066d95ecaa2575a3a5b871ae01ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8708c8fc727a84423161a806722c24880359066d95ecaa2575a3a5b871ae01ad->enter($__internal_8708c8fc727a84423161a806722c24880359066d95ecaa2575a3a5b871ae01ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_c8662bec902ac2d1ae7ac7204a5fa633112df7ea437abdf3c8471d23ce02a673->leave($__internal_c8662bec902ac2d1ae7ac7204a5fa633112df7ea437abdf3c8471d23ce02a673_prof);

        
        $__internal_8708c8fc727a84423161a806722c24880359066d95ecaa2575a3a5b871ae01ad->leave($__internal_8708c8fc727a84423161a806722c24880359066d95ecaa2575a3a5b871ae01ad_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
