<?php

/* :Upload:new.html.twig */
class __TwigTemplate_b3f732a88075ebdf15386a3301ac329d7095806ec0b1d60c8a418583a3626fd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", ":Upload:new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a07508b6f62a68338b64b6c5b5e76f0103a49b0cd12241d271a4fe0c94b8eed3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a07508b6f62a68338b64b6c5b5e76f0103a49b0cd12241d271a4fe0c94b8eed3->enter($__internal_a07508b6f62a68338b64b6c5b5e76f0103a49b0cd12241d271a4fe0c94b8eed3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Upload:new.html.twig"));

        $__internal_c825e1c6fb551f656efde7e6d4373930fa5dd6e8e75f6731c47499e93915ddc2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c825e1c6fb551f656efde7e6d4373930fa5dd6e8e75f6731c47499e93915ddc2->enter($__internal_c825e1c6fb551f656efde7e6d4373930fa5dd6e8e75f6731c47499e93915ddc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":Upload:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a07508b6f62a68338b64b6c5b5e76f0103a49b0cd12241d271a4fe0c94b8eed3->leave($__internal_a07508b6f62a68338b64b6c5b5e76f0103a49b0cd12241d271a4fe0c94b8eed3_prof);

        
        $__internal_c825e1c6fb551f656efde7e6d4373930fa5dd6e8e75f6731c47499e93915ddc2->leave($__internal_c825e1c6fb551f656efde7e6d4373930fa5dd6e8e75f6731c47499e93915ddc2_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_c2cc2828d2294069848e167191f1c79eab8396f3a33e66a8ab059cb41c9c89b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c2cc2828d2294069848e167191f1c79eab8396f3a33e66a8ab059cb41c9c89b6->enter($__internal_c2cc2828d2294069848e167191f1c79eab8396f3a33e66a8ab059cb41c9c89b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_198d6863192519614fe723fb5cee006d050662a1210f8272d06f9d03e8453916 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_198d6863192519614fe723fb5cee006d050662a1210f8272d06f9d03e8453916->enter($__internal_198d6863192519614fe723fb5cee006d050662a1210f8272d06f9d03e8453916_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "<div class=\"col-12 col-md-4\"></div>
<div class=\"col-12 col-md-4\">
    <center>
            <h3 class=\"form-signin-heading\">Ajouter un nouveau fichier</h3>
            <hr class=\"colorgraph\"><br><br>
              ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
              ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "name", array()), 'row');
        echo "
              ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "fileUpload", array()), 'row');
        echo "
              ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
</div>
<div class=\"col-12 col-md-4\"></div>
<div class=\"col-12 col-md-4\"></div>
<div class=\"col-12 col-md-4\">
    ";
        // line 17
        if (array_key_exists("msg", $context)) {
            // line 18
            echo "    <div class=\"alert alert-warning\">
      <strong>Warning!</strong> ";
            // line 19
            echo twig_escape_filter($this->env, ($context["msg"] ?? $this->getContext($context, "msg")), "html", null, true);
            echo "
    </div>
    ";
        }
        // line 22
        echo "    ";
        if (array_key_exists("success", $context)) {
            // line 23
            echo "    <div class=\"alert alert-success\">
      <strong>Parfait! </strong> ";
            // line 24
            echo twig_escape_filter($this->env, ($context["success"] ?? $this->getContext($context, "success")), "html", null, true);
            echo "
  </div>
    <center><a  href=\"";
            // line 26
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("showMyFile");
            echo "\" class=\"btn btn-lg btn-info\">Acceder a mes fichiers</a>
</center>
    ";
        }
        // line 29
        echo "</center>
</div>
";
        
        $__internal_198d6863192519614fe723fb5cee006d050662a1210f8272d06f9d03e8453916->leave($__internal_198d6863192519614fe723fb5cee006d050662a1210f8272d06f9d03e8453916_prof);

        
        $__internal_c2cc2828d2294069848e167191f1c79eab8396f3a33e66a8ab059cb41c9c89b6->leave($__internal_c2cc2828d2294069848e167191f1c79eab8396f3a33e66a8ab059cb41c9c89b6_prof);

    }

    public function getTemplateName()
    {
        return ":Upload:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 29,  98 => 26,  93 => 24,  90 => 23,  87 => 22,  81 => 19,  78 => 18,  76 => 17,  68 => 12,  64 => 11,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

{% block content %}
<div class=\"col-12 col-md-4\"></div>
<div class=\"col-12 col-md-4\">
    <center>
            <h3 class=\"form-signin-heading\">Ajouter un nouveau fichier</h3>
            <hr class=\"colorgraph\"><br><br>
              {{ form_start(form) }}
              {{ form_row(form.name) }}
              {{ form_row(form.fileUpload) }}
              {{ form_end(form) }}
</div>
<div class=\"col-12 col-md-4\"></div>
<div class=\"col-12 col-md-4\"></div>
<div class=\"col-12 col-md-4\">
    {% if  msg is defined %}
    <div class=\"alert alert-warning\">
      <strong>Warning!</strong> {{msg}}
    </div>
    {% endif %}
    {% if  success is defined %}
    <div class=\"alert alert-success\">
      <strong>Parfait! </strong> {{success}}
  </div>
    <center><a  href=\"{{ path('showMyFile')}}\" class=\"btn btn-lg btn-info\">Acceder a mes fichiers</a>
</center>
    {% endif %}
</center>
</div>
{% endblock %}", ":Upload:new.html.twig", "/home/ubuntu/workspace/app/Resources/views/Upload/new.html.twig");
    }
}
