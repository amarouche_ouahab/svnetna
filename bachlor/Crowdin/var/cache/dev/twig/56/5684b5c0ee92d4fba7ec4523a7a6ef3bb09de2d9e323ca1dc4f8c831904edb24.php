<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_c763e67be7fccde35334e51315fa2877b78ce320cfaa5dd01ba453cc8c185ced extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_903f79244ec02bca42a40a63003626fd2a0f64d3cdac6766cbcb4ee35121030e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_903f79244ec02bca42a40a63003626fd2a0f64d3cdac6766cbcb4ee35121030e->enter($__internal_903f79244ec02bca42a40a63003626fd2a0f64d3cdac6766cbcb4ee35121030e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_3173a30c2f7242235b18b8c307d029b087bc0dc8765a8529a802a01e452688f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3173a30c2f7242235b18b8c307d029b087bc0dc8765a8529a802a01e452688f7->enter($__internal_3173a30c2f7242235b18b8c307d029b087bc0dc8765a8529a802a01e452688f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_903f79244ec02bca42a40a63003626fd2a0f64d3cdac6766cbcb4ee35121030e->leave($__internal_903f79244ec02bca42a40a63003626fd2a0f64d3cdac6766cbcb4ee35121030e_prof);

        
        $__internal_3173a30c2f7242235b18b8c307d029b087bc0dc8765a8529a802a01e452688f7->leave($__internal_3173a30c2f7242235b18b8c307d029b087bc0dc8765a8529a802a01e452688f7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}
