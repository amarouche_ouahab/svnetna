<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_8142be88f30ddc2e15c390a592bfe4d1edcf12b006788748e7c1a989bb635821 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8bde7739d86e9482d073ca8edc25ead1717f078db414a9af47098715730b2004 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8bde7739d86e9482d073ca8edc25ead1717f078db414a9af47098715730b2004->enter($__internal_8bde7739d86e9482d073ca8edc25ead1717f078db414a9af47098715730b2004_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        $__internal_edbd7cc8ce186d6ad11541c54deae9fb2101815ef7b906d7f5631ab46acd9130 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_edbd7cc8ce186d6ad11541c54deae9fb2101815ef7b906d7f5631ab46acd9130->enter($__internal_edbd7cc8ce186d6ad11541c54deae9fb2101815ef7b906d7f5631ab46acd9130_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_8bde7739d86e9482d073ca8edc25ead1717f078db414a9af47098715730b2004->leave($__internal_8bde7739d86e9482d073ca8edc25ead1717f078db414a9af47098715730b2004_prof);

        
        $__internal_edbd7cc8ce186d6ad11541c54deae9fb2101815ef7b906d7f5631ab46acd9130->leave($__internal_edbd7cc8ce186d6ad11541c54deae9fb2101815ef7b906d7f5631ab46acd9130_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_32673630e87d5ef28c16d1c792c62d5254472a03cd3a5c5f16ceb87690c2f701 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_32673630e87d5ef28c16d1c792c62d5254472a03cd3a5c5f16ceb87690c2f701->enter($__internal_32673630e87d5ef28c16d1c792c62d5254472a03cd3a5c5f16ceb87690c2f701_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_4f12ad42256264f035723788756d85d855cc455dfd3ee330a49f7d0cbde61bc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f12ad42256264f035723788756d85d855cc455dfd3ee330a49f7d0cbde61bc8->enter($__internal_4f12ad42256264f035723788756d85d855cc455dfd3ee330a49f7d0cbde61bc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_4f12ad42256264f035723788756d85d855cc455dfd3ee330a49f7d0cbde61bc8->leave($__internal_4f12ad42256264f035723788756d85d855cc455dfd3ee330a49f7d0cbde61bc8_prof);

        
        $__internal_32673630e87d5ef28c16d1c792c62d5254472a03cd3a5c5f16ceb87690c2f701->leave($__internal_32673630e87d5ef28c16d1c792c62d5254472a03cd3a5c5f16ceb87690c2f701_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_9a4b273b03fdfba8ce9c27e9c216f9f39a061aedacb4cfed517b1ad8b5e48f55 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a4b273b03fdfba8ce9c27e9c216f9f39a061aedacb4cfed517b1ad8b5e48f55->enter($__internal_9a4b273b03fdfba8ce9c27e9c216f9f39a061aedacb4cfed517b1ad8b5e48f55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_dcba444bf0a9e72c87cb86730163d0dd503e346a2c8dfe15f3aea7115b9edb4e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dcba444bf0a9e72c87cb86730163d0dd503e346a2c8dfe15f3aea7115b9edb4e->enter($__internal_dcba444bf0a9e72c87cb86730163d0dd503e346a2c8dfe15f3aea7115b9edb4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_dcba444bf0a9e72c87cb86730163d0dd503e346a2c8dfe15f3aea7115b9edb4e->leave($__internal_dcba444bf0a9e72c87cb86730163d0dd503e346a2c8dfe15f3aea7115b9edb4e_prof);

        
        $__internal_9a4b273b03fdfba8ce9c27e9c216f9f39a061aedacb4cfed517b1ad8b5e48f55->leave($__internal_9a4b273b03fdfba8ce9c27e9c216f9f39a061aedacb4cfed517b1ad8b5e48f55_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_8e55b4afdca5f411f3854057185fe39a6a39acae9aaad44c08a5041d22be4c91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e55b4afdca5f411f3854057185fe39a6a39acae9aaad44c08a5041d22be4c91->enter($__internal_8e55b4afdca5f411f3854057185fe39a6a39acae9aaad44c08a5041d22be4c91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9b7067edc01cc4bb2d308d0f2c4ce10b365b78a7422e527527a41533282c114d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b7067edc01cc4bb2d308d0f2c4ce10b365b78a7422e527527a41533282c114d->enter($__internal_9b7067edc01cc4bb2d308d0f2c4ce10b365b78a7422e527527a41533282c114d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_9b7067edc01cc4bb2d308d0f2c4ce10b365b78a7422e527527a41533282c114d->leave($__internal_9b7067edc01cc4bb2d308d0f2c4ce10b365b78a7422e527527a41533282c114d_prof);

        
        $__internal_8e55b4afdca5f411f3854057185fe39a6a39acae9aaad44c08a5041d22be4c91->leave($__internal_8e55b4afdca5f411f3854057185fe39a6a39acae9aaad44c08a5041d22be4c91_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "TwigBundle::layout.html.twig", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
