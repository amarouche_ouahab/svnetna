<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_5c2e3e34ba69b993b9120b13741509502ac9547ff8b1c64c34053ae847af5eea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f311349693d77fcd006d602d634df9951d213f9fcb31ef4ba0dc02cf9e518b5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f311349693d77fcd006d602d634df9951d213f9fcb31ef4ba0dc02cf9e518b5e->enter($__internal_f311349693d77fcd006d602d634df9951d213f9fcb31ef4ba0dc02cf9e518b5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_b4e1023f649081ed20e0137f954f7521c7f68260354377af3da626a5c2a48153 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4e1023f649081ed20e0137f954f7521c7f68260354377af3da626a5c2a48153->enter($__internal_b4e1023f649081ed20e0137f954f7521c7f68260354377af3da626a5c2a48153_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_f311349693d77fcd006d602d634df9951d213f9fcb31ef4ba0dc02cf9e518b5e->leave($__internal_f311349693d77fcd006d602d634df9951d213f9fcb31ef4ba0dc02cf9e518b5e_prof);

        
        $__internal_b4e1023f649081ed20e0137f954f7521c7f68260354377af3da626a5c2a48153->leave($__internal_b4e1023f649081ed20e0137f954f7521c7f68260354377af3da626a5c2a48153_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
