<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_8340980ad37ed9874611702bd023534db5bdc7d5a1543540a08c576126476d1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3e5540070351667b3be938166489a31358db28713bc49cd2a86a20adb64b97b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e5540070351667b3be938166489a31358db28713bc49cd2a86a20adb64b97b1->enter($__internal_3e5540070351667b3be938166489a31358db28713bc49cd2a86a20adb64b97b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_9e2f15b8f14b1107484fbe90ee46d67a5697eb4362b5ba15049e7d85aa787c01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e2f15b8f14b1107484fbe90ee46d67a5697eb4362b5ba15049e7d85aa787c01->enter($__internal_9e2f15b8f14b1107484fbe90ee46d67a5697eb4362b5ba15049e7d85aa787c01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_3e5540070351667b3be938166489a31358db28713bc49cd2a86a20adb64b97b1->leave($__internal_3e5540070351667b3be938166489a31358db28713bc49cd2a86a20adb64b97b1_prof);

        
        $__internal_9e2f15b8f14b1107484fbe90ee46d67a5697eb4362b5ba15049e7d85aa787c01->leave($__internal_9e2f15b8f14b1107484fbe90ee46d67a5697eb4362b5ba15049e7d85aa787c01_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/home/ubuntu/workspace/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
