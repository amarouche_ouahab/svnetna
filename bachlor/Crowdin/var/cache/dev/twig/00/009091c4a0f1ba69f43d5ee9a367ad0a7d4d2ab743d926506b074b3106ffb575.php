<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_49a9f8b1520e7387f9429d5fef0ce5b17a97745716569f444faaada6c74d1a32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3aa68ad6a94b48e64429f5c32b5fcba13df88f15e415c69ade642f1dbc79d205 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3aa68ad6a94b48e64429f5c32b5fcba13df88f15e415c69ade642f1dbc79d205->enter($__internal_3aa68ad6a94b48e64429f5c32b5fcba13df88f15e415c69ade642f1dbc79d205_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_cd15db79cbce4bfcf174bdfd7646a447c67cc74c60aecafb1aa9692790e1da2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd15db79cbce4bfcf174bdfd7646a447c67cc74c60aecafb1aa9692790e1da2d->enter($__internal_cd15db79cbce4bfcf174bdfd7646a447c67cc74c60aecafb1aa9692790e1da2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3aa68ad6a94b48e64429f5c32b5fcba13df88f15e415c69ade642f1dbc79d205->leave($__internal_3aa68ad6a94b48e64429f5c32b5fcba13df88f15e415c69ade642f1dbc79d205_prof);

        
        $__internal_cd15db79cbce4bfcf174bdfd7646a447c67cc74c60aecafb1aa9692790e1da2d->leave($__internal_cd15db79cbce4bfcf174bdfd7646a447c67cc74c60aecafb1aa9692790e1da2d_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b8e46015dff395b7c056be3225d3696e7deae45317023390c009d0c85208593d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8e46015dff395b7c056be3225d3696e7deae45317023390c009d0c85208593d->enter($__internal_b8e46015dff395b7c056be3225d3696e7deae45317023390c009d0c85208593d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_a54b80d1c579afa21d2fb2fc0dbf80bb426e97dc7e137d50cc5bb2edc5f5d4db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a54b80d1c579afa21d2fb2fc0dbf80bb426e97dc7e137d50cc5bb2edc5f5d4db->enter($__internal_a54b80d1c579afa21d2fb2fc0dbf80bb426e97dc7e137d50cc5bb2edc5f5d4db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_a54b80d1c579afa21d2fb2fc0dbf80bb426e97dc7e137d50cc5bb2edc5f5d4db->leave($__internal_a54b80d1c579afa21d2fb2fc0dbf80bb426e97dc7e137d50cc5bb2edc5f5d4db_prof);

        
        $__internal_b8e46015dff395b7c056be3225d3696e7deae45317023390c009d0c85208593d->leave($__internal_b8e46015dff395b7c056be3225d3696e7deae45317023390c009d0c85208593d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
