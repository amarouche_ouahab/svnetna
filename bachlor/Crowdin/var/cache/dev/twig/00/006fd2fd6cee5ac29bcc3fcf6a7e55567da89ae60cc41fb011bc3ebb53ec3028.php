<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_f7b0bb00b19a7c28f1966a27f0e6ea988694acafa5ca8d02e0370e5715ca31ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d8215a10650066225e8b9452726e7aa257cf14f6ecb2b1e6c0d0db73ecd40d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d8215a10650066225e8b9452726e7aa257cf14f6ecb2b1e6c0d0db73ecd40d0->enter($__internal_8d8215a10650066225e8b9452726e7aa257cf14f6ecb2b1e6c0d0db73ecd40d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_68f9a5b3f7193c893d6357246bac0dec602af96b70a93d894d818af4ccdca5cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_68f9a5b3f7193c893d6357246bac0dec602af96b70a93d894d818af4ccdca5cb->enter($__internal_68f9a5b3f7193c893d6357246bac0dec602af96b70a93d894d818af4ccdca5cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8d8215a10650066225e8b9452726e7aa257cf14f6ecb2b1e6c0d0db73ecd40d0->leave($__internal_8d8215a10650066225e8b9452726e7aa257cf14f6ecb2b1e6c0d0db73ecd40d0_prof);

        
        $__internal_68f9a5b3f7193c893d6357246bac0dec602af96b70a93d894d818af4ccdca5cb->leave($__internal_68f9a5b3f7193c893d6357246bac0dec602af96b70a93d894d818af4ccdca5cb_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_97dd71f2879d56717a5ae9ae05259eeaa9bb3d883c002a65210b693b8bdbb9cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_97dd71f2879d56717a5ae9ae05259eeaa9bb3d883c002a65210b693b8bdbb9cf->enter($__internal_97dd71f2879d56717a5ae9ae05259eeaa9bb3d883c002a65210b693b8bdbb9cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_cd422ad363f0eff2efeaf032b02a9add79d686cd83099309c6f3a2473317ed97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd422ad363f0eff2efeaf032b02a9add79d686cd83099309c6f3a2473317ed97->enter($__internal_cd422ad363f0eff2efeaf032b02a9add79d686cd83099309c6f3a2473317ed97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_cd422ad363f0eff2efeaf032b02a9add79d686cd83099309c6f3a2473317ed97->leave($__internal_cd422ad363f0eff2efeaf032b02a9add79d686cd83099309c6f3a2473317ed97_prof);

        
        $__internal_97dd71f2879d56717a5ae9ae05259eeaa9bb3d883c002a65210b693b8bdbb9cf->leave($__internal_97dd71f2879d56717a5ae9ae05259eeaa9bb3d883c002a65210b693b8bdbb9cf_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
