<?php

/* Upload/new.html.twig */
class __TwigTemplate_fcfed349780d07cf119c9bd777ad4728fd90af70e6e4457181ea6392729ea959 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "Upload/new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-12 col-md-4\"></div>
<div class=\"col-12 col-md-4\">
    <center>
            <h3 class=\"form-signin-heading\">Ajouter un nouveau fichier</h3>
            <hr class=\"colorgraph\"><br><br>
              ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
              ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "name", array()), 'row');
        echo "
              ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "fileUpload", array()), 'row');
        echo "
              ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
</div>
<div class=\"col-12 col-md-4\"></div>
<div class=\"col-12 col-md-4\"></div>
<div class=\"col-12 col-md-4\">
    ";
        // line 17
        if (array_key_exists("msg", $context)) {
            // line 18
            echo "    <div class=\"alert alert-warning\">
      <strong>Warning!</strong> ";
            // line 19
            echo twig_escape_filter($this->env, ($context["msg"] ?? null), "html", null, true);
            echo "
    </div>
    ";
        }
        // line 22
        echo "    ";
        if (array_key_exists("success", $context)) {
            // line 23
            echo "    <div class=\"alert alert-success\">
      <strong>Parfait! </strong> ";
            // line 24
            echo twig_escape_filter($this->env, ($context["success"] ?? null), "html", null, true);
            echo "
  </div>
    <center><a  href=\"";
            // line 26
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("showMyFile");
            echo "\" class=\"btn btn-lg btn-info\">Acceder a mes fichiers</a>
</center>
    ";
        }
        // line 29
        echo "</center>
</div>
";
    }

    public function getTemplateName()
    {
        return "Upload/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 29,  80 => 26,  75 => 24,  72 => 23,  69 => 22,  63 => 19,  60 => 18,  58 => 17,  50 => 12,  46 => 11,  42 => 10,  38 => 9,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Upload/new.html.twig", "/home/ubuntu/workspace/app/Resources/views/Upload/new.html.twig");
    }
}
