<?php

/* Upload/showFiles.html.twig */
class __TwigTemplate_04084ef8c0bc50540f7ebc196956900fea3bd5140813c8a5b0192c195f0920f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "Upload/showFiles.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"col-12 col-md-2\"></div>
<div class=\"col-12 col-md-8\">
<table class=\"table table-bordered\">
  <thead>
    <tr>
      <th scope=\"col\">Nom du fichier</th>
      <th scope=\"col\">Ajouter par</th>
      <th scope=\"col\">Langues du fichier</th>
      <th scope=\"col\">Langue(s) souhaitée</th>
      <th scope=\"col\">Date d'ajout</th>
      <th scope=\"col\">Action</th>
    </tr>
  </thead>
  <tbody>
      ";
        // line 19
        if (twig_test_empty(($context["tab_files"] ?? null))) {
            // line 20
            echo "        <tr>
          <th colspan=\"100\"><center> Aucun fichier</center></th>
        </tr>
      ";
        } else {
            // line 24
            echo "         <tr>
           ";
            // line 26
            echo "          ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tab_files"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
                // line 27
                echo "            <tr>
              <th>";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["list"], 0, array(), "array"), "html", null, true);
                echo "</th>
              <td>";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["list"], 1, array(), "array"), "html", null, true);
                echo "</td>
              <td>";
                // line 30
                echo twig_escape_filter($this->env, $this->getAttribute($context["list"], 2, array(), "array"), "html", null, true);
                echo "</td>
              <td>
                ";
                // line 32
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["list"], 3, array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["lang"]) {
                    // line 33
                    echo "                ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["lang"], "langue", array()), "html", null, true);
                    echo "</br>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 35
                echo "              </td>
              <td>";
                // line 36
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["list"], 4, array(), "array"), "Y-m-d H:i:s", "Europe/Paris"), "html", null, true);
                echo "</td>
              <td><a  href=\"";
                // line 37
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailfile", array("id" => $this->getAttribute($context["list"], 5, array(), "array"))), "html", null, true);
                echo "\" class=\"btn btn-lg btn-info\"> detail</a>
              ";
                // line 39
                echo "              </td>
            </tr>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "        ";
            // line 43
            echo "        </tr>
      ";
        }
        // line 44
        echo "    
  </tbody>
</table>
</div>


";
    }

    public function getTemplateName()
    {
        return "Upload/showFiles.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 44,  114 => 43,  112 => 42,  104 => 39,  100 => 37,  96 => 36,  93 => 35,  84 => 33,  80 => 32,  75 => 30,  71 => 29,  67 => 28,  64 => 27,  59 => 26,  56 => 24,  50 => 20,  48 => 19,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Upload/showFiles.html.twig", "/home/ubuntu/workspace/app/Resources/views/Upload/showFiles.html.twig");
    }
}
