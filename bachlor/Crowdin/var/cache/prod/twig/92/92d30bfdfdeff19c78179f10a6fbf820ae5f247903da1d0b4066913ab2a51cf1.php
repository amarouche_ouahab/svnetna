<?php

/* Upload/detailFile.html.twig */
class __TwigTemplate_6555c58cb533470d1ca248b0826ef86131fc3bad1e0878220acc1aa195af0f26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "Upload/detailFile.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 7
        echo "    <div class=\"col-12 col-md-3 border border-primar rounded\">
            <center><h3>Detail du ficher</h3></center><br/>
            <b>nom: </b>";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute(($context["detail"] ?? null), "name", array()), "html", null, true);
        echo "<br/>
            <b> Date d'ajout: </b>";
        // line 10
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["detail"] ?? null), "dateadd", array()), "Y-m-d H:i:s", "Europe/Paris"), "html", null, true);
        echo "<br/>
            ";
        // line 12
        echo "    </div>
        <div class=\"col-12 col-md-6\">
            <table class=\"table table-bordered\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\">valeur</th>
                  ";
        // line 20
        echo "                  ";
        // line 21
        echo "                  ";
        // line 22
        echo "                  ";
        // line 23
        echo "                </tr>
              </thead>
              <tbody>
                  ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["table"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 27
            echo "                <tr>
                    <th>";
            // line 28
            if ($this->getAttribute($context["key"], 0, array(), "array", true, true)) {
                // line 29
                echo "                           ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 0, array(), "array"), "html", null, true);
                echo "
                        ";
            }
            // line 31
            echo "                    </th>
                    <td>
                        ";
            // line 33
            if ($this->getAttribute($context["key"], 2, array(), "array", true, true)) {
                // line 34
                echo "                           ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 2, array(), "array"), "html", null, true);
                echo "
                        ";
            }
            // line 36
            echo "                    </td>
                  ";
            // line 38
            echo "                  ";
            // line 39
            echo "                  ";
            // line 40
            echo "                  ";
            // line 41
            echo "                  ";
            // line 42
            echo "                  ";
            // line 43
            echo "                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "              </tbody>
            </table>
        </div>
        <div class=\"col-12 col-md-1\"></div>
        <div class=\"col-12 col-md-2 border border-primar rounded\">
            <center><h3>Traduire ce fichier</h3></center><br/>
             ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["detail"] ?? null), "langues", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["lang"]) {
            // line 52
            echo "             <center>
                <a class=\"btn btn-outline-warning btn-lg\" style=\"width: 50%;\" href=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("translate", array("langues" => $this->getAttribute($context["lang"], "langue", array()), "id" => $this->getAttribute(($context["detail"] ?? null), "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["lang"], "langue", array()), "html", null, true);
            echo "</a></br></br>
            </center>    
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "            ";
        // line 57
        echo "        </div>
    

";
        // line 61
        echo "    <div class=\"col-12 col-md-3 border border-primar rounded\" style=\"height: 440px;margin-top:  2%;\">
            <center><h3>Personne ayant traduit ce fichier</h3></center><br/>
             ";
        // line 63
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 64
            echo "                ";
            if ($this->getAttribute($context["user"], 0, array(), "array", true, true)) {
                // line 65
                echo "                Ce fichier à été en ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], 2, array(), "array"), "html", null, true);
                echo " par ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], 0, array(), "array"), "html", null, true);
                echo "
                <div style=\"margin-left: 70%;margin-top: -5%;\">
                    <a class=\"btn btn-outline-primary\" href=\"";
                // line 67
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("showtranslate", array("langues" => $this->getAttribute($context["user"], 2, array(), "array"), "id_file" => $this->getAttribute(($context["detail"] ?? null), "id", array()), "id_user" => $this->getAttribute($context["user"], 1, array(), "array"))), "html", null, true);
                echo "\">
                        Voire la traduction
                    </a>
                </div></br>
                ";
            }
            // line 72
            echo "
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "Upload/detailFile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 74,  173 => 72,  165 => 67,  157 => 65,  154 => 64,  150 => 63,  146 => 61,  141 => 57,  139 => 56,  128 => 53,  125 => 52,  121 => 51,  113 => 45,  106 => 43,  104 => 42,  102 => 41,  100 => 40,  98 => 39,  96 => 38,  93 => 36,  87 => 34,  85 => 33,  81 => 31,  75 => 29,  73 => 28,  70 => 27,  66 => 26,  61 => 23,  59 => 22,  57 => 21,  55 => 20,  46 => 12,  42 => 10,  38 => 9,  34 => 7,  31 => 4,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Upload/detailFile.html.twig", "/home/ubuntu/workspace/app/Resources/views/Upload/detailFile.html.twig");
    }
}
