<?php

/* @FOSUser/Profile/show_content.html.twig */
class __TwigTemplate_1311d22c23f77c8fc1ffdaa9776eedce9f4467b6d6c0662f26bba3fb2f5ec4bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
";
        // line 15
        echo "
";
        // line 17
        echo "<div class=\"col-12 col-md-3\">
    <div class=\"panel panel-success\">
        <div class=\"panel-heading\" style=\"font-size:20px;\">Mon Profile</div>
        <div class=\"panel-body\">
            <p style=\"font-size:15px;\">Bienvenue sur votre profile ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? null), "user", array()), "username", array()), "html", null, true);
        echo "
            </br>Email:  ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? null), "user", array()), "email", array()), "html", null, true);
        echo "</br>

        </p>
            <a class=\"btn btn-outline-primary\" href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_edit");
        echo "\">Modifier mon profile</a>
            <a class=\"btn btn-outline-primary\" href=\"";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_change_password");
        echo "\">Modifier mon Mot de passe</a>
        </div>
    </div>
</div>
<a class=\"btn btn-outline-primary\" href=\"";
        // line 30
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("showMyFile");
        echo "\">Voir mes fichiers</a>

<a class=\"btn btn-outline-primary\" href=\"";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_product_new");
        echo "\">upload</a>";
    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 32,  52 => 30,  45 => 26,  41 => 25,  35 => 22,  31 => 21,  25 => 17,  22 => 15,  19 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Profile/show_content.html.twig", "/home/ubuntu/workspace/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show_content.html.twig");
    }
}
