<?php

/* translate/usersTranslate.html.twig */
class __TwigTemplate_2a42107b2787b147b15033d261461d0cac084d76302579a032dbd9f81307a14d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "translate/usersTranslate.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "      <div class=\"col-12 col-md-2 border border-primar rounded\">
            <center><h3>Detail du ficher</h3></center><br/>
            <b>Ajouter par: </b> ";
        // line 5
        echo twig_escape_filter($this->env, ($context["user"] ?? null), "html", null, true);
        echo "<br/>
            <b> nom: </b>";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute(($context["detail"] ?? null), "name", array()), "html", null, true);
        echo "<br/>
            <b> Date d'ajout: </b>";
        // line 7
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["detail"] ?? null), "dateadd", array()), "Y-m-d H:i:s", "Europe/Paris"), "html", null, true);
        echo "<br/>
            ";
        // line 8
        echo "</br></br>
            <center><a  href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailfile", array("id" => ($context["id"] ?? null))), "html", null, true);
        echo "\"  class=\"btn btn-lg btn-info\">Acceder aux details du fichier</a>
           </center> </br>
    </div>
    <div class=\"col-12 col-md-1\"></div>
        <div class=\"col-12 col-md-4\" style=\"padding: 0 !important;\">
            <table class=\"table table-bordered\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\">valeur</th>
                </tr>
              </thead>
              <tbody>
                  ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["table"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 23
            echo "                <tr>
                  <th>";
            // line 24
            if ($this->getAttribute($context["key"], 0, array(), "array", true, true)) {
                // line 25
                echo "                       ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 0, array(), "array"), "html", null, true);
                echo "
                    ";
            }
            // line 26
            echo "</th>
                  <td>
                    ";
            // line 28
            if ($this->getAttribute($context["key"], 2, array(), "array", true, true)) {
                // line 29
                echo "                       ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 2, array(), "array"), "html", null, true);
                echo "
                    ";
            }
            // line 31
            echo "                    </td>

                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                 
              </tbody>
            </table>
        </div>
        ";
        // line 40
        echo "        <div class=\"\" style=\"position:absolute;margin-left: 57%;width: 16%;\">
           <table class=\"table table-bordered\">
              <thead>
                <tr>
                  <th scope=\"col\">Valeurs traduite</th>
                </tr>
              </thead>
              <tbody>
            ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["values"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["val"]) {
            // line 49
            echo "                <tr>
                  <td>";
            // line 50
            if (array_key_exists("val", $context)) {
                // line 51
                echo "                       <center>";
                echo twig_escape_filter($this->env, $context["val"], "html", null, true);
                echo "</center>
                    ";
            }
            // line 52
            echo "</td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['val'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "            </tbody>
            </table>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "translate/usersTranslate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 55,  133 => 52,  127 => 51,  125 => 50,  122 => 49,  118 => 48,  108 => 40,  102 => 35,  93 => 31,  87 => 29,  85 => 28,  81 => 26,  75 => 25,  73 => 24,  70 => 23,  66 => 22,  50 => 9,  47 => 8,  43 => 7,  39 => 6,  35 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "translate/usersTranslate.html.twig", "/home/ubuntu/workspace/app/Resources/views/translate/usersTranslate.html.twig");
    }
}
