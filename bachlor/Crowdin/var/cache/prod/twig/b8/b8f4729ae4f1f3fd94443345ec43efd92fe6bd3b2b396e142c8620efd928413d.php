<?php

/* translate/translateView.html.twig */
class __TwigTemplate_d1c5f8d121270043c4a08b5e0ef3c6ce8c4204881c45ce98378afac60d026328 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "translate/translateView.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
    ";
        // line 5
        echo "    ";
        // line 6
        echo "    ";
        // line 7
        echo "    ";
        // line 8
        echo "    ";
        // line 9
        echo "    ";
        // line 10
        echo "    <div class=\"col-12 col-md-1\"></div>
    <div class=\"col-12 col-md-6 border\">
            <table class=\"table\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\">les valeurs</th>
                </tr>
              </thead>
              <tbody>
                  ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["table"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 21
            echo "                <tr>
                  <th>";
            // line 22
            if ($this->getAttribute($context["key"], 0, array(), "array", true, true)) {
                // line 23
                echo "                       ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 0, array(), "array"), "html", null, true);
                echo "
                    ";
            }
            // line 24
            echo "</th>
                  <td>
                    ";
            // line 26
            if ($this->getAttribute($context["key"], 2, array(), "array", true, true)) {
                // line 27
                echo "                       ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 2, array(), "array"), "html", null, true);
                echo "
                    ";
            }
            // line 29
            echo "                    </td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "              </tbody>
            </table>
        </div>
        <div class=\"col-12 col-md-1\"></div>
        <div class=\"col-12 col-md-2 border border-primar rounded\">
           <form name=\"input\" action=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("translate", array("langues" => ($context["langues"] ?? null), "id" => ($context["id"] ?? null))), "html", null, true);
        echo "\" method=\"post\">
          <table class=\"table\">
              <thead>
                <tr>
                  <th scope=\"col\">Les cles du fichier</th>
                  <th scope=\"col\"> 
                  </th>
                </tr>
              </thead>
              <tbody>
                  ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["table"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 48
            echo "                <tr>
                    <td>
                          <input type=\"text\" name=\"value[";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["key"], 1, array(), "array"), "html", null, true);
            echo "]\"/>
                    </td>
                </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "              </tbody>
            </table>
             <input type=\"submit\" name=\"submit\" value=\"Submit\">
            </form>
            <div>
          </div>
        </div>
        
  <div class=\"col-12 col-md-3\"></div>   
  <div class=\"col-12 col-md-6\">  

  ";
        // line 65
        if (array_key_exists("success", $context)) {
            // line 66
            echo "  <div class=\"alert alert-success\">
    <strong>Parfait! </strong> ";
            // line 67
            echo twig_escape_filter($this->env, ($context["success"] ?? null), "html", null, true);
            echo "
  </div></br></br>
  <center><a  href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailfile", array("id" => ($context["id"] ?? null))), "html", null, true);
            echo "\" class=\"btn btn-lg btn-info\">Acceder au fichier</a>
  </center>
  ";
        }
        // line 72
        echo "</div> 
";
    }

    public function getTemplateName()
    {
        return "translate/translateView.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 72,  152 => 69,  147 => 67,  144 => 66,  142 => 65,  129 => 54,  119 => 50,  115 => 48,  111 => 47,  98 => 37,  91 => 32,  83 => 29,  77 => 27,  75 => 26,  71 => 24,  65 => 23,  63 => 22,  60 => 21,  56 => 20,  44 => 10,  42 => 9,  40 => 8,  38 => 7,  36 => 6,  34 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "translate/translateView.html.twig", "/home/ubuntu/workspace/app/Resources/views/translate/translateView.html.twig");
    }
}
