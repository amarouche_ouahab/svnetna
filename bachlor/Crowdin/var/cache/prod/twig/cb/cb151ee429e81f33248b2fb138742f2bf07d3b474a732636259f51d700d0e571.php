<?php

/* base.html.twig */
class __TwigTemplate_6894db751d72c5581793812eb59aa8bb95f84c39e851ae5ee9c79e6435b8d2bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 12
        echo "
";
        // line 14
        echo "
";
        // line 19
        echo "
";
        // line 26
        echo "
";
        // line 28
        echo "    
";
        // line 41
        echo "
";
        // line 45
        echo "
";
        // line 54
        echo "
<!DOCTYPE html>
<html>

<head>

\t<meta charset=\"utf-8\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>";
        // line 64
        $this->displayBlock('title', $context, $blocks);
        echo "</title>


\t<link rel=\"stylesheet\" href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/demo.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" href=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/header-login-signup.css"), "html", null, true);
        echo "\">
\t<link href='http://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
\t    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css\" integrity=\"sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy\" crossorigin=\"anonymous\">

 ";
        // line 73
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 74
        echo "</head>

<body>

<header class=\"header-login-signup\">

\t<div class=\"header-limiter\">

\t\t<h1><a href=\"";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_show");
        echo "\">Crow<span>din</span></a></h1>

 ";
        // line 84
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 85
            echo "        ";
            // line 86
            echo "        <nav>
\t\t\t<a href=";
            // line 87
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_show");
            echo ">Home</a>
\t\t\t<a href=";
            // line 88
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("showFile");
            echo ">Voir tout les fichers</a>
\t\t\t<a href=";
            // line 89
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("showMyFile");
            echo ">Voir mes fichers</a>
\t\t\t<a href=";
            // line 90
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("app_product_new");
            echo ">Ajouter un fichier</a>
\t\t</nav>
\t\t
\t\t<ul>
\t\t\t
\t\t\t<li><a href=\"";
            // line 95
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">Se deconnecter</a></li>
\t\t</ul>
    ";
        } else {
            // line 98
            echo "        <ul>
\t\t\t<li><a href=\"";
            // line 99
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">Login</a></li>
\t\t\t<li><a href=\"";
            // line 100
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
            echo "\">Sign up</a></li>
\t\t</ul>

    ";
        }
        // line 104
        echo "
\t</div>

</header>

<!-- The content of your page would go here. -->
<div class=\"row\" style=\"margin-left:  1%;margin-top:  5%;margin-right:  1%;margin-bottom: 15%;\">
 ";
        // line 111
        $this->displayBlock('content', $context, $blocks);
        // line 113
        echo "</div>
";
        // line 115
        echo "
";
        // line 117
        echo "
";
        // line 120
        echo "
";
        // line 130
        echo "
";
        // line 132
        echo "


<!-- Demo ads. Please ignore and remove. -->
<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>
";
        // line 138
        echo "
</body>

</html>
";
    }

    // line 64
    public function block_title($context, array $blocks = array())
    {
        echo "My crowdin";
    }

    // line 73
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 111
    public function block_content($context, array $blocks = array())
    {
        // line 112
        echo " ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 112,  189 => 111,  184 => 73,  178 => 64,  170 => 138,  163 => 132,  160 => 130,  157 => 120,  154 => 117,  151 => 115,  148 => 113,  146 => 111,  137 => 104,  130 => 100,  126 => 99,  123 => 98,  117 => 95,  109 => 90,  105 => 89,  101 => 88,  97 => 87,  94 => 86,  92 => 85,  90 => 84,  85 => 82,  75 => 74,  73 => 73,  65 => 68,  61 => 67,  55 => 64,  43 => 54,  40 => 45,  37 => 41,  34 => 28,  31 => 26,  28 => 19,  25 => 14,  22 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "/home/ubuntu/workspace/app/Resources/views/base.html.twig");
    }
}
