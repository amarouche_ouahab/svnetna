<?php
namespace  AppBundle\Services;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Upload;
use AppBundle\Entity\langues;
use AppBundle\Entity\KeysFile;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Valeurs;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AnotherService
{
    
    public function __construct( EntityManager $entityManager)
    {
        $this->em =  $entityManager;     
    }
    
    /**
     * @return 
     */
    public function getlistFiles($tab_files, $user_id)
    {
        $tabss= array();
      foreach($tab_files as $one)
      {
          $tabs= array();
          foreach($one as $two)
          { 
            if($user_id->getId() !== $two->getUpload()->getId()){
                $tab = array();
                array_push($tab,$two->getName());
                array_push($tab,$two->getUpload());
                array_push($tab,$two->getOriginLang());
                array_push($tab,$two->getLangues());
                // array_push($tab,$two->getOriginLang());
                array_push($tab,$two->getDateadd());
                array_push($tab,$two->getId());
                array_push($tabs, $tab);
            }
          }
         $tabss= array_merge($tabss, $tabs);
      }

    // dump(array_unique($tabss, SORT_REGULAR), $user_id->getId());
    return array_unique($tabss, SORT_REGULAR);
    }
    
}