<?php
namespace  AppBundle\Services;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Upload;
use AppBundle\Entity\langues;
use AppBundle\Entity\KeysFile;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Valeurs;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UploadService
{
    
    public function __construct( EntityManager $entityManager)
    {
        $this->em =  $entityManager;     
    }
    
    /**
     * @return 
     */
    public function addInArray($id)
    {
        $route = '/../../../web/uploads/files/';
        $table = array();
        $KeysFile = $this->em->getRepository('AppBundle:KeysFile');
        $query = $KeysFile->createQueryBuilder('p')->select('p.nameKey, p.id')->where('p.idFile =:idFile')->setParameter('idFile', $id)->getQuery();
        $res = $query->getResult();
        $valeurs = $this->em->getRepository('AppBundle:Valeurs');
        foreach ($res as $value) {
            $tab = array();
            $queryValeur = $valeurs->createQueryBuilder('r')->select('r.valeur')->where('r.id_key =:id_key')->setParameter('id_key', $value['id'])->getQuery();
            array_push($tab, $value['nameKey']);
            array_push($tab, $value['id']);
            $result = $queryValeur->getResult();
            if(!empty($result)){
                array_push($tab, $result[0]['valeur']);
            }
            array_push($table,$tab);
        }
        return $table;
    }
    /**
     * @return 
     */
    public function arrayOfUser($table){
        $users = array();
        $keyId = $this->em->getRepository('AppBundle:KeysFile')->findOneById($table[0][1]);
        // dump($keyId);
        $val = $this->em->getRepository('AppBundle:Valeurs');
        $query = $val->createQueryBuilder('p')->where('p.id_key =:id_key')->setParameter('id_key', $keyId->getId())->getQuery();
        $res = $query->getResult();
        
        foreach($res as $user){
            $tabs = array();
            $lang = $this->em->getRepository('AppBundle:langues')->findOneById($user->getLangueId());
            if(!empty($user->getIdUser()) && !empty($lang)){
                // dump($lang->getLangue());
                array_push($tabs,$user->getIdUser()->getUsername());
                array_push($tabs,$user->getIdUser()->getId());
                array_push($tabs,$lang->getLangue());
            }
             $users[] = $tabs;
        }
        $result = array_unique($users, SORT_REGULAR);
        //dump($result);
        return $result;
    }
    /**
     * @return 
     */
    public function arrayOfValueUser($table, $id_user, $langues){
        $users = array();
        $result = array();
         foreach($table as $tab){
            $keyId = $this->em->getRepository('AppBundle:KeysFile')->findOneById($tab[1]);
            $user = $this->em->getRepository('AppBundle:User')->findOneById($id_user);
            $lang =  $this->em->getRepository('AppBundle:langues')->findOneByLangue($langues);
            $val = $this->em->getRepository('AppBundle:Valeurs');
            $query = $val->createQueryBuilder('p')
            ->where('p.id_key =:id_key AND p.id_user =:id_user AND p.langue_id =:langue_id')
            ->setParameters(array(
                'id_key'=> $keyId->getId(),
                'langue_id'=> $lang->getId(),
                'id_user' => $user->getId()))
            ->getQuery();
            $res = $query->getResult();
            array_push($result,end ($res)->getValeur());
                 
            // }
         }
        return $result;
    }
    public function arrayOfLangeusUser_files($table, $id_user, $langues){
       
        return $result;
    }

}