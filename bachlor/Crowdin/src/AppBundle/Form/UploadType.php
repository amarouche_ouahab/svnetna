<?php

namespace AppBundle\Form;

use AppBundle\Entity\Upload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\langues;



class UploadType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',TextType::class, array('label' => 'choisissez un nom pour votre fichier'))
        ->add('fileUpload', FileType::class, array('label' => 'fichier a traduire'))
        // ->add('langues', ChoiceType::class, array('choices'  => array('Français' => 0, 'Anglais' => 1, 'Arabe' => 2),  'multiple'=> true))//, 'multiple'=> true
        ->add('langues',  EntityType::class, array('class' => langues::class, 'choice_label'=> 'Langue', 'multiple'=> true, 'label'=>'Les langues souhaité  '))
        ->add('origin_lang', ChoiceType::class, array('choices'  => array('Français' => 'Français', 'Anglais' => 'Anglais', 'Espagnol' => 'Espagnol', 'Arabe' => 'Arabe'), 'label'=>'La langue du fichier   '))
        ->add('save',SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Upload'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_upload';
    }


}
