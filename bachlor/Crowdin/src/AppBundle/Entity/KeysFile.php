<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * KeysFile
 *
 * @ORM\Table(name="keys_file")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\KeysFileRepository")
 */
class KeysFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Upload", inversedBy="idfileup")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     */
     
    private $idFile;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nameKey", type="string", length=255)
     */
    private $nameKey;
    
    
    
     /**
     * @var integer
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Valeurs", mappedBy="id_key",cascade={"persist"})
     */
    private $valeurs;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set nameKey
     *
     * @param string $nameKey
     *
     * @return KeysFile
     */
    public function setNameKey($nameKey)
    {
        $this->nameKey = $nameKey;

        return $this;
    }

    /**
     * Get nameKey
     *
     * @return string
     */
    public function getNameKey()
    {
        return $this->nameKey;
    }

    /**
     * Set idFile
     *
     * @param \AppBundle\Entity\Upload $idFile
     *
     * @return KeysFile
     */
    public function setIdFile(\AppBundle\Entity\Upload $idFile = null)
    {
        $this->idFile = $idFile;

        return $this;
    }

    /**
     * Get idFile
     *
     * @return \AppBundle\Entity\Upload
     */
    public function getIdFile()
    {
        return $this->idFile;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->valeurs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add valeur
     *
     * @param \AppBundle\Entity\Valeurs $valeur
     *
     * @return KeysFile
     */
    public function addValeur(\AppBundle\Entity\Valeurs $valeur)
    {
        $this->valeurs[] = $valeur;

        return $this;
    }

    /**
     * Remove valeur
     *
     * @param \AppBundle\Entity\Valeurs $valeur
     */
    public function removeValeur(\AppBundle\Entity\Valeurs $valeur)
    {
        $this->valeurs->removeElement($valeur);
    }

    /**
     * Get valeurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValeurs()
    {
        return $this->valeurs;
    }
}
