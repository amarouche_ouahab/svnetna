<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * listFile
 *
 * @ORM\Table(name="list_file")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\listFileRepository")
 */
class listFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="integer")
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="origin_lang", type="string", length=255)
     */
    private $originLang;

    /**
     * @var string
     *
     * @ORM\Column(name="to_lang", type="string", length=255)
     */
    private $toLang;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return listFile
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return listFile
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return listFile
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set originLang
     *
     * @param string $originLang
     *
     * @return listFile
     */
    public function setOriginLang($originLang)
    {
        $this->originLang = $originLang;

        return $this;
    }

    /**
     * Get originLang
     *
     * @return string
     */
    public function getOriginLang()
    {
        return $this->originLang;
    }

    /**
     * Set toLang
     *
     * @param string $toLang
     *
     * @return listFile
     */
    public function setToLang($toLang)
    {
        $this->toLang = $toLang;

        return $this;
    }

    /**
     * Get toLang
     *
     * @return string
     */
    public function getToLang()
    {
        return $this->toLang;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return listFile
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}

