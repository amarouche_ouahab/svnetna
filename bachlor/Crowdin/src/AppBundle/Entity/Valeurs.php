<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Valeurs
 *
 * @ORM\Table(name="valeurs")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ValeursRepository")
 */
class Valeurs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\langues",)
     * @ORM\JoinColumn(name="langue_id", referencedColumnName="id")
     */
    private $langue_id;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
     
    private $id_user;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\KeysFile", inversedBy="valeur")
     * @ORM\JoinColumn(name="id_key", referencedColumnName="id")
     */
    private $id_key;
    
   
    
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $valeur;

    /**
     * Get id
     *
     * @return int
     */
     
    public function getId()
    {
        return $this->id;
    }

   
    /**
     * Set idLang
     *
     * @param integer $idLang
     *
     * @return Valeurs
     */
    public function setIdLang($idLang)
    {
        $this->id_lang = $idLang;

        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer
     */
    public function getIdLang()
    {
        return $this->id_lang;
    }

    /**
     * Set valeur
     *
     * @param string $valeur
     *
     * @return Valeurs
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * Get valeur
     *
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    

    /**
     * Set idKey
     *
     * @param \AppBundle\Entity\KeysFile $idKey
     *
     * @return Valeurs
     */
    public function setIdKey(\AppBundle\Entity\KeysFile $idKey = null)
    {
        $this->id_key = $idKey;

        return $this;
    }

    /**
     * Get idKey
     *
     * @return \AppBundle\Entity\KeysFile
     */
    public function getIdKey()
    {
        return $this->id_key;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Valeurs
     */
    public function setIdUser(\AppBundle\Entity\User $idUser = null)
    {
        $this->id_user = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->id_user;
    }
   

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->langues = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add langue
     *
     * @param \AppBundle\Entity\langues $langue
     *
     * @return Valeurs
     */
    public function addLangue(\AppBundle\Entity\langues $langue)
    {
        $this->langues[] = $langue;

        return $this;
    }

    /**
     * Remove langue
     *
     * @param \AppBundle\Entity\langues $langue
     */
    public function removeLangue(\AppBundle\Entity\langues $langue)
    {
        $this->langues->removeElement($langue);
    }

    /**
     * Get langues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLangues()
    {
        return $this->langues;
    }

    /**
     * Set langues
     *
     * @param \AppBundle\Entity\langues $langues
     *
     * @return Valeurs
     */
    public function setLangues(\AppBundle\Entity\langues $langues = null)
    {
        $this->langues = $langues;

        return $this;
    }

    /**
     * Set langueId
     *
     * @param \AppBundle\Entity\langues $langueId
     *
     * @return Valeurs
     */
    public function setLangueId(\AppBundle\Entity\langues $langueId = null)
    {
        $this->langue_id = $langueId;

        return $this;
    }

    /**
     * Get langueId
     *
     * @return \AppBundle\Entity\langues
     */
    public function getLangueId()
    {
        return $this->langue_id;
    }
}
