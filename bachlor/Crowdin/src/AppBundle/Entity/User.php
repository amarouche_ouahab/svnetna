<?php

namespace AppBundle\Entity;


use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id 
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var integer
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Upload", mappedBy="upload",cascade={"persist"})
     */
    private $iduser;
    
    // /**
    //  * @var integer
    //  * 
    //  * @ORM\OneToMany(targetEntity="AppBundle\Entity\Upload", mappedBy="upload",cascade={"persist"})
    //  */
    // private $originlang;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
   /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\langues",)
     * @ORM\JoinColumn(name="langue_id", referencedColumnName="id")
     */
    private $langues;

    /**
     * Add iduser
     *
     * @param \AppBundle\Entity\Upload $iduser
     *
     * @return User
     */
    public function addIduser(\AppBundle\Entity\Upload $iduser)
    {
        $this->iduser[] = $iduser;

        return $this;
    }

    /**
     * Remove iduser
     *
     * @param \AppBundle\Entity\Upload $iduser
     */
    public function removeIduser(\AppBundle\Entity\Upload $iduser)
    {
        $this->iduser->removeElement($iduser);
    }

    /**
     * Get iduser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIduser()
    {
        return $this->iduser;
    }


    /**
     * Set langues
     *
     * @param \AppBundle\Entity\langues $langues
     *
     * @return User
     */
    public function setLangues(\AppBundle\Entity\langues $langues = null)
    {
        $this->langues = $langues;

        return $this;
    }

    /**
     * Get langues
     *
     * @return \AppBundle\Entity\langues
     */
    public function getLangues()
    {
        return $this->langues;
    }

    /**
     * Add langue
     *
     * @param \AppBundle\Entity\langues $langue
     *
     * @return User
     */
    public function addLangue(\AppBundle\Entity\langues $langue)
    {
        $this->langues[] = $langue;

        return $this;
    }

    /**
     * Remove langue
     *
     * @param \AppBundle\Entity\langues $langue
     */
    public function removeLangue(\AppBundle\Entity\langues $langue)
    {
        $this->langues->removeElement($langue);
    }
}
