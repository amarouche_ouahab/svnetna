<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Valeurs;
use AppBundle\Form\ValeursType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\UploadService;
use AppBundle\Entity\KeysFile;
use AppBundle\Entity\Upload;
use AppBundle\Entity\User;
use Symfony\Component\Form\FormFactoryInterface;


class TranslateController extends Controller
{
    /**
     * @Route("/translate/{langues}/{id}", name="translate")
     */
    public function listAction($langues, $id, UploadService $up, Request $request)
    {
        $table = $up->addInArray($id);
        
        $test = array();
        $user = $this->getUser();
        // foreach ($table as $value) {
        //     $valeurs = new Valeurs();
        //     $form = $this->createForm(ValeursType::class, $valeurs);
        //     $form->handleRequest($request); 
        //     // $form->createView();
        //     $test[] = $form->createView();
        // } 
            $lang = $this->getDoctrine()->getRepository('AppBundle:langues')->findOneByLangue($langues);

            $post = Request::createFromGlobals();
            if ($post->request->has('submit')) {
                $name = $post->request->get('value');
                foreach($name as $t => $val)
                {
                    $valeurs = new Valeurs(); 
                    $keyId = $this->getDoctrine()->getRepository('AppBundle:KeysFile')->findOneById($t);
                    $valeurs->setLangueId($lang);
                    $valeurs->setValeur($val);
                    $valeurs->setIdUser($user);
                    $valeurs->setIdKey($keyId);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($valeurs);
                    
                }
                $em->flush();
                return $this->render('translate/translateView.html.twig', array(
                            'table'=>$table,
                            'langues' => $langues,
                            'id' => $id,
                            'success'=>'Votre traduction a bien été prise en compte',
                        ));
            }

        return $this->render('translate/translateView.html.twig', array(
            'table'=>$table,
            'langues' => $langues,
            'id' => $id,
        ));
    }
    /**
     * @Route("/showtranslate/{langues}/{id_file}/{id_user}", name="showtranslate")
     */
    public function showTranslate($langues, $id_file, $id_user, UploadService $up, Request $request)
    {
        $table = $up->addInArray($id_file);
        $detail = $this->getDoctrine()->getRepository('AppBundle:Upload')->find($id_file);
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id_user);
        $value = $up->arrayOfValueUser($table,$id_user,$langues);
        // dump($table,$value,$detail);
        return $this->render('translate/usersTranslate.html.twig',array(
            'table' => $table,
            'values' => $value,
            'detail' => $detail,
            'id'=> $id_file,
            'user' => $user->getUsername(),
         ));
           
    }
    
}
