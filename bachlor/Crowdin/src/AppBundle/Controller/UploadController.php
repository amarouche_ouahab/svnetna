<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Upload;
use AppBundle\Entity\KeysFile;
use AppBundle\Entity\Valeurs;
use AppBundle\Entity\langues;
use AppBundle\Form\UploadType;

class UploadController extends Controller
{
    /**
     * @Route("/Upload", name="app_product_new")
     */
     public function newAction(Request $request)
    {
       
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $upload->getFileUpload();
            // dump($file->getClientOriginalName());
            $name = $upload->getName();
            $fileName = $this->generateUniqueFileName(). '.yaml';
            $pos = strpos($file->getClientOriginalName(), '.yaml');
            // dump($pos);
            $pos1 = strpos($file->getClientOriginalName(), '.yml');
            $pos2 = strpos($file->getClientOriginalName(), '.xliff');
            if($pos !== false || $pos1 !== false  || $pos2 !== false){
            $file->move(
                $this->getParameter('brochures_directory'),
                $fileName
            );
            $this->addToUploadTab($upload, $fileName);
            }
            else{
                return $this->render('Upload/new.html.twig', array(
                    'form' => $form->createView(),
                    'msg' => "Veuillez ajouter un fichier yaml ou xliff",
                ));
            }
         return $this->render('Upload/new.html.twig', array(
                'form' => $form->createView(),
                'success' => "Votre fichier a bien été ajouté",
            ));
            
        }
        return $this->render('Upload/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function addToUploadTab($upload, $fileName){
        $name = $upload->getName();
        $user = $this->getUser();
        // $upload->setOriginLang($user->getLangues());
        $upload->setDateadd(new\DateTime('now'));
        $upload->setUpload($user);
        $upload->setFileUpload($fileName);
        $upload->setName($name);
        $em = $this->getDoctrine()->getManager();
        $em->persist($upload);
        $em->flush();
        $this->ParseKeys($fileName, $upload);
    }
    
    public function ParseKeys($fileName, $upload){
        $route = '/../../../web/uploads/files/';
        if(file_exists(__DIR__.$route.$fileName)){
            $lines = file(__DIR__.$route.$fileName);
            foreach($lines as $line)
            {
                preg_match('/^.+(?=:)/',$line, $match);
                preg_match('/:.+/',$line, $matchval);
                if(array_key_exists('0',$match)){
                    if(array_key_exists('0',$matchval)){
                        $matchval= str_replace(':', '',$matchval[0]);
                        $this->addKeyToTable($match[0], $upload, trim($matchval));
                        //$this->addValueToTable($matchval, $upload);
                    }
                }
            }
            
        }
    }
    
    public function addKeyToTable($key, $up, $val)
    {
        $keyfile = new KeysFile();
        $keyfile->setNameKey($key);
        $keyfile->setIdFile($up);
        $em = $this->getDoctrine()->getManager();
        $em->persist($keyfile);
        $em->flush();
        $this->addValueToTable($val, $keyfile);
    }
    
    public function addValueToTable($val, $keys)
    {
        $valeur = new Valeurs();
        $user = $this->getUser();
        $valeur->setIdUser($user);
        $valeur->setValeur($val);
        $valeur->setIdKey($keys);
        $em = $this->getDoctrine()->getManager();
        $em->persist($valeur);
        $em->flush();
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
    
}
