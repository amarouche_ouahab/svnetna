<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Upload;
use Symfony\Component\Ldap\Adapter\ExtLdap\Query;
use AppBundle\Services\UploadService;
use AppBundle\Services\AnotherService;

class FileListController extends Controller
{
     /**
     * @Route("/showFile", name="showFile")
     */
    
    public function listAction(Request $request, AnotherService $another)
    {
        $tab_files = array();
        $langu = $this->getUser()->getLangues();
        foreach($langu as $lang)
        {
            $langs =  $this->getDoctrine()->getRepository('AppBundle:langues')->find($lang);
            $val_file = $this->getDoctrine()->getRepository('AppBundle:Upload');
            $query = $val_file->createQueryBuilder('p')
            ->where(':langues MEMBER OF p.langues')
            ->setParameter('langues', $langs->getId())
            ->getQuery();
            $res = $query->getResult();

            array_push($tab_files,$res);
            // dump($query->getResult());
        }
        // dump($another->getListFiles($tab_files));
        // $another->getListFiles($tab_files);
        // $user_lang = $this->getDoctrine()->getRepository('AppBundle:User')->findByLangues
        // $lists = $this->getDoctrine()->getRepository('AppBundle:Upload')->findAll();
        return $this->render('Upload/showFiles.html.twig', array(
            'tab_files'=>($another->getListFiles($tab_files, $this->getUser())),
        ));
    }
    
      /**
     * @Route("/detailfile/{id}", name="detailfile")
     */
    
    public function detailFile($id, UploadService $up)
    {
        
        $table = array();
        $users = array();
        $table = $up->addInArray($id);
        $users = $up->arrayOfUser($table);
        $langues = array();
        $detail = $this->getDoctrine()->getRepository('AppBundle:Upload')->find($id);
        // $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id_user);
        foreach($detail->getLangues() as $ka){
            foreach($this->getUser()->getLangues() as $user_lang){
                if($user_lang === $ka && $detail->getOriginLang() !== $ka){
                   array_push($langues,$ka->getLangue()); 
                }
            }
            
        }
        dump($langues);
        return $this->render('Upload/detailFile.html.twig', array(
            'table'=>$table,
            'detail'=> $detail,
            'id'=> $id,
            'users'=> $users,
            'langues'=>$langues,
        ));
        
    }
    

    
    
    
    
    
     /**
     * @Route("/showMyFile", name="showMyFile")
     */
    
    public function listMyFileAction(Request $request)
    {
        $user = $this->getUser();
        $list = $this->getDoctrine()->getRepository('AppBundle:Upload')->findBy(array('upload' =>  $user));
        return $this->render('Upload/showMyFiles.html.twig', array(
            'lists'=>$list,     
        ));
    }
    
     /**
     * @Route("/deleteMyFile/{id}", name="deleteMyFile")
     */
    
    public function deleteMyFileAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('AppBundle:Upload')->find($id);
        $em->remove($list);
        $em->flush();
        $this->addFlash(
            'notice',
            'fichier supprimer'
        );
        return $this->redirectToRoute('showMyFile');
    }
}
