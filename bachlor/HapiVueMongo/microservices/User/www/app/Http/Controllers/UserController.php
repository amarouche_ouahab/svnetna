<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use Log;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        //
    }

    public function showAllUsers()
    {
        Log::info('Showing user:');
        // Log::info('Showing user: '. (array)response()->json(User::all()));
        return response()->json(User::all());
    }

    public function showOneUser($id)
    {
        return response()->json(User::find($id));
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $data['password'] = md5($data['password']);
        // var_dump($data);
        Log::info('create user: '.response()->json($request->all()));
        $User = User::create($data);
        return response()->json($User, 201);
    }

    public function update($id, Request $request)
    {
        $User = User::findOrFail($id);
        $User->update($request->all());
        return response()->json($User, 200);
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function userValidation($id, Request $request){
        $msg = User::UserValidation($id, $request->all());
        return response($msg, 200);
    }

    public function login(Request $request)
    {
        $msg = User::UserToLogin($request->all());
        if(array_key_exists('code', $msg) && $msg["code"]=== 401){

        }
        // else if(array_key_exists('code', $msg) && $msg['code'] === 201){
        // }
        return response()->json($msg, 201);

    }

    // public function doLogin(Request $request)
	// {
	// 	// validate the info, create rules for the inputs
	// 	$rules = array(
	// 		'email'    => 'required|email', // make sure the email is an actual email
	// 		'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
	// 	);
    //     // var_dump($rules);
    //     $userdata = array(
    //         'email' 	=>'chris@scotch.io',
    //         'password' 	=> 'admin'
    //     );
	// 	// run the validation rules on the inputs from the form
	// 	$validator = Validator::make($userdata, $rules);

	// 	// if the validator fails, redirect back to the form
	// 	if ($validator->fails()) {
    //         return response(['status' => 'fail'],401);
	// 		// return Redirect::to('login')
	// 		// 	->withErrors($validator) // send back all errors to the login form
	// 		// 	->withInput('test'); // send back the input (not the password) so that we can repopulate the form
	// 	} else {

	// 		// create our user data for the authentication
	// 		$userdata = array(
	// 			'email' 	=>' chris@scotch.io',
	// 			'password' 	=> 'admin'
    //         );
    //         var_dump(Auth::attempt(['email' => 'chris@scotch.io', 'password' => 'admin']));
	// 		if (Auth::attempt($userdata)) {
	// 			echo 'SUCCESS!';

	// 		} else {
    //             return response(['status' => 'fail'],401);

	// 		}

	// 	}
	// }

}



