<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email','degrer',"validation", 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    static function UserValidation($id, $data){
        if( !array_key_exists('idUserDegre', $data)){
            return  ['key idUserDegre no found'];
        }
        $userDegre = User::where('id', $data['idUserDegre'])->first()->degrer;
        $newUser = User::where('id',$id);
        if(is_null($userDegre) || is_null($newUser->first())){
            return ['user no exsiste or no found key idUserDegre'];
        }
        if($userDegre == 'chef'){
            if($newUser->first()->validation !== 1){
                $newUser->update(['validation' => 1]);
                return ["messsage" => "le compte ".$newUser->first()->email." a bien été validé"];
            }
            else{
                return ["messsage" => "le compte ".$newUser->first()->email." a déja été validé"];

            }
        }else{
            return ["message"=> "Seul un chef peut valider un compte"];
        }
    }

    static function UserToLogin($data){
        $data = (array) $data;
        $user = User::where([['email','=', $data['email']], ['password' ,'=', md5($data['password'])]])->first();
        if(!is_null($user)){
            if(!$user['validation'])
                {
                    return ['message'=> "Votre compte n'est pas activer", "code" => 401 ];
                }
            else {
                return $user;
            }
        }else{
            return ['message'=> "Votre compte n'existe pas", "code" => 401 ];
        }
        return $data;
    }
}
