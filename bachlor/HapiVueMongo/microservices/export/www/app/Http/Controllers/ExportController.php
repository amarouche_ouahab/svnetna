<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Export;
use DB;

class ExportController extends Controller
{
    public function exportToCSV()
    {
        $users = DB::table('users')->get()->toArray();
        $path = Export::export($users);
        return response()->download($path);

        // return response()->json($path);
    }
}
