<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Export extends Model 
{
    static function export($users){
        $data = [];
        // var_dump(resource_path().'/users_export_'.date("Y-m-d_H:i:s"));
        foreach ($users as $key => $value) {
            $data[] = (array) $value;
        }
        Excel::create('users_export_'.date("Y-m-d_H"), function($excel) use($data) {
            $excel->setTitle('test');
            $excel->setDescription('A demonstration to change the file properties');
            $excel->sheet('Excel sheet', function($sheet) use ( $data) {
                $sheet->fromArray($data);
            });
        })->store('csv',  resource_path());
        return resource_path().'/users_export_'.date("Y-m-d_H").".csv";
        // return $data;

    }
}