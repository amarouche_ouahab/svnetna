'use strict';

const Users = require('../service/users');
const UserController = require('../controllers/userController');
const Export = require('../service/export');
const Crimes = require('../service/crimes');
const Hapi = require('hapi');
const config = require('config');
const server = new Hapi.server({
    port: config.get('api.port'),
    host: config.get('api.host')
});
// const server = Hapi.server(config.get("api"));
const options = {
    ops: {
        interval: 1000
    },
    reporters: {
        myConsoleReporter: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ log: '*', response: '*' }]
        }, {
            module: 'good-console'
        }, 'stdout'],
    }
}
const handlerPost =  (request, h) => {
    return 'post ok';
};

server.route({
    method: 'GET',
    path: '/',
    handler: (request, h) => {
        return 'Hello, world!';
    }
});

// server.route({
//     method: ['PUT','POST'],
//     path: '/api/user',
//     handler: (request, h) => {
//         return Users.addUser(request.payload)
//         // return request;

//     }
// });

// server.route({
//     method: ['PUT','POST'],
//     path: '/login',
//     handler: (request, h) => {
//         return Users.userLogin(request.payload)
//         // return request;

//     }
// });

const routes =[
    { method: 'GET', path: '/api/user',handler:// UserController.showAllUsers 
     (request, reply) => {
            // return ['rree']
            // return reply.response(Users.getUser()).code(200);
            return Users.getUser();
        }},
    { method: ['PUT','POST'],path: '/login',handler: (request, h) => {return Users.userLogin(request.payload)}},
    { method: ['PUT','POST'],path: '/api/user',handler: (request, h) => {return Users.addUser(request.payload)}},
    { method: ['PUT','POST'],path: '/validation/{id}',handler: (request, h) => {return Users.userValidation(request.payload, request.params.id)}},
    { method: 'GET', path: '/exportToCsv',handler: (request, reply) => {
        return Export.exportCsv()
        }
    },
    { method: 'GET', path: '/crimes',handler: (request, reply) => {return Crimes.getCrimes()}},
    { method: 'POST', path: '/crimes/add',handler: (request, reply) => {return Crimes.addCrimes(request.payload)}},
    { method: 'GET', path: '/crimes/search',handler: (request, reply) => {return Crimes.searchCrimes(request.payload)}},
    { method: 'POST', path: '/crimes/delete/{id}',handler: (request, reply) => {return Crimes.deleteCrimes(request.payload)}},
];
server.route(routes);
// server.route({
//     method: 'GET',
//     path: '/api/user',
//     handler: (request, reply) => {
//         console.log(reply)
//         // const data = Users.getUser()
//         //return reply( Users.getUser()).code(200)
//         // console.log(request.params)
//          return Users.getUser();
//     }
// });

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

const init = async () => {
    await server.register({
        plugin: require('good'),
        options,
    });
    await server.start();
    server.log(`Server running at: ${server.info.uri}`);
};

server.events.on('log', (event, tags) => {
    if (tags.error) {
        console.log(`Server error: ${event.error ? event.error.message : 'unknown'}`);
    }
});

init();

// var users =[{'user':'yye'},{'user':'yyfdfde'},{'user':'gfgfe'}]
// const server = Hapi.server({ port: 80 });
// request.get('http://localhost:8081')
//     .on('response', function(response) {
//     console.log(response.statusCode) // 200
//     console.log(response.headers['content-type']) // 'image/png'
// })
// const request = require('request');

// const startServer = async function() {
   

// const get = new Promise((resolve, reject) => {
//     try {
//         request('http://localhost:8081/api/user', { json: true }, (err, res, body) => {
//             if (err) { return console.log(err); }
//             // console.log(body,'');
//             resolve(body)
//             });
//         }
//         catch(e) {
//             reject('');
//             // console.log('Failed to load h2o2');
//         }
// });

// request.post('http://localhost:8081/api/user', {name:'vaue'})
// request('http://localhost:8081/api/user').then(function (htmlString) {
//         // Process html...
//     })
//     .catch(function (err) {
//         // Crawling failed...
//     });

// const server2 = new Hapi.Server({
//     port: 8081,
//     host: 'localhost'
// });

