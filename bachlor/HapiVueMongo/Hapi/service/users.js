const request = require('request');

class UserService {
    getUser(){
        return new Promise((resolve, reject) => {
            try {
                request('http://localhost:8081/api/user', { json: true }, (err, res, body) => {
                    if (err) { return console.log(err); }
                    resolve(body)
                    });
                }
                catch(e) {
                    reject('connexion api faile');
                }
        });
    }

    addUser(data){
        return new Promise((resolve, reject) => {
            try {//formData
                request.post('http://localhost:8081/api/user', {form:data}, (err, res, body) => {
                    if (err) { return console.log(err); }
                    console.log(body);
                    resolve(body)
                    });
                }
                catch(e) {
                    reject('connexion api faile');
                }
        });
    }
    userLogin(data){
        return new Promise((resolve, reject) => {
            try {//formData
                request.post('http://localhost:8081/api/login', {form:data}, (err, res, body) => {
                    if (err) { return console.log(err); }
                    console.log(body);
                    // console.log(data);
                    resolve(body)
                    });
                }
                catch(e) {
                    reject('connexion api faile');
                }
        });
    }
    userValidation(data,id){
        return new Promise((resolve, reject) => {
            try {//formData
                request.put('http://localhost:8081/api/user/validation/'+id, {form:data}, (err, res, body) => {
                    if (err) { return console.log(err); }
                    console.log(body);
                    // console.log(data);
                    resolve(body)
                    });
                }
                catch(e) {
                    reject('connexion api faile');
                }
        });
    }
}

module.exports = new UserService();