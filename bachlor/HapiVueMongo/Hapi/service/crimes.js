const request = require('request');

class CrimesService {
    getCrimes(){
        return new Promise((resolve, reject) => {
            try {
                request('http://localhost:8083/crimes', { json: true }, (err, res, body) => {
                    if (err) { return console.log(err); }
                    resolve(body)
                    });
                }
                catch(e) {
                    reject('connexion api faile');
                }
        });
    }
    searchCrimes(){
        return new Promise((resolve, reject) => {
            try {
                request('http://localhost:8083/crimes/search', { json: true }, (err, res, body) => {
                    if (err) { return console.log(err); }
                    resolve(body)
                    });
                }
                catch(e) {
                    reject('connexion api faile');
                }
        });
    }
    deleteCrimes(data,id){
        return new Promise((resolve, reject) => {
            try {//formData
                request.delete('http://localhost:8083/crimes/delete'+id, {form:["delete ok"]}, (err, res, body) => {
                    if (err) { return console.log(err); }
                    console.log(body);
                    // console.log(data);
                    resolve(body)
                    });
                }
                catch(e) {
                    reject('connexion api faile');
                }
        });
    }
    addCrimes(data){
        return new Promise((resolve, reject) => {
            try {//formData
                request.post('http://localhost:8083/crimes/add', {form:data}, (err, res, body) => {
                    if (err) { return console.log(err); }
                    console.log(body);
                    // console.log(data);
                    resolve(body)
                    });
                }
                catch(e) {
                    reject('connexion api faile');
                }
        });
    }
}

module.exports = new CrimesService();