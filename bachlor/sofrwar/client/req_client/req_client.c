#include <czmq.h>

int main(int argc, char *argv[])
{
    char send[1024];

    if (argc < 3) {
    printf("Port parameter is mandatory and identity\n");
    return 0;
    }

    printf("Connecting to echo...\n");
    zsock_t *req = zsock_new(ZMQ_REQ);
    zsock_set_identity(req, argv[2]);
    zsock_connect(req, "tcp://localhost:%s", argv[1]);

    while (1){
        if (!fgets(send, 1024, stdin)) {
            return 1;
        }
        zstr_sendf(req, "%s", send);
        char *message = zstr_recv(req);
        printf("Received : %s\n", message);
        zstr_free(&message);
    }
    zsock_destroy(&req);
    return 0;
}