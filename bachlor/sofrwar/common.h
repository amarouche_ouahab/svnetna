/*
** common.h for my_slack in /home/verpilc/Documents/my_slack
**
** Made by VERPILLAT Corentin
** Login   <verpil_c@etna-alternance.net>
**
** Started on  Wed Feb 14 13:54:45 2018 VERPILLAT Corentin
** Last update Wed Feb 14 13:54:46 2018 VERPILLAT Corentin
*/

#ifndef __COMMON_H__
#define __COMMON_H__

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <czmq.h>
#include <ctype.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <getopt.h>
#include <libgen.h>


typedef struct s_color t_color;
struct s_color
{
  char *color;
  char *unicode;
};

static const t_color g_color[] =
    {
        {"clear", "\033[H\033[2J"},
        {"red", "\033[31m"},
        {"green", "\033[32m"},
        {"yellow", "\033[33m"},
        {"blue", "\033[34m"},
        {"magenta", "\033[35m"},
        {"cyan", "\033[36m"},
        {NULL, NULL}};

void my_put_nbr(int n);
void my_put_nbr_color(const char *color, const int n);
void my_putchar(const char c);
int my_strlen(const char *str);
void my_putstr(const char *str);
int my_strcmp(const char *s1, const char *s2);
char *my_strdup(const char *str);
void my_putstr_color(const char *color, const char *str);
void my_puts(const char *str);
char *my_strncat(char *str, const char *src, int nb);
char *my_strncpy(char *dest, char *src, int n);

typedef struct s_notif_type t_notif_type;
struct s_notif_type
{
  char *notif;
};

static const t_notif_type notif_tab[] =
    {
        {"cycle_info"},
        {"game_started"},
        {"game_finished"},
        {"client_dead"},
        {"client_win"},
        {NULL}};

#endif
