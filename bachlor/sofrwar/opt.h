#ifndef __OPT_H__
#define __OPT_H_

int is_number(char *str);
int check_dir(char *dirname);
int check_file(char *filename);
void write_log(char *text);
void verb(char *text);

#endif
