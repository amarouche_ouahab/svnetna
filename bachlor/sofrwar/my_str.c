#include "common.h"

char *my_strncpy(char *str, char *src, int n)
{
  int i;

  i = 0;
  while (i < n && src[i] != '\0')
  {
    str[i] = src[i];
    i++;
  }
  while (i <= n)
  {
    str[i] = '\0';
    i++;
  }
  return (str);
}

void my_puts(const char *str)
{
  my_putstr(str);
  my_putstr("\n");
}

char *my_strncat(char *str, const char *src, int nb)
{
  int length;
  int i;

  length = my_strlen(str);
  i = 0;
  while (i < nb && src[i])
  {
    str[length + i] = src[i];
    i += 1;
  }
  str[length + i] = '\0';
  return (str);
}