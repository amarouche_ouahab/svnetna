#ifndef __GAME_INFO_H__
#define __GAME_INFO_H__


#include "energy_cell.h"
#include "player.h"

typedef struct s_args t_args;
struct s_args
{
    int verbose;
    int size;
    char *logfile;
    char *logdir;
    char *logpath;
    int cycle;
    int repPort;
    int pubPort;
};

typedef struct s_game_info t_game_info;
struct s_game_info
{
  unsigned int  map_size;
  unsigned int  game_status;
  t_listp  *p_players;
  t_listec *p_cells;
  t_args  *args;
};

t_args  *create_args();
t_game_info *create_game_info(unsigned int map_size, t_listp *p_players, t_listec *p_cells, t_args *args);
void remove_game_info(t_game_info **p_game);
t_args *check_opt(int argc, char **argv);

#endif
