#ifndef __THREAD_FNCT_H__
#define __THREAD_FNCT_H__

#include "player.h"

int check_if_position_empty(unsigned int x, unsigned int y);
int check_if_identify_exist(char *identity);
void decrement_energy_player();

#endif
