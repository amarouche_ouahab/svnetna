#include "server.h"

int connect_user(char *identity, char *socket_id)
{
    (void)socket_id;
    if (game->p_players->length >= 4){
        send_message("ko|game full\n", socket_id);
        return -1;
    }
    int length = game->p_players->length;
    int x = (length % 2 == 0) ? 0 : game->args->size - 1;
    int y = (length / 2 == 0) ? 0 : game->args->size - 1;
    int direction = (length / 2 == 0) ? 3 : 1;
    if (check_if_identify_exist(identity) == -1){
        send_message("ko|identity already exists\n", socket_id);
        return -1;
    }
    add_player(game->p_players, identity, x, y, 50, direction, socket_id);
    if (game->p_players->length == 4){
        game->game_status = 1;
        event_start_game = 1;
    }
    send_message("ok|connected\n", socket_id);
    return 0;
}