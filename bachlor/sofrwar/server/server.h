#ifndef __SERVER_H__
#define __SERVER_H__

#include "../common.h"
#include "energy_cell.h"
#include "player.h"
#include "game_info.h"
#include "../opt.h"
#include "thread.h"
#include "rfc_client.h"
#include "thread_fnct.h"

void send_message(char *message, char *socket_id);

#define DEF_REPPORT 4242
#define DEF_PUBPORT 4243
#define VERBOSE_MODE 0
#define SIZE_MIN 5
#define CYCLE_DURATION 500000
#define MIN_ENERGY_CYCLE 5
#define MAX_ENERGY_CYCLE 15

extern t_game_info	*game;

#endif