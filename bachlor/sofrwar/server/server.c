#include "server.h"

pthread_cond_t condition = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int event_cycle_info = 0;
int event_start_game = 0;
int event_end_game = 0;
int event_player_death = 0;
int event_player_win = 0;
zsock_t *router;

void* rep_server(void* arg)
{
	(void)arg;
	char *data;
	char *fnct;
	int	found;

	router = zsock_new(ZMQ_ROUTER);
	zsock_bind(router, "tcp://*:%d", game->args->repPort);
	while (!zsys_interrupted) {
		zmsg_t *message = zmsg_recv(router);
		zframe_t *identity = zmsg_pop(message);
		zframe_t *empty = zmsg_pop(message);
    	zframe_t *content = zmsg_pop(message);
		zmsg_destroy(&message);
    	printf("identity: %s\n", zframe_strdup(identity));
    	printf("empty: %s\n", zframe_strdup(empty));
    	printf("content: %s\n", zframe_strdup(content));
		fnct = parse(zframe_strdup(content), &data);
		found = 0;
		for(int i = 0; exec_tab[i].cha != NULL; i++){
			if (strcmp(exec_tab[i].cha, fnct) == 0)
			{
				exec_tab[i].fnct_server(data, zframe_strdup(identity));
				found = 1;
			}
			if (found == 0){
    			send_message("ko|command not found\n", zframe_strdup(identity));
			}
		}
		event_start_game = 1;
		if (event_cycle_info || event_start_game || event_end_game || event_player_death || event_player_win) {
			pthread_mutex_lock(&mutex);
			pthread_cond_signal(&condition);
			pthread_mutex_unlock(&mutex);
		}
		zframe_destroy(&identity);
		zframe_destroy(&empty);
		zframe_destroy(&content);
	}
	zsock_destroy(&router);
	pthread_exit(NULL);
}

void* pub_server(void* arg)
{
    (void)arg;

    zsock_t *chat_srv_socket = zsock_new(ZMQ_PUB);
    zsock_bind(chat_srv_socket, "tcp://*:%d", game->args->pubPort);
    printf("Server listening on tcp://*:%d\n", game->args->pubPort);
    while (!zsys_interrupted) {
		pthread_mutex_lock(&mutex);
		pthread_cond_wait(&condition, &mutex);
		if (event_cycle_info){
			fprintf(stderr, "print game info struct\n");
			zstr_sendf(chat_srv_socket, "%s\n", "game info json");
			event_cycle_info = 0;
		}
		else if (event_start_game) {
			fprintf(stderr, "game start\n");
			zstr_sendf(chat_srv_socket, "%s\n", "the game has start");
			game->game_status = 1;
			event_start_game = 0;
		}
		else if (event_end_game) {
			fprintf(stderr, "game end\n");
			zstr_sendf(chat_srv_socket, "%s\n", "the game has end");
			game->game_status = 2;
			event_end_game = 0;
		}
		else if (event_player_death){
			fprintf(stderr, "player die\n");
			zstr_sendf(chat_srv_socket, "the player %s die\n", "name");
			event_player_death = 0;
		}
		else if (event_player_win){
			fprintf(stderr, "player win\n");
			zstr_sendf(chat_srv_socket, "the player %s win\n", "name");
			event_player_win = 0;
		}
		pthread_mutex_unlock(&mutex);
    }
    zsock_destroy(&chat_srv_socket);
    pthread_exit(NULL);
}

void* cycle_thread(void* arg)
{
	(void)arg;


	srand(time(NULL));
	while(1){
		if (game->game_status == 1)
		{
			//new cycle
			int num = (rand() % (MAX_ENERGY_CYCLE - MIN_ENERGY_CYCLE + 1)) + MIN_ENERGY_CYCLE;
			int x = (rand() % ((game->args->size - 1) - 0 + 1)) + 0;
			int y = (rand() % ((game->args->size - 1) - 0 + 1)) + 0;
			while (check_if_position_empty(x, y) == -1){
				x = (rand() % ((game->args->size - 1) - 0 + 1)) + 0;
				y = (rand() % ((game->args->size - 1) - 0 + 1)) + 0;
			}
			add_cell(game->p_cells, x, y, num);
			// decrement_energy_player();
			usleep(game->args->cycle);
		}
	}
	pthread_exit(NULL);
}

void send_message(char *message, char *socket_id)
{
	zmsg_t *msg = zmsg_new ();
    zframe_t *identity2 = zframe_from(socket_id);
    zframe_t *empty = zframe_new ("", 0);
    zframe_t *frame = zframe_new (message, strlen(message));
    zmsg_prepend(msg, &identity2);
    zmsg_append(msg, &empty);
    zmsg_append(msg, &frame);
    zmsg_send (&msg, router);
}