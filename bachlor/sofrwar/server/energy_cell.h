#ifndef __ENERGY_CELL_H__
#define __ENERGY_CELL_H__

typedef struct s_energy_cell t_energy_cell;
struct s_energy_cell
{
  unsigned int  x;
  unsigned int  y;
  unsigned int  value;
  t_energy_cell *p_next;
  t_energy_cell *p_prev;
};

typedef struct s_listec t_listec;
struct s_listec
{
  size_t length;
  t_energy_cell *p_first;
  t_energy_cell *p_last;
};

t_listec *create_cells(void);
t_energy_cell *add_cell(t_listec *p_cells, unsigned int x, unsigned int y, unsigned int value);
void delete_cell(t_listec *p_cells, t_energy_cell *cell);
int del_condition_cell(t_listec *p_cells, t_energy_cell *cell, t_energy_cell **temp);
void remove_cells(t_listec **p_cells);


#endif
