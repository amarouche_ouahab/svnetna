#ifndef __RFC_CLIENT_H__
#define __RFC_CLIENT_H__

#include <stdlib.h>
#include "commands.h"

int    connect_user(char *identity, char *socket_id);

typedef struct s_client_exec t_client_exec;
struct s_client_exec
{
  char *cha;
  int (*fnct_server)(char *data, char *socket_id);
};

static const t_client_exec exec_tab[] =
    {
        {"identify", &connect_user},
        {NULL, NULL}
    };


#endif
