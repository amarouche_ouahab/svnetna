#ifndef __PLAYER_H__
#define __PLAYER_H__

typedef struct s_player t_player;
struct s_player
{
  char  *name;
  unsigned int  x;
  unsigned int  y;
  unsigned int  energy;
  unsigned int  looking;
  char  *socket_id;
  t_player *p_next;
  t_player *p_prev;
};

typedef struct s_listp t_listp;
struct s_listp
{
  size_t length;
  t_player *p_first;
  t_player *p_last;
};

t_listp *create_players(void);
t_player *add_player(t_listp *p_players, char *name, unsigned int x, unsigned int y, unsigned int energy, unsigned int looking, char *socket_id);
void delete_player(t_listp *p_players, t_player *player);
int del_condition(t_listp *p_players, t_player *player, t_player **temp);
void remove_players(t_listp **p_players);


#endif
