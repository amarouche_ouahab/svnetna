#include "server.h"

t_listec *create_cells(void)
{
    t_listec *p_cells;

    if ((p_cells = malloc(sizeof *p_cells)) != NULL)
    {
        p_cells->length = 0;
        p_cells->p_first = NULL;
        p_cells->p_last = NULL;
    }
    return (p_cells);
}

t_energy_cell *add_cell(t_listec *p_cells, unsigned int x, unsigned int y, unsigned int value)
{
    t_energy_cell *p_new;

    if (p_cells != NULL)
    {
        if ((p_new = malloc(sizeof *p_new)) != NULL)
        {
            p_new->x = x;
            p_new->y = y;
            p_new->value = value;
            p_new->p_next = NULL;
            if (p_cells->p_last == NULL)
            {
                p_new->p_prev = NULL;
                p_cells->p_first = p_new;
                p_cells->p_last = p_new;
            }
            else
            {
                p_cells->p_last->p_next = p_new;
                p_new->p_prev = p_cells->p_last;
                p_cells->p_last = p_new;
            }
            p_cells->length++;
        }
    }
    return (p_new);
}

void delete_cell(t_listec *p_cells, t_energy_cell *cell)
{
    t_energy_cell *temp;
    int del;

    if (p_cells != NULL && cell != NULL)
    {
        if (p_cells->length == 1)
        {
            p_cells->p_first = NULL;
            p_cells->p_last = NULL;
            p_cells->length -= 1;
        }
        else
        {
            del = 0;
            temp = p_cells->p_first;
            while (temp != NULL && !del)
            {
                del = del_condition_cell(p_cells, cell, &temp);
            }
            free(temp);
        }
    }
}

int del_condition_cell(t_listec *p_cells, t_energy_cell *cell, t_energy_cell **temp)
{
    if ((*temp) == cell)
    {
        if ((*temp)->p_next == NULL)
        {
            p_cells->p_last = (*temp)->p_prev;
            p_cells->p_last->p_next = NULL;
        }
        else if ((*temp)->p_prev == NULL)
        {
            p_cells->p_first = (*temp)->p_next;
            p_cells->p_first->p_prev = NULL;
        }
        else
        {
            (*temp)->p_next->p_prev = (*temp)->p_prev;
            (*temp)->p_prev->p_next = (*temp)->p_next;
        }
        p_cells->length -= 1;
        return (1);
    }
    else
        (*temp) = (*temp)->p_next;
    return (0);
}

void remove_cells(t_listec **p_cells)
{
    if (*p_cells != NULL)
    {
        t_energy_cell *temp = (*p_cells)->p_first;
        while (temp != NULL)
        {
            t_energy_cell *to_del = temp;
            temp = temp->p_next;
            free(to_del);
        }
        free(*p_cells);
        *p_cells = NULL;
    }
}