#ifndef __THREAD_H__
#define __THREAD_H__

#include <pthread.h>

void* rep_server(void* arg);
void* pub_server(void* arg);
void* cycle_thread(void* arg);

extern int event_cycle_info;
extern int event_start_game;
extern int event_end_game;
extern int event_player_death;
extern int event_player_win;
extern zsock_t *router;

#endif