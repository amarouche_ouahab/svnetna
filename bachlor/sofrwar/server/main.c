#include "server.h"

t_game_info	*game;

int main(int argc, char **argv)
{	
	t_listp *p_players;
	t_listec *p_cells;
	pthread_t thread_rep;
	pthread_t thread_pub;
	pthread_t thread_cycle;

    t_args *args = check_opt(argc, argv);
    if (args == NULL){
        return 1;
    }
	p_players = create_players();
    p_cells = create_cells();
    game = create_game_info(args->size, p_players, p_cells, args);
	verb("verbose");
	write_log("write in log file");

	pthread_create(&thread_rep, NULL, rep_server, (void*)NULL);
	pthread_create(&thread_pub, NULL, pub_server, (void*)NULL);
	pthread_create(&thread_cycle, NULL, cycle_thread, (void*)NULL);

	pthread_join (thread_rep, NULL);
	pthread_join (thread_pub, NULL);
	pthread_join (thread_cycle, NULL);
	
	return 0;
}

t_args *check_opt(int argc, char **argv)
{
	int verbose = VERBOSE_MODE;
	int size = -1;
	char *logfile;
	char *logdir;
	char logpath[1024];
	int cycle = CYCLE_DURATION;
	int repPort = DEF_REPPORT;
	int pubPort = DEF_PUBPORT;
    int c;
	const char *short_opt = "vh";
	struct option long_opt[] =
	{
		{"help",     no_argument      , NULL, 'h'},
		{"size",     required_argument, NULL, 's'},
		{"log",      required_argument, NULL, 'l'},
		{"cycle",    required_argument, NULL, 'c'},
		{"rep-port", required_argument, NULL, 'r'},
		{"pub-port", required_argument, NULL, 'p'},
		{NULL, 0, NULL, 0}
	};
    while ((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
	{
		switch(c)
		{
			case -1:
			case 0:
				break;

			case 'h':
				printf("Usage: %s [OPTIONS]\n", argv[0]);
				printf("  -h, --help                print this help and exit\n");
				printf("  --size     int            size of the grid\n");
				printf("  --log      file           log file\n");
				printf("  --cycle    int            microseconds per cycle (> 0)\n");
				printf("  --rep-port port           port used for client commands and responses\n");
				printf("  --pub-port port           port used for notifications\n");
				printf("\n");
				return NULL;

			case 's':
				if (!is_number(optarg)) {
					fprintf(stderr, "server: [size] must be a number\n");
					return NULL;
				}
				size = atoi(optarg);
				if (size < SIZE_MIN) {
					fprintf(stderr, "server: [size] must be greater than or equal to %d\n", SIZE_MIN);
					return NULL;
				}
				break;
			case 'v':
				verbose = 1;
				break;
			case 'l':
				logdir = dirname(optarg);
				logfile = basename(optarg);
				strcat(logpath, logdir);
				strcat(logpath, "/");
				strcat(logpath, logfile);

				if (!check_dir(logdir) || !check_file(logpath)) {
					fprintf(stderr, "server: %s is either not existing, not a regular file or not writable\n", logpath);
					return NULL;
				}
				break;

			case 'c':
				if (!is_number(optarg) || (cycle = atoi(optarg)) <= 0) {
					fprintf(stderr, "server: [cycle] must be a positive number\n");
					return NULL;
				}
				break;

			case 'r':
				if (!is_number(optarg) || (repPort = atoi(optarg)) <= 0 || repPort >= 65536) {
					fprintf(stderr, "server: [repPort] must be between 0 and 65536\n");
					return NULL;
				}
				break;

			case 'p':
				if (!is_number(optarg) || (pubPort = atoi(optarg)) <= 0 || pubPort >= 65536) {
					fprintf(stderr, "server: [pubPort] must be between 0 and 65536\n");
					return NULL;
				}
				printf("[p] you entered \"%s\"\n", optarg);
				break;

			case ':':
			case '?':
				fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
				return NULL;

			default:
				fprintf(stderr, "%s: invalid option -- %c\n", argv[0], c);
				fprintf(stderr, "Try `%s --help' for more information.\n", argv[0]);
				return NULL;
		};
	}
    if (size == -1){
		fprintf(stderr, "server: [size] has no value\n");
	    return NULL;
    }
    if (repPort == pubPort) {
		fprintf(stderr, "server: [pubPort] and [repPort] must not be the same\n");
		return NULL;
	}
	t_args *args = create_args(verbose, size, logfile, logpath, cycle, repPort, pubPort);
    return args;
}