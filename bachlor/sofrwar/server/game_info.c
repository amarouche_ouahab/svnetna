#include "server.h"

t_args  *create_args(int verbose, int size, char *logfile, char *logpath, int cycle, int repPort, int pubPort)
{
    t_args  *args;

    if ((args = malloc(sizeof *args)) != NULL)
    {
        args->verbose = verbose;
        args->size = size;
        args->logfile = logfile;
        args->logpath = logpath;
        args->cycle = cycle;
        args->repPort = repPort;
        args->pubPort = pubPort;
    }
    return (args);
}

t_game_info *create_game_info(unsigned int map_size, t_listp *p_players, t_listec *p_cells, t_args *args)
{
    t_game_info *p_game;

    if ((p_game = malloc(sizeof *p_game)) != NULL)
    {
        p_game->map_size = map_size;
        p_game->game_status = 0;
        p_game->p_cells = p_cells;
        p_game->p_players = p_players;
        p_game->args = args;
    }
    return (p_game);
}

void remove_game_info(t_game_info **p_game)
{
    if (*p_game != NULL)
    {
        if ((*p_game)->p_cells != NULL)
        {
            remove_cells(&(*p_game)->p_cells);
        }
        if ((*p_game)->p_players != NULL)
        {
            remove_players(&(*p_game)->p_players);
        }
        free(*p_game);
        *p_game = NULL;
    }
}