#include "server.h"

int check_if_position_empty(unsigned int x, unsigned int y)
{
    t_player *temp_player;
    t_energy_cell *temp_cell;

    if (game->p_players != NULL)
    {
        temp_player = game->p_players->p_first;
        while (temp_player != NULL)
        {
            if (temp_player->x == x || temp_player->y == y)
            {
                return (-1);
            }
            temp_player = temp_player->p_next;
        }
    }
    if (game->p_cells != NULL)
    {
        temp_cell = game->p_cells->p_first;
        while (temp_cell != NULL)
        {
            if (temp_cell->x == x || temp_cell->y == y)
            {
                return (-1);
            }
            temp_cell = temp_cell->p_next;
        }
    }
    return (0);
}

int check_if_identify_exist(char *identity)
{
    t_player *temp;

    if (game->p_players != NULL)
    {
        temp = game->p_players->p_first;
        while (temp != NULL)
        {
            if (strcmp(temp->name, identity) == 0)
            {
                return (-1);
            }
            temp = temp->p_next;
        }
    }
    return (0);
}

void decrement_energy_player()
{
    t_player *temp;

    if (game->p_players != NULL)
    {
        temp = game->p_players->p_first;
        while (temp != NULL)
        {
            if (temp->energy <= 2){
                event_player_death = 1;
                continue;
            }
            temp->energy -= 2;
            temp = temp->p_next;
        }
    }
}