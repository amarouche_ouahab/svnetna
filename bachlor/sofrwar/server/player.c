#include "server.h"

t_listp *create_players(void)
{
    t_listp *p_players;

    if ((p_players = malloc(sizeof *p_players)) != NULL)
    {
        p_players->length = 0;
        p_players->p_first = NULL;
        p_players->p_last = NULL;
    }
    return (p_players);
}

t_player *add_player(t_listp *p_players, char *name, unsigned int x, unsigned int y, unsigned int energy, unsigned int looking, char *socket_id)
{
    t_player *p_new;

    if (p_players != NULL)
    {
        if ((p_new = malloc(sizeof *p_new)) != NULL)
        {
            if ((p_new->name = my_strdup(name)) == NULL)
            {
                return (NULL);
            }
            if ((p_new->socket_id = my_strdup(socket_id)) == NULL)
            {
                return (NULL);
            }
            p_new->energy = energy;
            p_new->looking = looking;
            p_new->x = x;
            p_new->y = y;
            p_new->p_next = NULL;
            if (p_players->p_last == NULL)
            {
                p_new->p_prev = NULL;
                p_players->p_first = p_new;
                p_players->p_last = p_new;
            }
            else
            {
                p_players->p_last->p_next = p_new;
                p_new->p_prev = p_players->p_last;
                p_players->p_last = p_new;
            }
            p_players->length++;
        }
    }
    return (p_new);
}

void delete_player(t_listp *p_players, t_player *player)
{
    t_player *temp;
    int del;

    if (p_players != NULL && player != NULL)
    {
        if (p_players->length == 1)
        {
            p_players->p_first = NULL;
            p_players->p_last = NULL;
            p_players->length -= 1;
        }
        else
        {
            del = 0;
            temp = p_players->p_first;
            while (temp != NULL && !del)
            {
                del = del_condition(p_players, player, &temp);
            }
            free(temp->socket_id);
            free(temp->name);
            free(temp);
        }
    }
}

int del_condition(t_listp *p_players, t_player *player, t_player **temp)
{
    if ((*temp) == player)
    {
        if ((*temp)->p_next == NULL)
        {
            p_players->p_last = (*temp)->p_prev;
            p_players->p_last->p_next = NULL;
        }
        else if ((*temp)->p_prev == NULL)
        {
            p_players->p_first = (*temp)->p_next;
            p_players->p_first->p_prev = NULL;
        }
        else
        {
            (*temp)->p_next->p_prev = (*temp)->p_prev;
            (*temp)->p_prev->p_next = (*temp)->p_next;
        }
        p_players->length -= 1;
        return (1);
    }
    else
        (*temp) = (*temp)->p_next;
    return (0);
}

void remove_players(t_listp **p_players)
{
    if (*p_players != NULL)
    {
        t_player *temp = (*p_players)->p_first;
        while (temp != NULL)
        {
            t_player *to_del = temp;
            temp = temp->p_next;
            free(to_del);
        }
        free(*p_players);
        *p_players = NULL;
    }
}