#include "server/server.h"

int is_number(char *str)
{
	int i;
	for (i = 0; str[i]; i++) {
		if (!isdigit(str[i])) {
			return 0;
		}
	}
	return 1;
}

int check_dir(char *dirname)
{
	struct stat dir_stat;
	stat(dirname, &dir_stat);

	return (S_ISDIR(dir_stat.st_mode) && (access(dirname, W_OK) == 0));
}

int check_file(char *path)
{
	struct stat path_stat;
	stat(path, &path_stat);

	return (access(path, F_OK) == -1 || (S_ISREG(path_stat.st_mode) && (access(path, W_OK) == 0)));
}

void write_log(char *text)
{
	time_t rawtime;
	struct tm *timeinfo;
	FILE *handle = fopen(game->args->logfile, "a");

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	if (handle == NULL) {
		fprintf(stdout, "[%s]: %s\n", strtok(asctime(timeinfo), "\n"), text);
	} else {
		fprintf(handle, "[%s]: %s\n", strtok(asctime(timeinfo), "\n"), text);
	}

	fclose(handle);
}

void verb(char *text)
{
	if (game->args->verbose) {
		printf("%s\n", text);
	}
}