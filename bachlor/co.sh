#!/bin/bash
# Author Ryan Kulla (rkulla@gmail.com)
#
# A shell script (in Bash) that allows you to do SVN checkouts
# with a shorter command than the default subversion command.
# Handy if your repos have long, hard to remember URLs.
#
# Put this script in your PATH (e.g., ~/bin/)
# Make sure it's executable:
#   $ chmod u+x ~/bin/co
#
# USAGE
# Make repo_url your repositories location. Then run:
#
#   $ co <rev> <reponame> [<dir>] (default=trunk) [<checkout location>]
#
# The default <dir> is trunk. The default checkout location is ./reponame
# For <rev> enter a number or keywords like HEAD. Examples:
#
#   $ co HEAD foo-repo
#   $ co 100 foo-repo branch/branch-foo /tmp/branch-foo

# Change the value of repo_url to your repository
repo_url="http://svn/repos"
rev="amarou_o"
repo_name="$2"
# rev = "amarou_o"
usage() {
    printf "Usage:\n"
    printf "co <rev> <reponame> [<dir>] (default=trunk) [<checkout location>] \n\n"
    printf "co appears to be installed to %s\n\n" "${0%/*}"
    printf "checkout location defaults to ./reponame\n\n"
    exit
}

if (( $# < 2 )); then
    usage
fi

# if (( $# < 3 )); then
#     dir="trunk"
# else
#     dir="$3"
# fi
if (( $# < 3 )); then
    checkout_location="$3"
else
    checkout_location="$3"
fi

# repo_location="$repo_url/$repo_name/$dir"
repo_location="$2"
printf "Attempting to checkout %s to %s...\n" "$repo_location" "$checkout_location"

if [[ -L "$checkout_location" ]]; then
    printf "%s is a symlink. Aborting.\n\n" "$checkout_location"
    exit 1
fi

if [[ -e "$checkout_location" ]]; then
    printf '\n\n%s already exists...\n\n' "$checkout_location"
    read -n 1 -p 'rm: y/n? ' prompt_rm
    if [[ "$prompt_rm" = 'y' ]]; then
        rm -rfv "$checkout_location"
    else
        printf '\nAborting.\n\n'
        exit
    fi
fi
# echo "$rev" + "$repo_location" + "$checkout_location"
svn co  --username "$rev" "$repo_location" "$checkout_location"

# cd "$checkout_location" && svn info