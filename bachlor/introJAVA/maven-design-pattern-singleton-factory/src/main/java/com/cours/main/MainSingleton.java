/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.main;

import com.cours.factory.SingletonFactory;
import com.cours.singletons.AbstractSingleton;
import com.cours.singletons.CsvSingleton;
import com.cours.singletons.JsonSingleton;
import com.cours.singletons.XmlSingleton;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author elhad
 */
public class MainSingleton {

    private static final Log log = LogFactory.getLog(MainSingleton.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        AbstractSingleton mySingleton;// = JsonSingleton.getInstance();
        mySingleton = SingletonFactory.getFactory(SingletonFactory.FactorySingletonType.CSV_SINGLETON_FACTORY);
        System.out.println(mySingleton.getPersonnes().size());
        mySingleton = SingletonFactory.getFactory(SingletonFactory.FactorySingletonType.XML_SINGLETON_FACTORY);
        System.out.println(mySingleton.getPersonnes().size());
        mySingleton = SingletonFactory.getFactory(SingletonFactory.FactorySingletonType.JSON_SINGLETON_FACTORY);
        System.out.println(mySingleton.getPersonnes().size());
    }
}
