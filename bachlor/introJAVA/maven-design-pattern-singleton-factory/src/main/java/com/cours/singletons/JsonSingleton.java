/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.singletons;

import com.cours.entities.Personne;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Element;

import java.io.*;

/**
 *
 * @author ElHadji
 */
public class JsonSingleton extends AbstractSingleton {

    final String personnesJsonPathFile = "personnesJson.json";


    private JsonSingleton() {
        this.extractPersonnesDatas();
    }

    private static class JsonSingletonHolder
    {
        private final static JsonSingleton instance = new JsonSingleton();
    }

    public static JsonSingleton getInstance()
    {
        return JsonSingletonHolder.instance;
    }


    public Personne createPersonneWithFileObject(JSONObject jsonObjectPerson) {
        Personne person = null;
        if( jsonObjectPerson.get("id") != null && jsonObjectPerson.get("prenom") != null && jsonObjectPerson.get("nom") != null
                && jsonObjectPerson.get("poids") != null &&jsonObjectPerson.get("taille") != null && jsonObjectPerson.get("rue") != null
                && jsonObjectPerson.get("ville") != null && jsonObjectPerson.get("codePostal") != null ){

            Long id = (Long) jsonObjectPerson.get("id");
            Long poids = (Long) jsonObjectPerson.get("poids");
            Long taille =(Long) jsonObjectPerson.get("taille");
            person = new Personne(
                    Math.toIntExact(id),
                    (String)jsonObjectPerson.get("prenom"),
                    (String)jsonObjectPerson.get("nom"),
                    Double.valueOf(poids),
                    Double.valueOf(taille),
                    (String) jsonObjectPerson.get("rue"),
                    (String) jsonObjectPerson.get("ville"),
                    (String) jsonObjectPerson.get("codePostal"));

        }

        return person;
    }

    @Override
    protected void extractPersonnesDatas() {
        FileReader reader = null;
        try {
            reader = new FileReader(personnesJsonPathFile);
            JSONParser parser = new JSONParser();
            Object object = parser.parse(reader);
            JSONObject jsonObject = (JSONObject)object;
            JSONArray lines = (JSONArray)jsonObject.get("personnes");

            for(Object s : lines)
            {
                JSONObject personneData = (JSONObject)s;
                Personne line = this.createPersonneWithFileObject(personneData);
                this.personnes.add(line);
            }
//            for (int i = 0; i < this.personnes.size(); i++) {
//                System.out.println(this.personnes.get(i).getImc());
//                System.out.println(this.personnes.get(i).isMaigre());
//                System.out.println(this.personnes.get(i).isSurPoids());
//                System.out.println(this.personnes.get(i).isObese());
//            }
        } catch ( FileNotFoundException | ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
