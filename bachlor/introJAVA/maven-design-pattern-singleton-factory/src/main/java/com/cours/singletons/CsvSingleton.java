/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.singletons;

import com.cours.entities.Personne;
import com.cours.utils.Constants;
import com.opencsv.CSVReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ElHadji
 */
public class CsvSingleton extends AbstractSingleton {

    final String personnesCsvPathFile = "personnesCsv.csv";
    private static Constants constants = new Constants();

    private CsvSingleton() {
        this.extractPersonnesDatas();
    }
    
    private static class CsvSingletonHolder
    {       
        private final static CsvSingleton instance = new CsvSingleton();
    }
 
    public static CsvSingleton getInstance()
    {
        return CsvSingletonHolder.instance;
    }

    private Personne createPersonneWithFileObject(String[] attributs) {
        Personne person = null;
        if(attributs.length >= 8){
            person = new Personne(Integer.parseInt(attributs[0]), attributs[1], attributs[2], Double.parseDouble(attributs[3]), Double.parseDouble(attributs[4]),attributs[5],attributs[6],attributs[7]);
        }
        return person;
    }

    @Override
    protected void extractPersonnesDatas() {
         Reader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(personnesCsvPathFile));
            CSVReader csvReader = new CSVReader(reader);
            List<String[]> records = csvReader.readAll();
            int iteration = 0;
            for (String[] record : records) {
                if(iteration == 0){
                    iteration++;
                }
                else{
                    String[] personneData = record[0].split(constants.CSV_SEPARATOR);
                    Personne line = this.createPersonneWithFileObject(personneData);
                    this.personnes.add(line);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(CsvSingleton.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
