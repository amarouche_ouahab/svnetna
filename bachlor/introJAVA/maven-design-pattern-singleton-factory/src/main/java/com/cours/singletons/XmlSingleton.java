/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.singletons;

import com.cours.entities.Personne;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author ElHadji
 */
public class XmlSingleton extends AbstractSingleton {

    final String personnesXmlPathFile = "personnesXml.xml";

    private XmlSingleton() {
            this.extractPersonnesDatas();
    }

    private static class XmlSingletonHolder
    {
        private final static XmlSingleton instance = new XmlSingleton();
    }

    public static XmlSingleton getInstance()
    {
        return XmlSingletonHolder.instance;
    }

    public Personne createPersonneWithFileObject(Element elementPersonne) {
        Personne person = null;
        person = new Personne(
                Integer.parseInt(elementPersonne.getAttribute("id")),
                this.getValue("prenom", elementPersonne),
                this.getValue("nom", elementPersonne),
                Double.parseDouble(this.getValue("poids", elementPersonne)),
                Double.parseDouble(this.getValue("taille", elementPersonne)),
                this.getValue("rue", elementPersonne),
                this.getValue("ville", elementPersonne),
                this.getValue("codePostal", elementPersonne));
        return person;
    }

    @Override
    protected void extractPersonnesDatas() {
        File fXmlFile = new File(personnesXmlPathFile);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            try {
                Document doc = dBuilder.parse(fXmlFile);
                doc.getDocumentElement().normalize();
                NodeList nodeList = doc.getElementsByTagName("personne");
                for (int temp = 0; temp < nodeList.getLength(); temp++) {
                    Node nNode = nodeList.item(temp);
                    Element element = (Element) nNode;
                    Personne line = this.createPersonneWithFileObject(element);
                    this.personnes.add(line);
                }
//                for (int i = 0; i < this.personnes.size(); i++) {
//                    System.out.println(this.personnes.get(i).getImc());
//                }
            } catch (SAXException |IOException e) {
                e.printStackTrace();
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private static String getValue(String tag, Element element) {
        String result = null;
        if(element != null && element.getElementsByTagName(tag) !=null &&  element.getElementsByTagName(tag).item(0) != null){
            result =  element.getElementsByTagName(tag).item(0).getTextContent();
        }
        return result;
    }

}
