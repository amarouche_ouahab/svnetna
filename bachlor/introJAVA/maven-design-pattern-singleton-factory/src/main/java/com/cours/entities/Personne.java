package com.cours.entities;

import static java.lang.Math.pow;

import java.util.Objects;
import com.cours.utils.Constants;

/**
 *
 * @author ElHadji
 */
public class Personne {
    private static Constants constants = new Constants();

    private int idPersonne;
    private String prenom;
    private String nom;
    private double poids;
    private double taille;
    private String rue;
    private String ville;
    private String codePostal;
    
    
    public  Personne(int idPersonne, String prenom, String nom, double poids, double taille, String rue,String ville,String codePostal){
        this.idPersonne = idPersonne;
        this.prenom = prenom;
        this.nom = nom;
        this.poids = poids;
        this.taille = taille;
        this.rue = rue;
        this.ville = ville;
        this.codePostal = codePostal;
    }

    @Override
    public String toString() {
        return idPersonne + prenom + nom + poids + taille + rue + ville +codePostal;
    }

    public int getIdPersonne() {
        return idPersonne;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public double getPoids() {
        return poids;
    }

    public double getTaille() {
        return taille;
    }

    public String getRue() {
        return rue;
    }

    public String getVille() {
        return ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne personne = (Personne) o;
        return idPersonne == personne.idPersonne &&
                Double.compare(personne.poids, poids) == 0 &&
                Double.compare(personne.taille, taille) == 0 &&
                Objects.equals(prenom, personne.prenom) &&
                Objects.equals(nom, personne.nom) &&
                Objects.equals(rue, personne.rue) &&
                Objects.equals(ville, personne.ville) &&
                Objects.equals(codePostal, personne.codePostal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPersonne, prenom, nom, poids, taille, rue, ville, codePostal);
    }

    public double getImc() {
//        System.out.println(poids);
//        System.out.println(taille);
        return poids / pow((taille/100),2);
    }
    public boolean isMaigre() {
        boolean isMaigre = this.getImc() >= constants.LIMITE_INF_MAIGRE && this.getImc() <= constants.LIMITE_SUP_MAIGRE;
        return isMaigre;
    }
    public boolean isSurPoids() {
        boolean isSurPoids =  this.getImc() >= constants.LIMITE_INF_SURPOIDS && this.getImc() <= constants.LIMITE_SUP_SURPOIDS;
        return isSurPoids;
    }
    public boolean isObese() {
        boolean isObese =  this.getImc() > constants.LIMITE_SUP_SURPOIDS;
        return isObese;        
    }
}
