equals()
La méthode equals() vérifie l'égalité de deux objets : son rôle est de vérifier si deux instances sont sémantiquement équivalentes même si ce sont deux instances distinctes.
Chaque classe peut avoir sa propre implémentation de l'égalité mais généralement deux objets sont égaux si tout ou partie de leurs états sont égaux.
L'implémentation par défaut fournie par le JDK est basée sur l'emplacement de la mémoire. 
Deux objets sont égaux si et seulement s'ils sont stockés dans la même adresse mémoire.

hashCode()
Est une méthode fournie par java.lang.Object qui renvoie une représentation entière de l'adresse de mémoire de l'objet.
Par défaut, cette méthode retourne un entier aléatoire unique pour chaque instance.
Cet entier peut changer entre plusieurs exécutions de l'application et ne restera pas le même.

toString()
La méthode toString() en java permet de donner un aperçut d'un objet instancié. 
C'est à dire que cette méthode va  retourne une chaine de caractère représentant l'objet désiré : affichage de la valeur des propriétés par exemple.
pour l'utilisé il suffit d'implémente cette fonction dans la classe où on souhaite utiliser le toString.