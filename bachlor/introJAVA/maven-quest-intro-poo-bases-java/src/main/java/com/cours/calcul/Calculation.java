/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.calcul;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author elhad
 */
public class Calculation implements ICalculation {

    @Override
    public int addition(int value1, int value2) {
        return value1 + value2;
    }

    @Override
    public int[] initMyArray(int taille) {
         int [] entiers = new int[taille];
            int n = 0;
            int rand;
            while(n < entiers.length){
                int randomNum = ThreadLocalRandom.current().nextInt(1, 100 + 1);
//                System.out.println(randomNum);
                entiers[n] = randomNum;
                n++;
            }
//            System.out.println(Arrays.toString(entiers));
        return entiers;
    }

    @Override
    public int[] sortMyArray(int[] arrayToSort) {
        int[] arraySorted = null;
        int lenght = arrayToSort.length;
        for (int i = 1; i < lenght; ++i) {
           int elem = arrayToSort[i];
           int j;
           for (j = i; j > 0 && arrayToSort[j - 1] > elem; j--) {
               arrayToSort[j] = arrayToSort[j - 1];
           }
        arrayToSort[j] = elem;
        }
        return arrayToSort;
    }

    @Override
    public int factorielItterative(int number) {
        int factoriel = 1;
        for (int i = 1; i <= number; i++) {
            factoriel = factoriel * i;
        }
        return factoriel;
    }

    @Override
    public int factorielRecursive(int number) {
        if(number < 1){
            return 1;
        }
       else 
           return  number * factorielRecursive(number - 1);
        // si n est inférieur à 1, on renvoie 1 sinon on renvoit l'appel de la fonction avec n - 1 multiplié par n
    }

    /**
     * renvois 0 si les deux chaines de caractères sont identiques, 1 si la
     * premiere chaine est supérieur à la seconde chaine (en terme de code
     * ASCII), -1 si la premiere chaine est inférieur à la seconde chaine (en
     * terme de code ASCII)
     *
     * @param firstChaine
     * @param secondeChaine
     * @return result
     */
    @Override
    public int compareChaines(String firstChaine, String secondeChaine) {
        int result = 0;
        int firstChaineLength =  firstChaine.length();
        int secondeChaineLength =  secondeChaine.length();
//        System.out.println(firstChaineLength);
//        System.out.println(secondeChaineLength);
          for(int i = 0; i < firstChaineLength && i < secondeChaineLength ; i++) {
            result =   firstChaine.charAt(i) - secondeChaine.charAt(i);
            if(result < 0){
               return result = -1;
            }
            else if(result > 1){
               return result = 1;
            }
        }
        return result;
    }

    /**
     * renvois 0 si le nombre est égale à 0, 1 s’il est pair et positif, 2 s’il
     * est négatif et paire, 3 s’il est impair et positif et 4 s’il est négatif
     * et impair
     *
     * @param chaine
     * @return result
     */
    @Override
    public int verifyParite(String chaine) {
        int x;
        int number = Integer.parseInt(chaine);
        if(number == 0){
            return 0;
        }
        else if((number %2)==0){
            if (number > 0)
                x= 1;
            else
                x= 2;
        }
        else{
            if (number > 0)
                x= 3;
            else
                x= 4;
        }
        return x;
    }

    @Override
    public int nombreMagique(int[] array) {
        int max = Arrays.stream(array).max().getAsInt();
        int min = Arrays.stream(array).min().getAsInt();
//        System.out.println(max);
//        System.out.println(min);
        int nbMagique = (max + min) / 2;
        return nbMagique;
    }
}
