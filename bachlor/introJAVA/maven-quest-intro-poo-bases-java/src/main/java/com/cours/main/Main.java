/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.main;

import com.cours.calcul.ICalculation;
import com.cours.calcul.Calculation;
import com.cours.process.IProcessAnimal;
import com.cours.process.ProcessAnimal;

import com.cours.entities.Animal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import java.util.Scanner;
import java.util.*; 
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author elhad
 */
public class Main {

    private static final Log log = LogFactory.getLog(Main.class);
    private static ICalculation calculation = new Calculation();
    private static IProcessAnimal processAnimal = new ProcessAnimal();
//    private static Animal Animal = new Animal();

    private static Scanner reader = new Scanner(System.in); 

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
// TODO code application logic here
        //testVerifyParite();
        //Animal.description();
//        processAnimal.loadAnimalsManually();
//        processAnimal.loadAnimalsFile();
//        processAnimal.sortAnimalsByName();
//        processAnimal.sortAnimalsById();

//        processAnimal.generateFileByName();
//        processAnimal.generateFileByWeight();
        //processAnimal.calculEcartTypePoidsAnimaux();
        //processAnimal.calculMoyennePoidsAnimaux();
        //testCompareChaines();
        //testFactorielItterative();
        //testFactorielRecursive();
        //testNombreMagique();
        //testSortMyArray();
        //testProcessAnimal();
    }

    public static void testVerifyParite() {
        System.out.println("Entrez un nombre entier: ");
        String n = Integer.toString(reader.nextInt()); 
        int result = calculation.verifyParite(n);
        String msg [] ={
            "Le nombre est egal a zero",
            "Le nombre est positif et pair",
            "Le nombre est négatif et pair",
            "Le nombre est positif et impair",
            "Le nombre est négatif et impair",
        }; 
        System.out.println(msg[result]);
    }
    
    public static void testCompareChaines() {
        System.out.println("Entrez le premier mot: ");
        String firstChaine = reader.nextLine();
        System.out.println("Entrez le deuxieme mot: ");
        String secondeChaine = reader.nextLine();
        int result = calculation.compareChaines(firstChaine.toLowerCase(),secondeChaine.toLowerCase());
        
        String [] msg = null;
        if(result == -1){
            System.out.println("La deuxième chaîne est supérieure à la première ");
        }
        else if(result == 0){
            System.out.println("Les deux chaînes sont identiques");
        }
        else if(result == 1){
            System.out.println("La première chaîne est supérieure à la deuxième.");
        }
    }

    /**
     * Attention pour simplifier l'exercice on ne mettre que de petit nombre
     * entier inferieur à 17. Sinon on risque de depasser la limite prévu pour
     * le type int. Vous pouvez donc utiliser le type entier int.
     */
    public static void testFactorielItterative() {
        System.out.println("Entrez le nombre positif entre 0 et 16: ");
        int n = reader.nextInt();
        if(n < 0 && n >16){
            System.out.println("Veuillez entrer un nombre positif entre 0 et 16");
        }else {
            int number = calculation.factorielItterative(n);
            System.out.println(number);
        }
    }

    /**
     * Attention pour simplifier l'exercice on ne mettre que de petit nombre
     * entier inferieur à 17. Sinon on risque de depasser la limite prévu pour
     * le type int. Vous pouvez donc utiliser le type entier int.
     */
    public static void testFactorielRecursive() {
        System.out.println("Entrez le nombre positif entre 0 et 16: ");
        int n = reader.nextInt();
        if(n < 0 && n >16){
            System.out.println("Veuillez entrer un nombre positif entre 0 et 16");
        }else {
            int number = calculation.factorielRecursive(n);
            System.out.println(number);
        }
    }

    public static void testNombreMagique() {
        int  entiers [] = calculation.initMyArray(100);
        System.out.println(Arrays.toString(entiers));
        int  nbMagique = calculation.nombreMagique(entiers);
        System.out.println(nbMagique);    
    }

    public static void testSortMyArray() {
        int  entiers [] = calculation.initMyArray(100);
        System.out.println(Arrays.toString(entiers));
        int  arrayToSort [] = calculation.sortMyArray(entiers);
        System.out.println(Arrays.toString(arrayToSort));
    }

    public static void testProcessAnimal() {
        processAnimal.loadAnimalsManually();
        processAnimal.loadAnimalsFile();
        processAnimal.calculMoyennePoidsAnimaux();
        processAnimal.calculEcartTypePoidsAnimaux();
    }
}
