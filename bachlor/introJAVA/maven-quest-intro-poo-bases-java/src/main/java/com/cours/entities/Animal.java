package com.cours.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class Animal {
    private final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private int idAnimal;
    private String nom;
    private double poids;
    private java.util.Date dateNaissance;
    private String couleur;
    private int nombrePattes;
    private boolean estCarnivore;
  
    public  Animal(int idAnimal, String nom, double poids, java.util.Date dateNaissance, String couleur, int nombrePattes, boolean estCarnivor){
        this.idAnimal = idAnimal;
        this.nom = nom;
        this.poids = poids;
        this.dateNaissance = dateNaissance;
        this.couleur = couleur;
        this.nombrePattes = nombrePattes;
        this.estCarnivore = estCarnivore;
    }
  
    public  String marcher (){
        String phrase =  "L’animal " + nom + " marche avec "+ nombrePattes + " pattes.";
        return phrase;
    }
    
    public  String description (){
        String isCarnivore = estCarnivore ? "carnivore":" n’est pas carnivore";
        String phrase =  "L'animal " + nom + " est née le " + dateFormat.format(dateNaissance) +
                ", il pèse " + poids + ", il est de couleur " + couleur + ", il a "
                + nombrePattes + " pattes, il " + isCarnivore;
        return phrase;
    }
    
    public int getId() {
        return idAnimal;
    }
    public String getNom() {
        return nom;
    }
    public double getPoids() {
        return poids;
    }
    public String getCouleur() {
        return couleur;
    }
    
    public String getDate() {
        return dateFormat.format(dateNaissance);
    }
     public int getNombrePattes() {
        return nombrePattes;
    }
    public boolean getEstCarnivore() {
        return estCarnivore;
    }
    @Override
    public int hashCode() {
        return Objects.hash(idAnimal,nom, poids, dateNaissance, couleur, nombrePattes, estCarnivore);
    }
    
    @Override
    public String toString() {
        return "ID: "+idAnimal;
    }
    
    @Override
    public boolean equals(Object Obj) {
        if (Obj == this){
            return true;
        }
        if ((Obj == null)  || !(Obj instanceof Animal)){
            return false;
        }
        Animal list = (Animal) Obj;
        return list.hashCode() == hashCode();
    }
    
}
