/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.process;

import com.cours.entities.Animal;
import java.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.text.*;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.cours.calcul.ICalculation;
import com.cours.calcul.Calculation;

/**
 *
 * @author elhad
 */
public class ProcessAnimal implements IProcessAnimal {

    private static final Log log = LogFactory.getLog(ProcessAnimal.class);
    private List<Animal> animals =  new ArrayList<Animal>();
    private final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private static ICalculation calculation = new Calculation();


    @Override
    public List<Animal> loadAnimalsManually() {
        animals.clear();
        try{
            animals.add(new Animal(1, "Lion", 200, dateFormat.parse("23/12/1988"),"Marron", 4, true));
            animals.add(new Animal(2, "Elephant", 700, dateFormat.parse("23/12/1978"), "Noir", 4, true));
            animals.add(new Animal(3, "Tigre", 150, dateFormat.parse("23/12/1998"),"Blanc", 4,true));
            animals.add(new Animal(4, "Requin", 10, dateFormat.parse("23/12/1978"),"Blanc",0,true));
            animals.add(new Animal(5, "Chat", 5, dateFormat.parse("23/12/2000"), "Noir",  4, true));
            animals.add(new Animal(6, "Mouton", 25,dateFormat.parse("23/12/2001"), "Blanc", 4, false));
            animals.add(new Animal(7, "Chevre",35, dateFormat.parse("23/12/2012"),"Noir", 4, false));
            animals.add(new Animal(8, "Poule", 1, dateFormat.parse("23/12/2009"), "Marron",2, false));
            animals.add(new Animal(9, "Porc", 20,  dateFormat.parse("23/12/2003"),"Blanc", 4, true ));
            animals.add(new Animal(10, "Singe", 25, dateFormat.parse("23/12/2004"), "Noir",4, false));
            animals.add(new Animal(11, "Giraffe", 175, dateFormat.parse("23/12/2013"), "Marron et Noir", 4, false));
          } catch (ParseException ex) {
            log.error(ex);
        }
        return animals;
    }

    @Override
    public List<Animal> loadAnimalsFile() {
        animals.clear();
        BufferedReader file = null;
        try {
            file = new BufferedReader(new FileReader("animaux.txt"));
            String line;
            while ((line = file.readLine()) != null) {
                String[] animalData = line.split(", ?");
                Animal newAnimal = new Animal(
                    Integer.parseInt(animalData[0]),
                    animalData[1],
                    Double.parseDouble(animalData[2]),
                    dateFormat.parse(animalData[3]),
                    animalData[4], 
                    Integer.parseInt(animalData[5]),
                    Boolean.parseBoolean(animalData[6])
                );
                animals.add(newAnimal);
            }
        } catch (IOException  e) {
            e.printStackTrace();
        } catch (ParseException ex) {
            Logger.getLogger(ProcessAnimal.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return animals;
    }

    @Override
    public Double calculMoyennePoidsAnimaux() {
        double total = 0.0;
        for(int i = 0; i < animals.size(); i++) {
            total = total + animals.get(i).getPoids();
        }
        double moyenne =  total / animals.size();
//        System.out.println(moyenne);
        return moyenne;
    }

    @Override
    public Double calculEcartTypePoidsAnimaux() {
        double ecartType = 0.0;
        double result = 0.0;
        for(int i = 0; i < animals.size(); i++) {
            ecartType += Math.pow(animals.get(i).getPoids() - calculMoyennePoidsAnimaux(), 2);
        }
        result = Math.sqrt(ecartType / animals.size());
//        System.out.println(result);
        return result;
    }

    @Override
    public List<Animal> sortAnimalsById() {
        List<Animal> animals = this.animals;
        Collections.sort(animals, new Comparator<Animal>() {
            @Override
               public int compare(Animal val1, Animal val2) {
               if (val1.getId() > val2.getId())
                   return 1;
               if (val1.getId() < val2.getId())
                   return -1;
               return 0;
           }
        });
        return animals;
    }

    @Override
    public List<Animal> sortAnimalsByName() {

        List<Animal> animals = this.animals;
        Collections.sort(animals, new Comparator<Animal>() {
            @Override
               public int compare(Animal val1, Animal val2) {
                   int r = calculation.compareChaines(val1.getNom(),val2.getNom());
                   return r;
           }
        });
        return animals;
    }

    @Override
    public List<Animal> sortAnimalsByWeight() {
        List<Animal> animals = this.animals;
        Collections.sort(animals, new Comparator<Animal>() {
            @Override
               public int compare(Animal val1, Animal val2) {
               if (val1.getPoids() > val2.getPoids())
                   return 1;
               if (val1.getPoids() < val2.getPoids())
                   return -1;
               return 0;
           }
       });
        return animals;
    }

    @Override
    public List<Animal> sortAnimalsByColor() {
        Collections.sort(animals, new Comparator<Animal>() {
            @Override
               public int compare(Animal val1, Animal val2) {
                   int r = calculation.compareChaines(val1.getCouleur(),val2.getCouleur());
                   return r;
           }
        });
        return animals;
    }

    @Override
    public void generateFileByName() {
        PrintWriter newFile = null;
        try {
            newFile = new PrintWriter(new FileWriter("animauxParNom.txt"));
        } catch (IOException ex) {
            Logger.getLogger(ProcessAnimal.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(Animal animal : animals){
            int poid = (int)  animal.getPoids();
            newFile.printf("%d, %s, %d, %s, %s, %d, %b\n",
                animal.getId(),
                animal.getNom(),
                poid,
                animal.getDate(),
                animal.getCouleur(),
                animal.getNombrePattes(),
                animal.getEstCarnivore()
            );
        }
        newFile.close();     
    }

    @Override
    public void generateFileByWeight() {
        PrintWriter newFile = null;
        try {
            newFile = new PrintWriter(new FileWriter("animauxParPoids.txt"));
        } catch (IOException ex) {
            Logger.getLogger(ProcessAnimal.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(Animal animal : animals){
            int poid = (int)  animal.getPoids();

            newFile.printf("%d, %s, %d, %s, %s, %d, %b\n",
                animal.getId(),
                animal.getNom(),
                poid,
                animal.getDate(),
                animal.getCouleur(),
                animal.getNombrePattes(),
                animal.getEstCarnivore()
            );
        }
        newFile.close(); 
    }

    @Override
    public List<Animal> getAnimals() {
        return animals;
    }
}
