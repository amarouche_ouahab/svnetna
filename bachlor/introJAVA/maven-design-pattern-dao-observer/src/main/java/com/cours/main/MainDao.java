package com.cours.main;
import com.cours.dao.IPersonneDao;
import com.cours.dao.factory.AbstractDaoFactory;
import com.cours.dao.impl.xml.XmlPersonneDaoImpl;
import com.cours.entities.Personne;
import com.cours.dao.impl.csv.CsvPersonneDaoImpl;
import com.cours.dao.impl.json.JsonPersonneDaoImpl;
import com.cours.dao.impl.csv.AbstractCsvDao;

import java.util.List;

public class MainDao {

    public static void main(String[] args) {
//        CsvPersonneDaoImpl myCsv = new CsvPersonneDaoImpl();
//        XmlPersonneDaoImpl myXml = new XmlPersonneDaoImpl();
//        JsonPersonneDaoImpl myJson = new JsonPersonneDaoImpl();
        AbstractDaoFactory Csv = AbstractDaoFactory.getDaoFactory(AbstractDaoFactory.FactoryType.CSV_DAO);
        AbstractDaoFactory xml = AbstractDaoFactory.getDaoFactory(AbstractDaoFactory.FactoryType.XML_DAO);
        AbstractDaoFactory json = AbstractDaoFactory.getDaoFactory(AbstractDaoFactory.FactoryType.JSON_DAO);

        IPersonneDao CsvPersonneDao = xml.getPersonneDao();
        Personne personne = new Personne(21, "ouahab", "amarouche", 60.0, 150.0, "rue ", "Laval", "53000");
//        List<Personne> perosn = CsvPersonneDao.findAll();
//        CsvPersonneDao.create(personne);
//        myCsv.create(personne);
//        myJson.create(personne);
//        myJson.create(personne);

//        System.out.println(myXml.f.indAll());
//        System.out.println(CsvPersonneDao.generateIdNewPersonne());
//        System.out.println(myXml.generateIdNewPersonne());
//        System.out.println(myXml.delete(personne));
        System.out.println(CsvPersonneDao.delete(personne));
//        System.out.println(myCsv.delete(personne));
//        personne = myCsv.findById(2);
        personne = CsvPersonneDao.findById(2);
        System.out.println(personne.getNom());
//          System.out.println(myJson.findAll());
//        List<Personne> perosn = myCsv.findAll();

        List<Personne> perosn = CsvPersonneDao.findAll();
        for (int i = 0; i < perosn.size(); i++) {
            System.out.println(perosn.get(i));
        }
//        myCsv.delete(personne);
    }
}
