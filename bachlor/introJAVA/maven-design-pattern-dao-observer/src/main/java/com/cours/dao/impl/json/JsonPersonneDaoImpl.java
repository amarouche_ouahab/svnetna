package com.cours.dao.impl.json;

import com.cours.dao.IPersonneDao;
import com.cours.entities.Personne;
import com.cours.observer.MySubjectObserver;
import com.cours.utils.Constants;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.lang.Long;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonPersonneDaoImpl /*extends AbstractJsonDao<Personne>*/ implements IPersonneDao {

    private final String personnesJsonPathFile = Constants.PERSONNES_JSON_PATH_FILE;
    private MySubjectObserver subject = null;
    private boolean sendNotification = true;

    public JsonPersonneDaoImpl() {
    }

    public JsonPersonneDaoImpl(MySubjectObserver subject) {
        //super(Personne.class, subject, Constants.PERSONNES_JSON_PATH_FILE);
        this.subject = subject;
    }

    @Override
    public Personne findById(int idPersonne) {
        List<Personne> personnes = this.findAll();
        for (int i = 0; i < personnes.size(); i++) {
            if (personnes.get(i).getIdPersonne() == idPersonne) {
                return personnes.get(i);
            }
        }
        return null;
    }

    @Override
    public Personne create(Personne obj) {
        JSONArray personnes = null;
        JSONObject data = new JSONObject();
        JSONParser parser = new JSONParser();
        JSONObject jsonObj = null;
        obj.setIdPersonne(this.generateIdNewPersonne());
        try {

            jsonObj = (JSONObject) parser.parse(new FileReader(this.personnesJsonPathFile));
            personnes = (JSONArray) jsonObj.get("personnes");
            JSONObject JsonObj = this.JsonObj(obj);
            personnes.add(JsonObj);
            data.put("personnes", personnes);
            FileWriter file = new FileWriter(this.personnesJsonPathFile);
            file.write(data.toJSONString());
            file.flush();
            file.close();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public boolean delete(Personne person) {
        Boolean deleted = false;
        FileReader reader = null;
        JSONObject NewList = new JSONObject();
        JSONArray list = new JSONArray();
        try {
            reader = new FileReader(this.personnesJsonPathFile);
            JSONParser parser = new JSONParser();
            Object object = parser.parse(reader);
            JSONObject jsonObject = (JSONObject)object;
            JSONArray lines = (JSONArray)jsonObject.get("personnes");
            for(Object s : lines)
            {
                JSONObject personneData = (JSONObject)s;
                if((Long) personneData.get("id") ==  Long.parseLong(String.valueOf(person.getIdPersonne()))){
                    deleted = true;
                    continue;
                }
                list.add(personneData);
            }
            FileWriter file = new FileWriter(this.personnesJsonPathFile);
            NewList.put("personnes", list);
            file.write(NewList.toJSONString());
            file.flush();
            file.close();

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return deleted;
    }

    @Override
    public int generateIdNewPersonne() {
        List<Personne> personnes = this.findAll();
        int id = 0;
        for (int i = 0; i < personnes.size(); i++) {
            if(id < personnes.get(i).getIdPersonne() ){
                id = personnes.get(i).getIdPersonne();
            }
        }
        return id + 1;
    }

    @Override
    public List<Personne> findAll() {
        FileReader reader = null;
        List<Personne> personnes = new ArrayList<>();
        try {
            reader = new FileReader(this.personnesJsonPathFile);
            JSONParser parser = new JSONParser();
            Object object = parser.parse(reader);
            JSONObject jsonObject = (JSONObject)object;
            JSONArray lines = (JSONArray)jsonObject.get("personnes");
            for(Object s : lines)
            {
                JSONObject personneData = (JSONObject)s;
                Personne line = this.createPersonneWithFileObject(personneData);
                personnes.add(line);
            }
        } catch ( FileNotFoundException | ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return personnes;
    }

    @Override
    public boolean sendNotification() {
        return sendNotification;
    }

    @Override
    public void setSendNotification(boolean sendNotification) {
        this.sendNotification = sendNotification;
    }

    public Personne createPersonneWithFileObject(JSONObject jsonObjectPerson) {
        Personne person = null;
        if( jsonObjectPerson.get("id") != null && jsonObjectPerson.get("prenom") != null && jsonObjectPerson.get("nom") != null
                && jsonObjectPerson.get("poids") != null &&jsonObjectPerson.get("taille") != null && jsonObjectPerson.get("rue") != null
                && jsonObjectPerson.get("ville") != null && jsonObjectPerson.get("codePostal") != null ){
            Long id = (Long) jsonObjectPerson.get("id");
            Long poids = (Long) jsonObjectPerson.get("poids");
            Long taille =(Long) jsonObjectPerson.get("taille");
            person = new Personne(
                    Math.toIntExact(id),
                    (String)jsonObjectPerson.get("prenom"),
                    (String)jsonObjectPerson.get("nom"),
                    Double.valueOf(poids),
                    Double.valueOf(taille),
                    (String) jsonObjectPerson.get("rue"),
                    (String) jsonObjectPerson.get("ville"),
                    (String) jsonObjectPerson.get("codePostal"));

        }

        return person;
    }

    public JSONObject JsonObj(Personne obj){
        Double obdj = obj.getPoids();
        long poids = obdj.longValue();
        Double tailleDbl = obj.getTaille();
        long taille = tailleDbl.longValue();
        JSONObject JsonObj =  new JSONObject();
        JsonObj.put("id", obj.getIdPersonne());
        JsonObj.put("prenom", obj.getPrenom());
        JsonObj.put("nom", obj.getNom());
        JsonObj.put("poids", poids);
        JsonObj.put("taille",taille);
        JsonObj.put("rue", obj.getRue());
        JsonObj.put("ville", obj.getVille());
        JsonObj.put("codePostal", obj.getCodePostal());
        return JsonObj;
    }
}
