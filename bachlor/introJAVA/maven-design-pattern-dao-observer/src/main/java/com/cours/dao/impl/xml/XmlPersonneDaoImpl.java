package com.cours.dao.impl.xml;

import com.cours.dao.IPersonneDao;
import com.cours.entities.Personne;
import com.cours.observer.MySubjectObserver;
import com.cours.utils.Constants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XmlPersonneDaoImpl /*extends AbstractXmlDao<Personne>*/ implements IPersonneDao {

    private final String personnesXmlPathFile = Constants.PERSONNES_XML_PATH_FILE;
    private MySubjectObserver subject = null;
    private boolean sendNotification = true;

    public XmlPersonneDaoImpl() {
    }

    public XmlPersonneDaoImpl(MySubjectObserver subject) {
        //super(Personne.class, subject, Constants.PERSONNES_XML_PATH_FILE);
        this.subject = subject;
    }

    @Override
    public List<Personne> findAll() {
        File fXmlFile = new File(personnesXmlPathFile);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        List<Personne> personnes = new ArrayList<>();
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            try {
                Document doc = dBuilder.parse(fXmlFile);
                doc.getDocumentElement().normalize();
                NodeList nodeList = doc.getElementsByTagName("personne");
                for (int temp = 0; temp < nodeList.getLength(); temp++) {
                    Node nNode = nodeList.item(temp);
                    Element element = (Element) nNode;
                    Personne line = this.createPersonneWithFileObject(element);
                    personnes.add(line);
                }
            } catch (SAXException |IOException e) {
                e.printStackTrace();
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return personnes;
    }

    @Override
    public Personne findById(int idPersonne) {
        List<Personne> personnes = this.findAll();
        for (int i = 0; i < personnes.size(); i++) {
            if (personnes.get(i).getIdPersonne() == idPersonne) {
                return personnes.get(i);
            }
        }
        return null;
    }

    @Override
    public Personne create(Personne obj) {
        File file = new File(this.personnesXmlPathFile);
        Document doc = null;
        obj.setIdPersonne(this.generateIdNewPersonne());
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("personnes");
            Element element = (Element) nodeList.item(0);
            Element newPersonne = this.newPersonne(doc, obj);
            element.appendChild(newPersonne);
            this.createXmlFile(doc);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        catch (SAXException | IOException  e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean delete(Personne person) {
        List<Personne> personnes = this.findAll();
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        Document doc = null;
        Boolean deleted =  false;
            try {
                docBuilder = docFactory.newDocumentBuilder();
                doc = docBuilder.newDocument();
                Element newElement = doc.createElement("personnes");
                doc.appendChild(newElement);
                for (Personne personne : personnes) {
                    if(personne.getIdPersonne() == person.getIdPersonne()){
                        deleted = true;
                        continue;
                    }
                    newElement.appendChild(this.newPersonne(doc, personne));
                }
                this.createXmlFile(doc);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
        return deleted;
    }

    @Override
    public int generateIdNewPersonne() {
        List<Personne> personnes = this.findAll();
        int id = 0;
        for (int i = 0; i < personnes.size(); i++) {
            if(id < personnes.get(i).getIdPersonne() ){
                id = personnes.get(i).getIdPersonne();
            }
        }
        return id + 1;
    }
    
    @Override
    public boolean sendNotification() {
        return sendNotification;
    }

    @Override
    public void setSendNotification(boolean sendNotification) {
        this.sendNotification = sendNotification;
    }

    public Personne createPersonneWithFileObject(Element elementPersonne) {
        Personne person = null;
        person = new Personne(
                Integer.parseInt(elementPersonne.getAttribute("id")),
                this.getValue("prenom", elementPersonne),
                this.getValue("nom", elementPersonne),
                Double.parseDouble(this.getValue("poids", elementPersonne)),
                Double.parseDouble(this.getValue("taille", elementPersonne)),
                this.getValue("rue", elementPersonne),
                this.getValue("ville", elementPersonne),
                this.getValue("codePostal", elementPersonne));
        return person;
    }

    private static String getValue(String tag, Element element) {
        String result = null;
        if(element != null && element.getElementsByTagName(tag) !=null &&  element.getElementsByTagName(tag).item(0) != null){
            result =  element.getElementsByTagName(tag).item(0).getTextContent();
        }
        return result;
    }

    public Element newPersonne(Document doc, Personne obj) {
        Element nodePerson = doc.createElement("personne");
        nodePerson.setAttribute("id", String.valueOf(obj.getIdPersonne()));
        Element prenom = doc.createElement("prenom");
        prenom.setTextContent(obj.getPrenom());
        nodePerson.appendChild(prenom);

        Element nom = doc.createElement("nom");
        nom.setTextContent(obj.getNom());
        nodePerson.appendChild(nom);

        Element poids = doc.createElement("poids");
        poids.setTextContent(String.valueOf(obj.getPoids()));
        nodePerson.appendChild(poids);

        Element taille = doc.createElement("taille");
        taille.setTextContent(String.valueOf(obj.getTaille()));
        nodePerson.appendChild(taille);

        Element rue = doc.createElement("rue");
        rue.setTextContent(obj.getRue());
        nodePerson.appendChild(rue);

        Element ville = doc.createElement("ville");
        ville.setTextContent(obj.getVille());
        nodePerson.appendChild(ville);

        Element codePostal = doc.createElement("codePostal");
        codePostal.setTextContent(obj.getCodePostal());
        nodePerson.appendChild(codePostal);

        return nodePerson;
    }

    public void createXmlFile(Document doc) {
        TransformerFactory transformerFactory = null;
        Transformer transformer = null;
        try {
            transformerFactory = TransformerFactory.newInstance();
            transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(this.personnesXmlPathFile));
            transformer.transform(source, result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
