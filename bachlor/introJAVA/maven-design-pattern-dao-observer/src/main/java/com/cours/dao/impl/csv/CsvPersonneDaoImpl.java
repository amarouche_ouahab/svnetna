package com.cours.dao.impl.csv;

import com.cours.dao.IPersonneDao;
import com.cours.entities.Personne;
import com.cours.observer.MySubjectObserver;
import com.cours.utils.Constants;
import com.opencsv.CSVReader;
import org.apache.commons.csv.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CsvPersonneDaoImpl /*extends AbstractCsvDao<Personne>*/ implements IPersonneDao {

    private final String personnesCsvPathFile = Constants.PERSONNES_CSV_PATH_FILE;
    private MySubjectObserver subject = null;
    private boolean sendNotification = true;

    public CsvPersonneDaoImpl() {
    }

    public CsvPersonneDaoImpl(MySubjectObserver subject) {
        //super(Personne.class, subject, Constants.PERSONNES_CSV_PATH_FILE);
        this.subject = subject;
    }

    @Override
    public Personne findById(int idPersonne) {
        List<Personne> csv = this.findAll();
        for (int i = 0; i < csv.size(); i++) {
            if (csv.get(i).getIdPersonne() == idPersonne) {
                return csv.get(i);
            }
        }
        return null;
    }

    @Override
    public Personne create(Personne obj) {
        File file = new File(this.personnesCsvPathFile);
        FileWriter writer = null;
        try {
            writer = new FileWriter(file,true);
            writer.write("\n" + this.generateIdNewPersonne() + "; " + obj.getPrenom() + "; " + obj.getNom() + "; " + obj.getPoids() + "; " + obj.getTaille() + "; " + obj.getRue() + "; " + obj.getVille() + "; " + obj.getCodePostal());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public boolean delete(Personne person) {
        List<String[]> csv = this.ReadCsv();
        FileWriter outputfile = null;
        Boolean deleted = false;
        try {
            outputfile = new FileWriter(this.personnesCsvPathFile);
            CSVPrinter csvPrinter = new CSVPrinter(outputfile, CSVFormat.DEFAULT
                    .withHeader("idPersonne", "Prenom", "Nom", "Poids", "Taille", "Rue", "Ville", "Code Postal")
                    .withDelimiter(';'));
            for (String[] record :  csv.subList(1, csv.size())) {
                String[] attributs = record[0].split(";");
                if(person.getIdPersonne() == Integer.parseInt(attributs[0])){
                    deleted = true;
                    continue;
                }
                csvPrinter.printRecord(Integer.parseInt(attributs[0]), attributs[1], attributs[2], attributs[3], attributs[4], attributs[5], attributs[6], attributs[7]);
            }
            outputfile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deleted;
    }

    @Override
    public List<Personne> findAll() {
        List<String[]> csv = this.ReadCsv();
        List<Personne> personnes = new ArrayList<>();
        for (String[] record :  csv.subList(1, csv.size())) {
            String[] attributs = record[0].split(";");
//            Personne personne = new Personne(Integer.parseInt(attributs[0]), attributs[1], attributs[2], Double.parseDouble(attributs[3]), Double.parseDouble(attributs[4]),attributs[5],attributs[6],attributs[7]);
            Personne personne =  this.createPersonneWithFileObject(attributs);
            personnes.add(personne);
        }
        return personnes;
    }

    @Override
    public int generateIdNewPersonne() {
        List<String[]> csv = this.ReadCsv();
        int iteration = 0;
        int id = 0;
        for (String[] record : csv) {
            if(iteration == 0){
                iteration++;
            }
            else{
                String[] personneData = record[0].split(";");
                if(id < Integer.parseInt(personneData[0])){
                    id =  Integer.parseInt(personneData[0]);
                }
            }
        }
        return id + 1;
    }

    @Override
    public boolean sendNotification() {
        return sendNotification;
    }

    @Override
    public void setSendNotification(boolean sendNotification) {
        this.sendNotification = sendNotification;
    }

    public List<String[]> ReadCsv() {
        Reader reader = null;
        List<String[]> records = null;
        try {
            reader = Files.newBufferedReader(Paths.get(personnesCsvPathFile));
            CSVReader csvReader = new CSVReader(reader);
            records = csvReader.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return records;
    }

    private Personne createPersonneWithFileObject(String[] attributs) {
        Personne person = null;
        if(attributs.length >= 8){
            person = new Personne(Integer.parseInt(attributs[0]), attributs[1], attributs[2], Double.parseDouble(attributs[3]), Double.parseDouble(attributs[4]),attributs[5],attributs[6],attributs[7]);
        }
        return person;
    }

}
