package com.cours.dao.factory;

import com.cours.dao.IPersonneDao;
import com.cours.dao.impl.json.JsonPersonneDaoImpl;
import com.cours.observer.MySubjectObserver;

public class JsonDaoFactory extends AbstractDaoFactory {

    private IPersonneDao personneDao = null;

    public JsonDaoFactory() {
        this.personneDao = new JsonPersonneDaoImpl();
    }

    private static class JsonDaoFactoryHolder {
        private static JsonDaoFactory instance = new JsonDaoFactory();
    }

    public static JsonDaoFactory getInstance() {
        return JsonDaoFactoryHolder.instance;
    }

    @Override
    public IPersonneDao getPersonneDao() {
        return personneDao;
    }

    @Override
    public void updateSource() {
    }
}
