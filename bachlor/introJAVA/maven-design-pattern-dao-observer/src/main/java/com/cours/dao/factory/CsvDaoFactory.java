package com.cours.dao.factory;

import com.cours.dao.IPersonneDao;
import com.cours.dao.impl.csv.CsvPersonneDaoImpl;
import com.cours.observer.MySubjectObserver;

public class CsvDaoFactory extends AbstractDaoFactory {

    private IPersonneDao personneDao = null;

    public CsvDaoFactory() {
        this.personneDao = new CsvPersonneDaoImpl();
    }

    private static class CsvDaoFactoryHolder {
        private static CsvDaoFactory instance = new CsvDaoFactory();
    }

    public static CsvDaoFactory getInstance() {
        return CsvDaoFactoryHolder.instance;
    }
    @Override
    public IPersonneDao getPersonneDao() {
        return personneDao;
    }

    @Override
    public void updateSource() {
    }
}
