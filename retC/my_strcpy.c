char    *my_strncpy(char *dest, char *src, int n)
{
  int   x;

  x = 0;
  while (x < n)
    {
      while (src[x] != '\0')
        {
          dest[x] = src[x];
          x = x + 1;
        }
    }
  return dest;
}
