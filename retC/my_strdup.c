#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "fonc.h"

char    *my_strcpy(char *dest, char *src);
int     my_strlen(char *str);

char    *my_strdup(char *str)
{
  char  *x;
  char  y;

  y = my_strlen(str);
  x = malloc(y * sizeof(char));
  my_strcpy(x, str);
  return (x);
}
