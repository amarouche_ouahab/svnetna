#include "point.h"

int     my_strcmp(char *s1, char *s2)
{
  int   a;

  a = 0;
  while (s1[a] != '\0' && s2[a] != '\0')
    {
      if (s1[a] < s2[a])
        {
          return (-1);
        }
      else if (s1[a] > s2[a])
        {
          return (1);
        }
      a = a + 1;
    }
  if (s1[a] > s2[a])
    {
      return (1);
    }
  else if (s2[a] > s1[a])
    {
      return (-1);
    }
  return (0);
}
