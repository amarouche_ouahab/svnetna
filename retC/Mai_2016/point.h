#ifndef _POINT_H_
#define _POINT_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

typedef struct s_list
{
  char x;
  struct s_list *next;

}t_list;

void    my_putchar(char c);
void    my_putstr(char *str);
void    my_put_nbr(int n);
int     my_strcmp(char *s1, char *s2);
char	*my_strcpy(char *dest, char *src);
char	*readLine();
t_list *add_end(t_list *list, char  nb);
int cree_random(int x);
void dodo(int i);
void read_list(t_list *list);

#endif

