#include "point.h"

void    my_put_nbr(int n)
{
  int   x;
  int   puiss;

  puiss = 1;
  x = 0;
  if (n < 0)
    {
      my_putchar('-');
      {
        if (n == -2147483648)
          {
            x = 1;
            n += 1;
          }
      }
      n = n * -1;
    }
  while ((n / puiss) >= 10)
    {
      puiss *= 10;
    }
  while (puiss >= 1)
    {
      my_putchar((n / puiss) % 10 + '0');
      puiss /= 10;
    }
  if (x  == 1)
    {
      x = 0;
      my_putchar(n % 10 + '1');
    }
}
