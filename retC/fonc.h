#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void    my_putchar(char c);
void    my_putstr(char *str);
int     my_strcmp(char *s1, char *s2);
char    *my_strcpy(char *dest, char *src);
int     my_strlen(char *str);
