#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

void    my_putchar(char c);

void    my_putstr(char *str)
{
  int   i;

  i = 0;
  while (*(str + i) != '\0')
    {
      my_putchar(*(str + i));
      i += 1;
    }
}
