/*
** my_strcmp.c for project in /my_printf/amarou_o
**
** Made by AMAROUCHE Ouahab Koussaila
** Login   <amarou_o@etna-alternance.net>
**
** Started on  Tue Apr 05 10:08:01 2016 AMAROUCHE Ouahab Koussaila
** Last update Wed Apr 08 14:08:10 2016 AMAROUCHE Ouahab Koussaila
*/

#include "my_printf.h"

int	my_strcmp(char *s1, char *s2)
{
  int	a;

  a = 0;
  while (s1[a] != '\0' && s2[a] != '\0')
    {
      if (s1[a] < s2[a])
        {
	  return (-1);
        }
      else if (s1[a] > s2[a])
        {
          return (1);
        }
      a = a + 1;
    }
  if (s1[a] > s2[a])
    {
      return (1);
    }
  else if (s2[a] > s1[a])
    {
      return (-1);
    }
  return (0);
}
