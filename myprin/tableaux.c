/*
** tableaux.c for project in /my_printf/amarou_o
**
** Made by AMAROUCHE Ouahab Koussaila
** Login   <amarou_o@etna-alternance.net>
**
** Started on  Tue Apr 05 10:07:03 2016 AMAROUCHE Ouahab Koussaila
** Last update Wed Apr 08 16:00:34 2016 AMAROUCHE Ouahab Koussaila
*/

#include "my_printf.h"

char	*tab_opt()
{
  char*	tableau;

  if ((tableau = malloc((sizeof(char) * 4) + 1)) == NULL)
    return (0);
  tableau[0] = 's';
  tableau[1] = 'd';
  tableau[2] = 'c';
  tableau[3] = 'o';
  tableau[4] = 'u';
  tableau[5] = '%';
  tableau[6] = 'x';
  tableau[7] = 'X';
  tableau[8] = 'i';
  return (tableau);
}

void	recup(int b, va_list ap)
{
  void (*tab_op[9]) (va_list);
  tab_op[0] = opt_s;
  tab_op[1] = opt_d;
  tab_op[2] = opt_c;
  tab_op[3] = opt_o;
  tab_op[4] = opt_u;
  tab_op[5] = opt_r;
  tab_op[6] = opt_x;
  tab_op[7] = opt_X;
  tab_op[8] = opt_d;
  (*tab_op [b]) (ap);
}

int	recup_option(char* str, va_list ap, int i)
{
  int	b;
  char	*tab;

  b = 0;
  tab = tab_opt();
  while (b <= 8)
    {
      if (str[i] == tab[b])
	{
	  recup(b, ap);
	}
      b++;
    }
  free(tab);
  return (i);
}
