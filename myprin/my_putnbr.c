/*
** my_putnbr.c for project in /my_printf/amarou_o
**
** Made by AMAROUCHE Ouahab Koussaila
** Login   <amarou_o@etna-alternance.net>
**
** Started on  Wed Apr 06 14:06:17 2016 AMAROUCHE Ouahab Koussaila
** Last update Thu Apr 08 16:19:25 2016 AMAROUCHE Ouahab Koussaila
*/

#include "my_printf.h"

void		my_put_nbro(unsigned int n)
{
  unsigned int	puiss;

  puiss = 1;
  while ((n / puiss) >= 8)
    {
      puiss *= 8;
    }
  while (puiss >= 1)
    {
      my_putchar((n / puiss) % 8 + '0');
      puiss /= 8;
    }
}

void		my_put_nbru(unsigned int n)
{
  unsigned int	puiss;

  puiss = 1;
  while ((n / puiss) >= 10)
    puiss *= 10;
  while (puiss >= 1)
    {
      my_putchar((n / puiss) % 10 + '0');
      puiss /= 10;
    }
}

void		put_nbrx(unsigned int nbr)
{
  unsigned int	modulo;
  char*	tab;
  int	i;

 if ((tab = malloc((sizeof(char) * 4) + 1)) == NULL)
   my_putstr("error tableau");
  for(i = 0; nbr >= 16; i++)
    {
      modulo = (nbr % 16);
      nbr = ((nbr - modulo) / 16);
      if (modulo >= 10 && modulo <= 15)
        tab[i] = (modulo + 87);
      else if (modulo < 9)
        tab[i] = (modulo + 48);
    }
  if (nbr < 9 )
    tab[i] = nbr + 48;
  else
    tab[i] = nbr + 87;
  while (i >= 0)
    {
      my_putchar(tab[i]);
      i--;
    }
  free(tab);
}

void		put_nbrx2(unsigned int nbr)
{
  unsigned int	modulo;
  int	x;
  char*	tab;

  if ((tab = malloc((sizeof(char) * 4) + 1)) == NULL)
    my_putstr("error tableau");
  for (x = 0; nbr >= 16; x++)
    {
      modulo = (nbr % 16);
      nbr = ((nbr - modulo) / 16);
      if (modulo >= 10 && modulo <= 15)
        tab[x] = (modulo + 55);
      else if (modulo < 9)
	tab[x] = (modulo + 48);
    }
  if (nbr < 9)
    tab[x] = nbr + 48;
  else
    tab[x] = nbr + 55;
  while (x >= 0)
    {
      my_putchar(tab[x]);
      x--;
    }
  free(tab);
}
