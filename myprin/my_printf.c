/*
** my_printf.c for project in /my_printf/amarou_o
**
** Made by AMAROUCHE Ouahab Koussaila
** Login   <amarou_o@etna-alternance.net>
**
** Started on  Tue Apr 05 10:02:58 2016 AMAROUCHE Ouahab Koussaila
** Last update Thu Apr 08 16:19:09 2016 AMAROUCHE Ouahab Koussaila
*/

#include "my_printf.h"

int	my_printf(char *str, ...)
{
  va_list	ap;
  int	i;

  i = 0;
  va_start(ap, str);
  while (str[i] != '\0' )
    {
      if (str[i] == '%')
	i = recup_option(str, ap, i + 1);
      else
	my_putchar(str[i]);
      i++;
    }
  va_end(ap);
  return (0);
}
