/*
** option.c for project in /my_printf/amarou_o
**
** Made by AMAROUCHE Ouahab Koussaila
** Login   <amarou_o@etna-alternance.net>
**
** Started on  Tue Apr 05 14:12:22 2016 AMAROUCHE Ouahab Koussaila
** Last update Wed Apr 08 15:53:45 2016 AMAROUCHE Ouahab Koussaila
*/

#include "my_printf.h"

void	opt_s(va_list ap)
{
  my_putstr(va_arg(ap, char *));
}

void	opt_d(va_list ap)
{
  my_put_nbr(va_arg(ap, int));
}

void	opt_c(va_list ap)
{
  my_putchar(va_arg(ap, int));
}

void	opt_r(va_list ap)
{
  if (ap != NULL)
    my_putchar('%');
}
