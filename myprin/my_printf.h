/*
** my_putnbr.c for project in /my_printf/amarou_o
**
** Made by AMAROUCHE Ouahab Koussaila
** Login   <amarou_o@etna-alternance.net>
**
** Started on  Wed Apr 06 14:06:17 2016 AMAROUCHE Ouahab Koussaila
** Last update Thu Apr 08 16:19:25 2016 AMAROUCHE Ouahab Koussaila
*/
#ifndef __MY_PRINTF_H__
# define __MY_PRINTF_H__

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>

char	*tab_opt();
void	my_putchar(char c);
int	my_strcmp(char *s1, char *s2);
void	my_putstr(char *str);
void	my_put_nbr(int n);
int	my_strlen(char* str);
char	*my_strcpy(char *dest, char *src);
int	my_printf(char *str, ...);
void	recup (int b, va_list ap);
void	opt_s(va_list ap);
void	opt_d(va_list ap);
void	opt_c(va_list ap);
void	my_put_nbro(unsigned int nbr);
void	opt_o(va_list ap);
void	opt_u(va_list ap);
void	my_put_nbru(unsigned int nbr);
void	put_nbrx(unsigned int nbr);
void	opt_x(va_list ap);
void	put_nbrx2(unsigned int nbr);
void	opt_X(va_list ap);
void	opt_r(va_list ap);
int	recup_option(char* str, va_list ap, int i);
void	suit_x(unsigned int modulo);
int	suit_putnbr(int x, int n);

#endif
