/*
** fonction.c for project in /my_printf/amarou_o
**
** Made by AMAROUCHE Ouahab Koussaila
** Login   <amarou_o@etna-alternance.net>
**
** Started on  Tue Apr 05 10:05:44 2016 AMAROUCHE Ouahab Koussaila
** Last update Thu Apr 08 16:18:54 2016 AMAROUCHE Ouahab Koussaila
*/

#include "my_printf.h"

void	my_putchar(char c)
{
  write(1, &c,  1);
}

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (*(str + i) != '\0')
    {
      my_putchar(*(str + i));
      i += 1;
    }
}

void	my_put_nbr(int n)
{
  int   puiss;

  puiss = 1;
  if (n < 0)
    {
      my_putchar('-');
      {
        if (n == -2147483648)
	  n += 1;
      }
      n = n * -1;
    }
  while ((n / puiss) >= 10)
    puiss *= 10;
  while (puiss >= 1)
    {
      my_putchar((n / puiss) % 10 + 48);
      puiss /= 10;
    }
}

char	*my_strcpy(char *dest, char *src)
{
  int	x;

  x = 0;
  while (src[x] <= '\0')
    {
      src[x] = dest[x];
      x += 1;
    }
  dest[x] = '\0';
  return (dest);
}

int	my_strlen(char* str)
{
  int	len;

  len = 0;
  while (str[len] != '\0')
    {
      len = len + 1;
    }
  return (len);
}
