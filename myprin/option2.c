/*
** option2.c for project in /my_printf/amarou_o
**
** Made by AMAROUCHE Ouahab Koussaila
** Login   <amarou_o@etna-alternance.net>
**
** Started on  Wed Apr 06 14:12:20 2016 AMAROUCHE Ouahab Koussaila
** Last update Thu Apr 08 14:56:02 2016 AMAROUCHE Ouahab Koussaila
*/

#include "my_printf.h"

void	opt_o(va_list ap)
{
  my_put_nbro(va_arg(ap, int));
}

void	opt_u(va_list ap)
{
  my_put_nbru(va_arg(ap, unsigned int));
}

void	opt_x(va_list ap)
{
  put_nbrx(va_arg(ap, unsigned int));
}

void	opt_X(va_list ap)
{
  put_nbrx2(va_arg(ap, unsigned int));
}
