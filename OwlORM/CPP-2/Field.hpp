//
//  Field.hpp
//  CPP-2
//
//  Created by Quentin Le Gal on 16/05/2017.
//  Copyright © 2017 Quentin Le Gal. All rights reserved.
//

#ifndef Field_hpp
#define Field_hpp

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "BaseField.hpp"

namespace  ORM {
    template<class T>
    class Field : public BaseField {
    private:
    public:
//        T address
        T value;
        Field();
        ~Field();
        ORM::Field<T> operator=(const T item) {
            T tmp = item;
            ORM::Field<T> field;
            field.value = item;
            data = item;
            return field;
        }
    };
}

template <class T>
ORM::Field<T>::Field() {
//    std::cout<<"Constructor, allocate..."<< std::endl;
}

// destructor
template <class T>
ORM::Field<T>::~Field() {
//    std::cout<<"Destructor, deallocate..."<< std::endl;
}

#endif /* Field_hpp */
