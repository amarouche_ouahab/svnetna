#ifndef ORM_hpp
#define ORM_hpp

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include "Table.hpp"
#include "mysql_driver.h"
#include "mysql_connection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
//typedef struct s_utilisateurs {
 //   std::string username;
  //  int user_id = -1;
//} Utilisateur;

namespace ORM {
    class DB {
    private:
        sql::Connection *con;
        void createTableIfNotExist(Table &);
    public:
        bool connect(std::string address, std::string user, std::string pwd);
        
        bool isKeyEmpty(Table);
        
        template <class T>
        std::vector<T> selectAll();
        
        template <class T>
        T findById();
        
        template <class T>
        bool Delete(T&);
        bool save(Table &);
        bool add(Table &);
        
        template<class T>
        T findById(int);
        
        template<class T>
        bool update(T &);
        
        ~DB();
    };
    
}
// The pointer is being allocated - not the object inself.
#endif /* ORM_hpp */
