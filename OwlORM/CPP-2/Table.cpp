//
//  Table.cpp
//  CPP-2
//
//  Created by Quentin Le Gal on 16/05/2017.
//  Copyright © 2017 Quentin Le Gal. All rights reserved.
//
#include <cstdint>
#include "Table.hpp"

ORM::Table::Table(std::string tbl, std::string prKey) : table(tbl), primaryKey(prKey) {
    std::cout << "generating table from fields..." << std::endl;
    
}

ORM::Table::~Table() {
    std::cout << "destroying the table" << std::endl;
}

void ORM::Table::addField(std::string name, ORM::BaseField& field) {
    tableFields.insert( { name, field });
    std::cout << "Adding " << name << std::endl;
}

// SET UP THE SQL STATEMENT (CREATE A TABLE IF NOT EXIST)
std::string ORM::Table::setUpSQLRequestCreateTables() {
    std::string sqlString;
    sqlString = "CREATE TABLE IF NOT EXISTS "+ table +" ("+primaryKey+" INT AUTO_INCREMENT NOT NULL,";
    for ( auto it = tableFields.begin(); it != tableFields.end(); ++it ) {
        if (it->first != primaryKey)
        sqlString += it->first + " varchar(250) NOT NULL default '', ";
    }
    sqlString += "primary key ("+ primaryKey +"))";
    return sqlString;
}

std::string ORM::Table::setUpSQLRequestListAll() {
    std::string sqlString;
    sqlString = "SELECT ";
    for (auto it = tableFields.begin(); it != tableFields.end(); ++it ) {
        sqlString += it->first + ", ";
    }
    sqlString = sqlString.substr(0, sqlString.size()-2);
    sqlString += " FROM " + table;
    std::cout << sqlString << std::endl;
    return sqlString;
}

std::string ORM::Table::setUpSQLDelete() {
    std::string sqlString;
    sqlString = "DELETE FROM " + table + " WHERE "+ primaryKey + " = ";
    for (auto it = tableFields.begin(); it != tableFields.end(); ++it ) {
        if (it->first == primaryKey)
            sqlString += it->second.getValue() + ")";
    }
    std::cout << sqlString << std::endl;
    return sqlString;
}

std::string ORM::Table::setUpSQLUpdate() {
    std::string sqlString;
    sqlString = "UPDATE "+table+" SET ";
    for (auto it = tableFields.begin(); it != tableFields.end(); ++it ) {
        if (it->first != primaryKey)
            sqlString += it->first + " = "+ it->second.getValue() + ", ";
    }
    sqlString = sqlString.substr(0, sqlString.size()-2);
    sqlString += " WHERE "+primaryKey +" = ";
    for (auto it = tableFields.begin(); it != tableFields.end(); ++it ) {
        if (it->first == primaryKey)
            sqlString += it->second.getValue();
    }
    std::cout << sqlString << std::endl;
    return sqlString;
}

std::string ORM::Table::setUpSQLFindById(int id) {
    std::string sqlString;
    sqlString = "SELECT * FROM "+table+" WHERE "+ primaryKey+"="+ std::to_string(id);
    std::cout << sqlString << std::endl;
    return sqlString;
}

std::string ORM::Table::setUpSQLAddToDB() {
    std::string sqlString;
    sqlString = "INSERT INTO "+table+"(";
    for (auto it = tableFields.begin(); it != tableFields.end(); ++it ) {
        if (it->first != primaryKey)
        sqlString += it->first + ", ";
    }
    sqlString = sqlString.substr(0, sqlString.size()-2);
    sqlString += ") VALUES (";
    for (auto it = tableFields.begin(); it != tableFields.end(); ++it ) {
        if (it->first != primaryKey)
            sqlString += "'" + it->second.getValue() + "', ";
    }
    sqlString = sqlString.substr(0, sqlString.size()-2);
    sqlString += ")";
    std::cout << sqlString << std::endl;
    return sqlString;
}
