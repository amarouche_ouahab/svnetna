#include "ORM.hpp"
#include "Table.hpp"

ORM::DB::~DB() {
    if (con)
        delete con;
    printf("connection closed !\n");
}

bool ORM::DB::connect(std::string address, std::string user, std::string pwd) {
    sql::mysql::MySQL_Driver *driver;
    driver = sql::mysql::get_driver_instance();
    con = driver->connect("tcp://"+address+":3306", user, pwd);
    if (con) {return true;}
    else {return false;}
}

bool ORM::DB::isKeyEmpty(Table table) {
    return table.user_id == -1 ? true: false;
}

template <class T>
bool ORM::DB::Delete(T& table) {
    sql::Statement *stmt;
    stmt = con->createStatement();
    stmt->execute("USE CPP");
    createTableIfNotExist(table);
    stmt->execute(table.setUpSQLDelete());
    delete stmt;
    return true;
}

void ORM::DB::createTableIfNotExist(Table & item) {
    sql::Statement *stmt;
    stmt = con->createStatement();
    stmt->execute("USE CPP");
    stmt->execute(item.setUpSQLRequestCreateTables());
    delete stmt;
}

bool ORM::DB::save(Table & table) {
    //if (isKeyEmpty(table)) {
        add(table);
//    } else {
//        update(table);
//    }
    return true ;
}


template<class T>
 std::vector<T> ORM::DB::selectAll() {
    sql::Statement *stmt;
    sql::ResultSet *res = nullptr;
    stmt = con->createStatement();
    stmt->execute("USE CPP");
    T tmp;
    createTableIfNotExist(tmp);
    res = stmt->executeQuery(tmp.setUpSQLRequestListAll());
    std::vector<T> itemList = std::vector<T>();
    while (res->next()) {
        T * tmp = new T();
        tmp->user_id = std::stoi(res->getString(tmp->primaryKey)); // getInt(1) returns the first column
        tmp->username = res->getString("username");
        itemList.push_back(*tmp);
        std::cout << res->getString("username") << std::endl;
    }
    delete res;
    delete stmt;
    return itemList;
}

bool ORM::DB::add(Table & element) {
    sql::Statement *stmt;
    stmt = con->createStatement();
    stmt->execute("USE CPP");
    createTableIfNotExist(element);
    stmt->execute(element.setUpSQLAddToDB());
    sql::ResultSet *res = stmt->executeQuery("SELECT @@identity AS"+element.primaryKey);
    res->next();
    element.user_id = res->getInt(1);
    delete stmt;
    return true;
}


template<class T>
bool ORM::DB::update(T & element) {
    sql::Statement *stmt;
    stmt = con->createStatement();
    stmt->execute("USE CPP");
    createTableIfNotExist(element);
    stmt->execute(element.setUpSQLUpdate());
    delete stmt;
    return true;
}

template<class T>
T ORM::DB::findById(int id) {
    sql::Statement *stmt;
    stmt = con->createStatement();
    stmt->execute("USE CPP");
    T tmp;
    //CreateTableIfNotExist(Table stuff);
    sql::ResultSet *res = stmt->executeQuery(tmp.setUpSQLFindById(id));
    res->next();
    //std::cout << "RESULT = " << res->getInt64("id") << std::endl;
    T item = T();
    item.user_id = id;
    try {
         item.username = res->getString("username");
    } catch (sql::InvalidArgumentException) {
        std::cout << "ORM::DB::findById -> error no user found with id : "+std::to_string(id)+" !" << std::endl;
        throw;
    }
    delete stmt;
    return item;
}

// )-------------------

class Utilisateur : public ORM::Table {
    /* ORM::Table est une classe de votre ORM  */
    /*
     * ici, ORM::Field est un template.
     * Si vous avez peur de ne pas réussir à les utiliser correctement,
     * vous pouvez définir que ORM::Field est une classe qui ne gère que des strings,
     * mais vous risquez d'être pénalisés.
     *
     * Si vous décidez de template-r votre ORM::Field, faites les hériter d'une classe
     * ORM::BaseField, non templatées, pour pouvoir stocker des
     * pointeurs / références dans un vecteur.
     * (vous ne pouvez pas faire un vecteur de ORM::Field<tout>).
     *
     * Les champs auront un "operator T" (T étant le type template – si vous ne templatez pas ORM::Field, vous aurez un "operator std::string"),
     * et auront un "operator=(T newValue)" (sans template, "operator=(std::string newValue)" pour remplacer la valeur).
     *
     * Les champs doivent pouvoir être NULL, comme en SQL.
     */
    public:
    ORM::Field<int> userID;
    ORM::Field<std::string> username;
//    ORM::Field<int> password;
    
    /*
     * Ici, on appelle le constructeur du parent
     * avec le nom de la table, et le nom de la clef primaire (qui sera utilisé pour .findById et .update)
     * La clef primaire, côté SQL, sera en AUTO INCREMENT (vous n'avez pas à en générer).
     */
    
    Utilisateur() : ORM::Table("utilisateur", "user_id") {
        
        /* dans votre ORM::Table, vous aurez un addField(std::string name, ORM::BaseField& field) */
        addField("user_id", userID);
        addField("username", username);
//        addField("password", password);
        /*
         * Vous pouvez, dans cette fonction addField, ajouter le champ dans une
         * std::unordered_map<std::string, ORM::BaseField&>.
         *
         * Lorsque vous ferez des SELECT, vous pourrez ensuite utiliser cette liste de champs
         * pour setter les données.
         */
    }
};




int main() {
    //	/* Vous pouvez transformer ORM::DB en singleton */
    ORM::DB db;
    db.connect("localhost", "root", "root");
    
    std::vector<Utilisateur> users = db.selectAll<Utilisateur>();
    for (Utilisateur& user : users) {
        std::cout << "Utilisateur #" << &user.user_id << ": " << &user.username << "\n";
        //db.Delete(user); /* ou alors user.delete(), si votre DB est un singleton */
    }
    //
    Utilisateur newUser; /* Création */
    newUser.username = "Nouvel utilisateur";
//    newUser.password = 1234;
    db.save(newUser); /* ou alors user.save(), si votre DB est un singleton */
    std::cout << "User ID = " << newUser.user_id << std::endl;
    //	/* L'ORM sait que "save" fait un INSERT, car la clef primaire, userID, est vide */
    //	/* Après l'INSERT, vous devez remplir cette clef primaire avec le nouvel ID */
    std::cout << "Nouvel utilisateur: #" << newUser.user_id << "\n";
    //
    newUser.username = "Utilisateur changé";
    db.save(newUser); /* fait un UPDATE, car la clef primaire n'est *PAS* vide */

    /* ... */
    
    /* Génere un SELECT * FROM utilisateurs WHERE user_id = 1 */
    /* Si l'utilisateur n'existe pas, vous pouvez (par exemple) lancer une exception */
    Utilisateur firstUser = db.findById<Utilisateur>(1);
    std::cout << "Nom du premier utilisateur : " << &firstUser.username << "\n";
}
