//
//  BaseField.hpp
//  CPP-2
//
//  Created by Quentin Le Gal on 16/05/2017.
//  Copyright © 2017 Quentin Le Gal. All rights reserved.
//

#ifndef BaseField_hpp
#define BaseField_hpp

#include <iostream>
#include <stdio.h>


namespace ORM {
    class BaseField {
    protected:
        std::string data;
    private:
    public:
        BaseField();
        virtual ~BaseField();
        std::string getValue() {
            return data;
        }
    };
}

#endif /* BaseField_hpp */
