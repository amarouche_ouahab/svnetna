//
//  Table.hpp
//  CPP-2
//
//  Created by Quentin Le Gal on 16/05/2017.
//  Copyright © 2017 Quentin Le Gal. All rights reserved.
//

#ifndef Table_hpp
#define Table_hpp

#include <stdio.h>
#include <iostream>
#include <unordered_map>
#include <vector>
#include "Field.hpp"
#include "BaseField.hpp"

namespace ORM {
    class Table {
    public:
        int user_id;
        std::string table;
        std::string username; 
        Table(std::string, std::string);
        ~Table();
        std::string primaryKey;
        std::string setUpSQLRequestCreateTables();
        std::string setUpSQLRequestListAll();
        std::string setUpSQLAddToDB();
        std::string setUpSQLDelete();
        std::string setUpSQLUpdate();
        std::string setUpSQLFindById(int id);
    private:
        std::unordered_map<std::string, ORM::BaseField&> tableFields;
    protected:
        void addField(std::string, ORM::BaseField& field);
    };
}


    
#endif /* Table_hpp */
