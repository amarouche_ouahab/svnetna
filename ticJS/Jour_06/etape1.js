function prepare_dom(){

	var div = $("<div>");
	$(div).attr("id","from");
	$("body").append(div);

	var div1 = $("<div>")
	var input = $("<input>");
	$(input).attr("type", "text");
	$(input).attr("id","name");
	$(input).attr("placeholder", "Name");
	$(div1).append(input);
	$(div).append(div1);

	var div2 = $("<div>");
	var input1 = $("<input>");
	$(input1).attr("id","level");
	$(input1).attr("placeholder", "Level");
	$(div2).append(input1);
	$(div).append(div2);
	
	var div3 = $("<div>");
	var select = $("<select>");
	$(select).attr("id","");
	var option1 = $("<option>");
	var option2 = $("<option>");
	$(option1).attr("value", "1")
	$(option2).attr("value", "2")
	$(option1).append("List 1");
	$(option2).append("List 2");
	$(option1).appendTo(select);
	$(option2).appendTo(select)
	$(select).appendTo(div3);
	$(div3).appendTo(div);
	
	var div4 = $("<div>");
	var button = $("<button>");
	$(button).attr("id","submit");
	$(button).append("Save");
	$(div4).append(button);
	$(div).append(div4);

	var div5 = $("<div>");
	$(div5).attr("style","width: 500px");
	$("body").append(div5);

	var div6 = $("<div>");
	$(div6).attr("id","collection1");
	$(div6).attr("style","width:50%; float: left");
	$(div6).append("Collection 1");
	$(div5).append(div6);
	
	var div7 = $("<div>");
	$(div7).attr("id","collection2");
	$(div7).attr("style","width:50%; float: right");
	$(div7).append("Collection 2");
	$(div5).append(div7);

	var div8 = $("<div>");
	$(div8).attr("id","details");
	$("body").append(div8);

	var div9 = $("<div>");
	$(div9).attr("id","itemTemplate");
	$(div9).attr("style","display:none;");
	var span1 = $("<span>");
	var span2 = $("<span>");
	$(div9).append(span1);
	$(div9).append(span2);
	
	var span3 = $("<span>");
	var button2 = $("<button>");
	$(button2).append("Edit");
	$(span3).append(button2);
	$(div9).append(span3);

	var span4 = $("<span>");
	var button3 = $("<button>");
	$(button3).append("X");
	$(span4).append(button3);
	$(div9).append(span4);
	$("body").append(div9);
}