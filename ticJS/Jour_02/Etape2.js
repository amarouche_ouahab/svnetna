function make_custom_array (start, stop, step) {
var array = [];
if ( start <= stop)
	{
	while (start <= stop && step > 0)
    	{
   		    array.push(start);
			start = start + step;
		}
	}
if ( start >= stop)
	{
      while (start >= stop && step > 0)
    	{
 	    	array.push(start);
			start = start - step;
		}
	}
return (array);
};

