<?php
// capitalize_file.php for  capitalize_file.php in /home/amarou_o/FDI-DPHP/Jour_02/capitalize_file
// 
// Made by AMAROUCHE Ouahab Koussaila
// Login   <amarou_o@etna-alternance.net>
// 
// Started on  Wed Oct 21 09:18:10 2015 AMAROUCHE Ouahab Koussaila
// Last update Wed Oct 21 13:02:21 2015 AMAROUCHE Ouahab Koussaila
//
$z = 1;
$y = 0;
if (file_exists($argv[$z]) == False)
  echo "capitalize_file: {$argv[$z]}: No such file or directory\n";
else if (is_dir($argv[$z]) == true)
  echo "capitalize_file: {$argv[$z]}: Is a directory\n";
else if (is_readable($argv[$z]) == true)
  {
    if ($handle = fopen($argv[$z], "r"))
      {
	$contenu = fread($handle, filesize($argv[$z]));
	fclose($handle);
	$handle = fopen($argv[$z], "w");
	while (isset($contenu[$y]))
	  {
	    if (($y == 0) || (($contenu[$y] == "!" || $contenu[$y] == ".")
			      && ($contenu[$y + 1] == " " || $contenu[$y + 1] == "\n")))
	      {
		if (($contenu[$y] == "!" || $contenu[$y] == ".")
		    && ($contenu[$y + 1] == " " || $contenu[$y + 1] == "\n"))
		  {
		    $y = $y + 2;
		  }
		if (ord($contenu[$y]) <= 122 && ord($contenu[$y]) >= 97)
		  {
		    $contenu[$y] = chr(ord($contenu[$y]) - 32);
		  }
	      }
	    $y++;
	  }
	fwrite($handle, $contenu);
	fclose($handle);
      }
    else if (fopen($argv[$z], "r") == false)
      echo "capitalize_file: $argv[$z]:  Cannot open file\n";
  }
else
  echo "capitalize_file: {$argv[$z]}: Permission denied\n";