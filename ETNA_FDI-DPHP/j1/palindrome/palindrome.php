<?php
// palindrome.php for  palindrome.php in /home/amarou_o/FDI-DPHP/Jour_01/palindrome
// 
// Made by AMAROUCHE Ouahab Koussaila
// Login   <amarou_o@etna-alternance.net>
// 
// Started on  Mon Oct 19 13:38:00 2015 AMAROUCHE Ouahab Koussaila
// Last update Mon Oct 19 17:45:55 2015 AMAROUCHE Ouahab Koussaila
//
function palindrome($param)
{
  $i = 0;
  $j = 0;
  
  while (isset($param[$i]))
    {
      $i++;
    }
  $i--;
  while ($i >= $j)
    {
      if (ord($param[$i]) == 32)
	$i--;
      if (ord($param[$j]) == 32)
	$j++;
      if (ord($param[$i]) != ord($param[$j])
	  && ord($param[$i]) + 32 != ord($param[$j])
	  && ord($param[$j]) + 32 != ord($param[$i]))
	{
	  echo "False\n";
	  return;
	}
      $i--;
      $j++;
    }
  echo "True\n";
}