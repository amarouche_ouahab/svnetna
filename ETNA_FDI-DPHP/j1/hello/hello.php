<?php
// hello.php for hello.php in /home/amarou_o/FDI-DPHP/Jour_01/hello
// 
// Made by AMAROUCHE Ouahab Koussaila
// Login   <amarou_o@etna-alternance.net>
// 
// Started on  Mon Oct 19 11:18:55 2015 AMAROUCHE Ouahab Koussaila
// Last update Mon Oct 19 17:03:04 2015 AMAROUCHE Ouahab Koussaila
//
function hello($param = "world")
{
  echo "Hello ".$param." !\n";
}