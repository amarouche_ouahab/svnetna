<?php
// microshell.php for microshell.php in /home/amarou_o/FDI-DPHP/MicroShell
// 
// Made by AMAROUCHE Ouahab Koussaila
// Login   <amarou_o@etna-alternance.net>
// 
// Started on  Fri Oct 23 10:56:43 2015 AMAROUCHE Ouahab Koussaila
// Last update Sat Oct 24 11:29:27 2015 AMAROUCHE Ouahab Koussaila
//
require_once('include/affichage.php');
require_once('include/decoupage.php');
require_once('include/commandes.php');


$fd = fopen('php://stdin', 'r');
$env = $_SERVER;
if ($fd != false)
  {
    //     aff_prompt();
    while (($line = fgets($fd)) !== false)
      {
        $params = decoup_params($line);
	
	$ptr = 'func_' . $params[0][0];
	
	if (function_exists($ptr))
	  {
	    $return = $ptr($params);
	    if($return === 0)
	      return (0);
	  }
	else 
	  echo "Command not found\n";
	aff_prompt();
      }
    fclose($fd);
  }