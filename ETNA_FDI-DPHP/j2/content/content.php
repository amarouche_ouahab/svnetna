<?php
// content.php for  content.php in /home/amarou_o/FDI-DPHP/Jour_02/content
// 
// Made by AMAROUCHE Ouahab Koussaila
// Login   <amarou_o@etna-alternance.net>
// 
// Started on  Tue Oct 20 13:18:00 2015 AMAROUCHE Ouahab Koussaila
// Last update Tue Oct 20 15:45:55 2015 AMAROUCHE Ouahab Koussaila
//
$z = 1;
while ($z < $argc)
  {
    if (file_exists($argv[$z]) == true)
      {
	if (is_dir($argv[$z]) == true)
	  {
	    echo "content.php: {$argv[$z]}: is a directory\n";
	  }
	else if (is_readable($argv[$z]) == true)
	  {
	    if (($handle = fopen($argv[$z], "r")) != NULL)
	      {
                $contenu = fread($handle, filesize($argv[$z]));
		fclose($handle);
		echo "{$contenu}";
	      }
	    else
	      {
		echo "content.php: {$argv[$z]}: Cannot open file\n";
	      }
	  }
	else 
	  {
	    echo "content.php: {$argv[$z]}: Permission denied\n";
	  }
      }
    else if (file_exists($argv[$z]) == FALSE)
      echo "content.php: {$argv[$z]}: No such file or directory\n";
    $z++;
  }