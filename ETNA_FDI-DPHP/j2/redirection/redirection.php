<?php
// redirection.php for  redirection.php in /home/amarou_o/FDI-DPHP/Jour_02/redirection
//                                                                      
// Made by AMAROUCHE Ouahab Koussaila                                   
// Login   <amarou_o@etna-alternance.net>                               
//                                                                      
// Started on  Tue Oct 20 14:18:09 2015 AMAROUCHE Ouahab Koussaila      
// Last update Tue Oct 20 18:55:37 2015 AMAROUCHE Ouahab Koussaila      
//  
if ($argc != 4)
  {
    echo "Usage : ./redirection.php 'string' '[> >>]' 'File'\n";
  }
else
  {
    if (file_exists($argv[3]) == FALSE)
      {
	$handle = fopen($argv[3], "a");
	fclose($handle);
      }
    if (is_dir($argv[3]) == TRUE)
      {
        echo "redirection.php: {$argv[3]}: Is a directory\n";
      }
    else if (is_writable($argv[3]) == TRUE)
      {
	if ($argv[2] == ">>")
	  {
	    if (($handle = fopen($argv[3], "a")) != NULL)
	      {
		fwrite($handle, $argv[1]."\n");
		fclose($handle);
	      }
	  }
	else if ($argv[2] == ">") 
	  {
	    if (($handle = fopen($argv[3], "w")) != NULL)
	      {
		fwrite($handle, $argv[1]."\n");
		fclose($handle);
	      }
	  }
	else
          {
	    echo "redirection.php: {$argv[3]}: Cannot open file\n";
          }
      }
    else
      {
	echo "redirection.php: {$argv[3]}: Permission denied\n";
      }
  }