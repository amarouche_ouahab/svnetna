import axios from 'axios';
import querystring from 'querystring';
import { navigate } from 'gatsby'

export const REQUEST_BEGIN = 'request/REQUEST_BEGIN';
export const REQUEST_SUCCESS = 'request/REQUEST_SUCCESS';
export const REQUEST_FAILURE = 'request/REQUEST_FAILURE';
export const RESET_REQUEST_STATE = 'request/RESET_REQUEST_STATE';

const requestBegin = () => {
  return { type: REQUEST_BEGIN, }
}

const requestSuccess = (data) => {
  return {
    type: REQUEST_SUCCESS,
    data
  }
}

const requestFailure = (error) => {
  return {
    type: REQUEST_FAILURE,
    error
  }
}

export const resetRequestState = () => {
  return { type: RESET_REQUEST_STATE, }
}

const status = (response) => {
  let promise;
  if (response.status >= 200 && response.status < 300) {
    promise = Promise.resolve(response);
  } else {
    promise = Promise.reject(new Error(response));
  }
  return (promise);
};

export function request(options) {
  return (dispatch) => {
    dispatch(requestBegin());
    // return axios.post(`https://g0rfhwcs4j.execute-api.us-east-1.amazonaws.com/dev/getInfos`, querystring.stringify({
    //   lat: 48.813920,
    //   lng: 2.392310,
    // }))
    return axios({
      method: 'post',
      url: 'https://g0rfhwcs4j.execute-api.us-east-1.amazonaws.com/dev/getInfos',
      data: { lat: 48.813920, lng: 2.392310, hasChild: true, activities: [] },
      headers: { 
        "Content-Type": "application/x-www-form-urlencoded",
        // "Cache-Control": "no-cache",
        // "Postman-Token": "42e6c291-9a09-c29f-f28f-11872e2490a5"
      }
    })
      .then(status)
      .then((res) => {
        /* eslint-disable no-console */
        console.log(JSON.parse(res.request.response));
        dispatch(requestSuccess(res.request.response.data));
        navigate("/myspace")
      })
      .catch((err) => {
        /* eslint-disable no-console */
        // console.log(err.response);
        console.log('before dispatch');
        dispatch(requestFailure(err.response ? err.response : null));
        console.log('after dispatch');
      });
  }
}