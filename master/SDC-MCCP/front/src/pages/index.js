import React, { Component } from 'react';
import Layout from '../components/layout';
import Intro from '../components/mask';
import SEO from '../components/seo';
import Carousel from '../components/carousel';
import AboutSection from '../components/aboutSection'
import Card from '../components/card'
import { MDBRow } from 'mdbreact';

import image1 from "../images/portrait-of-mature-man-repairing-garden-fence-P4TK2VN.jpg"
import image2 from "../images/fashion-woman-at-sunset-PRYVL7Q.jpg"
import image3 from "../images/male-architect-in-office-PTV3CYY.jpg"

class App extends Component {

  render() {
    var cardData = [
      { title: "Lucian Roche", description: "Je suis convaincu qu’Edem’ est l’outil qu’il faut aux français pour les assister au quotidien dans leurs déménagements.", image: image1 },
      { title: "Haya Nimani", description: "Edem’ m’a permis de trouver l’école de mes enfants et de ne pas oublier de résilier mon contrat internet ! Chose que j’avais oublié de faire lors de mon précédent changement d’adresse.", image: image2 },
      { title: "Mickael Kyle", description: "Suite à une mutation surprise de mon travail, nous avons dû organiser notre déménagement dans l’urgence, grâce à Edem’ aucuns détails n’ont été négligées et tout s’est très bien déroulé malgré le délais impartis.", image: image3 },
    ];

    return (
      <>
        <Layout>
          <SEO title="Home" keywords={[`déménagement`, `demenagement`, `move`, `mutation`]} />
          <Carousel />
          <Intro />
          <main>
            <AboutSection />
            <section id="cardSection">
              <h2 className="h1-responsive text-center font-weight-bold mb-5">
                Retour d'expériences
            </h2>
              <MDBRow className="m-0" center>
                <Card image={cardData[0].image} description={cardData[0].description} title={cardData[0].title} >jean</Card>
                <Card image={cardData[1].image} description={cardData[1].description} title={cardData[1].title} >jean</Card>
                <Card image={cardData[2].image} description={cardData[2].description} title={cardData[2].title} >jean</Card>
              </MDBRow>
            </section>
          </main>
        </Layout>
      </>
    );
  }
}

export default App;
