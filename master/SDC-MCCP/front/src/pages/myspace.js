import React, { Component } from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';
import { MDBRow, MDBContainer, MDBCol, MDBIcon } from 'mdbreact';
import Marker from '../components/marker.js';
import IconDesc from '../components/iconDesc';

import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class MySpace extends Component {

  state = {
    checkbox: {}
  };

  render() {

    var markers = [
      { title: "College", lat: 43.604652, lng: 1.4442090000000007, color: '#ff5722' },
      { title: "Lycee", lat: 43.614652, lng: 1.4742090000000007, color: '#ff5722' },
    ];

    return (
      <>
        <Layout>
          <SEO title="Home" keywords={[`déménagement`, `demenagement`, `move`, `mutation`]} />
          <main>
            <MDBContainer tag="section" className="text-center my-5">
              <h2 className="h1-responsive font-weight-bold my-5">
                L'état de mon déménagement
              </h2>
              <MDBRow className="mb-2">
                <MDBCol md="3">
                  <IconDesc
                    icon="bolt"
                    color="yellow-text"
                    title="Electricité"
                    description="J'ai résilié ou effectué un changement d'adresse pour mon contrat"
                  />
                </MDBCol>
                <MDBCol md="3">
                  <IconDesc
                    icon="wifi"
                    color="grey-text"
                    title="Internet"
                    description="J'ai contacté mon fournisseur internet pour l'informer de mon déménagement"
                  />
                </MDBCol>
                <MDBCol md="3">
                  <IconDesc
                    icon="tint"
                    color="blue-text"
                    title="Eau"
                    description="J'ai régularisé ma facture d'eau avant de quitter mon logement"
                  />
                </MDBCol>
                <MDBCol md="3">
                  <IconDesc
                    icon="user-md"
                    color="red-text"
                    title="Médecin traitant"
                    description="J'ai trouvé mon nouveau traitant et je l'ai déclaré auprès de la sécurité social et de ma mutuelle"
                  />
                </MDBCol>
              </MDBRow>
              <MDBRow className="mb-5">
                <MDBCol md="3">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="bolt" />
                    <label className="custom-control-label" htmlFor="bolt"></label>
                  </div>
                </MDBCol>
                <MDBCol md="3">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="wifi" />
                    <label className="custom-control-label" htmlFor="wifi"></label>
                  </div>
                </MDBCol>
                <MDBCol md="3">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="tint" />
                    <label className="custom-control-label" htmlFor="tint"></label>
                  </div>
                </MDBCol>
                <MDBCol md="3">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="user-md" />
                    <label className="custom-control-label" htmlFor="user-md"></label>
                  </div>
                </MDBCol>
              </MDBRow>
              <MDBRow className="mb-2">
                <MDBCol md="3">
                  <IconDesc
                    icon="house-damage"
                    color="green-text"
                    title="Assurance habitation"
                    description="J'ai bien communiqué ma nouvelle adresse à mon assurance habition"
                  />
                </MDBCol>
                <MDBCol md="3">
                  <IconDesc
                    icon="piggy-bank"
                    color="purple-text"
                    title="Banque"
                    description="Je n'ai pas oublié de changer d'agence (ou prévenu ma banque que je suis à l'autre bout de la France)"
                  />
                </MDBCol>
                <MDBCol md="3">
                  <IconDesc
                    icon="car-crash"
                    color="orange-text"
                    title="Assurance voiture"
                    description="J'ai informé mon assureur voiture pour arriver au bon endroit en cas d'accident"
                  />
                </MDBCol>
                <MDBCol md="3">
                  <IconDesc
                    icon="hand-holding-usd"
                    color="brown-text"
                    title="Impôt"
                    description="Je n'ai bien évidement pas oublié de prévenir mon centre d'impôt afin d'éviter une 'petite' majoration"
                  />
                </MDBCol>
              </MDBRow>
              <MDBRow className="mb-5">
                <MDBCol md="3">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="house-damage" />
                    <label className="custom-control-label" htmlFor="house-damage"></label>
                  </div>
                </MDBCol>
                <MDBCol md="3">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="piggy-bank" />
                    <label className="custom-control-label" htmlFor="piggy-bank"></label>
                  </div>
                </MDBCol>
                <MDBCol md="3">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="car-crash" />
                    <label className="custom-control-label" htmlFor="car-crash"></label>
                  </div>
                </MDBCol>
                <MDBCol md="3">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="hand-holding-usd" />
                    <label className="custom-control-label" htmlFor="hand-holding-usd"></label>
                  </div>
                </MDBCol>
              </MDBRow>

            </MDBContainer>
            <section id="cardSection" style={{ 'padding-top': '0' }}>
              <MDBRow className="m-0" center>
                <h2 className="h1-responsive font-weight-bold my-5">
                  Mon quartier-zer
                </h2>
                <div style={{ height: '100vh', width: '100%' }}>
                  <GoogleMapReact
                    bootstrapURLKeys={{ key: 'AIzaSyDKIACNG4QTFZuD_mxSGHzr9FnxN15FHsg' }}
                    defaultCenter={[43.604652, 1.4442090000000007]}
                    defaultZoom={16}
                  >
                    {markers.map(function (marker, index) {
                      console.log("ON MAP");
                      return <Marker
                        key={index}
                        text={marker.title}
                        lat={marker.lat}
                        lng={marker.lng}
                        color={marker.color}
                      />;
                    })}
                    <Marker
                      key={0}
                      text={"Jean lou"}
                      lat={43.604652}
                      lng={1.4442090000000007}
                      color={"#ff5722"}
                    />
                  </GoogleMapReact>
                </div>
              </MDBRow>
            </section>
          </main>
        </Layout>
      </>
    );
  }
}

export default MySpace;
