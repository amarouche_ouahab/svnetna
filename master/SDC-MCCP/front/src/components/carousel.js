import React from "react";
import { MDBCarousel, MDBCarouselInner, MDBCarouselItem, MDBView, MDBMask, MDBCarouselCaption } from
    "mdbreact";
import image1 from "../images/moving-house-and-boxes-PN7P4GX.jpg"
import image2 from "../images/moving-concept-father-and-son-moving-to-a-new-ZRBEGWK.jpg"
import image3 from "../images/black-family-moving-to-new-house-PGZPHQF.jpg"
import image4 from "../images/moving-to-new-house-concept-PUYEJ75.jpg"

const CarouselPage = () => {
    return (
        <MDBCarousel activeItem={1} length={4} showControls={true} showIndicators={false} className="z-depth-1 w-100">
            <MDBCarouselInner>
                <MDBCarouselItem itemId="1">
                    <MDBView>
                        <img className="d-block w-100" src={image1} alt="First slide" />
                        <MDBMask overlay="black-light" />
                    </MDBView>
                </MDBCarouselItem>
                <MDBCarouselItem itemId="2">
                    <MDBView>
                        <img className="d-block w-100" src={image2} alt="Second slide" />
                        <MDBMask overlay="black-light" />
                    </MDBView>
                </MDBCarouselItem>
                <MDBCarouselItem itemId="3">
                    <MDBView>
                        <img className="d-block w-100" src={image3} alt="Mattonit's item" />
                        <MDBMask overlay="black-light" />
                    </MDBView>
                </MDBCarouselItem>
                <MDBCarouselItem itemId="4">
                    <MDBView>
                        <img className="d-block w-100" src={image4} alt="Mattonit's item" />
                        <MDBMask overlay="black-light" />
                    </MDBView>
                </MDBCarouselItem>
            </MDBCarouselInner>
        </MDBCarousel>
    );
}

export default CarouselPage;
