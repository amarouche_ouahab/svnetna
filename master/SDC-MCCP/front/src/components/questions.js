import React, { Component } from 'react';
// import Layout from './layout';
// import SEO from './seo';
import { connect } from 'react-redux'
// import LocationSearchInput from './locationSearchInput.js';
import { MDBContainer, /* MDBRow, MDBCol, */ MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import Select from 'react-select';

import { request } from '../actions/request.actions'

const options =  {
  types: ['address'],
  componentRestrictions: {country: "fr"},
}

const childrenOptions = [
  { value: true, label: 'Oui' },
  { value: false, label: 'Non' },
];

const scholarOptions = [
  { value: "maternelle", label: 'Maternelle' },
  { value: "primaire", label: 'Primaire' },
  { value: "college", label: 'Collège' },
  { value: "lycee", label: 'Lycée' },
];

const activitiesOptions = [
  { value: "football", label: 'Football' },
  { value: "handball", label: 'Handball' },
  { value: "basket", label: 'Basket' },
  { value: "natation", label: 'Natation' },
  { value: "tennis", label: 'Tennis' },
  { value: "badminton", label: 'Badminton' },
];

class Questions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      // modal: false,
      // selectedOption: false,
      // multiScolarSelectedOption: null,
      // multiActivitySelectedOption: null,
      address: null,
      form: {
        address: null,
        hasChild: null,
        scholarOptions: null,
        activitiesOptions: null,
      },
      errors: {
        address: false,
        hasChild: false,
        scholarOptions: false,
        activitiesOptions: false,
      },
    };
  }

  handleChangeSelect = option => {
    this.setState((prevState) => ({ form: { ...prevState.form, hasChild: option.value } }));
    console.log(`Option selected:`, option);
  }

  handleChangeMultiScolarSelect = option => {
    // this.setState({ multiScolarSelectedOption });
    this.setState((prevState) => ({ form: { ...prevState.form, scholarOptions: option.map(a => a.value) } }));
    
    console.log(`Option multi selected:`, option)
    console.log(option.map(a => a.value))
  }

  handleChangeMultiActivitySelect = (option) => {
    // this.setState({ multiActivitySelectedOption });
    this.setState((prevState) => ({ form: { ...prevState.form, activitiesOptions: option.map(a => a.value) } }));
    console.log(`Option multi selected:`, option);
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  toggleRequest = () => {
    const { requestReducer, request } = this.props
    const { form } = this.state

    // console.log(this.state.form)

    const errors = this.validate(form)

    console.log('errors', errors)

    this.setState((prevState) => ({
      errors: { ...prevState.form, address: errors.address, hasChild: errors.hasChild, scholarOptions: errors.scholarOptions }
    }))

    if (errors.address || errors.hasChild || errors.scholarOptions) return

    console.log('toggleRequest')
    if (!requestReducer.isLoading) {
      request()
      console.log('request start')
    } else
      console.log('Is already loading')
  }

  handleChange = address => {
    this.setState({ address });
  };

  handleSelect = address => {
    this.setState({ address });
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {this.setState((prevState) => ({ form: { ...prevState.form, address: latLng } })); console.log('Success', latLng)})
      .catch(error => console.error('Error', error));
  };

  validate = ({ address, hasChild, scholarOptions, }) => {
    return {
        address: !address || !address.lng || !address.lat
            ? 'Veuillez entrer une adresse valide'
            : false,
        hasChild:
            hasChild == null
            ? 'Veuillez choisir une option'
            : false,
        scholarOptions:
            hasChild && scholarOptions.length === 0
            ? 'Veuillez choisir au moins une option'
            : false,
    };
};

  render() {
    const { isLoading } = this.props.requestReducer

    const { hasChild } = this.state.form;
    const { errors } = this.state
    const hidden = hasChild == null || (hasChild != null && hasChild.value === false) ? { display: 'none' } : {};

    return (
      <div>
        <MDBBtn color="orange" onClick={this.toggle}>Questions</MDBBtn>
        <MDBContainer style={{color: "black"}}>
          <MDBModal isOpen={this.state.modal} toggle={this.toggle} backdrop={false} size="lg" style={{color: "elegant-color"}} centered >
            <MDBModalHeader toggle={this.toggle} style={{color: 'black'}}>Questionnaire</MDBModalHeader>
            <MDBModalBody>
              <form>
                <PlacesAutocomplete
                  value={this.state.address}
                  onChange={this.handleChange}
                  onSelect={this.handleSelect}
                  searchOptions={options}
                >
                  {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div>
                      <label htmlFor="defaultFormRegisterEmailEx" className="grey-text">
                                Votre future adresse
                              </label>
                      <input
                        {...getInputProps({
                          placeholder: '',
                          className: `location-search-input form-control ${errors.address && 'is-invalid'}`,
                        })}
                      />
                      <div className="autocomplete-dropdown-container">
                        {loading && <div>Loading...</div>}
                        {suggestions.map(suggestion => {
                          const className = suggestion.active
                            ? 'suggestion-item--active'
                            : 'suggestion-item';
                          // inline style for demonstration purpose
                          const style = suggestion.active
                            ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                            : { backgroundColor: '#ffffff', cursor: 'pointer' };
                          return (
                            <div
                              {...getSuggestionItemProps(suggestion, {
                                className,
                                style,
                              })}
                            >
                              <span>{suggestion.description}</span>
                            </div>
                          );
                        })}
                      </div>
                      <br/>
                    </div>
                  )}
                </PlacesAutocomplete>
                {/* <LocationSearchInput></LocationSearchInput> */}
                <label htmlFor="defaultFormRegisterNameEx" className="grey-text">
                  Avez vous des enfants ?
                </label>
                <Select
                  // value={hasChild}
                  onChange={this.handleChangeSelect}
                  options={childrenOptions}
                  placeholder=""
                  className={`${errors.hasChild && 'is-invalid'}`}
                />
                <br />
                <div style={hidden}>
                  <label htmlFor="defaultFormRegisterEmailEx" className="grey-text">
                    Dans quel(s) type(s) d'établissement(s) est-il/sont-ils scolarisé(s) ?
                      </label>
                  <Select
                    // value={multiScolarSelectedOption}
                    onChange={this.handleChangeMultiScolarSelect}
                    isMulti={true}
                    options={scholarOptions}
                    placeholder=""
                    styles={{"color": "elegant-color"}}
                  />
                  <br/>
                </div>
                <label
                  htmlFor="defaultFormRegisterConfirmEx"
                  className="grey-text"
                >
                  Quel(s) activité(s) faites-vous (ou vos proches) actuellement ?
                    </label>
                <Select
                  // value={multiActivitySelectedOption}
                  onChange={this.handleChangeMultiActivitySelect}
                  isMulti={true}
                  isSearchable={true}
                  options={activitiesOptions}
                  placeholder=""
                />
              </form>
            </MDBModalBody>
            <MDBModalFooter>
              <MDBBtn color="yellow" onClick={this.toggle}>Fermer</MDBBtn>
              <MDBBtn color="orange" onClick={this.toggleRequest}>
              {isLoading ? (
                <div className="spinner-border" role="status">
                {/* <span class="sr-only">Loading...</span> */}
              </div>
              ) : ('Envoyer')}
              </MDBBtn>
            </MDBModalFooter>
          </MDBModal>
        </MDBContainer>
      </div>
    );
  }
}

// export default Questions;

const mapStateToProps = state => {
  return {
    requestReducer: state.requestReducer,
  };
}

const mapDispatchToProps = {
  request,
}

export default connect(mapStateToProps, mapDispatchToProps)(Questions);