import React from 'react';
import { MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBCol } from 'mdbreact';

function CardExample({ image, title, description }) {
    return (
        <MDBCol style={{ maxWidth: "22rem" }}>
            <MDBCard testimonial>
            <MDBCardImage className="img-fluid" src={image} alt=" "  waves />
                <MDBCardBody>
                    <MDBCardTitle>{title}</MDBCardTitle>
                    <MDBCardText className="font-italic">" {description} "</MDBCardText>
                </MDBCardBody>
            </MDBCard>
        </MDBCol>
    )
}

export default CardExample;
