import React from 'react';
import { MDBContainer, MDBBtn, MDBIcon, MDBRow, MDBCol } from "mdbreact";
import Questions from './questions';

const Intro = ({ children }) => {
    return (
        <MDBContainer style={{marginTop: "25vh", marginBottom: "50vh"}}>
            <MDBRow >
                <MDBCol md="12" className="white-text text-center">
                    <h2 className="h1-responsive font-weight-bold white-text mb-0 pt-md-5 pt-5">Votre déménagement personnalisé</h2>
                    <hr className="hr-light my-4 w-50" />
                    <h4 className="subtext-header h4-responsive mt-2 mb-4">Suivez votre déménagement en temps réel et en toute simplicité !</h4>
                    <Questions></Questions>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

export default Intro;
