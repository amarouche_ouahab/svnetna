import React, { Component } from "react";
import {
  MDBContainer, MDBNavbar,  MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBFormInline,
  MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBIcon, MDBBtn
} from "mdbreact";
import { Link } from 'gatsby';
import CustomNavLink from './customLink';
import logo from "../images/logo.png";

class NavbarPage extends Component {
  state = {
    isOpen: false
  };

  toggleCollapse = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    return (
      <MDBNavbar color="white" expand="md">
        <MDBContainer>
          <img className="d-block" src={logo} width="75" height="75" alt="First slide" />
          <Link to="/" className="navbar-brand">
            <strong className="ml-3 black-text">Edem'</strong></Link>
          <MDBNavbarToggler name="navbar-toggler" onClick={this.toggleCollapse} />
          <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
            <MDBNavbarNav left>
                <CustomNavLink className="black-text" to="/">Accueil</CustomNavLink>
                <CustomNavLink className="black-text" to="/myspace/">Mon espace</CustomNavLink>
            </MDBNavbarNav>
            <MDBNavbarNav right>
              <div className="d-flex align-items-center">
                <MDBBtn color="yellow" > Se connecter</MDBBtn>
                <MDBBtn color="amber" > S'inscrire</MDBBtn>
              </div>
            </MDBNavbarNav>
          </MDBCollapse>
        </MDBContainer>
      </MDBNavbar>
    );
  }
}

export default NavbarPage;
