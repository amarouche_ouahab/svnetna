import React from 'react';
import { MDBIcon } from 'mdbreact';

function IconDesc({ icon, title, color, description }) {
    return (
        <div>
            <MDBIcon icon={icon} size="3x" className={color} />
            <h5 className="font-weight-bold my-4">{title}</h5>
            <p className="grey-text mb-md-0 mb-5">
                {description}
            </p>
        </div>
    )
}

export default IconDesc;
