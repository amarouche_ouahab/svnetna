import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

// import reducers from '../reducers/rootReducers'
// import { verifyAuth } from '../actions/auth.actions'
import rootReducer from '../reducers/root.reducer'

const reducer = require('../reducers/root.reducer').default
// const createStore = () => reduxCreateStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
// const configureStore = () => {
//   createStore(reducers, applyMiddleware(thunk))
// }
// export default configureStore

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?   
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

export function configureStore(initialState) {
  const store = createStore(
      rootReducer,
      initialState,
      composeEnhancers(applyMiddleware(thunk)),
  );

  if (module.hot) {
      // Enable Webpack hot module replacement for reducers
      module.hot.accept('../reducers/root.reducer', () => {
          const nextRootReducer = reducer
          store.replaceReducer(nextRootReducer)
      });
  }

//   store.dispatch(verifyAuth());

  return store
}