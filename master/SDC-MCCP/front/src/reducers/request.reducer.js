import {
  REQUEST_BEGIN,
  REQUEST_FAILURE,
  REQUEST_SUCCESS,
  RESET_REQUEST_STATE,
} from '../actions/request.actions'

const initialState = {
  isLoading: false,
  error: null,
  items: [],
  doctors: [],
  activities: [],
}

const orders = (state = initialState, action) => {
  switch (action.type) {
      case RESET_REQUEST_STATE:
        return initialState
      case REQUEST_BEGIN:
        return { isLoading: true, error: null, items: [], doctors: [], activities: [], }
      case REQUEST_FAILURE:
        return { ...state, error: action.error, isLoading: false }
      case REQUEST_SUCCESS:
        return { ...state, isLoading: false }
      default:
        return state
  }
}

export default orders