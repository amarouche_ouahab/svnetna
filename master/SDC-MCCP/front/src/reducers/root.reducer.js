// import reducers
import { combineReducers } from 'redux';
import requestReducer from './request.reducer';

const rootReducer = combineReducers({
    requestReducer,
});

export default rootReducer;