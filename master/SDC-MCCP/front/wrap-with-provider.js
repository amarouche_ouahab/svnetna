import React from "react";
import { Provider } from "react-redux";

import { configureStore } from "./src/store/createStore";

const initialState = {}
const store = configureStore(initialState);

export default ({ element }) => <Provider store={store}>{element}</Provider>


