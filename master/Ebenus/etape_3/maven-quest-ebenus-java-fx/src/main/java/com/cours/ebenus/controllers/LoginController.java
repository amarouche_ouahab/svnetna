/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.cours.ebenus.dao.entities.Utilisateur;
import com.cours.ebenus.service.ServiceFacade;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.cours.ebenus.service.IServiceFacade;


/**
 * FXML Controller class
 *
 * @author elhad
 */
public class LoginController implements Initializable {

    private static final Log logger = LogFactory.getLog(LoginController.class);
    private static IServiceFacade serviceFacade = null;

    private Utilisateur connectedUser;

    @FXML
    private TextField identifiant;

    @FXML
    private PasswordField motPasse;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override


    public void initialize(URL url, ResourceBundle rb) {
        serviceFacade = new ServiceFacade();
    }

    public Utilisateur getConnectedUser(){
        return this.connectedUser;
    }

    private void setConnectedUser(Utilisateur user){
        this.connectedUser = user;
    }

    public void authenticate(ActionEvent event) {
        String email = identifiant.getText();
        String password = motPasse.getText();
        if(!email.isEmpty() && !password.isEmpty()){
            Utilisateur user = serviceFacade.getUtilisateurDao().authenticate(email, password);
            System.out.println(user);

            if(user == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Email ou mot de passe incorrect");
                alert.showAndWait();
            }
            else {
                try {
                    setConnectedUser(user);
                    FXMLLoader loadHome = new FXMLLoader(HomeController.class.getClassLoader().getResource("views/home.fxml"));
                    Parent index = loadHome.load();
                    HomeController homeController = loadHome.getController();
                    homeController.setCurrentUser(getConnectedUser());
                    Scene home = new Scene(index);
                    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    stage.setScene(home);
                    stage.show();

                } catch (IOException | NullPointerException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText(e.toString());
                    logger.debug(e.getCause());
                    logger.debug(e.getMessage());
                    e.printStackTrace();
                    alert.showAndWait();
                }
            }
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Veulliez remprlir tous les champs !");
            alert.showAndWait();
        }
    }
}
