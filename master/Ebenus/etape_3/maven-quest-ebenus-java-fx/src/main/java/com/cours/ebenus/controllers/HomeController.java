/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.controllers;

import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.entities.Utilisateur;
import com.cours.ebenus.ihm.utils.Constants;
import com.cours.ebenus.models.UserModel;

import java.io.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.*;
//import javafx.fxml.FXMLLoader;
//import javafx.fxml.Initializable;
//import javafx.scene.Node;
//import javafx.scene.Parent;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
//import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.xml.bind.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.google.gson.*;
import org.json.simple.*;
import org.w3c.dom.Document;

import org.w3c.dom.Element;




//import com.google.gson.JsonParser;

//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Marshaller;

import static com.cours.ebenus.ihm.utils.Constants.APP_PATH;

/**
 * FXML Controller class
 *
 * @author elhad
 */
public class HomeController implements Initializable {

    private static final Log logger = LogFactory.getLog(HomeController.class);
    private static Connection connect = DriverManagerSingleton.getInstance().getConnectionInstance();


    @FXML
    private TableView<UserModel> tableViewUsers;

    @FXML
    private TableColumn<UserModel, Boolean> actionColumn;

    @FXML
    private Label loggedUser;

    private Utilisateur connectedUser;
    private static IServiceFacade serviceFacade = null;
    private static List<Utilisateur> usersList = null;
    private ObservableList<UserModel> observableListUserModel = null;



    public void setCurrentUser(Utilisateur user){
        this.connectedUser = user;
        this.loggedUser.setText( connectedUser.getPrenom() + " " + connectedUser.getNom() + " avec le rôle " + connectedUser.getRole().getIdentifiant() );
    }

    public HomeController() {
        super();
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        serviceFacade = new ServiceFacade();
        usersList = serviceFacade.getUtilisateurDao().findAllUtilisateurs();
        initUserModels();
        initUsersTableView();
    }

    private void initUserModels() {
        List<UserModel> list = new ArrayList<>();
        for (Utilisateur usr : usersList){
            UserModel userModelList = new UserModel();
            userModelList.setIdUtilisateur(usr.getIdUtilisateur());
            userModelList.setCivilite(usr.getCivilite());
            userModelList.setPrenom(usr.getPrenom());
            userModelList.setNom(usr.getNom());
            userModelList.setIdentifiant(usr.getIdentifiant());
            userModelList.setMotPasse(usr.getMotPasse());
            if (usr.getDateNaissance() != null){
                userModelList.setDateNaissance(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(usr.getDateNaissance()));
            } else{
                userModelList.setDateNaissance(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            }
            userModelList.setDateCreation(usr.getDateCreation().toString());
            userModelList.setDateModification(usr.getDateModification().toString());
            userModelList.setRole(usr.getRole().getIdentifiant());
            list.add(userModelList);
        }
        observableListUserModel = FXCollections.observableList(list);
    }

    private void initUsersTableView() {
        actionColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<UserModel, Boolean>, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<UserModel, Boolean> features) {
                return new SimpleBooleanProperty(features.getValue() != null);
            }
        });
        Callback<TableColumn<UserModel, Boolean>, TableCell<UserModel, Boolean>> cellFactory
                = //
                new Callback<TableColumn<UserModel, Boolean>, TableCell<UserModel, Boolean>>() {
                    @Override
                    public TableCell call(final TableColumn<UserModel, Boolean> param) {
                        final TableCell<UserModel, Boolean> cell = new TableCell<UserModel, Boolean>() {
                            InputStream editAsStream = getImage(Constants.APP_PATH + "/src/main/resources/edit.png");
                            Image edit = new Image(editAsStream);
                            final Button updatePersonneBtn = new Button();
                            InputStream deleteAsStream = getImage(Constants.APP_PATH + "/src/main/resources/delete.png");
                            Image delete = new Image(deleteAsStream);
                            final Button deletePersonneBtn = new Button();

                            @Override
                            public void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    updatePersonneBtn.setGraphic(new ImageView(edit));
                                    updatePersonneBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            if (connectedUser.getRole().getIdentifiant().equals("Administrateur")) {
                                                try {
                                                    FXMLLoader loadHome = new FXMLLoader(CrudUserController.class.getClassLoader().getResource("views/addUpdateUser.fxml"));
                                                    Parent parent = loadHome.load();
                                                    CrudUserController crudUser = loadHome.getController();
                                                    crudUser.setUserModelToUpdate((UserModel) getTableRow().getItem());
                                                    Scene home = new Scene(parent);
                                                    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                                    stage.setScene(home);
                                                    stage.show();

                                                } catch (IOException | NullPointerException e) {
                                                    Alert alert = new Alert(Alert.AlertType.ERROR);
                                                    alert.setContentText(e.toString());
                                                    e.printStackTrace();
                                                    alert.showAndWait();
                                                }
                                            }
                                            else {
                                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                                alert.setContentText("Attention vous n’êtes pas autorisé à effectuer cette opération.");
                                                alert.showAndWait();
                                            }
                                        }
                                    });
                                    deletePersonneBtn.setGraphic(new ImageView(delete));
                                    deletePersonneBtn.setOnAction(new EventHandler<ActionEvent>() {
                                        @Override
                                        public void handle(ActionEvent event) {
                                            if (connectedUser.getRole().getIdentifiant().equals("Administrateur")) {
                                                try {
                                                    UserModel user = (UserModel) getTableRow().getItem();
                                                    if (serviceFacade.getUtilisateurDao().findUtilisateurById(user.getIdUtilisateur()).equals(connectedUser))
                                                    {
                                                        Alert alert = new Alert(Alert.AlertType.ERROR);
                                                        alert.setContentText("Attention vous n’êtes pas autorisé à effectuer cette opération, votre session est ouverte.");
                                                        alert.showAndWait();
                                                    }
                                                    else {
                                                        serviceFacade.getUtilisateurDao().deleteUtilisateur(serviceFacade.getUtilisateurDao().findUtilisateurById(user.getIdUtilisateur()));
                                                        FXMLLoader loader = new FXMLLoader(HomeController.class.getClassLoader().getResource("views/home.fxml"));
                                                        Parent homeParent = loader.load();
                                                        HomeController homeController = loader.getController();
                                                        homeController.setCurrentUser(connectedUser);
                                                        Stage currentStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                                        Scene scene = new Scene(homeParent);
                                                        currentStage.setScene(scene);
                                                        currentStage.show();
                                                    }
                                                } catch (IOException | NullPointerException e) {
                                                    Alert alert = new Alert(Alert.AlertType.ERROR);
                                                    alert.setContentText(e.toString());
                                                    e.printStackTrace();
                                                    alert.showAndWait();
                                                }
                                            }
                                            else {
                                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                                alert.setContentText("Attention vous n’êtes pas autorisé à effectuer cette opération.");
                                                alert.showAndWait();
                                            }
                                        }
                                    });
                                    HBox pane = new HBox(updatePersonneBtn, deletePersonneBtn);
                                    setGraphic(pane);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };
        actionColumn.setCellFactory(cellFactory);
        tableViewUsers.setItems(observableListUserModel);
    }

    private InputStream getImage(String path) {
        InputStream imageAsStream = null;
        try {
            imageAsStream = new FileInputStream(new File(path));
        } catch (Exception ex) {
            logger.error("--> Erreur lors de l'execution, Exception: " + ex.getMessage());
        }
        return imageAsStream;
    }

    public void logout(ActionEvent event) {
        try {
            Parent parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/login.fxml"));
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }catch (NullPointerException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.toString());
            e.printStackTrace();
            alert.showAndWait();
        } catch (IOException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.toString());
            e.printStackTrace();
            alert.showAndWait();
        }
    }

    public void addUser(ActionEvent event) {
        try {
            Parent parent = FXMLLoader.load(getClass().getClassLoader().getResource("views/addUpdateUser.fxml"));
//            Scene scene = new Scene(parent);
//            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//            stage.setScene(scene);
            newStage(event,parent).show();
        }
        catch (IOException | NullPointerException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.toString());
            e.printStackTrace();
            alert.showAndWait();
        }
    }

    public void exportCsv(ActionEvent event) {
        String name = "/exportUsers_" + new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss").format(new Date()) + ".csv";
        File file = new File(APP_PATH + name);
        FileWriter writer = null;
        try {
            writer = new FileWriter(file,true);
            for (Utilisateur user: usersList) {
                writer.write( user.getRole().getIdRole() + ";" + user.getCivilite()  + ";" + user.getNom()
                        + ";" + user.getPrenom() + ";" + user.getIdentifiant() + ";" + user.getDateNaissance() + ";" + user.getDateCreation()+ ";" + user.getDateModification()
                        + ";" + user.isActif()+ ";" + user.isMarquerEffacer()+ ";" + user.getVersion()+"\n");
            }
            writer.close();
        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exportXml(ActionEvent event) {
        String name = "/exportUsers_" + new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss").format(new Date()) + ".xml";
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("utilisateurs");
            doc.appendChild(rootElement);

            for (Utilisateur user: usersList) {
               Element newPersonne = this.newPersonne(doc, user);
                rootElement.appendChild( newPersonne);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(APP_PATH + name));
            transformer.transform(source, result);
            System.out.println("File saved!");
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    public void exportJson(ActionEvent event) {
        List<Utilisateur> users = usersList;
        String json = new Gson().toJson(users);
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("users", new JsonParser().parse(json).getAsJsonArray());
        String name = "/exportUsers_" + new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss").format(new Date()) + ".json";
        try {
            PrintWriter file = new PrintWriter(APP_PATH + name);
            file.write(jsonObj.toJSONString());
            file.close();
            System.out.println("ok");
        }
        catch (NullPointerException | IOException e ){
            System.out.println(e.getMessage());
        }
    }

    public void importCsv(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv", "*.csv"));
        File file = fileChooser.showOpenDialog(stage);
        try {
            if (file != null) {
                String path = file.getAbsolutePath();
                String sql = "LOAD DATA INFILE ? INTO TABLE utilisateur FIELDS TERMINATED BY ';' LINES TERMINATED BY '\\n' (idRole, civilite, prenom, nom, identifiant, motPasse, dateNaissance, dateCreation, dateModification, actif, marquerEffacer, version)";
                PreparedStatement stmt = connect.prepareStatement(sql);
                stmt.setString(1, path);
                stmt.executeQuery();
            }
            FXMLLoader loadHome = new FXMLLoader(HomeController.class.getClassLoader().getResource("views/home.fxml"));
            Parent parent = loadHome.load();
            HomeController homeCtr = loadHome.getController();
            homeCtr.setCurrentUser(connectedUser);
            Scene scene = new Scene(parent);
            stage.setScene(scene);
            stage.show();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    public Stage newStage(ActionEvent event, Parent parent) {
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        return  stage;
    }

    public Element newPersonne(Document doc, Utilisateur obj) {
        Element nodePerson = doc.createElement("utilisateur");
        nodePerson.setAttribute("id", String.valueOf(obj.getIdUtilisateur()));

        Element idRole = doc.createElement("idRole");
        idRole.setTextContent(String.valueOf(obj.getRole().getIdRole()));
        nodePerson.appendChild(idRole);

        Element civilite = doc.createElement("civilite");
        civilite.setTextContent(obj.getCivilite());
        nodePerson.appendChild(civilite);

        Element prenom = doc.createElement("prenom");
        prenom.setTextContent(String.valueOf(obj.getPrenom()));
        nodePerson.appendChild(prenom);

        Element nom = doc.createElement("nom");
        nom.setTextContent(String.valueOf(obj.getNom()));
        nodePerson.appendChild(nom);

        Element identifiant = doc.createElement("identifiant");
        identifiant.setTextContent(obj.getIdentifiant());
        nodePerson.appendChild(identifiant);

        Element dateNaissance = doc.createElement("dateNaissance");
        dateNaissance.setTextContent(String.valueOf(obj.getDateNaissance()));
        nodePerson.appendChild(dateNaissance);

        Element dateCreation = doc.createElement("dateCreation");
        dateCreation.setTextContent(String.valueOf(obj.getDateCreation()));
        nodePerson.appendChild(dateCreation);

        Element version = doc.createElement("version");
        version.setTextContent(String.valueOf(obj.getVersion()));
        nodePerson.appendChild(version);

        return nodePerson;
    }
}
