package com.cours.ebenus.controllers;

import com.cours.ebenus.dao.entities.Role;
import com.cours.ebenus.dao.entities.Utilisateur;
import com.cours.ebenus.models.UserModel;

import java.io.IOException;
import java.net.URL;
import java.time.*;
//import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;

import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.*;
//import javafx.scene.Node;
//import javafx.scene.Parent;
//import javafx.scene.Scene;
//import javafx.scene.control.*;
import javafx.stage.Stage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CrudUserController implements Initializable {

    private static final Log logger = LogFactory.getLog(CrudUserController.class);

    @FXML
    private ComboBox civilite;

    @FXML
    private TextField prenom;

    @FXML
    private TextField nom;

    @FXML
    private TextField identifiant;

    @FXML
    private PasswordField motPasse;

    @FXML
    private DatePicker dateNaissance;

    @FXML
    private ComboBox role;

    private UserModel userModelToUpdate = null;

//    private Utilisateur user;
    private IServiceFacade serviceFacade = null;

    private Utilisateur utilisateur;
    //private IServiceFacade serviceFacade = null;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        serviceFacade = new ServiceFacade();
        role.getItems().clear();
        for (Role my_role : serviceFacade.getRoleDao().findAllRoles()) {
            role.getItems().add(my_role.getIdentifiant());
        }
        civilite.getItems().clear();
        civilite.getItems().addAll("Mr", "Mme");

    }

    public void addUpdateUtilisateur(ActionEvent event) {
        try {
            logger.debug("this role get selecteditem " + this.role.getSelectionModel().getSelectedItem());
            if ((this.nom.getText() == null || this.nom.getText().length() == 0) ||
                    (this.prenom.getText() == null || this.prenom.getText().length() == 0) ||
                    (this.identifiant.getText() == null || this.identifiant.getText().length() == 0) ||
                    (this.motPasse.getText() == null || this.motPasse.getText().length() == 0) ||
                    (this.civilite.getSelectionModel().getSelectedItem() == null) ||
                    (this.role.getSelectionModel().getSelectedItem() == null)) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Veuillez remplir tous les champs.");
                alert.showAndWait();
            } else {
                if (userModelToUpdate != null)
                    setUser(serviceFacade.getUtilisateurDao().findUtilisateurById(this.userModelToUpdate.getIdUtilisateur()));
                else {
                    setUser(new Utilisateur());
                    utilisateur.setMotPasse(this.motPasse.getText());
                }
                utilisateur.setCivilite(this.civilite.getSelectionModel().getSelectedItem().toString());
                utilisateur.setPrenom(this.prenom.getText());
                utilisateur.setNom(this.nom.getText());
                utilisateur.setIdentifiant(this.identifiant.getText());
                if (utilisateur.getDateNaissance() != null) {
                    LocalDate ld = this.dateNaissance.getValue();
                    Date date = Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
                    utilisateur.setDateNaissance(date);
                } else{
                    utilisateur.setDateNaissance(new Date(System.currentTimeMillis()));
                }
                utilisateur.setRole(serviceFacade.getRoleDao().findRoleByIdentifiant(this.role.getSelectionModel().getSelectedItem().toString()).get(0));
                if (userModelToUpdate != null){
                    serviceFacade.getUtilisateurDao().updateUtilisateur(utilisateur);
                } else{
                    serviceFacade.getUtilisateurDao().createUtilisateur(utilisateur);
                }
                Parent root = FXMLLoader.load(HomeController.class.getClassLoader().getResource("views/home.fxml"));
                Scene scene = new Scene(root);
                Stage currentStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                currentStage.setScene(scene);
                currentStage.show();
            }
        }
        catch(IOException | NullPointerException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.toString());
            e.printStackTrace();
            alert.showAndWait();
        }
    }

    /*public void setUtilisateur(Utilisateur utilisateur) {
     this.utilisateur = utilisateur;
     }*/
    public void setUserModelToUpdate(UserModel userModelToUpdate) {
        try {
            this.userModelToUpdate = userModelToUpdate;
            this.nom.setText(userModelToUpdate.getNom());
            this.prenom.setText(userModelToUpdate.getPrenom());
            this.identifiant.setText(userModelToUpdate.getIdentifiant());
            this.motPasse.setText(userModelToUpdate.getMotPasse());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDate date = LocalDate.parse(userModelToUpdate.getDateNaissance(), formatter);
            this.dateNaissance.setValue(date);
            civilite.getSelectionModel().select(userModelToUpdate.getCivilite());
            role.getSelectionModel().select(userModelToUpdate.getRole());

        } catch(NullPointerException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.toString());
            e.printStackTrace();
            alert.showAndWait();
        }
    }

    public void setUser(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
}
