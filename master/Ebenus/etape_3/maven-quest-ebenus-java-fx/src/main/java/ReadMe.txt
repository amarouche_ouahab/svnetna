Reponse question 1 :

Un client lourd est un logiciel qui propose des fonctionnalités complexes avec un traitement autonome, installé sur les ordinateurs des utilisateurs. La notion de client s'entend dans une architecture client-serveur. Et contrairement au client léger,
le client lourd ne dépend du serveur que pour l'échange des données dont il prend généralement en charge l'intégralité du traitement.

Reponse question 2 :

Les avantages:
Les clients lourds proposent une interface riche donc plus intuitive pour les utilisateurs, plus performante et plus réactive aussi.
Ils utilisent au mieux toutes les ressources de la machine où ils sont installés.
Dans la majorité des cas, pas besoin de connexion externe, l'application peut tourner en monoposte.

Les inconvénients:
Déploiement et les versions:
étant installée sur chacun des postes clients, le déploiement d’une solution de type client lourd est un projet en soi qui peut devenir très complexe lorsque l’on fait face à un parc informatique hétérogène.
Par la suite, à chaque montée de version il est nécessaire de ré-déployer la nouvelle version sur chacun des postes.
Compatibilité :
étant compilé pour une architecture processeur et faisant appel à des fonctionnalités au niveau OS,
une solution de type client lourd offre une compatibilité restreinte à une configuration matérielle et un système d’exploitation donné

Reponse question 3 :

Polymorphisme vient du grec et signifie qui peut prendre plusieurs formes, Cette caractéristique est un des concepts essentiels de la programmation orientée objet.
Alors que l'héritage concerne les classes (et leur hiérarchie),le polymorphisme est relatif aux méthodes des objets
Pour simplifier, le polymorphisme permet au développeur d'utiliser une méthode ou un attribute selon plusieurs manières,
en fonction du besoin. D'ailleurs, le mot polymorpsihm est apparu dans la Grèce antique. Il signifie quelque chose qui peut prendre plusieurs formes.

Exemples d'application concrètes de cette notion :
Une classe mère "FormeGeometrique" et une méthode "getPerimetre" abstraite
Les classes filles "cercle" et "triange" doivent redéfinir la méthode car le calcul n'est pas le même.






