/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.impl;

import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.IUtilisateurDao;
import com.cours.ebenus.dao.entities.Role;
import com.cours.ebenus.dao.entities.Utilisateur;

import java.util.Date;
import java.util.List;
import java.sql.*;
import java.util.ArrayList;

import com.cours.ebenus.dao.exception.EbenusException;
import com.cours.ebenus.utils.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static com.cours.ebenus.dao.ConnectionHelper.closeSqlResources;

/**
 *
 * @author ElHadji
 */
public class UtilisateurDao /*extends AbstractDao<Utilisateur>*/ implements IUtilisateurDao {

    private static final Log log = LogFactory.getLog(UtilisateurDao.class);
    private static Connection connect = DriverManagerSingleton.getInstance().getConnectionInstance();
    //public UtilisateurDao() {
    //    super(Utilisateur.class);
    //}
    @Override
    public List<Utilisateur> findAllUtilisateurs() {
        try {
            List<Utilisateur> users = new ArrayList<Utilisateur>();
            Statement stmt = connect.createStatement();
            String sql = "SELECT *, u.idRole as idRole_user, r.idRole as idRole_role, u.identifiant as user_identifiant," +
                    " r.identifiant as role_identifiant, u.version as user_version, r.version as role_version FROM utilisateur u JOIN role r ON u.idRole = r.idRole";
            ResultSet userDb = stmt.executeQuery(sql);
            while(userDb.next()){
                Utilisateur user = getUserDataBase(userDb);
                users.add(user);
            }
            userDb.close();
            stmt.close();
            return users;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Utilisateur findUtilisateurById(int idUtilisateur) {
        try {
            String sql = "SELECT *, u.idRole as idRole_user, r.idRole as idRole_role, u.identifiant as user_identifiant, r.identifiant as role_identifiant, u.version as user_version, r.version as role_version FROM utilisateur u join role r ON u.idRole = r.idRole WHERE idUtilisateur = ? ";
            PreparedStatement stmt = connect.prepareStatement(sql);
            stmt.setInt(1, idUtilisateur);
            ResultSet userDb = stmt.executeQuery();
            while(userDb.next()){
                Utilisateur user = getUserDataBase(userDb);
                closeSqlResources(stmt, userDb);
                return user;
            }
            return null;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    @Override
    public List<Utilisateur> findUtilisateursByPrenom(String prenom) {
        return getUserByColumn("prenom", prenom);
    }

    @Override
    public List<Utilisateur> findUtilisateursByNom(String nom) {
        return getUserByColumn("nom", nom);
    }

    @Override
    public List<Utilisateur> findUtilisateurByIdentifiant(String identifiant) {
        return getUserByColumn("u.identifiant", identifiant);
    }


    @Override
    public List<Utilisateur> findUtilisateursByIdRole(int idRole) {
        try {
            List<Utilisateur> users = new ArrayList<Utilisateur>();
            PreparedStatement stmt = connect.prepareStatement("SELECT *, u.idRole as idRole_user, r.idRole as idRole_role, u.identifiant as user_identifiant, r.identifiant as role_identifiant, u.version as user_version, r.version as role_version FROM utilisateur u" +
                    " JOIN role r ON  r.idRole = u.idRole AND r.idRole = ?");
            stmt.setInt(1, idRole);
            ResultSet userDB = stmt.executeQuery();
            while(userDB.next()){
                Utilisateur user = getUserDataBase(userDB);
                users.add(user);
            }
            closeSqlResources(stmt, userDB);
            return users;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Utilisateur> findUtilisateursByIdentifiantRole(String identifiantRole) {
        try {
            List<Utilisateur> users = new ArrayList<Utilisateur>();
            PreparedStatement stmt = connect.prepareStatement("SELECT *, u.idRole as idRole_user, r.idRole as idRole_role, u.identifiant as user_identifiant, r.identifiant as role_identifiant, u.version as user_version," +
                    " r.version as role_version FROM utilisateur u JOIN role r ON r.idRole = u.idRole AND r.identifiant = ? ");
            stmt.setString(1, identifiantRole);
            ResultSet userDB = stmt.executeQuery();
            while(userDB.next()){
                Utilisateur user = getUserDataBase(userDB);
                users.add(user);
            }
            closeSqlResources(stmt, userDB);
            return users;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Utilisateur createUtilisateur(Utilisateur user) {
        try {
            if (!findUtilisateurByIdentifiant(user.getIdentifiant()).isEmpty()){
                throw new EbenusException(Constants.EXCEPTION_CODE_USER_ALREADY_EXIST);
            }
            int idUser = newUserId();
            user.setIdUtilisateur(idUser);
            PreparedStatement stmt = connect.prepareStatement("INSERT INTO utilisateur(idRole, civilite, prenom, nom, identifiant, motPasse,dateNaissance," +
                    "dateCreation, dateModification, actif, marquerEffacer, version) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            stmt.setString(2, user.getCivilite());
            stmt.setString(3, user.getPrenom());
            stmt.setString(4, user.getNom());
            stmt.setString(5, user.getIdentifiant());
            stmt.setString(6, user.getMotPasse());
            if (user.getDateNaissance() != null){
                stmt.setDate(7, new java.sql.Date(user.getDateNaissance().getTime()));
            }
            else{
                stmt.setDate(7, new java.sql.Date(new Date(System.currentTimeMillis()).getTime()));
            }
            stmt.setDate(8, new java.sql.Date(new Date(System.currentTimeMillis()).getTime()));
            stmt.setDate(9, new java.sql.Date(new Date(System.currentTimeMillis()).getTime()));
            stmt.setBoolean(10, user.isActif());
            stmt.setBoolean(11, user.isMarquerEffacer());
            stmt.setInt(12, 1);
            if (user.getRole() != null){
                stmt.setInt(1, user.getRole().getIdRole());
            }
            else{
                stmt.setInt(1, 3);
            }
            user.setDateCreation(new Date(System.currentTimeMillis()));
            user.setDateModification(new Date(System.currentTimeMillis()));
            stmt.executeUpdate();
            return user;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Utilisateur updateUtilisateur(Utilisateur user) {
        try {
            PreparedStatement stmt = connect.prepareStatement("UPDATE utilisateur SET idRole = ? , civilite = ? ,prenom = ? , nom = ? , identifiant = ? , motPasse = ? ," +
                    " dateNaissance = ? , dateModification = ? , actif = ? , marquerEffacer = ? , version = ? WHERE idUtilisateur = ? ");
            stmt.setInt(12, user.getIdUtilisateur());
            stmt.setString(2, user.getCivilite());
            stmt.setString(3, user.getPrenom());
            stmt.setString(4, user.getNom());
            stmt.setString(5, user.getIdentifiant());
            stmt.setString(6, user.getMotPasse());
            if (user.getDateNaissance() != null){
                stmt.setDate(7, new java.sql.Date(user.getDateNaissance().getTime()));
            } else{
                stmt.setDate(7, new java.sql.Date(new Date(System.currentTimeMillis()).getTime()));
            }
            stmt.setDate(8, new java.sql.Date(new Date(System.currentTimeMillis()).getTime()));
            stmt.setBoolean(9, user.isActif());
            stmt.setBoolean(10, user.isMarquerEffacer());
            stmt.setInt(11, user.getVersion()+1);
            if (user.getRole() != null){
                stmt.setInt(1, user.getRole().getIdRole());
            } else{
                stmt.setInt(1, 3);
            }
            stmt.executeUpdate();
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteUtilisateur(Utilisateur user) {
        try {
            PreparedStatement stmt = connect.prepareStatement("DELETE FROM utilisateur where idUtilisateur = ?");
            stmt.setInt(1, user.getIdUtilisateur());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Méthode qui vérifie les logs email / password d'un utilisateur dans la
     * base de données
     *
     * @param email L'email de l'utilisateur
     * @param password Le password de l'utilisateur
     * @return L'utilisateur qui tente de se logger si trouvé, null sinon
     */
    @Override
    public Utilisateur authenticate(String email, String password) {
        try {
            String sql = "SELECT *, u.idRole as idRole_user, r.idRole as idRole_role, u.identifiant as user_identifiant, r.identifiant as role_identifiant, u.version as user_version, r.version as role_version FROM utilisateur u JOIN role r ON r.idRole = u.idRole WHERE u.identifiant = ? AND u.motPasse= ?";
            PreparedStatement stmt = connect.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.setString(2, password);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                Utilisateur user = getUserDataBase(rs);
                closeSqlResources(stmt, rs);
                return user;
            }
            return null;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        }


    private Utilisateur getUserDataBase(ResultSet userDb){
        try {
            Utilisateur user = new Utilisateur();
            user.setIdUtilisateur(userDb.getInt("idUtilisateur"));
            user.setCivilite(userDb.getString("civilite"));
            user.setPrenom(userDb.getString("prenom"));
            user.setNom(userDb.getString("nom"));
            user.setIdentifiant(userDb.getString("user_identifiant"));
            user.setMotPasse(userDb.getString("motPasse"));
            user.setDateModification(userDb.getDate("dateNaissance"));
            user.setDateCreation(userDb.getDate("dateCreation"));
            user.setDateModification(userDb.getDate("dateModification"));
            user.setActif(userDb.getBoolean("actif"));
            user.setMarquerEffacer(userDb.getBoolean("marquerEffacer"));
            user.setVersion(userDb.getInt("user_version"));
            Role role = new Role();
            role.setIdRole(userDb.getInt("idRole_role"));
            role.setDescription(userDb.getString("description"));
            role.setIdentifiant(userDb.getString("role_identifiant"));
            role.setVersion(userDb.getInt("role_version"));
            user.setRole(role);
            return user;
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Utilisateur> getUserByColumn(String table, String column){
        try {
            List<Utilisateur> users = new ArrayList<Utilisateur>();
            String sql = "SELECT *, u.idRole as idRole_user, r.idRole as idRole_role, u.identifiant as user_identifiant, " +
                    "r.identifiant as role_identifiant, u.version as user_version, r.version as role_version FROM utilisateur u JOIN role r ON u.idRole = r.idRole AND " + table + " = ?";
            PreparedStatement stmt = connect.prepareStatement(sql);
            stmt.setString(1, column);
            ResultSet userDb = stmt.executeQuery();
            while(userDb.next()){
                Utilisateur user = getUserDataBase(userDb);
                users.add(user);
            }
            closeSqlResources(stmt, userDb);
            return users;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    private int newUserId(){
        try {
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT idUtilisateur FROM utilisateur ORDER BY idUtilisateur DESC LIMIT 0, 1");
            while(rs.next()){
                return rs.getInt("idUtilisateur")+1;
            }
            return 1;
        } catch (SQLException se){
            return 0;
        }
    }
}
