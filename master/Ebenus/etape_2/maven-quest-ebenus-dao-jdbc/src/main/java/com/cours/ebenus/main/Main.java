/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.main;

import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.entities.Role;
import com.cours.ebenus.dao.entities.Utilisateur;
import com.cours.ebenus.dao.impl.RoleDao;
import com.cours.ebenus.dao.impl.UtilisateurDao;
import com.cours.ebenus.factory.AbstractDaoFactory;
import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.*;
import java.util.List;

import static com.cours.ebenus.factory.AbstractDaoFactory.FactoryDaoType.JDBC_DAO_FACTORY;

/**
 *
 * @author elhad
 */
public class Main {

    private static final Log log = LogFactory.getLog(Main.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        ResultSet resultats = null;
//        Connection con = DriverManagerSingleton.getConnectionInstance();
//        try {
//            Statement stmt = con.createStatement();
//            resultats = stmt.executeQuery("SELECT * FROM Utilisateur");
////            System.out.println(résultats.);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        try {
//            ResultSetMetaData rsmd = resultats.getMetaData();
//            int nbCols = rsmd.getColumnCount();
//            while (resultats.next()) {
//                for (int i = 1; i <= nbCols; i++)
//                System.out.print(resultats.getString(i) + " ");
//                System.out.println();
//            }
//            resultats.close();
//        } catch (SQLException e) {
//        }
//        AbstractDaoFactory t = AbstractDaoFactory.getFactory(JDBC_DAO_FACTORY);
//        IServiceFacade serviceFacade = new ServiceFacade();
//        List user = serviceFacade.getUtilisateurDao().findAllUtilisateurs();

        UtilisateurDao userdao = new UtilisateurDao();
        RoleDao roledao = new RoleDao();
        List<Utilisateur> users = userdao.findAllUtilisateurs();
//        System.out.println("get users ");
//        for(Utilisateur user : users){
//            System.out.println(user);
//        }
//        System.out.println("id : " + userdao.findUtilisateurById(1));
//        System.out.println(" prenom: " + userdao.findUtilisateursByPrenom("Nicolas"));
//        System.out.println("user by role : " + userdao.findUtilisateursByIdentifiantRole("Administrateur"));
//        System.out.println("user by role id : " + userdao.findUtilisateursByIdRole(1));
//        Utilisateur user = new Utilisateur("Mr", "achou", "amarouche", "ouahab@gmail.com", "pass", null);
        Utilisateur user = new Utilisateur("Mr", "achou", "amarouche", "ouahab@gmail.com", "pass", null);
        user = userdao.createUtilisateur(user);
//        Utilisateur user = userdao.findUtilisateurById(21);
        user.setNom("ddddd");
        userdao.updateUtilisateur(user);
        System.out.println(user);
        // TODO code application logic here
//        List<Role> roles = roledao.findAllRoles();
////        for(Role role: roles){
////            System.out.println(role);
////        }
////        System.out.println("by id" + roledao.findRoleById(1));
////        System.out.println("by ident : " + roledao.findRoleByIdentifiant("Vendeur"));
//        Role role = new Role("okok", "okoko");
//        role = roledao.createRole(role);
//        role.setDescription("coucou");
//        System.out.println(roledao.updateRole(role));
//        System.out.println(role);
    }
}
