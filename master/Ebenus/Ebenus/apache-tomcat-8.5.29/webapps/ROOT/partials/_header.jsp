<%@ page import="com.cours.dao.entities.Utilisateur" %>
<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<div class="header-outer" id="header-outer">
    <header id="header"  class="header">
        <div class="header">
            <a href="../pages/index.html" title="Ebenus Commerce" class="logo">
                <img src="${pageContext.request.contextPath}/images/logo.png" alt="Ebenus Commerce">
            </a>
            <ul class="main-menu">
                <li><a href="${pagecontext.request.contextpath}/HomeServlet">Accueil</a></li>
                <li><a href="${pagecontext.request.contextpath}/ProductServlet">Produits</a></li>
                <li><a href="../pages/qui-sommes-nous.html">Qui sommes-nous</a></li>
                <li><a href="../pages/contact.html">Contactez-nous</a></li>
                <li>
                    <a href="#" class="account-link">Mon Compte</a>
                    <ul class="sub-menu">
                        <% if (user != null) { %>
                            <li><a href="${pageContext.request.contextPath}/UserInfo">Mon Compte</a></li>
                            <li class="guest">
                                <a href="${pageContext.request.contextPath}/Logout">Logout</a>
                            </li>
                        <% } else { %>
                            <li class="guest">
                                <a href="${pageContext.request.contextPath}/Login">Login</a>
                            </li>
                            <li><a href="${pageContext.request.contextPath}/SignUp">Créer Compte</a></li>
                        <% } %>
                        <li><a href="../pages/panier.html">Panier</a></li>
                    </ul>
                </li>
            </ul>
            <div class="phone-block">
                <i class="icon-phone"></i>
                Appelez nous<br>
                <b>+33 5678 890</b>
            </div>
            <div class="search-area">
                <a href="javascript:void(0)" class="search-icon">
                    <!-- <i class="fa fa-search"></i> -->
                    <i class="icon-search"></i>
                </a>
                <form id="search_mini_form" action="" method="get">
                    <div class="form-search">
                        <input id="search" placeholder="Rechercher un produit" type="text" name="q" class="input-text" autocomplete="off">
                        <button type="submit" title="Search" class="button"><i class="icon-search"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </header>
</div>
