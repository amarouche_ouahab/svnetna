

/**================================================
 JS : MY CUSTOM SCRIPTS
 ===================================================*/

(function($){
    $(function() {
        /**================================================*
         Search bar toggle display
         */
        $('a.search-icon').on('click',function(){
            $(this).next('#search_mini_form').toggleClass('show');
        })
        /**================================================*
         Slider home page
         */
        var owl = $("#slider");
        owl.owlCarousel({
            animateOut: 'fadeOut',
            items:1,
            nav : true,
            navElement : 'i',
            navClass : ['icon-chevron-left owl-prev','icon-chevron-right owl-next'],
            navText : ['',''],
            loop : true,
            autoplay : false,
            smartSpeed:450
        });
        /**================================================*
         Sticky responsive menu
         */
// When the user scrolls the page, execute myFunction 
        window.onscroll = function() {myFunction()};

// Get the navbar
        var navbar = document.getElementById("header-outer");
        console.log(window.pageYOffset)
// Get the offset position of the navbar
        var sticky = navbar.offsetTop+50;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
        function myFunction() {
            if (window.pageYOffset >= sticky) {
                navbar.classList.add("sticky")
            } else {
                navbar.classList.remove("sticky");
            }
        }
        /**================================================*
         Cart Inc Dec Buttons
         */
        $(".qty-holder .qty-changer > a").on("click", function() {

            var $button = $(this);
            var oldValue = $button.parent().parent().find($("input")).val();

            if ($button.find($("em")).text() == "+") {
                var newVal = parseFloat(oldValue) + 1;
                console.log(newVal)
            } else {
                // Don't allow decrementing below zero
                if (oldValue > 0) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 0;
                }
            }

            $button.parent().parent().find($("input")).val(newVal);

        });

        $('.main-menu').slicknav({
            appendTo : $('.header-outer'),
            label : ""
        });

        /**================================================*
         Create Accaunt, toggle dispplay adresse
         */

        jQuery("#shippingAdress").bind('change', function(){
            val = this.checked; //<---
            if(val){
                $('.adress-box-shipBill').css('display', 'none');
                $('.adress-box-shipping').css('display', 'block');
            }else{
                $('.adress-box-shipping').css('display', 'none');
            }
        });

        jQuery("#billAdress").bind('change', function(){
            val = this.checked; //<---
            if(val){
                $('.adress-box-shipBill').css('display', 'none');
                $('.adress-box-billing').css('display', 'block');
            }else{
                $('.adress-box-billing').css('display', 'none');
            }
        });

        jQuery("#BillShip").bind('change', function(){
            val = this.checked; //<---
            if(val){
                $('.adress-box-billing').css('display', 'none');
                $('.adress-box-shipping').css('display', 'none');
                $('.adress-box-shipBill').css('display', 'block');
            }else{
                $('.adress-box-shipBill').css('display', 'none');
            }
        });


        $(".chb1").change(function() {
            var checked = $(this).is(':checked');
            $(".chb1").prop('checked',false);
            if(checked) {
                $(this).prop('checked',true);
            }
        });

        $(".chb2").change(function() {
            var checked = $(this).is(':checked');
            $(".chb2").prop('checked',false);
            if(checked) {
                $(this).prop('checked',true);
            }
        });


        $('[data-toggle="datepicker"]').datepicker({
            language: 'fr-FR'
        });


        $('.fa-trash,.fa-trash-o').each(function(index, el) {
            $(this).click(function(event) {
                event.preventDefault();
                $(this).closest('tr').remove();
                $(this).closest('.product').remove();

            });
        });

        var path = window.location.href;
        $('.facet-navigation ul li').find('a').each(function() {
            if (this.href === path) {
                $(this).parent().addClass('active');
            }
        });

// $("#select-adress-ship").change(function() {
//     $(".shipAddr").css('display', 'block');
//     getval( this );

// });

        $("#select-adress-ship").on('change', function() {
            $(".ship_adresse").css('display', 'none');
            console.log($("select[name='select-adress']"));
            console.log( $(this).find(":selected").val() );
            $("#"+$(this).find(":selected").val()).css('display', 'block');
        });

        /**================================================*
         Cart Inc Dec Buttons
         */

    });
})(jQuery);

