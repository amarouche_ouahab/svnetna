<%@page pageEncoding="UTF-8" %>
<%@page import="com.cours.dao.entities.Utilisateur" %>

<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<% if (request.getAttribute("javax.servlet.forward.request_uri") == null) { %>
<% response.sendRedirect("/frontOffice/UserInfoServlet"); %>
<% } else if (user == null) { %>
<% response.sendRedirect("/backOffice/LoginServlet"); %>
<% } %>
<jsp:include page="./template.jsp">
    <jsp:param name="content" value="user_info_content"/>
    <jsp:param name="title" value="Information personnelles"/>
</jsp:include>

