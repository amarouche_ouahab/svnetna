<section>
    <div class="content">
        <div class="account-create">
            <h1>Créer un compte</h1>
            <div class="common-form-controls">
                <form id="form-validate" class="row no-gutters common-form-controls" action="${pageContext.request.contextPath}/SignUp" method="post">
                    <fieldset class="col-md-6">
                        <h2>Informations personnelles</h2>
                        <label for="firstname">Prénom</label>
                        <input name="firstname" id="firstname" type="text">
                        <label for="lastname">Nom</label>
                        <input name="lastname" id="lastname" type="text">
                        <label for="email">Identifiant</label>
                        <input name="email" id="email" type="email">
                        <label for="password">Mot de passe</label>
                        <input name="password" id="password" type="password">
                        <label for="password">Confimer mot de psse</label>
                        <input name="password" id="password-C" type="password">
                        <label>Civilité</label>
                        <div class="gender">
                            <input type="radio" id="male" value="Mr" name="sex" checked="checked"/>
                            <label for="male">
                                <i class="fa fa-male" aria-hidden="true"></i>
                            </label>
                            <input type="radio" id="female" value="Mme" name="sex"/>
                            <label for="female">
                                <i class="fa fa-female" aria-hidden="true"></i>
                            </label>
                        </div>
                        <label> Date de naissance</label>
                        <div class="birth-day justify-content-between">
                            <div class="sel-container">
                                <div class="sel">
                                    <select name="select-day" id="select-day">
                                        <option value="Jour" disabled>Jour</option>
                                        <option value=" 1 "> 1 </option>
                                        <option value=" 2 "> 2 </option>
                                        <option value=" 3 "> 3 </option>
                                        <option value=" 4 "> 4 </option>
                                        <option value=" 5 "> 5 </option>
                                        <option value=" 6 "> 6 </option>
                                        <option value=" 7 "> 7 </option>
                                        <option value=" 8 "> 8 </option>
                                        <option value=" 9 "> 9 </option>
                                        <option value="10 ">10 </option>
                                        <option value="11 ">11 </option>
                                        <option value="12 ">12 </option>
                                        <option value="13 ">13 </option>
                                        <option value="14 ">14 </option>
                                        <option value="15 ">15 </option>
                                        <option value="16 ">16 </option>
                                        <option value="17 ">17 </option>
                                        <option value="18 ">18 </option>
                                        <option value="19 ">19 </option>
                                        <option value="20 ">20 </option>
                                        <option value="21 ">21 </option>
                                        <option value="22 ">22 </option>
                                        <option value="23 ">23 </option>
                                        <option value="24 ">24 </option>
                                        <option value="25 ">25 </option>
                                        <option value="26 ">26 </option>
                                        <option value="27 ">27 </option>
                                        <option value="28 ">28 </option>
                                        <option value="29 ">29 </option>
                                        <option value="30 ">30 </option>
                                        <option value="31 ">31 </option>

                                    </select>
                                </div>
                            </div>
                            <div class="sel-container">
                                <div class="sel">
                                    <select name="select-month" id="select-month">
                                        <option value="Mois" disabled>Mois</option>
                                        <option value="Janvier ">Janvier </option>
                                        <option value="Février ">Février </option>
                                        <option value="Mars ">Mars </option>
                                        <option value="Avril ">Avril </option>
                                        <option value="Mai ">Mai </option>
                                        <option value="Juin ">Juin </option>
                                        <option value="Juillet ">Juillet </option>
                                        <option value="Aout ">Aout </option>
                                        <option value="Septembre ">Septembre </option>
                                        <option value="Octobre ">Octobre </option>
                                        <option value="Novembre ">Novembre </option>
                                        <option value="Décembre ">Décembre </option>

                                    </select>
                                </div>
                            </div>
                            <div class="sel-container">
                                <div class="sel">
                                    <select name="select-year" id="select-year">
                                        <option value="Année" disabled>Année</option>
                                        <option value="1990 ">1990 </option>
                                        <option value="1989 ">1989 </option>
                                        <option value="1988 ">1988 </option>
                                        <option value="1987 ">1987 </option>
                                        <option value="1986 ">1986 </option>
                                        <option value="1985 ">1985 </option>
                                        <option value="1984 ">1984 </option>
                                        <option value="1983 ">1983 </option>
                                        <option value="1982 ">1982 </option>
                                        <option value="1981 ">1981 </option>
                                        <option value="1980 ">1980 </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="col-md-6">
                        <h2>Adresse</h2>
                        <div class="radio-box">
                            <span>Ceci est mon :</span>
                            <form action="#">
                                <p>
                                    <input class="chb1" type="checkbox" id="shippingAdress" name="shippingAdress" checked>
                                    <label for="shippingAdress">Adresse de livraison</label>
                                </p>
                                <p>
                                    <input class="chb2" type="checkbox" id="billAdress" name="billAdress">
                                    <label for="billAdress">Adresse de facturation</label>
                                </p>
                                <p>
                                    <input class="chb1 chb2" type="checkbox" id="BillShip" name="BillShip">
                                    <label for="BillShip">Adresse de livraison et de facturation</label>
                                </p>
                            </form>
                        </div>
                        <div class="adress-box-shipping">
                            <h2>Adresse de livraison</h2>
                            <label for="street-ship">Rue</label>
                            <input name="streetShip" id="street-ship" type="text">
                            <label for="postalcodeShip">Code Postale</label>
                            <input name="postalcodeShip" id="postalcodeShip" type="text">
                            <label for="cityShip">Ville</label>
                            <input name="cityShip" id="cityShip" type="text">
                            <label for="countryShip">Pays</label>
                            <input name="countryShip" id="countryShip" type="text">
                        </div>
                        <div class="adress-box-billing">
                            <h2>Adresse de facturation</h2>
                            <label for="streetBill">Rue</label>
                            <input name="streetBill" id="streetBill" type="text">
                            <label for="postalcodeBill">Code Postale</label>
                            <input name="postalcodeBill" id="postalcodeBill" type="text">
                            <label for="cityBill">Ville</label>
                            <input name="cityBill" id="cityBill" type="text">
                            <label for="countryBill">Pays</label>
                            <input name="countryBill" id="countryBill" type="text">
                        </div>

                        <div class="adress-box-shipBill">
                            <h2>Adresse de livraison et de factuartion</h2>
                            <label for="street-BillShip">Rue</label>
                            <input name="streetBillShip" id="street-BillShip" type="text">
                            <label for="postalcodeBillShip">Code Postale</label>
                            <input name="postalcodeBillShip" id="postalcodeBillShip" type="text">
                            <label for="cityBillShip">Ville</label>
                            <input name="cityBillShip" id="cityBillShip" type="text">
                            <label for="countryBillShip">Pays</label>
                            <input name="countryBillShip" id="countryBillShip" type="text">
                        </div>
                        <button type="submit" class="btn btn-default common-form-controls">S'enregistrer</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</section>
