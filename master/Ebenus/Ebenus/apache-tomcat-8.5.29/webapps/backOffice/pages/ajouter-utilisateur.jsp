<%@ page import="com.cours.ebenus.dao.entities.Role" %>
<%@ page import="com.cours.ebenus.dao.entities.Utilisateur" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ page pageEncoding="UTF-8" %>

<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<% if (request.getAttribute("javax.servlet.forward.request_uri") == null) { %>
<% response.sendRedirect("/backOffice/UserServlet"); %>
<% } else if (user == null) { %>
<% response.sendRedirect("/backOffice/LoginServlet"); %>
<% } %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/images/favicon.png">
  <title>Ebenus Admin</title>
  <!-- Custom Fonts -->
  <!-- <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/master.css">
  
</head>
<body>
  <div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="DashboardServlet">Ebenus Admin</a>
      </div>
      <!-- Top Menu Items -->
      <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-user"></i>
                <% if (user != null) { %>
                <%= String.format(Locale.FRANCE, "%s %s", user.getPrenom(), user.getNom()) %>
                <% } %>
                <b class="caret"></b>
            </a>
          <ul class="dropdown-menu">
            <li>
                <a href="DashboardServlet?logout=true"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
            </li>
          </ul>
        </li>
      </ul>
      <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
          <li>
            <a href="DashboardServlet"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
          </li>
          <li class="active">
            <a href="UserServlet"><i class="fa fa-users"></i> Utilisateurs</a>
          </li>
          <li>
            <a href="RoleServlet"><i class="fa fa-user"></i> Rôles</a>
          </li>
          <li>
            <a href="produits.html"><i class="fa fa-money"></i> Produits</a>
          </li>
          <li>
            <a href="commandes.html"><i class="fa fa-shopping-cart"></i> Commandes</a>
          </li>
        </ul>
      </div>
    <!-- /.navbar-collapse -->
    </nav>
    <div id="page-wrapper">
      <div class="container-fluid">
        
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              Nouveau Utilisateur
            </h1>
              <ol class="breadcrumb">
              <li>
                <i class="fa fa-dashboard"></i>  <a href="DashboardServlet">Dashboard</a>
              </li>
              <li class="active">
                <i class="fa fa-edit"></i> Nouveau Utilisateur
              </li>
            </ol>
          </div>
        </div>
        <!-- /.row -->
      <form class="form-horizontal" method="post" action="UserServlet?add-user">
         <div class="row">
            <div class="well well-lg clearfix text-right">
              <a href="UserServlet" class="btn btn-lg "><span><i class="fa fa-arrow-left"></i>Retour</span></a>
              <button type="submit" class="btn btn-default btn-lg pull-right">
                Enregistrer
              </button>
            </div>
         </div>

         <div class="row">
              <fieldset>

              <!-- Multiple Checkboxes -->
              <div class="form-group">
                <label class="col-md-3 control-label" for="user-state">Statut</label>
                <div class="col-md-3">
                <div class="checkbox">
                  <input type="checkbox" name="actif" id="user-state">
                  <label for="user-state" ></label>
                </div>
                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-3 control-label" for="firstname">Prénom</label>
                <div class="col-md-9">
                <input id="firstname" name="firstname" type="text" placeholder="" class="form-control input-md" required="">
                </div>
              </div>
              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-3 control-label" for="lastname">Nom</label>
                <div class="col-md-9">
                <input id="lastname" name="lastname" type="text" placeholder="" class="form-control input-md" required="">
                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-3 control-label" for="select-role">Rôle</label>
                <div class="col-md-9">
                <div class="sel-container">
                  <div class="sel">
                    <select name="select-role" id="select-role">
                      <option value="-1" disabled selected>Rôle</option>
                        <% if (request.getAttribute("roles") == null) return; %>
                        <% for (Role role : (List<Role>) request.getAttribute("roles")) { %>
                        <option value="<%=role.getIdRole()%>"><%=role.getIdentifiant()%></option>
                        <% } %>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              

              <div class="form-group">
                <label class="col-md-3 control-label civility">Civilité</label>
                <div class="gender col-md-9">
                  <input type="radio" id="male" name="civilite" value="male" checked="checked"/>
                  <label for="male">
                    <i class="fa fa-male" aria-hidden="true"></i>
                  </label>
                  <input type="radio" id="female" name="civilite" value="female"/>
                  <label for="female">
                    <i class="fa fa-female" aria-hidden="true"></i>
                  </label>
                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-3 control-label" for="identifiant">Identifiant</label>
                <div class="col-md-9">
                <input id="identifiant" name="identifiant" type="email" placeholder="" class="form-control input-md" required="">
                  
                </div>
              </div>

              <!-- Birth day-->
          <div class="form-group">
            <label class="col-md-3 control-label">Date de naissance</label>
            <div class="birth-day col-md-9">
              <div class="sel-container">
                <div class="sel">
                <select name="select-day" id="select-day">
                  <option value="-1" selected disabled>Jour</option>

                <option value="01">1</option>
                <option value="02">2</option>
                <option value="03">3</option>
                <option value="04">4</option>
                <option value="05">5</option>
                <option value="06">6</option>
                <option value="07">7</option>
                <option value="08">8</option>
                <option value="09">9</option>
                 <option value="10">10</option>
                 <option value="11">11</option>
                 <option value="12">12</option>
                 <option value="13">13</option>
                 <option value="14">14</option>
                 <option value="15">15</option>
                 <option value="16">16</option>
                 <option value="17">17</option>
                 <option value="18">18</option>
                 <option value="19">19</option>
                 <option value="20">20</option>
                 <option value="21">21</option>
                 <option value="22">22</option>
                 <option value="23">23</option>
                 <option value="24">24</option>
                 <option value="25">25</option>
                 <option value="26">26</option>
                 <option value="27">27</option>
                 <option value="28">28</option>
                 <option value="29">29</option>
                 <option value="30">30</option>
                 <option value="31">31</option>

                </select>
                            </div>
                          </div>
                          <div class="sel-container">
                            <div class="sel">
                <select name="select-month" id="select-month">
                  <option value="-1" selected disabled>Mois</option>
                 <option value="01">Janvier</option>
                 <option value="02">Février</option>
                 <option value="03">Mars</option>
                 <option value="04">Avril</option>
                 <option value="05">Mai</option>
                 <option value="06">Juin</option>
                 <option value="07">Juillet</option>
                 <option value="08">Aout</option>
                 <option value="09">Septembre</option>
                  <option value="10">Octobre</option>
                  <option value="11">Novembre</option>
                  <option value="12">Décembre</option>

                </select>
                            </div>
                          </div>
                          <div class="sel-container">
                            <div class="sel">
                <select name="select-year" id="select-year">
                  <option value="-1" selected  disabled>Année</option>
                 <option value="2000">2000</option>
                 <option value="1999">1999</option>
                 <option value="1998">1998</option>
                 <option value="1997">1997</option>
                 <option value="1996">1996</option>
                 <option value="1995">1995</option>
                 <option value="1994">1994</option>
                 <option value="1993">1993</option>
                 <option value="1992">1992</option>
                 <option value="1991">1991</option>
                 <option value="1990">1990</option>
                 <option value="1989">1989</option>
                 <option value="1988">1988</option>
                 <option value="1987">1987</option>
                 <option value="1986">1986</option>
                 <option value="1985">1985</option>
                 <option value="1984">1984</option>
                 <option value="1983">1983</option>
                 <option value="1982">1982</option>
                 <option value="1981">1981</option>
                    <option value="1980">1980</option>

                </select>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-3 control-label" for="pass">Mot de passe</label>
                <div class="col-md-9">
                <input id="pass" name="pass" type="password" placeholder="" class="form-control input-md" required="">
                  
                </div>
              </div>
              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-3 control-label" for="pass-bis">Confirmer Mot de passe</label>
                <div class="col-md-9">
                <input id="pass-bis" name="pass-bis" type="password" placeholder="" class="form-control input-md" required="">
                  
                </div>
              </div>
<!-- 
/*=====================================
=            Adress System            =
=====================================*/
 -->
             
        <div class="row">
            <div class="col-md-3"></div>
          <div class="radio-box col-md-9">
              <div>
            <div class="form-group">
              <h2>Adresse</h2>
            </div>
                <span class="adressTypeForm">Ceci est mon :</span> 
                  <p>
                    <input class="chb1" type="checkbox" id="shippingAdress" name="checkShipAdr" checked>
                    <label for="shippingAdress">Adresse de livraison</label>
                  </p>
                  <p>
                    <input class="chb2" type="checkbox" id="billAdress" name="checkBillAdr">
                    <label for="billAdress">Adresse de facturation</label>
                  </p>
                  <p>
                    <input class="chb1 chb2" type="checkbox" id="BillShip" name="checkBothAdr">
                    <label for="BillShip">Adresse de livraison et de facturation</label>
                  </p>
              </div>
            </div>
        </div>


        <div class="row">
          <div class="adress-box-shipping">
            
            <div>
            <div class="form-group">
              <div class="col-md-3"></div>
                <h2 class="col-md-9">Adresse de livraison</h2>
            </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="street-ship">Rue</label>

                <div class="col-md-9">
                    <input class="form-control" name="street-ship" id="street-ship" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label" for="zipcode-ship">Code Postale</label>

            <div class="col-md-9">
                <input class="form-control" name="zipcode-ship" id="zipcode-ship" type="number">
            </div>
            </div>

            <div class="form-group">
            <label class="col-md-3 control-label" for="city-ship">Ville</label>
            <div class="col-md-9">
            <input class="form-control" name="city-ship" id="city-ship" type="text">
            </div>
            </div>

             <div class="form-group">
                <label class="col-md-3 control-label" for="country-ship">Pays</label>
               <div class="col-md-9">
                   <input class="form-control" name="country-ship" id="country-ship" type="text">
                   </div>
             </div>

        </div>
        </div>

        <div class="row">
          <div class="adress-box-billing">
            <div>
            <div class="form-group">
              <div class="col-md-3"></div>
                <h2 class="col-md-9">Adresse de facturation</h2>
            </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" for="street-bill">Rue</label>
              <div class="col-md-9">
                <input class="form-control" name="street-bill" id="street-bill" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" for="zipcode-bill">Code Postale</label>
              <div class="col-md-9">
                <input class="form-control" name="zipcode-bill" id="zipcode-bill" type="number">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" for="city-bill">Ville</label>
              <div class="col-md-9">
                <input class="form-control" name="city-bill" id="city-bill" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" for="country-bill">Pays</label>
              <div class="col-md-9">
                <input class="form-control" name="country-bill" id="country-bill" type="text">
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="adress-box-shipBill"> 
            <div class="">
            <div class="form-group">
              <div class="col-md-3"></div>
              <h2 class="col-md-9">Adresse de livraison et de factuartion</h2>
            </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" class="col-md-3" for="street-bill-ship">Rue</label>
              <div class="col-md-9">
                <input class="form-control" name="street-bill-ship" id="street-bill-ship" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" class="col-md-3" for="zipcode-bill-ship">Code Postale</label>
              <div class="col-md-9">
                <input class="form-control" name="zipcode-bill-ship" id="zipcode-bill-ship" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" class="col-md-3" for="city-bill-ship">Ville</label>
              <div class="col-md-9">
                <input class="form-control" name="city-bill-ship" id="city-bill-ship" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label" class="col-md-3" for="country-bill-ship">Pays</label>
              <div class="col-md-9">
                <input class="form-control" name="country-bill-ship" id="country-bill-ship" type="text">
              </div>
            </div>
          </div>
        </div>
     


 <!-- /.row -->
         <div class="row">
            <div class="well well-lg clearfix text-right">
              <a href="UserServlet" class="btn btn-lg"><span><i class="fa fa-arrow-left"></i>Retour</span></a>
              <button type="submit" class="btn btn-default btn-lg pull-right">
                Enregistrer
              </button>
            </div>
         </div>
         </fieldset>
        </div>
  </form>
      <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
  </div>
  <!-- /#wrapper -->
      <script src="${pageContext.request.contextPath}/js/bower.js" type="text/javascript" charset="utf-8"></script>
      <script src="${pageContext.request.contextPath}/js/application.js" type="text/javascript" charset="utf-8"></script>
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
      <% if (Boolean.TRUE.equals(request.getAttribute("responseAddUser"))) { %>
      <script>
          swal("${requestScope.titleModal}", "${requestScope.txtModal}", "${requestScope.typeModal}");
      </script>
      <% } %>
      </div>
    </body>
</html>
