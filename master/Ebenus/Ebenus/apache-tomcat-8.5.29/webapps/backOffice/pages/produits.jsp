<%@ page import="com.cours.ebenus.dao.entities.Utilisateur" %>
<%@ page import="com.cours.ebenus.dao.entities.Role" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ page import="com.cours.ebenus.dao.entities.Produit" %>
<%@ page pageEncoding="UTF-8" %>

<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<% if (request.getAttribute("javax.servlet.forward.request_uri") == null) { %>
<% response.sendRedirect("/backOffice/ProduitServlet"); %>
<% } else if (user == null) { %>
<% response.sendRedirect("/backOffice/LoginServlet"); %>
<% } %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/images/favicon.png">
    <title>Ebenus Admin</title>
    <!-- Custom Fonts -->
    <!-- <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/master.css">

</head>
<body>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="dashboard.jsp">Ebenus Admin</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user"></i>
                    <% if (user != null) { %>
                    <%= String.format(Locale.FRANCE, "%s %s", user.getPrenom(), user.getNom()) %>
                    <% } %>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="DashboardServlet?logout=true"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active">
                    <a href="DashboardServlet"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a href="UserServlet"><i class="fa fa-users"></i> Utilisateurs</a>
                </li>
                <li >
                    <a href="RoleServlet"><i class="fa fa-user"></i> Rôles</a>
                </li>
                <li class="active">
                    <a href="ProduitServlet"><i class="fa fa-money"></i> Produits</a>

                    <%--<a href="${pageContext.request.contextPath}/pages/produits.html"><i class="fa fa-money"></i> Produits</a>--%>
                </li>
                <li>
                    <a href="CommandeServlet"><i class="fa fa-shopping-cart"></i> Commandes</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Produits
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="DashboardServlet">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i> Produits
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="well well-lg clearfix">
                    <form action="ProduitServlet?add-produit">
                        <button class="btn btn-default btn-lg pull-right">
                            Ajouter Produit
                        </button>
                    </form>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money"></i> Liste des Produits</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="well well-sm col-lg-6">
                                <form action="#" method="get">
                                    <div class="input-group">
                                        <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
                                        <input class="form-control" id="system-search" name="q" placeholder="Rechercher un produit" required>
                                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </span>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-6">
                                <!-- Select Basic -->
                                <div class="form-group selectexport-box">
                                    <div class="input-file-box">
                                        <input type="file" name="file-2[]" id="file-2" class="inputfile inputfile-2" data-multiple-caption="{count} files selected" multiple />
                                        <label for="file-2"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Importer un fichier CSV</span></label>
                                    </div>
                                    <button type="button" class="btn btn-default">Import</button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive listProducts">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Image</th>
                                    <th>Nom</th>
                                    <th>Référence</th>
                                    <th>Prix</th>
                                    <th>Quantité</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <% if (request.getAttribute("produits") == null) return; %>
                                    <% for (Produit produit : (List<Produit>) request.getAttribute("produits")) { %>
                                    <tr>
                                        <td><%=produit.getIdProduit()%></td>
                                        <td><img class="admin__control-thumbnail" src="https://www.materiel.net/live/360365.350.350.jpg" alt="Lorem ipsum dolor."></td>
                                        <td><%=produit.getNom()%></td>
                                        <td><%=produit.getReference()%></td>
                                        <td><%=produit.getPrix()%></td>
                                        <td><%=produit.getStock()%></td>
                                        <td>
                                            <a href="editer-produit.html"><i class="fa fa-edit"></i></a>
                                            <button class="no-style-btn"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                    <% } %>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<script src="${pageContext.request.contextPath}/js/bower.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/application.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>