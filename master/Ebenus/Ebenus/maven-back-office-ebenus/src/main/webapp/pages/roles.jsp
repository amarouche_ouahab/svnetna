<%@ page import="com.cours.ebenus.dao.entities.Utilisateur" %>
<%@ page import="com.cours.ebenus.dao.entities.Role" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ page pageEncoding="UTF-8" %>

<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<% if (request.getAttribute("javax.servlet.forward.request_uri") == null) { %>
<% response.sendRedirect("/backOffice/RoleServlet"); %>
<% } else if (user == null) { %>
<% response.sendRedirect("/backOffice/LoginServlet"); %>
<% } %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/images/favicon.png">
  <title>Ebenus Admin</title>
  <!-- Custom Fonts -->
  <!-- <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/master.css">
  
</head>
<body>
  <div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="dashboard.jsp">Ebenus Admin</a>
      </div>
      <!-- Top Menu Items -->
      <ul class="nav navbar-right top-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            <% if (user != null) { %>
            <%= String.format(Locale.FRANCE, "%s %s", user.getPrenom(), user.getNom()) %>
            <% } %>
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="DashboardServlet?logout=true"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
            </li>
          </ul>
        </li>
      </ul>
      <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
          <li class="active">
            <a href="DashboardServlet"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
          </li>
          <li>
            <a href="UserServlet"><i class="fa fa-users"></i> Utilisateurs</a>
          </li>
          <li class="active">
            <a href="RoleServlet"><i class="fa fa-user"></i> Rôles</a>
          </li>
          <li>
            <a href="ProduitServlet"><i class="fa fa-money"></i> Produits</a>

            <%--<a href="${pageContext.request.contextPath}/pages/produits.html"><i class="fa fa-money"></i> Produits</a>--%>
          </li>
          <li>
            <a href="CommandeServlet"><i class="fa fa-shopping-cart"></i> Commandes</a>

          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </nav>
    <div id="page-wrapper">
      <div class="container-fluid">
         <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              Rôles
            </h1>
            <ol class="breadcrumb">
              <li>
                <i class="fa fa-dashboard"></i>  <a href="DashboardServlet">Dashboard</a>
              </li>
              <li class="active">
                <i class="fa fa-edit"></i> Rôles
              </li>
            </ol>
          </div>
        </div>
        <!-- /.row -->
         <div class="row">
            <div class="well well-lg clearfix">
              <a href="RoleServlet?add-role" class="btn btn-default btn-lg pull-right">Ajouter un Rôle</a>
            </div>
         </div>
          <!-- /.row -->
          <div class="row">
            <div class="panel panel-default">
              
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-users"></i> Liste des Rôles</h3>
              </div>
              <div class="panel-body">
                
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped">
                    <thead>
                      <tr>
                        <th>IdRole</th>
                        <th>Identifiant</th>
                        <th>Description</th>
                        <th>Editer</th>
                        <th>Supprimer</th>
                       
                      </tr>
                    </thead>
                    <tbody>
                    <% if (request.getAttribute("roles") == null) return; %>
                    <% for (Role role : (List<Role>) request.getAttribute("roles")) { %>
                    <tr>
                        <td><%=role.getIdRole()%></td>
                        <td><%=role.getIdentifiant()%></td>
                        <td><%=role.getDescription()%></td>
                        <td><a href="RoleServlet?edit-role=<%=role.getIdRole()%>"><i class="fa fa-edit"></i></a></td>
                        <td><a href="RoleServlet?del-role=<%=role.getIdRole()%>"><i class="fa fa-trash-o"></i></a></td>
                      </tr>
                    <% } %>
                    </tbody>
                  </table>
                </div>
                
              </div>
            </div>
          </div>

          
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
  </div>
  <!-- /#wrapper -->
  <script src="${pageContext.request.contextPath}/js/bower.js" type="text/javascript" charset="utf-8"></script>
  <script src="${pageContext.request.contextPath}/js/application.js" type="text/javascript" charset="utf-8"></script>
  <script src="${pageContext.request.contextPath}/js/utils.js" type="text/javascript" charset="utf-8"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <% if (Boolean.TRUE.equals(request.getAttribute("responseDelUser"))) { %>
  <script>
      swal("${requestScope.titleModal}", "${requestScope.txtModal}", "${requestScope.typeModal}");
  </script>
  <% } %>
  <script>
      function deleteRolr(id) {
          swal({
              title: "Êtes-vous sûr de vouloir supprimer ce Role ?",
              text: "L'opération ne sera pas reversible",
              icon: "warning",
              buttons: true,
              dangerMode: true
          })
              .then(function (result) {
                  if (result) {
                      post("/backOffice/RoleServlet", "del-user", id)
                  }
              })
      }
  </script>
</body>
</html>