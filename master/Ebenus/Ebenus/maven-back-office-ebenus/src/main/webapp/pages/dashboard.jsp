<%@ page import="com.cours.ebenus.dao.entities.Commande" %>
<%@ page import="com.cours.ebenus.dao.entities.Utilisateur" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page pageEncoding="UTF-8" %>

<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<% if (request.getAttribute("javax.servlet.forward.request_uri") == null) { %>
<% response.sendRedirect("/backOffice/DashboardServlet"); %>
<% } else if (user == null) { %>
<% response.sendRedirect("/backOffice/LoginServlet"); %>
<% } %>
<% SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); %>
<% SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss"); %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/images/favicon.png">
  <title>Ebenus Admin</title>
  <!-- Custom Fonts -->
  <!-- <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/master.css">
  
</head>
<body>
  <div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="dashboard.jsp">Ebenus Admin</a>
      </div>
      <!-- Top Menu Items -->
      <ul class="nav navbar-right top-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            <% if (user != null) { %>
            <%= String.format(Locale.FRANCE, "%s %s", user.getPrenom(), user.getNom()) %>
            <% } %>
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="DashboardServlet?logout=true"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
            </li>
          </ul>
        </li>
      </ul>
      <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
          <li class="active">
            <a href="DashboardServlet"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
          </li>
          <li>
            <a href="UserServlet"><i class="fa fa-users"></i> Utilisateurs</a>
          </li>
          <li>
            <a href="RoleServlet"><i class="fa fa-user"></i> Rôles</a>
          </li>
          <li>
            <a href="ProduitServlet"><i class="fa fa-money"></i> Produits</a>

            <%--<a href="${pageContext.request.contextPath}/pages/produits.html"><i class="fa fa-money"></i> Produits</a>--%>
          </li>
          <li>
            <a href="CommandeServlet"><i class="fa fa-shopping-cart"></i> Commandes</a>

          </li>
        </ul>
      </div>
    <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">
      <div class="container-fluid">


        <!-- Page Heading -->
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              Dashboard <small>Un aperçu sur les Statistiques</small>
            </h1>
            <ol class="breadcrumb">
              <li class="active">
                <i class="fa fa-dashboard"></i> Dashboard
              </li>
            </ol>
          </div>
        </div>
        <!-- /.row -->

        

        <div class="row">
          
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Panneau des transactions</h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped">
                    <thead>
                      <tr>
                        <th>Commande</th>
                        <th>Date Commande </th>
                        <th>Heure Commande</th>
                        <th>Total (Euro)</th>
                      </tr>
                    </thead>
                    <tbody>
                    <% if (request.getAttribute("commandes") == null) return; %>
                    <% for (Commande commande : (List<Commande>) request.getAttribute("commandes")) { %>
                      <tr>
                        <td><%=commande.getIdCommande()%></td>
                        <td><%=dateFormat.format(commande.getDateCommande())%></td>
                        <td><%=hourFormat.format(commande.getDateCommande())%></td>
                        <td>€ <%=commande.getTotalCommande()%></td>
                      </tr>
                    <% } %>
                    </tbody>
                  </table>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

  </div>
  <!-- /#wrapper -->

  <script src="${pageContext.request.contextPath}/js/bower.js" type="text/javascript" charset="utf-8"></script>
  <script src="${pageContext.request.contextPath}/js/application.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>
