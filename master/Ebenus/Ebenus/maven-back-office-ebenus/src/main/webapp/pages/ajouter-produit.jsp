<%@ page import="com.cours.ebenus.dao.entities.Utilisateur" %>
<%@ page import="com.cours.ebenus.dao.entities.Produit" %>

<%@ page import="java.util.Locale" %>
<%@ page pageEncoding="UTF-8" %>

<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<% if (request.getAttribute("javax.servlet.forward.request_uri") == null) { %>
<% response.sendRedirect("/backOffice/ProductServlet"); %>
<% } else if (user == null) { %>
<% response.sendRedirect("/backOffice/LoginServlet"); %>
<% } %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/images/favicon.png">
    <title>Ebenus Admin</title>
    <!-- Custom Fonts -->
    <!-- <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/master.css">

</head>
<body>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="dashboard.jsp">Ebenus Admin</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user"></i>
                    <% if (user != null) { %>
                    <%= String.format(Locale.FRANCE, "%s %s", user.getPrenom(), user.getNom()) %>
                    <% } %>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="DashboardServlet?logout=true"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li >
                    <a href="DashboardServlet"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a href="UserServlet"><i class="fa fa-users"></i> Utilisateurs</a>
                </li>
                <li >
                    <a href="RoleServlet"><i class="fa fa-user"></i> Rôles</a>
                </li>
                <li class="active">
                    <a href="ProduitServlet"><i class="fa fa-money"></i> Produits</a>

                    <%--<a href="${pageContext.request.contextPath}/pages/produits.html"><i class="fa fa-money"></i> Produits</a>--%>
                </li>
                <li>
                    <a href="CommandeServlet"><i class="fa fa-shopping-cart"></i> Commandes</a>

                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Nouveau Produit
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="login.jsp">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i> Nouveau Produit
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            <form action="ProduitServlet?add-produit" method="post" class="form-horizontal">

            <div class="row">
                <div class="well well-lg clearfix text-right">
                    <a href="ProduitServlet" class="btn btn-lg "><span><i class="fa fa-arrow-left"></i>Retour</span></a>
                    <button type="submit" class="btn btn-default btn-lg pull-right">
                        Enregistrer
                    </button>
                </div>
            </div>

            <div class="row">
                <form class="form-horizontal">
                    <fieldset>


                        <!-- Multiple Checkboxes -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="checkboxes">Activer Produit</label>
                            <div class="col-md-3">
                                <div class="checkbox">
                                    <input type="checkbox" name="activer" id="checkboxes-0" value="1">
                                    <label for="checkboxes-0">
                                    </label>

                                </div>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="ImgInputFileCat">Image acceuil (400 X 400)</label>
                            <div class="col-md-9">
                                <input id="ImgInputFileCat" name="ImgInputFileCat" type="file"  accept="image/*" placeholder="" class="form-control input-md" required="">
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="ImgInputFileFiche">Image fiche produit (700 X 700)</label>
                            <div class="col-md-9">
                                <input id="ImgInputFileFiche" name="ImgInputFileFiche" type="file"  accept="image/*" placeholder="" class="form-control input-md" required="">
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="ImgInputFilethumb">Image dans panier (75 X 75)</label>
                            <div class="col-md-9">
                                <input id="ImgInputFilethumb" name="ImgInputFilethumb" type="file"  accept="image/*" placeholder="" class="form-control input-md" required="">
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="textinput">Référence</label>
                            <div class="col-md-9">
                                <input id="reference" name="reference" type="text" placeholder="" class="form-control input-md" required="">

                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="textinput">Intitulé Produit</label>
                            <div class="col-md-9">
                                <input id="nom" name="nom" type="text" placeholder="" class="form-control input-md" required="">

                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="textinput">Prix</label>
                            <div class="col-md-9">
                                <input id="prix" name="prix" type="text" placeholder="" class="form-control input-md" required="">

                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="selectbasic">Stock</label>
                            <div class="col-md-9">
                                <input id="stock" value="" name="stock" type="number" placeholder="" class="form-control input-md" required="">
                            </div>
                        </div>

                        <!-- Textarea -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="textarea">Description</label>
                            <div class="col-md-9">
                                <textarea class="form-control" id="description" name="description" rows="6"></textarea>
                            </div>
                        </div>



                    </fieldset>
                </form>
            </div>
            </form>

            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <script src="${pageContext.request.contextPath}/js/bower.js" type="text/javascript" charset="utf-8"></script>
    <script src="${pageContext.request.contextPath}/js/application.js" type="text/javascript" charset="utf-8"></script>
        <% if (Boolean.TRUE.equals(request.getAttribute("responseAddProduit"))) { %>
    <script>
        swal("${requestScope.titleModal}", "${requestScope.txtModal}", "${requestScope.typeModal}");
    </script>
        <% } %>
</body>
</html>
