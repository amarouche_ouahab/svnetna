<%@ page import="com.cours.ebenus.dao.entities.Utilisateur" %>
<%@ page import="com.cours.ebenus.dao.entities.Role" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ page import="com.cours.ebenus.dao.entities.Commande" %>
<%@ page import="com.cours.ebenus.dao.entities.ArticleCommande" %>
<%@ page import="com.cours.ebenus.service.IServiceFacade" %>
<%@ page import="com.cours.ebenus.service.ServiceFacade" %>
<%ServiceFacade serviceFacade = new ServiceFacade();%>

<%--<%IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();%>--%>

<%@ page pageEncoding="UTF-8" %>

<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<% if (request.getAttribute("javax.servlet.forward.request_uri") == null) { %>
<% response.sendRedirect("/backOffice/CommandeServelt"); %>
<% } else if (user == null) { %>
<% response.sendRedirect("/backOffice/LoginServlet"); %>
<% } %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
    <title>Ebenus Admin</title>
    <!-- Custom Fonts -->
    <!-- <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/master.css">


</head>
<body>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="dashboard.jsp">Ebenus Admin</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user"></i>
                    <% if (user != null) { %>
                    <%= String.format(Locale.FRANCE, "%s %s", user.getPrenom(), user.getNom()) %>
                    <% } %>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="DashboardServlet?logout=true"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="DashboardServlet"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
                <li >
                    <a href="UserServlet"><i class="fa fa-users"></i> Utilisateurs</a>
                </li>
                <li>
                    <a href="RoleServlet"><i class="fa fa-user"></i> Rôles</a>
                </li>
                <li>
                    <a href="ProduitServlet"><i class="fa fa-money"></i> Produits</a>
                    <%--<a href="${pageContext.request.contextPath}/pages/produits.html"><i class="fa fa-money"></i> Produits</a>--%>
                </li>
                <li class="active">
                    <a href="CommandeServlet"><i class="fa fa-shopping-cart"></i> Commandes</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">
        <div class="container-fluid order-detail">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Détail Commande
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="DashboardServlet">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i> Détail commande
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            <% if (request.getAttribute("commande") == null) return; %>
            <% Commande cmd = (Commande) request.getAttribute("commande");%>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Commande : DFFDDF555645DF</div>
                        <table class="table">
                            <!-- <div class="well well-sm">Commande : DFFDDF555645DF</div> -->
                            <tbody>
                            <tr>
                                <th>Date création commande</th>
                                <td><%=cmd.getDateCommande()%></td>

                            </tr>
                            <tr>
                                <th>Date modification commande</th>
                                <td><%=cmd.getDateModification()%></td>

                            </tr>
                            <tr>
                                <th>Statut du commande</th>
                                <%
                                    String st = "En attente";
                                    if(cmd.getStatut().equals("V")){
                                            st = "Validé";
                                    }
                                %>
                                <td><%=st%></td>

                            </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Informations compte</div>
                        <table class="table">
                            <!-- <div class="well well-sm">Commande : DFFDDF555645DF</div> -->
                            <tbody>
                            <tr>
                                <th>Client</th>
                                <td><%= cmd.getUtilisateur().getPrenom() + " "+  cmd.getUtilisateur().getNom()%></td>

                            </tr>
                            <tr>
                                <th>Identifiant</th>
                                <td><%= cmd.getUtilisateur().getIdentifiant() %></td>


                            </tr>
                            <tr>
                                <th>Passer par</th>
                                <td><%= cmd.getUtilisateur().getPrenom() + " "+  cmd.getUtilisateur().getNom()%></td>

                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Adresse de facturation</div>
                        <p>
                            <%= cmd.getAdresse().getRue() + " "+  cmd.getAdresse().getVille() + " " +cmd.getAdresse().getPays() %>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Adresse de livraison</div>
                        <p>
                            <%= cmd.getAdresse().getRue() + " "+  cmd.getAdresse().getVille() + " " +cmd.getAdresse().getPays() %>
                        </p>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Informations sur paiement</div>
                        <p>
                            chèque
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Informations sur livraison</div>
                        <p>
                            Transport inclus
                        </p>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Produits commandés</div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Produit</th>
                                    <th>Référence</th>
                                    <th>Prix</th>
                                    <th>Quantité</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <% for (ArticleCommande ar :  serviceFacade.getArticleCommandeDao().findAllArticlesCommande()) { %>
                                <% if (cmd.getIdCommande().equals(ar.getCommande().getIdCommande())) { %>

                                <tr>
                                    <td><%=ar.getProduit().getNom()%></td>
                                    <td><%=ar.getProduit().getReference()%></td>
                                    <td> <%=ar.getProduit().getPrix()%> €</td>
                                    <td> <%=ar.getQuantite()%></td>
                                    <td> <%=ar.getTotalArticleCommande()%></td>
                                </tr>
                                <% }
                                }%>

                                <%--<tr>--%>
                                    <%--<td>Joust Duffle Bag</td>--%>
                                    <%--<td>DSD5454DFDEF</td>--%>
                                    <%--<td>145 €</td>--%>
                                    <%--<td>2</td>--%>
                                    <%--<td>10.000</td>--%>
                                <%--</tr>--%>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <script src="${pageContext.request.contextPath}/js/bower.js" type="text/javascript" charset="utf-8"></script>
    <script src="${pageContext.request.contextPath}/js/application.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>
