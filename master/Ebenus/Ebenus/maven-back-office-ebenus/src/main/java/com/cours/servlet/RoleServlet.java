package com.cours.servlet;

import com.cours.constants.Display;
import com.cours.constants.URIs;
import com.cours.ebenus.dao.entities.Commande;
import com.cours.ebenus.dao.entities.Role;
//import com.cours.ebenus.factory.ServiceFactory;
//import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "RoleServlet", urlPatterns = {"/RoleServlet"})
public class RoleServlet extends HttpServlet {

//    private static final IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page = URIs.URL_ROLES;

        if (request.getSession().getAttribute("user") == null) {
            request.getSession().removeAttribute("user");
            response.sendRedirect(URIs.SERVLET_LOGIN);
            return;
        }

        if (request.getParameter("add-role") != null) {
            page = URIs.URL_ADD_ROLES;
        }
        else if (request.getParameter("edit-role") != null) {
            page = URIs.URL_EDIT_ROLES;
            getRole(request);
        }
        else if (request.getParameter("del-role") != null) {
            System.out.println(request.getParameter("del-role") + "===s");
            Role role = serviceFacade.getRoleDao().findRoleById(Integer.parseInt(request.getParameter("del-role")));
//            Role role = (Role) request.getParameter("del-role");
//            serviceFacade.getRoleDao().deleteRole(role);
            Boolean delet =  serviceFacade.getRoleDao().deleteRoleId(Integer.parseInt(request.getParameter("del-role")));
            if (delet) {
                request.setAttribute("txtModal", Display.TEXT_MODAL_DEL_ROLE_SUCCESS);
                request.setAttribute("typeModal", Display.SWAL_SUCCESS);
            }
            else {
                request.setAttribute("txtModal", Display.TEXT_MODAL_DEL_ROLE_ERROR);
                request.setAttribute("typeModal", Display.SWAL_ERROR);
            }

            getRoles(request);
        }
        else {
            getRoles(request);
        }

        this.getServletContext().getRequestDispatcher(page).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameter("add-role") != null) {
            request.setAttribute("responseAddRole", true);
            request.setAttribute("titleModal", Display.TITLE_MODAL_ADD_ROLE);
            addRole(request);
        }
        else if (request.getParameter("edit-role") != null) {
            request.setAttribute("responseEditRole", true);
            request.setAttribute("titleModal", Display.TITLE_MODAL_EDIT_ROLE);
            editRole(request);
        }

        doGet(request, response);
    }

    private void getRoles(HttpServletRequest request) {
        List<Role> roles = serviceFacade.getRoleDao().findAllRoles();
        request.setAttribute("roles", roles);
    }

    private void getRole(HttpServletRequest request) {
        Role role = serviceFacade.getRoleDao().findRoleById(Integer.parseInt(request.getParameter("edit-role")));
        request.setAttribute("role", role);
    }

    private void addRole(HttpServletRequest request) {

        Role role = new Role();
        role.setIdentifiant(request.getParameter("id-role"));
        role.setDescription(request.getParameter("description-role"));

        role = serviceFacade.getRoleDao().createRole(role);

        if (role.getIdRole() != null) {
            request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_ROLE_SUCCESS);
            request.setAttribute("typeModal", Display.SWAL_SUCCESS);
        }
        else {
            request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_ROLE_ERROR);
            request.setAttribute("typeModal", Display.SWAL_ERROR);
        }
    }

    private void editRole(HttpServletRequest request) {
        Role role = new Role();
        role.setIdRole(Integer.parseInt(request.getParameter("edit-role")));
        role.setIdentifiant(request.getParameter("id-role"));
        role.setDescription(request.getParameter("description-role"));

        serviceFacade.getRoleDao().updateRole(role);

        request.setAttribute("txtModal", Display.TEXT_MODAL_EDIT_ROLE_SUCCESS);
        request.setAttribute("typeModal", Display.SWAL_SUCCESS);
    }

    private void deleteRole(int id) {
        Role role = serviceFacade.getRoleDao().findRoleById(id);
        serviceFacade.getRoleDao().deleteRole(role);
    }
}
