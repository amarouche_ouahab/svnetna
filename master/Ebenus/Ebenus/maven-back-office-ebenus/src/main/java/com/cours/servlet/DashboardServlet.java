package com.cours.servlet;

import com.cours.constants.URIs;
import com.cours.ebenus.dao.entities.Commande;
//import com.cours.ebenus.factory.ServiceFactory;
//import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "DashboardServlet", urlPatterns = {"/DashboardServlet"})
public class DashboardServlet extends HttpServlet {

//    private static final IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getSession().getAttribute("user") == null || disconnect(request)) {
            request.getSession().removeAttribute("user");
            response.sendRedirect(URIs.SERVLET_LOGIN);
            return;
        }

        getLastCommande(request);
        this.getServletContext().getRequestDispatcher(URIs.URL_DASHBOARD).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    private void getLastCommande(HttpServletRequest request) {
        List<Commande> commandes = serviceFacade.getCommandeDao().findAllCommandes();
        request.setAttribute("commandes", commandes.subList(0,10));
    }

    private boolean disconnect(HttpServletRequest request) {
        return "true".equals(request.getParameter("logout"));
    }
}
