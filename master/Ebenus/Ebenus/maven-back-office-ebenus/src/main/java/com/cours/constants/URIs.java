package com.cours.constants;

public class URIs {

    private static final String URL_BASE = "/pages";

    public static final String URL_LOGIN = URL_BASE + "/login.jsp";
    public static final String URL_ADD_USER = URL_BASE + "/ajouter-utilisateur.jsp";
    public static final String URL_EDIT_USER = URL_BASE + "/editer-utilisateur.jsp";
    public static final String URL_DASHBOARD = URL_BASE + "/dashboard.jsp";
    public static final String URL_USERS = URL_BASE + "/utilisateurs.jsp";
    public static final String URL_ROLES = URL_BASE + "/roles.jsp";
    public static final String URL_ADD_ROLES = URL_BASE + "/ajouter-role.jsp";
    public static final String URL_EDIT_ROLES = URL_BASE + "/editer-role.jsp";
    public static final String URL_COMMANDES_DETAIL = URL_BASE + "/detail_commande.jsp";

    public static final String URL_PRODUITS = URL_BASE + "/produits.jsp";
    public static final String URL_ADD_PRODUITS = URL_BASE + "/ajouter-produit.jsp";
    public static final String URL_EDIT_PRODUITS = URL_BASE + "/editer-produit.jsp";

    public static final String URL_COMMANDES = URL_BASE + "/commandes.jsp";
    public static final String URL_ADD_COMMANDES = URL_BASE + "/ajouter-commande.jsp";
    public static final String URL_EDIT_COMMANDES = URL_BASE + "/editer-commande.jsp";

    private static final String SERVLET_BASE = "/backOffice";

    public static final String SERVLET_LOGIN = SERVLET_BASE + "/LoginServlet";
    public static final String SERVLET_DASHBOARD = SERVLET_BASE + "/DashboardServlet";
}
