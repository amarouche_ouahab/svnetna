package com.cours.servlet;

import com.cours.constants.Display;
import com.cours.constants.URIs;
import com.cours.ebenus.dao.entities.Commande;
import com.cours.ebenus.dao.entities.Produit;
import com.cours.ebenus.dao.entities.Role;
//import com.cours.ebenus.factory.ServiceFactory;
//import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ProductServlet", urlPatterns = {"/ProductServlet"})
public class ProductServlet extends HttpServlet {

//    private static final IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page = URIs.URL_PRODUITS;

        if (request.getSession().getAttribute("user") == null) {
            request.getSession().removeAttribute("user");
            response.sendRedirect(URIs.SERVLET_LOGIN);
            return;
        }
        if (request.getParameter("add-produit") != null) {
            page = URIs.URL_ADD_PRODUITS;
        }
        else if (request.getParameter("edit-produit") != null) {
            page = URIs.URL_EDIT_PRODUITS;
            getProduct(request);
        }
        else if (request.getParameter("del-produit") != null) {
            deleteProduct(Integer.valueOf(request.getParameter("del-produit")));
            getProducts(request);
        }
        else {
            getProducts(request);
        }

        this.getServletContext().getRequestDispatcher(page).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameter("add-produit") != null) {
            request.setAttribute("responseAddProduit", true);
            request.setAttribute("titleModal", Display.TITLE_MODAL_ADD_PRODUIT);
            addProduct(request);
        }
        else if (request.getParameter("edit-produit") != null) {
            request.setAttribute("responseEditProduit", true);
            request.setAttribute("titleModal", Display.TITLE_MODAL_EDIT_PRODUIT);
            editProduit(request,Integer.valueOf(request.getParameter("edit-produit")));
        }
        doGet(request, response);
    }
//
    private void getProducts(HttpServletRequest request) {
        List<Produit> produits = serviceFacade.getProduitDao().findAllProduits();
        request.setAttribute("produits", produits);
    }


    private void getProduct(HttpServletRequest request) {
        Produit produit = serviceFacade.getProduitDao().findProduitById(Integer.parseInt(request.getParameter("edit-produit")));
        request.setAttribute("produit", produit);
    }
//
    private void addProduct(HttpServletRequest request) {

        Produit produit = new Produit();
        produit.setNom(request.getParameter("nom"));
        produit.setActive(Boolean.valueOf(request.getParameter("activer")));

//        produit.setStock(10);
        produit.setStock(Integer.parseInt(request.getParameter("stock")));
        produit.setPrix(Double.parseDouble(request.getParameter("prix")));
//        produit.setPrix(10.0);
        produit.setDescription(request.getParameter("description"));
        produit.setReference(request.getParameter("reference"));

        produit = serviceFacade.getProduitDao().createProduit(produit);

        if (produit.getIdProduit() != null) {
            request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_PRODUIT_SUCCESS);
            request.setAttribute("typeModal", Display.SWAL_SUCCESS);
        }
        else {
            request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_PRODUIT_ERROR);
            request.setAttribute("typeModal", Display.SWAL_ERROR);
        }
    }

    private void deleteProduct(int id) {
        Produit produit = serviceFacade.getProduitDao().findProduitById(id);
        serviceFacade.getProduitDao().deleteProduit(produit);
    }
//
    private void editProduit(HttpServletRequest request,int id) {
        Produit produit = new Produit();
        System.out.println(request.getParameter("prix") + "====== prix");
        produit.setPrix(Double.parseDouble(request.getParameter("prix")));
        produit.setIdProduit(id);
        produit.setStock(Integer.parseInt(request.getParameter("stock")));
        produit.setNom(request.getParameter("nom"));
        produit.setReference(request.getParameter("reference"));
        produit.setDescription(request.getParameter("description"));

        serviceFacade.getProduitDao().updateProduit(produit);

        request.setAttribute("txtModal", Display.TEXT_MODAL_EDIT_PRODUIT_SUCCESS);
        request.setAttribute("typeModal", Display.SWAL_SUCCESS);
    }
}
