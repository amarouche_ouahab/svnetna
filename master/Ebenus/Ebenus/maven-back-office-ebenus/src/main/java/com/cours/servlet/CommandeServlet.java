package com.cours.servlet;

import com.cours.constants.URIs;
import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.entities.Commande;

//import com.cours.ebenus.factory.ServiceFactory;
//import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import static com.cours.constants.Display.APP_PATH;



@WebServlet(name = "CommandeServlet", urlPatterns = {"/CommandeServlet"})
public class CommandeServlet extends HttpServlet {

//    private static final IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();
    private static Connection connect = DriverManagerSingleton.getInstance().getConnectionInstance();
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page = URIs.URL_COMMANDES;

        if (request.getSession().getAttribute("user") == null) {
            request.getSession().removeAttribute("user");
            response.sendRedirect(URIs.SERVLET_LOGIN);
            return;
        }
        if (request.getParameter("detail") != null) {
            System.out.println(URIs.URL_COMMANDES_DETAIL + request.getParameter("detail") + "=====");
            Commande cmd = serviceFacade.getCommandeDao().findCommandeById(Integer.parseInt(request.getParameter("detail") ));
            request.setAttribute("commande", cmd);
            System.out.println(URIs.URL_COMMANDES_DETAIL + request.getParameter("detail") + "=====" +cmd.getTotalCommande());
            page = URIs.URL_COMMANDES_DETAIL;
//            response.sendRedirect(URIs.URL_COMMANDES_DETAIL);
        }
        if(request.getParameter("delete") != null){
            Commande cmd = serviceFacade.getCommandeDao().findCommandeById(Integer.parseInt(request.getParameter("delete") ));
            serviceFacade.getCommandeDao().deleteCommande(cmd);
        }
        if (request.getParameter("selectexport") != null) {
            if(request.getParameter("selectexport").equals("1")){
                this.exportXml();
                System.out.println(request.getParameter("selectexport"));

            }
            else if(request.getParameter("selectexport").equals("2")){
                System.out.println(request.getParameter("selectexport"));
                this.exportJson();
            }
            else if(request.getParameter("selectexport").equals("3")) {
                System.out.println(request.getParameter("selectexport"));
                this.exportCsv();

            }

//            page = URIs.URL_ADD_ROLES;
        }
//        else if (request.getParameter("edit-role") != null) {
//            page = URIs.URL_EDIT_ROLES;
////            getRole(request);
//        }
//        else {
        getCommande(request);
//        }

        this.getServletContext().getRequestDispatcher(page).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//        if (request.getParameter("add-role") != null) {
//            request.setAttribute("responseAddRole", true);
//            request.setAttribute("titleModal", Display.TITLE_MODAL_ADD_ROLE);
//            addRole(request);
//        }
//        else if (request.getParameter("edit-role") != null) {
//            request.setAttribute("responseEditRole", true);
//            request.setAttribute("titleModal", Display.TITLE_MODAL_EDIT_ROLE);
//            editRole(request);
//        }

        doGet(request, response);
    }
    //
    private void getCommande(HttpServletRequest request) {
        List<Commande> commandes = serviceFacade.getCommandeDao().findAllCommandes();
//
        request.setAttribute("commandes", commandes);
    }


    public void exportXml() {
        List<Commande> CmdList = serviceFacade.getCommandeDao().findAllCommandes();

        String name = "/exportCommande_" + new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss").format(new Date()) + ".xml";
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Commandes");
            doc.appendChild(rootElement);

            for (Commande cms: CmdList) {
                Element newCmd = this.newCmd(doc, cms);
                rootElement.appendChild( newCmd);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(APP_PATH + name));
            transformer.transform(source, result);
            System.out.println("File saved!");
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    public Element newCmd(Document doc, Commande obj) {
        Element nodePerson = doc.createElement("commande");
        nodePerson.setAttribute("id", String.valueOf(obj.getIdCommande()));

        Element idRole = doc.createElement("IdUtilisateur");
        idRole.setTextContent(String.valueOf(obj.getUtilisateur().getIdUtilisateur()));
        nodePerson.appendChild(idRole);

        Element civilite = doc.createElement("totalCommande");
        civilite.setTextContent(String.valueOf(obj.getTotalCommande()));
        nodePerson.appendChild(civilite);

        Element prenom = doc.createElement("statut");
        prenom.setTextContent(String.valueOf(obj.getStatut()));
        nodePerson.appendChild(prenom);

        Element nom = doc.createElement("idAdresse");
        nom.setTextContent(String.valueOf(obj.getAdresse().getIdAdresse()));
        nodePerson.appendChild(nom);

        Element identifiant = doc.createElement("dateCommande");
        identifiant.setTextContent(String.valueOf(obj.getDateCommande()));
        nodePerson.appendChild(identifiant);

        Element dateNaissance = doc.createElement("DateModification");
        dateNaissance.setTextContent(String.valueOf(obj.getDateModification()));
        nodePerson.appendChild(dateNaissance);

        Element version = doc.createElement("version");
        version.setTextContent(String.valueOf(obj.getVersion()));
        nodePerson.appendChild(version);

        return nodePerson;
    }
    public void exportCsv() {
        List<Commande> CmdList = serviceFacade.getCommandeDao().findAllCommandes();

        String name = "/exportCommande_" + new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss").format(new Date()) + ".csv";
        File file = new File(APP_PATH + name);
        FileWriter writer = null;
        try {
            writer = new FileWriter(file,true);
            for (Commande cmd: CmdList) {
                writer.write( cmd.getIdCommande() + ";" + cmd.getUtilisateur().getIdUtilisateur()  + ";" + cmd.getTotalCommande()
                        + ";" + cmd.getStatut() + ";" + cmd.getAdresse().getIdAdresse() + ";" + cmd.getDateCommande() + ";" + cmd.getDateModification()+ ";" + cmd.getVersion() +"\n");
            }
            writer.close();
            System.out.println("csvok");

        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }



    public void exportJson() {
//        List<Utilisateur> users = usersList;
        List<Commande> CmdList = serviceFacade.getCommandeDao().findAllCommandes();

        String json = new Gson().toJson(CmdList);
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("Commandes", new JsonParser().parse(json).getAsJsonArray());
        String name = "/exportCommande_" + new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss").format(new Date()) + ".json";
        try {
            PrintWriter file = new PrintWriter(APP_PATH + name);
            file.write(jsonObj.toJSONString());
            file.close();
            System.out.println("ok");
        }
        catch (NullPointerException | IOException e ){
            System.out.println(e.getMessage());
        }
    }

//
//    private void getRole(HttpServletRequest request) {
//        Role role = serviceFacade.getRoleDao().findRoleById(Integer.parseInt(request.getParameter("edit-role")));
//        request.setAttribute("role", role);
//    }
//
//    private void addRole(HttpServletRequest request) {
//
//        Role role = new Role();
//        role.setIdentifiant(request.getParameter("id-role"));
//        role.setDescription(request.getParameter("description-role"));
//
//        role = serviceFacade.getRoleDao().createRole(role);
//
//        if (role.getIdRole() != null) {
//            request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_ROLE_SUCCESS);
//            request.setAttribute("typeModal", Display.SWAL_SUCCESS);
//        }
//        else {
//            request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_ROLE_ERROR);
//            request.setAttribute("typeModal", Display.SWAL_ERROR);
//        }
//    }
//
//    private void editRole(HttpServletRequest request) {
//        Role role = new Role();
//        role.setIdRole(Integer.parseInt(request.getParameter("edit-role")));
//        role.setIdentifiant(request.getParameter("id-role"));
//        role.setDescription(request.getParameter("description-role"));
//
//        serviceFacade.getRoleDao().updateRole(role);
//
//        request.setAttribute("txtModal", Display.TEXT_MODAL_EDIT_ROLE_SUCCESS);
//        request.setAttribute("typeModal", Display.SWAL_SUCCESS);
//    }
}
