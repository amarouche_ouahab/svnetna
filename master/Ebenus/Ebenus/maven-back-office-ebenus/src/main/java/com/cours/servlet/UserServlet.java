package com.cours.servlet;

import com.cours.constants.Display;
import com.cours.constants.FieldsValuesDB;
import com.cours.constants.URIs;
import com.cours.ebenus.dao.entities.Adresse;
import com.cours.ebenus.dao.entities.Role;
import com.cours.ebenus.dao.entities.Utilisateur;
//import com.cours.ebenus.factory.ServiceFactory;
//import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.cours.constants.FieldsValuesDB.R_ADMIN;

@WebServlet(name = "UserServlet", urlPatterns = {"/UserServlet"})
public class UserServlet extends HttpServlet {

//    private static final IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page = URIs.URL_USERS;

        if (request.getSession().getAttribute("user") == null) {
            response.sendRedirect(URIs.SERVLET_LOGIN);
            return;
        }

        if (request.getParameter("add-user") != null) {
            page = URIs.URL_ADD_USER;
            getListRoles(request);
        }
        else if (request.getParameter("edit-user") != null) {
            page = URIs.URL_EDIT_USER;
            getListRoles(request);
            getUserInfo(request);
        }
        else {
            getAllUsers(request);
        }

        this.getServletContext().getRequestDispatcher(page).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameter("add-user") != null) {
            request.setAttribute("responseAddUser", true);
            request.setAttribute("titleModal", Display.TITLE_MODAL_ADD_USER);
            addUser(request);
        }
        else if (request.getParameter("edit-user") != null) {
            request.setAttribute("responseAddUser", true);
            request.setAttribute("titleModal", Display.TITLE_MODAL_EDIT_USER);
            updateUser(request);
        }
        else if (request.getParameter("del-addr") != null) {
            deleteAddress(Integer.parseInt(request.getParameter("del-addr")));
        }
        else if (request.getParameter("del-user") != null) {
            deleteUser(request, Integer.parseInt(request.getParameter("del-user")));
        }

        doGet(request, response);
    }

    private void getAllUsers(HttpServletRequest request) {
        List<Utilisateur> users;
        String search = request.getParameter("search");
        users = serviceFacade.getUtilisateurDao().findAllUtilisateurs();
        if(search != null){
            users = serviceFacade.getUtilisateurDao().findUtilisateursByPrenom(search);
            if(users.isEmpty()){
                users = serviceFacade.getUtilisateurDao().findUtilisateursByNom(search);
            }
        }
//        System.out.println("=============size users  " + users.size());
        for (Utilisateur user : users) {
            user.setAdresses(serviceFacade.getAdresseDao().findAdressesByIdUtilisateur(user.getIdUtilisateur()));
        }
//        System.out.println("=============size users 2  " + users.size());

        request.setAttribute("users", users);
    }

    private void getListRoles(HttpServletRequest request) {
        List<Role> roles = serviceFacade.getRoleDao().findAllRoles();
        request.setAttribute("roles", roles);
    }

    private void getUserInfo(HttpServletRequest request) {

        int id = Integer.parseInt(request.getParameter("edit-user"));
        Utilisateur user = serviceFacade.getUtilisateurDao().findUtilisateurById(id);
        List<Adresse> adresses = serviceFacade.getAdresseDao().findAdressesByIdUtilisateur(id);
        Adresse billAddress = new Adresse();

        for (Adresse adresse : adresses) {
            if (FieldsValuesDB.BILLING.equals(adresse.getTypeAdresse())) {
                billAddress = adresse;
                break;
            }
        }

        adresses.remove(billAddress);
        request.setAttribute("currentUser", user);
        request.setAttribute("currentUserAddress", adresses);
        request.setAttribute("currentUserBillAddress", billAddress);
    }

    private void addUser(final HttpServletRequest request) {

        if (handleMissingFieldAddUser(request) || handleAddress(request)) {
            request.setAttribute("typeModal", Display.SWAL_WARNING);
            return;
        }

        Utilisateur user = fillUserBean(request);
        user = serviceFacade.getUtilisateurDao().createUtilisateur(user);

        if (user.getIdUtilisateur() != null) {
            addAddress(request, user);
            request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_USER_SUCCESS);
            request.setAttribute("typeModal", Display.SWAL_SUCCESS);
        }
        else {
            request.setAttribute("typeModal", Display.SWAL_ERROR);
        }
    }

    private void addAddress(HttpServletRequest request, Utilisateur user) {
        List<Adresse> adresses = retrieveAddress(request, user);

        for (Adresse adresse : adresses) {
            serviceFacade.getAdresseDao().createAdresse(adresse);
        }
    }

    private Utilisateur fillUserBean(final HttpServletRequest request) {
        Utilisateur user = new Utilisateur();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String birth = request.getParameter("select-day") + "/" +
                request.getParameter("select-month") + "/" +
                request.getParameter("select-year");

        user.setActif(request.getParameter("actif") != null);
        user.setPrenom(request.getParameter("firstname"));
        user.setNom(request.getParameter("lastname"));
        user.setRole(new Role() {{ setIdRole(Integer.valueOf(request.getParameter("select-role"))); }});
        user.setCivilite("male".equals(request.getParameter("civilite")) ? FieldsValuesDB.CIVILITE_M : FieldsValuesDB.CIVILITE_F);
        user.setIdentifiant(request.getParameter("identifiant"));
        user.setMotPasse(request.getParameter("pass"));
        user.setDateCreation(new Date(System.currentTimeMillis()));
        user.setDateModification(new Date(System.currentTimeMillis()));

        try { user.setDateNaissance(dateFormat.parse(birth)); }
        catch (Exception e) { Logger.getGlobal().log(Level.WARNING, e.getMessage()); }

        return user;
    }

    private Adresse fillBillAddressBean(HttpServletRequest request, Utilisateur user) {
        Adresse adresse = new Adresse();
        adresse.setUtilisateur(user);
        adresse.setRue(request.getParameter("street-bill"));
        adresse.setCodePostal(request.getParameter("zipcode-bill"));
        adresse.setVille(request.getParameter("city-bill"));
        adresse.setPays(request.getParameter("country-bill"));
        adresse.setTypeAdresse(FieldsValuesDB.BILLING);
        return adresse;
    }

    private boolean handleMissingFieldAddUser(HttpServletRequest request) {

        String txt = Display.TEXT_MODAL_ADD_USER_ERROR;
        boolean handled = false;

        if (request.getParameter("select-role") == null) {
            txt = Display.TEXT_MODAL_ADD_USER_MISS_ROLE;
            handled = true;
        }
        else if (request.getParameter("select-day") == null || request.getParameter("select-month") == null
                || request.getParameter("select-year") == null ) {
            txt = Display.TEXT_MODAL_ADD_USER_MISS_BIRTH;
            handled = true;
        }
        else if (!request.getParameter("pass").equals(request.getParameter("pass-bis"))) {
            txt = Display.TEXT_MODAL_ADD_USER_ERROR_PASS;
            handled = true;
        }

        request.setAttribute("txtModal", txt);

        return handled;
    }

    private boolean handleAddress(HttpServletRequest request) {
        boolean handled = false;

        if (request.getParameter("checkShipAdr") != null && ("".equals(request.getParameter("street-ship")) ||
               "".equals(request.getParameter("zipcode-ship")) ||"".equals(request.getParameter("city-ship")) ||
               "".equals(request.getParameter("country-ship")))) {
            handled = true;
        }
        if (request.getParameter("checkBillAdr") != null && ("".equals(request.getParameter("street-bill")) ||
               "".equals(request.getParameter("zipcode-bill")) ||"".equals(request.getParameter("city-bill")) ||
               "".equals(request.getParameter("country-bill")))) {
            handled = true;
        }
        if (request.getParameter("checkBothAdr") != null && ("".equals(request.getParameter("street-bill-ship")) ||
               "".equals(request.getParameter("zipcode-bill-ship")) ||"".equals(request.getParameter("city-bill-ship")) ||
               "".equals(request.getParameter("country-bill-ship")))) {
            handled = true;
        }

        if (handled) request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_USER_MISS_ADR);

        return handled;
    }

    private List<Adresse> retrieveAddress(final HttpServletRequest request, final Utilisateur user) {

        List<Adresse> adresses = new ArrayList<>();

        if (request.getParameter("checkShipAdr") != null) {
            Adresse adresse = new Adresse();
            adresse.setUtilisateur(user);
            adresse.setRue(request.getParameter("street-ship"));
            adresse.setCodePostal(request.getParameter("zipcode-ship"));
            adresse.setVille(request.getParameter("city-ship"));
            adresse.setPays(request.getParameter("country-ship"));
            adresse.setPrincipale(true);
            adresse.setTypeAdresse(FieldsValuesDB.SHIPPING);
            adresses.add(adresse);
        }

        if (request.getParameter("checkBillAdr") != null) {
            adresses.add(fillBillAddressBean(request, user));
        }

        if (request.getParameter("checkBothAdr") != null) {
            adresses.add(new Adresse() {{
                setUtilisateur(user);
                setRue(request.getParameter("street-bill-ship"));
                setCodePostal(request.getParameter("zipcode-bill-ship"));
                setVille(request.getParameter("city-bill-ship"));
                setPays(request.getParameter("country-bill-ship"));
                setPrincipale(true);
                setTypeAdresse(FieldsValuesDB.SHIPPING);
            }});
            adresses.add(new Adresse() {{
                setUtilisateur(user);
                setRue(request.getParameter("street-bill-ship"));
                setCodePostal(request.getParameter("zipcode-bill-ship"));
                setVille(request.getParameter("city-bill-ship"));
                setPays(request.getParameter("country-bill-ship"));
                setTypeAdresse(FieldsValuesDB.BILLING);
            }});
        }

        return adresses;
    }

    private void updateUser(HttpServletRequest request) {
        handleAddressUpdate(request);

        if (handleMissingFieldAddUser(request)) {
            request.setAttribute("typeModal", Display.SWAL_WARNING);
            return;
        }

        Utilisateur user = fillUserBean(request);
        user.setIdUtilisateur(Integer.valueOf(request.getParameter("edit-user")));

        serviceFacade.getUtilisateurDao().updateUtilisateur(user);
        updateAddress(request);
        request.setAttribute("txtModal", Display.TEXT_MODAL_EDIT_USER_SUCCESS);
        request.setAttribute("typeModal", Display.SWAL_SUCCESS);

    }

    private void updateAddress(HttpServletRequest request) {
        Adresse adresse = fillBillAddressBean(request,
                serviceFacade.getUtilisateurDao().findUtilisateurById(Integer.parseInt(request.getParameter("edit-user"))));
        adresse.setIdAdresse(Integer.parseInt(request.getParameter("edit-user")));

        serviceFacade.getAdresseDao().updateAdresse(adresse);
    }

    private boolean handleAddressUpdate(HttpServletRequest request) {
        boolean handled = false;

        if ("".equals(request.getParameter("street-ship")) || "".equals(request.getParameter("zipcode-ship")) ||
                "".equals(request.getParameter("city-ship")) || "".equals(request.getParameter("country-ship"))) {
            handled = true;
        }
        if ("".equals(request.getParameter("street-bill")) || "".equals(request.getParameter("zipcode-bill")) ||
                "".equals(request.getParameter("city-bill")) || "".equals(request.getParameter("country-bill"))) {
            handled = true;
        }

        if (handled) request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_USER_MISS_ADR);

        return handled;
    }

    private void deleteAddress(int id) {
        Adresse adresse = serviceFacade.getAdresseDao().findAdresseById(id);
        serviceFacade.getAdresseDao().deleteAdresse(adresse);
    }

    private void deleteUser(HttpServletRequest request, int id) {
        Utilisateur user = serviceFacade.getUtilisateurDao().findUtilisateurById(id);

        request.setAttribute("responseDelUser", true);
        request.setAttribute("titleModal", Display.TITLE_MODAL_DEL_USER);

        if (user != null && user.getRole() != null && R_ADMIN.equals(user.getRole().getIdRole())) {
            request.setAttribute("txtModal", Display.TEXT_MODAL_DEL_USER_ROLE_ERROR);
            request.setAttribute("typeModal", Display.SWAL_WARNING);
            return;
        }

        if (serviceFacade.getUtilisateurDao().deleteUtilisateur(user)) {
            request.setAttribute("txtModal", Display.TEXT_MODAL_DEL_USER_SUCCESS);
            request.setAttribute("typeModal", Display.SWAL_SUCCESS);
        }
        else {
            request.setAttribute("txtModal", Display.TEXT_MODAL_DEL_USER_ERROR);
            request.setAttribute("typeModal", Display.SWAL_ERROR);
        }
    }
}
