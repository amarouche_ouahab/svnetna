Reponse question 1 :
Une injection SQL est une ou plusieurs méthodes d'exploitation d'une faille de sécurité d'une application interagissant avec une base de données,  L'attaquant détourne les requêtes en y injectant une chaîne non prévue par le développeur et pouvant compromettre la sécurité du système


Reponse question 2 :
PreparedStatement possede quelques méthodes qui permet de prendre en charge des paramètres, au contraire  dU Stetatement, qui lui permet d'excucter une requete SQL sans parametres et qui n'est pas dynamique.
L’utilisation de PreparedStatement rend l’application rapide car les requêtes paramètres sont compilées une seule fois et évite les injections SQL.


Reponse question 3 :

La jointure totale va prendre toutes les données qui correspondent à la condition qu'elles soient dans une table ou l'autre mais pas nécessairement dans les 2.

La jointure INNER correspond à l’intersection entre les deux tables, cette jointure rend donc la correspondance nécessaire pour obtenir des résultats.
Cette jointure permet de récupérer tous les résultats depuis la table A uniquement si elle possède une correspondance dans B.

La jointure LEFT JOIN  est un type de jointure entre 2 tables. Cela permet de lister tous les résultats de la table de gauche même s’il n’y a pas de correspondance dans la deuxième tables.
à l'inverse d'une jointure RIGHT JOIN.
