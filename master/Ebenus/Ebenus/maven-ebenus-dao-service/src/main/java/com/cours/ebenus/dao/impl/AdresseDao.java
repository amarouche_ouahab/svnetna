/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.impl;

import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.ConnectionHelper;
import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.IAdresseDao;
import com.cours.ebenus.dao.entities.Adresse;
import com.cours.ebenus.dao.entities.Role;
import com.cours.ebenus.dao.entities.Utilisateur;
import com.cours.ebenus.dao.exception.EbenusException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author ElHadji
 */
public class AdresseDao /*extends AbstractDao<Adresse>*/ implements IAdresseDao {

    private static final Log log = LogFactory.getLog(AdresseDao.class);
    private static Connection connection = DriverManagerSingleton.getInstance().getConnectionInstance();
    private static ConnectionHelper connectionHelper = new ConnectionHelper();

    //public AdresseDao() {
    //    super(Adresse.class);
    //}
    @Override
    public List<Adresse> findAllAdresses() {
        try {
            PreparedStatement stmp = connection.prepareStatement("SELECT * FROM Adresse a LEFT JOIN Utilisateur u ON a.`idUtilisateur` = u.`idUtilisateur` LEFT JOIN Role r ON u.idRole = r.idRole;");
            ResultSet result = stmp.executeQuery();
            List<Adresse> adresses = new ArrayList<Adresse>();

            while (result.next()) {
                adresses.add(createAdresseInstance(result));
            }

            connectionHelper.closeSqlResources(stmp, result);
            return adresses;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Adresse findAdresseById(int idAdresse) {
        try {
            PreparedStatement stmp = connection.prepareStatement("SELECT * FROM Adresse a LEFT JOIN Utilisateur u ON a.`idUtilisateur` = u.`idUtilisateur` LEFT JOIN Role r ON u.idRole = r.idRole WHERE a.idAdresse = ?;");
            stmp.setInt(1, idAdresse);
            ResultSet result = stmp.executeQuery();

            while (result.next()) {
                Adresse adresse = createAdresseInstance(result);
                connectionHelper.closeSqlResources(stmp, result);
                return adresse;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<Adresse> findAdressesByIdUtilisateur(int idUtilisateur) {

        try {
            PreparedStatement stmp = connection.prepareStatement("SELECT * FROM Adresse a LEFT JOIN Utilisateur u ON a.`idUtilisateur` = u.`idUtilisateur` LEFT JOIN Role r ON u.idRole = r.idRole WHERE a.idUtilisateur = ? ;");

            stmp.setInt(1, idUtilisateur);
            ResultSet result = stmp.executeQuery();
            List<Adresse> adresses = new ArrayList<Adresse>();

            while (result.next()) {
                adresses.add(createAdresseInstance(result));
                connectionHelper.closeSqlResources(stmp, result);
                return adresses;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public Adresse createAdresse(Adresse adresse) {
        if (adresse == null) {
            throw new EbenusException("adresse is null", -1);
        }
        try {
            Utilisateur user = adresse.getUtilisateur();
            PreparedStatement stmp = connection.prepareStatement("INSERT INTO Adresse(idUtilisateur, rue, codePostal, ville, pays, statut, typeAdresse, principale, version) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            Integer idUser = adresse.getUtilisateur().getIdUtilisateur();
            System.out.println( "====================================idUser");
            System.out.println( idUser);
            stmp.setInt(1, user.getIdUtilisateur());
            stmp.setString(2, adresse.getRue());
            stmp.setString(3, adresse.getCodePostal());
            stmp.setString(4, adresse.getVille());
            stmp.setString(5, adresse.getPays());
            stmp.setString(6, adresse.getStatut());
            stmp.setString(7, adresse.getTypeAdresse());
            stmp.setInt(8, adresse.getPrincipale() ? 1 : 0);
            stmp.setInt(9, adresse.getVersion() != null ? adresse.getVersion() : 0);
            stmp.executeUpdate();

            ResultSet resultSet = stmp.getGeneratedKeys();

            if (resultSet.next())
                adresse.setIdAdresse(resultSet.getInt(1));

            connectionHelper.closeSqlResources(stmp, resultSet);

            return adresse;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Adresse updateAdresse(Adresse adresse) {
        try {
            PreparedStatement stmp = connection.prepareStatement("UPDATE Adresse SET idUtilisateur = ?, rue = ?, codePostal = ?, ville = ?, pays = ?, statut = ?, typeAdresse = ?, principale = ?, version = ? WHERE idAdresse = ?");
            stmp.setInt(1, adresse.getUtilisateur().getIdUtilisateur());
            stmp.setString(2, adresse.getRue());
            stmp.setString(3, adresse.getCodePostal());
            stmp.setString(4, adresse.getVille());
            stmp.setString(5, adresse.getPays());
            stmp.setString(6, adresse.getStatut());
            stmp.setString(7, adresse.getTypeAdresse());
            stmp.setInt(8, adresse.getPrincipale() ? 1 : 0);
            stmp.setInt(9, adresse.getVersion());
            stmp.setInt(10, adresse.getIdAdresse());
            stmp.executeUpdate();

            connectionHelper.closeSqlResources(stmp, null);

            return adresse;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteAdresse(Adresse adresse) {
        try {
            PreparedStatement stmp = connection.prepareStatement("DELETE FROM Adresse WHERE idAdresse = ?");
            stmp.setInt(1, adresse.getIdAdresse());
            stmp.executeUpdate();

            connectionHelper.closeSqlResources(stmp, null);
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Adresse createAdresseInstance(ResultSet result) {
        try {
            return new Adresse(
                result.getInt(1), result.getString(3),
                result.getString(4), result.getString(5),
                result.getString(6), result.getString(7),
                result.getString(8), result.getInt(9) > 0 ? true : false,
                result.getInt(10),
                new Utilisateur(
                        result.getInt(11), result.getString(13),
                        result.getString(14), result.getString(15),
                        result.getString(16), result.getString(17),
                        result.getTimestamp(18), result.getTimestamp(19),
                        result.getTimestamp(20), result.getInt(21) > 0 ? true : false,
                        result.getInt(22) > 0 ? true : false, result.getInt(23),
                        new Role(
                                result.getInt(24), result.getString(25),
                                result.getString(26), result.getInt(27))
                ));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
