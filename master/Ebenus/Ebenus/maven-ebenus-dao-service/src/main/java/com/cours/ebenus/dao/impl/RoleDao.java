/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.impl;

import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.IRoleDao;
import com.cours.ebenus.dao.entities.Role;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static com.cours.ebenus.dao.ConnectionHelper.closeSqlResources;

/**
 *
 * @author ElHadji
 */
public class RoleDao /*extends AbstractDao<Role>*/ implements IRoleDao {

    private static final Log log = LogFactory.getLog(RoleDao.class);
    private static Connection connect = DriverManagerSingleton.getInstance().getConnectionInstance();

    //public RoleDao() {
    //    super(Role.class);
    //}
    @Override
    public List<Role> findAllRoles() {
        try {
            List<Role> roles = new ArrayList<Role>();
            Statement stmt = connect.createStatement();
            ResultSet roleDb = stmt.executeQuery("SELECT * FROM role");
            while(roleDb.next()){
                Role role = getRoleFromDataBase(roleDb);
                roles.add(role);
            }
            roleDb.close();
            stmt.close();
            return roles;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Role findRoleById(int idRole) {
        try {
            PreparedStatement stmt = connect.prepareStatement("SELECT * FROM role WHERE idRole = ?");
            stmt.setInt(1, idRole);
            ResultSet roleDb = stmt.executeQuery();
            while(roleDb.next()){
                Role role = getRoleFromDataBase(roleDb);
                closeSqlResources(stmt, roleDb);
                return role;
            }
            return null;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Role> findRoleByIdentifiant(String identifiantRole) {
        try {
            List<Role> roles = new ArrayList<Role>();
            PreparedStatement stmt = connect.prepareStatement("SELECT * FROM role WHERE identifiant = ?");
            stmt.setString(1, identifiantRole);
            ResultSet roleDb = stmt.executeQuery();
            while(roleDb.next()){
                Role role = getRoleFromDataBase(roleDb);
                roles.add(role);
            }
            closeSqlResources(stmt, roleDb);
            return roles;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Role createRole(Role role) {
        try {
            PreparedStatement stmt = connect.prepareStatement("INSERT INTO role(identifiant, description, version) VALUES (?, ?, ?)");
            stmt.setString(1, role.getIdentifiant());
            stmt.setString(2, role.getDescription());
            if(role.getVersion() != null){
                stmt.setInt(3, role.getVersion());
            }else{
                stmt.setInt(3, 0);
            }
            stmt.executeUpdate();
            PreparedStatement pstmt = connect.prepareStatement("SELECT * from role where identifiant= ? AND description = ? AND version = ?");
            pstmt.setString(1, role.getIdentifiant());
            pstmt.setString(2, role.getDescription());
            pstmt.setInt(3, 0);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                role.setIdRole(rs.getInt("idRole"));
            }
            return role;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Role updateRole(Role role) {
        try {
            PreparedStatement stmt = connect.prepareStatement("UPDATE role SET identifiant = ? , description = ? , version = ? WHERE idRole = ? ");
            stmt.setString(1, role.getIdentifiant());
            stmt.setString(2, role.getDescription());
            stmt.setInt(3, role.getVersion() + 1);
            stmt.setInt(4, role.getIdRole());
            stmt.executeUpdate();
            return role;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteRole(Role role) {
        try {
            PreparedStatement stmt = connect.prepareStatement("DELETE FROM role where idRole = ? ");
            stmt.setInt(1, role.getIdRole());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    @Override
    public boolean deleteRoleId(int id) {
        try {
            PreparedStatement stmt = connect.prepareStatement("DELETE FROM role where idRole = ? ");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private Role getRoleFromDataBase(ResultSet rs){
        try {
            Role role = new Role();
            role.setIdRole(rs.getInt("idRole"));
            role.setIdentifiant(rs.getString("identifiant"));
            role.setDescription(rs.getString("description"));
            role.setVersion(rs.getInt("version"));
            return role;
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private int newUserId(){
        try {
            Statement stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT idRole FROM role ORDER BY idRole DESC LIMIT 0, 1");
            while(rs.next()){
                return rs.getInt("idRole");
            }
            return 1;
        } catch (SQLException se){
            return 0;
        }
    }
}
