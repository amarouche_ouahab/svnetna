/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.impl;

import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.ConnectionHelper;
import com.cours.ebenus.dao.IArticleCommandeDao;
import com.cours.ebenus.dao.entities.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ElHadji
 */
public class ArticleCommandeDao /*extends AbstractDao<ArticleCommande>*/ implements IArticleCommandeDao {

    private static final Log log = LogFactory.getLog(ArticleCommandeDao.class);
    private static Connection connection = DriverManagerSingleton.getInstance().getConnectionInstance();
    private static ConnectionHelper connectionHelper = new ConnectionHelper();

    //public ArticleCommandeDao() {
    //    super(ArticleCommande.class);
    //}
    @Override
    public List<ArticleCommande> findAllArticlesCommande() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ArticleCommande ac LEFT JOIN Utilisateur u ON ac.`idUtilisateur` = u.`idUtilisateur` LEFT JOIN Role r ON u.idRole = r.idRole LEFT JOIN Adresse a ON ac.`idAdresse` = a.`idAdresse` LEFT JOIN Commande c ON ac.`idCommande` = c.`idCommande` LEFT JOIN Produit p ON ac.`idProduit` = p.`idProduit`;");
            ResultSet result = preparedStatement.executeQuery();
            List<ArticleCommande> articlesCommande = new ArrayList<ArticleCommande>();

            while (result.next()) {
                articlesCommande.add(createACInstance(result));
            }

            connectionHelper.closeSqlResources(preparedStatement, result);
            return articlesCommande;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ArticleCommande findArticleCommandeById(int idArticleCommande) {

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM ArticleCommande ac LEFT JOIN Utilisateur u ON ac.`idUtilisateur` = u.`idUtilisateur` LEFT JOIN Role r ON u.idRole = r.idRole LEFT JOIN Adresse a ON ac.`idAdresse` = a.`idAdresse` LEFT JOIN Commande c ON ac.`idCommande` = c.`idCommande` LEFT JOIN Produit p ON ac.`idProduit` = p.`idProduit` WHERE ac.idArticleCommande = ?;");
            preparedStatement.setInt(1, idArticleCommande);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                ArticleCommande articleCommande = createACInstance(result);
                connectionHelper.closeSqlResources(preparedStatement, result);
                return articleCommande;
            }

            connectionHelper.closeSqlResources(preparedStatement, result);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public ArticleCommande createArticleCommande(ArticleCommande articleCommande) {

        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            PreparedStatement stmp = connection.prepareStatement("INSERT INTO ArticleCommande(idCommande, idUtilisateur, idAdresse, idProduit, totalArticleCommande, reference, quantite, statut, dateModification, version) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmp.setInt(1, articleCommande.getCommande().getIdCommande());
            stmp.setInt(2, articleCommande.getUtilisateur().getIdUtilisateur());
            stmp.setInt(3, articleCommande.getAdresse().getIdAdresse());
            stmp.setInt(4, articleCommande.getProduit().getIdProduit());
            stmp.setDouble(5, articleCommande.getTotalArticleCommande());
            stmp.setString(6, articleCommande.getReference());
            stmp.setInt(7, articleCommande.getQuantite());
            stmp.setString(8, articleCommande.getStatut());
            stmp.setTimestamp(9, timestamp);
            stmp.setInt(10, articleCommande.getVersion() != null ? articleCommande.getVersion() : 0);
            stmp.executeUpdate();

            ResultSet result = stmp.getGeneratedKeys();

            if (result.next()){
                articleCommande.setIdArticleCommande(result.getInt(1));
            }
            articleCommande.setDateModification(timestamp);
            connectionHelper.closeSqlResources(stmp, result);
            return articleCommande;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ArticleCommande updateArticleCommande(ArticleCommande articleCommande) {
        try {
            Timestamp timestamp = new Timestamp(new java.util.Date().getTime());
            PreparedStatement stmp = connection.prepareStatement("UPDATE ArticleCommande SET idCommande = ?, idUtilisateur = ?, idAdresse = ?, idProduit = ?, totalArticleCommande = ?, reference = ?, quantite = ?, statut = ?, dateModification = ?," +
                    " version = ? WHERE idArticleCommande = ?");
            stmp.setInt(1, articleCommande.getCommande().getIdCommande());
            stmp.setInt(2, articleCommande.getUtilisateur().getIdUtilisateur());
            stmp.setInt(3, articleCommande.getAdresse().getIdAdresse());
            stmp.setInt(4, articleCommande.getProduit().getIdProduit());
            stmp.setDouble(5, articleCommande.getTotalArticleCommande());
            stmp.setString(6, articleCommande.getReference());
            stmp.setInt(7, articleCommande.getQuantite());
            stmp.setString(8, articleCommande.getStatut());
            stmp.setTimestamp(9, timestamp);
            stmp.setInt(10, articleCommande.getVersion() != null ? articleCommande.getVersion() : 0);
            stmp.setInt(11, articleCommande.getIdArticleCommande());
            stmp.executeUpdate();

            connectionHelper.closeSqlResources(stmp, null);
            articleCommande.setDateModification(timestamp);
            return articleCommande;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteArticleCommande(ArticleCommande articleCommande) {
        try {
            PreparedStatement stmp = connection.prepareStatement("DELETE FROM ArticleCommande WHERE idArticleCommande = ?");
            stmp.setInt(1, articleCommande.getIdArticleCommande());
            stmp.executeUpdate();

            connectionHelper.closeSqlResources(stmp, null);
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private ArticleCommande createACInstance(ResultSet result) {
        try {

            Produit produit = new Produit(
                    result.getInt(48), result.getString(49),
                    result.getDouble(50), result.getString(51),
                    result.getString(52), result.getInt(53),
                    result.getInt(54) > 0 ? true : false, result.getInt(55) > 0 ? true : false,
                    result.getInt(55));

            Utilisateur utilisateur = new Utilisateur(
                    result.getInt(12), result.getString(14),
                    result.getString(15), result.getString(16),
                    result.getString(17), result.getString(18),
                    result.getTimestamp(19), result.getTimestamp(20),
                    result.getTimestamp(21), result.getInt(22) > 0 ? true : false,
                    result.getInt(23) > 0 ? true : false, result.getInt(24),
                    new Role(
                            result.getInt(13), result.getString(26),
                            result.getString(27), result.getInt(29),
                            result.getInt(28) > 0 ? true : false)
            );

            Adresse adresse = new Adresse(
                    result.getInt(30), result.getString(32),
                    result.getString(33), result.getString(34),
                    result.getString(35), result.getString(36),
                    result.getString(37), result.getInt(38) > 0 ? true : false,
                    result.getInt(39), utilisateur);

             Commande commande = new Commande(
                    result.getInt(40), result.getDouble(41),
                    result.getString(44), result.getTimestamp(45),
                    result.getTimestamp(46), result.getInt(47),
                    utilisateur, adresse);

             return new ArticleCommande(
                     result.getInt(1), result.getDouble(6),
                     result.getString(7), result.getInt(8),
                     result.getString(9), result.getTimestamp(10),
                     result.getInt(11), commande,
                     utilisateur, adresse,
                     produit);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
