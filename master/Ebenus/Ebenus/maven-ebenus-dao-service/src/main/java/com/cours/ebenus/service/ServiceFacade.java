/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.service;

import com.cours.ebenus.dao.*;
import com.cours.ebenus.factory.AbstractDaoFactory;
import com.cours.ebenus.factory.AbstractDaoFactory.FactoryDaoType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ElHadji
 */
public class ServiceFacade implements IServiceFacade {


    private static final Log log = LogFactory.getLog(ServiceFacade.class);
    private final AbstractDaoFactory.FactoryDaoType DEFAULT_IMPLEMENTATION = AbstractDaoFactory.FactoryDaoType.JDBC_DAO_FACTORY;

    // On liste toutes les DAO : un DAO pour chaque entité (Utilisateur,Role ect ....)

    private IAdresseDao adresseDao = null;
    private IArticleCommandeDao articleCommandeDao = null;
    private ICommandeDao commandeDao = null;
    private IProduitDao produitDao = null;
    private IRoleDao roleDao = null;
    private IUtilisateurDao utilisateurDao = null;

    public ServiceFacade() {
        // mettre tous les DAO
        adresseDao = AbstractDaoFactory.getFactory(DEFAULT_IMPLEMENTATION).getAdresseDao();
        articleCommandeDao = AbstractDaoFactory.getFactory(DEFAULT_IMPLEMENTATION).getArticleCommandeDao();
        commandeDao = AbstractDaoFactory.getFactory(DEFAULT_IMPLEMENTATION).getCommandeDao();
        produitDao = AbstractDaoFactory.getFactory(DEFAULT_IMPLEMENTATION).getProduitDao();
        utilisateurDao = AbstractDaoFactory.getFactory(DEFAULT_IMPLEMENTATION).getUtilisateurDao();
        roleDao = AbstractDaoFactory.getFactory(DEFAULT_IMPLEMENTATION).getRoleDao();
    }

    public ServiceFacade(FactoryDaoType daoType) {
        // mettre tous les DAO
        adresseDao = AbstractDaoFactory.getFactory(daoType).getAdresseDao();
        articleCommandeDao = AbstractDaoFactory.getFactory(daoType).getArticleCommandeDao();
        commandeDao = AbstractDaoFactory.getFactory(daoType).getCommandeDao();
        produitDao = AbstractDaoFactory.getFactory(daoType).getProduitDao();
        utilisateurDao = AbstractDaoFactory.getFactory(daoType).getUtilisateurDao();
        roleDao = AbstractDaoFactory.getFactory(daoType).getRoleDao();
    }

    @Override
    public IAdresseDao getAdresseDao() {
        return adresseDao;
    }

    @Override
    public IArticleCommandeDao getArticleCommandeDao() {
        return articleCommandeDao;
    }

    @Override
    public ICommandeDao getCommandeDao() {
        return commandeDao;
    }

    @Override
    public IProduitDao getProduitDao() {
        return produitDao;
    }

    @Override
    public IUtilisateurDao getUtilisateurDao() {
        return utilisateurDao;
    }

    @Override
    public IRoleDao getRoleDao() {
        return roleDao;
    }
}
