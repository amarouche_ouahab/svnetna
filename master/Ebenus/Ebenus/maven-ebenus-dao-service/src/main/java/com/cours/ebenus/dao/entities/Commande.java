/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.entities;


import java.util.Date;

/**
 *
 * @author elhad
 */
public class Commande {

    private static final long serialVersionUID = 1L;
    private Integer idCommande;
    private Double totalCommande;
    private Utilisateur utilisateur;
    private Adresse adresse;
    private String statut;
    private Date dateCommande;
    private Date dateModification;
    private Integer version = 0;

    public Commande() {
    }

    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut, Date dateCommande, Date dateModification, Integer version) {
        this.idCommande = idCommande;
        this.totalCommande = totalCommande;
        this.utilisateur = utilisateur;
        this.adresse = adresse;
        this.statut = statut;
        this.dateCommande = dateCommande;
        this.dateModification = dateModification;
        this.version = version;
    }

    public Commande(Integer idCommande, Double totalCommande, String statut, Date dateCommande, Date dateModification, Integer version, Utilisateur utilisateur, Adresse adresse) {
        this.idCommande = idCommande;
        this.totalCommande = totalCommande;
        this.utilisateur = utilisateur;
        this.adresse = adresse;
        this.statut = statut;
        this.dateCommande = dateCommande;
        this.dateModification = dateModification;
        this.version = version;
    }

    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut, Date dateCommande, Date dateModification) {
        this(idCommande, totalCommande, utilisateur, adresse, statut, dateCommande, dateModification, 0);
    }

    public Commande(Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut, Date dateCommande, Date dateModification) {
        this(null, totalCommande, utilisateur, adresse, statut, dateCommande, dateModification, 0);
    }

    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut, Date dateCommande) {
        this(idCommande, totalCommande, utilisateur, adresse, statut, dateCommande, null, 0);
    }

    public Commande(Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut, Date dateCommande) {
        this(null, totalCommande, utilisateur, adresse, statut, dateCommande, null, 0);
    }

    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut) {
        this(idCommande, totalCommande, utilisateur, adresse, statut, null, null, 0);
    }

    public Commande(Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut) {
        this(null, totalCommande, utilisateur, adresse, statut, null, null, 0);
    }

    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur, Adresse adresse) {
        this(idCommande, totalCommande, utilisateur, adresse, null, null, null, 0);
    }

    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur) {
        this(idCommande, totalCommande, utilisateur, null, null, null, null, 0);
    }

    public Commande(Integer idCommande, Double totalCommande) {
        this(idCommande, totalCommande, null, null, null, null, null, 0);
    }

    public Commande(Integer idCommande) {
        this(idCommande, null, null, null, null, null, null, 0);
    }

    public Integer getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(Integer idCommande) {
        this.idCommande = idCommande;
    }

    public Double getTotalCommande() {
        return totalCommande;
    }

    public void setTotalCommande(Double totalCommande) {
        this.totalCommande = totalCommande;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCommande != null ? idCommande.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Commande)) {
            return false;
        }
        Commande other = (Commande) object;
        if ((this.idCommande == null && other.idCommande != null) || (this.idCommande != null && !this.idCommande.equals(other.idCommande))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "idCommande=" + idCommande +
                ", totalCommande=" + totalCommande +
                ", utilisateur=" + utilisateur +
                ", adresse=" + adresse +
                ", statut='" + statut + '\'' +
                ", dateCommande=" + dateCommande +
                ", dateModification=" + dateModification +
                ", version=" + version +
                '}';
    }
}
