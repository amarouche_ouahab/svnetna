/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.entities;

import java.util.Date;

/**
 *
 * @author elhad
 */
public class ArticleCommande {

    private static final long serialVersionUID = 1L;
    private Integer idArticleCommande;
    private Commande commande;
    private Utilisateur utilisateur;
    private Adresse adresse;
    private Produit produit;
    private Double totalArticleCommande = 0.0;
    private String reference;
    private Integer quantite = 0;
    private String statut;
    private Date dateModification;
    private Integer version = 0;

    public ArticleCommande() {
    }

    public ArticleCommande(Integer idArticleCommande, Commande commande, Utilisateur utilisateur, Adresse adresse, Produit produit, Double totalArticleCommande, String reference, Integer quantite, String statut, Date dateModification, Integer version) {
        this.idArticleCommande = idArticleCommande;
        this.commande = commande;
        this.utilisateur = utilisateur;
        this.adresse = adresse;
        this.produit = produit;
        this.totalArticleCommande = totalArticleCommande;
        this.reference = reference;
        this.quantite = quantite;
        this.statut = statut;
        this.dateModification = dateModification;
        this.version = version;
    }

    public ArticleCommande(Integer idArticleCommande, Double totalArticleCommande, String reference, Integer quantite, String statut, Date dateModification, Integer version, Commande commande, Utilisateur utilisateur, Adresse adresse, Produit produit) {
        this.idArticleCommande = idArticleCommande;
        this.commande = commande;
        this.utilisateur = utilisateur;
        this.adresse = adresse;
        this.produit = produit;
        this.totalArticleCommande = totalArticleCommande;
        this.reference = reference;
        this.quantite = quantite;
        this.statut = statut;
        this.dateModification = dateModification;
        this.version = version;
    }

    public ArticleCommande(Double totalArticleCommande, String reference, Integer quantite, String statut, Date dateModification, Integer version, Commande commande, Utilisateur utilisateur, Adresse adresse, Produit produit) {
        this(null, commande, utilisateur, adresse, produit, totalArticleCommande, reference, quantite, statut, dateModification, 0);

    }

    public ArticleCommande(Integer idArticleCommande, Commande commande, Utilisateur utilisateur, Adresse adresse, Produit produit, Double totalArticleCommande, String reference, Integer quantite, String statut, Date dateModification) {
        this(idArticleCommande, commande, utilisateur, adresse, produit, totalArticleCommande, reference, quantite, statut, dateModification, 0);
    }

    public ArticleCommande(Integer idArticleCommande, Commande commande, Utilisateur utilisateur, Adresse adresse, Produit produit, Double totalArticleCommande, String reference, Integer quantite, String statut) {
        this(idArticleCommande, commande, utilisateur, adresse, produit, totalArticleCommande, reference, quantite, statut, null, 0);
    }

    public ArticleCommande(Integer idArticleCommande, Commande commande, Utilisateur utilisateur, Adresse adresse, Produit produit, Double totalArticleCommande, String reference, Integer quantite) {
        this(idArticleCommande, commande, utilisateur, adresse, produit, totalArticleCommande, reference, quantite, null, null, 0);
    }

    public ArticleCommande(Integer idArticleCommande, Commande commande, Utilisateur utilisateur, Adresse adresse, Produit produit, Double totalArticleCommande, String reference) {
        this(idArticleCommande, commande, utilisateur, adresse, produit, totalArticleCommande, reference, 0, null, null, 0);
    }

    public ArticleCommande(Integer idArticleCommande, Commande commande, Utilisateur utilisateur, Adresse adresse, Produit produit, Double totalArticleCommande) {
        this(idArticleCommande, commande, utilisateur, adresse, produit, totalArticleCommande, null, 0, null, null, 0);
    }

    public ArticleCommande(Integer idArticleCommande, Commande commande, Utilisateur utilisateur, Adresse adresse, Produit produit) {
        this(idArticleCommande, commande, utilisateur, adresse, produit, null, null, 0, null, null, 0);
    }

    public ArticleCommande(Integer idArticleCommande, Commande commande, Utilisateur utilisateur, Adresse adresse) {
        this(idArticleCommande, commande, utilisateur, adresse, null, null, null, 0, null, null, 0);
    }

    public ArticleCommande(Integer idArticleCommande, Commande commande, Utilisateur utilisateur) {
        this(idArticleCommande, commande, utilisateur, null, null, null, null, 0, null, null, 0);
    }

    public ArticleCommande(Integer idArticleCommande, Commande commande) {
        this(idArticleCommande, commande, null, null, null, null, null, 0, null, null, 0);
    }

    public ArticleCommande(Integer idArticleCommande) {
        this(idArticleCommande, null, null, null, null, null, null, 0, null, null, 0);
    }

    public Integer getIdArticleCommande() {
        return idArticleCommande;
    }

    public void setIdArticleCommande(Integer idArticleCommande) {
        this.idArticleCommande = idArticleCommande;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Double getTotalArticleCommande() {
        return totalArticleCommande;
    }

    public void setTotalArticleCommande(Double totalArticleCommande) {
        this.totalArticleCommande = totalArticleCommande;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

//    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut, Date dateCommande, Date dateModification, Integer version) {
//        this.idCommande = idCommande;
//        this.totalCommande = totalCommande;
//        this.utilisateur = utilisateur;
//        this.adresse = adresse;
//        this.statut = statut;
//        this.dateCommande = dateCommande;
//        this.dateModification = dateModification;
//        this.version = version;
//    }

//    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut, Date dateCommande, Date dateModification) {
//        this(idCommande, totalCommande, utilisateur, adresse, statut, dateCommande, dateModification, 0);
//    }
//
//    public Commande(Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut, Date dateCommande, Date dateModification) {
//        this(null, totalCommande, utilisateur, adresse, statut, dateCommande, dateModification, 0);
//    }
//
//    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut, Date dateCommande) {
//        this(idCommande, totalCommande, utilisateur, adresse, statut, dateCommande, null, 0);
//    }
//
//    public Commande(Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut, Date dateCommande) {
//        this(null, totalCommande, utilisateur, adresse, statut, dateCommande, null, 0);
//    }
//
//    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut) {
//        this(idCommande, totalCommande, utilisateur, adresse, statut, null, null, 0);
//    }
//
//    public Commande(Double totalCommande, Utilisateur utilisateur, Adresse adresse, String statut) {
//        this(null, totalCommande, utilisateur, adresse, statut, null, null, 0);
//    }
//
//    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur, Adresse adresse) {
//        this(idCommande, totalCommande, utilisateur, adresse, null, null, null, 0);
//    }
//
//    public Commande(Integer idCommande, Double totalCommande, Utilisateur utilisateur) {
//        this(idCommande, totalCommande, utilisateur, null, null, null, null, 0);
//    }
//
//    public Commande(Integer idCommande, Double totalCommande) {
//        this(idCommande, totalCommande, null, null, null, null, null, 0);
//    }
//
//    public Commande(Integer idCommande) {
//        this(idCommande, null, null, null, null, null, null, 0);
//    }
//
//    public Integer getIdCommande() {
//        return idCommande;
//    }
//
//    public void setIdCommande(Integer idCommande) {
//        this.idCommande = idCommande;
//    }
//
//    public Double getTotalCommande() {
//        return totalCommande;
//    }
//
//    public void setTotalCommande(Double totalCommande) {
//        this.totalCommande = totalCommande;
//    }
//
//    public Utilisateur getUtilisateur() {
//        return utilisateur;
//    }
//
//    public void setUtilisateur(Utilisateur utilisateur) {
//        this.utilisateur = utilisateur;
//    }
//
//    public Adresse getAdresse() {
//        return adresse;
//    }
//
//    public void setAdresse(Adresse adresse) {
//        this.adresse = adresse;
//    }
//
//    public String getStatut() {
//        return statut;
//    }
//
//    public void setStatut(String statut) {
//        this.statut = statut;
//    }
//
//    public Date getDateCommande() {
//        return dateCommande;
//    }
//
//    public void setDateCommande(Date dateCommande) {
//        this.dateCommande = dateCommande;
//    }
//
//    public Date getDateModification() {
//        return dateModification;
//    }
//
//    public void setDateModification(Date dateModification) {
//        this.dateModification = dateModification;
//    }
//
//    public Integer getVersion() {
//        return version;
//    }
//
//    public void setVersion(Integer version) {
//        this.version = version;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idArticleCommande != null ? idArticleCommande.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ArticleCommande)) {
            return false;
        }
        ArticleCommande other = (ArticleCommande) object;
        if ((this.idArticleCommande == null && other.idArticleCommande != null) || (this.idArticleCommande != null && !this.idArticleCommande.equals(other.idArticleCommande))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ArticleCommande{" +
                "idArticleCommande=" + idArticleCommande +
                ", commande=" + commande +
                ", utilisateur=" + utilisateur +
                ", adresse=" + adresse +
                ", produit=" + produit +
                ", totalArticleCommande=" + totalArticleCommande +
                ", reference='" + reference + '\'' +
                ", quantite=" + quantite +
                ", statut='" + statut + '\'' +
                ", dateModification=" + dateModification +
                ", version=" + version +
                '}';
    }
}
