/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.entities;

/**
 *
 * @author elhad
 */
public class Produit {

//    private static final long serialVersionUID = 1L;
    private Integer idProduit;
    private String reference;
    private Double prix;
    private String nom;
    private String description;
    private Integer stock = 0;
    private Boolean active = true;
    private Boolean marquerEffacer = false;
    private Integer version = 0;

    public Produit(){
    }

    public Produit(Integer idProduit, String reference, Double prix, String nom, String description, Integer stock, Boolean active, Boolean marquerEffacer, Integer version) {
        this.idProduit = idProduit;
        this.reference = reference;
        this.prix = prix;
        this.nom = nom;
        this.description = description;
        this.stock = stock;
        this.active = active;
        this.marquerEffacer = marquerEffacer;
        this.version = version;
    }

    public Produit(Integer idProduit, String reference, Double prix, String nom, String description, Integer stock, Boolean active, Boolean marquerEffacer) {
        this(idProduit, reference, prix, nom, description, stock, active, marquerEffacer, 0);
    }

    public Produit(String reference, Double prix, String nom, String description, Integer stock, Boolean active, Boolean marquerEffacer) {
        this(null, reference, prix, nom, description, stock, active, marquerEffacer, 0);
    }

    public Produit(Integer idProduit, String reference, Double prix, String nom, String description, Integer stock, Boolean active) {
        this(idProduit, reference, prix, nom, description, stock, active, false, 0);
    }

    public Produit(Integer idProduit, String reference, Double prix, String nom, String description, Integer stock) {
        this(idProduit, reference, prix, nom, description, stock, true, false, 0);
    }

    public Produit(String reference, Double prix, String nom, String description, Integer stock) {
        this(null, reference, prix, nom, description, stock, true, false, 0);
    }

    public Produit(Integer idProduit, String reference, Double prix, String nom, String description) {
        this(idProduit, reference, prix, nom, description, 0, true, false, 0);
    }

    public Produit(Integer idProduit, String reference, Double prix, String nom) {
        this(idProduit, reference, prix, nom, null, 0, true, false, 0);
    }

    public Produit(Integer idProduit, String reference, Double prix) {
        this(idProduit, reference, prix, null, null, 0, true, false, 0);
    }

    public Produit(Integer idProduit, String reference) {
        this(idProduit, reference, 0.0, null, null, 0, true, false, 0);
    }

    public Produit(Integer idProduit) {
        this(idProduit, null, 0.0, null, null, 0, true, false, 0);
    }

    public Integer getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Integer idProduit) {
        this.idProduit = idProduit;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getMarquerEffacer() {
        return marquerEffacer;
    }

    public void setMarquerEffacer(Boolean marquerEffacer) {
        this.marquerEffacer = marquerEffacer;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Produit{" +
                "idProduit=" + idProduit +
                ", reference='" + reference + '\'' +
                ", prix=" + prix +
                ", nom='" + nom + '\'' +
                ", description='" + description + '\'' +
                ", stock=" + stock +
                ", active=" + active +
                ", marquerEffacer=" + marquerEffacer +
                ", version=" + version +
                '}';
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProduit != null ? idProduit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Produit)) {
            return false;
        }
        Produit other = (Produit) object;
        if ((this.idProduit == null && other.idProduit != null) || (this.idProduit != null && !this.idProduit.equals(other.idProduit))) {
            return false;
        }
        return true;
    }

}
