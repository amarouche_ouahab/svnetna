/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.impl;

import com.cours.ebenus.dao.ConnectionHelper;
import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.ICommandeDao;
import com.cours.ebenus.dao.entities.Adresse;
import com.cours.ebenus.dao.entities.Commande;
import com.cours.ebenus.dao.entities.Role;
import com.cours.ebenus.dao.entities.Utilisateur;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ElHadji
 */
public class CommandeDao /*extends AbstractDao<Commande>*/ implements ICommandeDao {

    private static final Log log = LogFactory.getLog(CommandeDao.class);
    private static Connection connection = DriverManagerSingleton.getInstance().getConnectionInstance();
    private static ConnectionHelper connectionHelper = new ConnectionHelper();

    //public CommandeDao() {
    //    super(Commande.class);
    //}
    @Override
    public List<Commande> findAllCommandes() {
        try {
            PreparedStatement stmp = connection.prepareStatement("SELECT * FROM Commande c LEFT JOIN Utilisateur u ON c.`idUtilisateur` = u.`idUtilisateur` LEFT JOIN Role r ON u.idRole = r.idRole LEFT JOIN Adresse a ON c.`idAdresse` = a.`idAdresse`;");
            ResultSet result = stmp.executeQuery();
            List<Commande> commandes = new ArrayList<Commande>();

            while (result.next()) {
                commandes.add(createCommandeInstance(result));
            }

            connectionHelper.closeSqlResources(stmp, result);
            return commandes;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Commande findCommandeById(int idCmd) {
        try {
            PreparedStatement stmp = connection.prepareStatement("SELECT * FROM Commande c LEFT JOIN Utilisateur u ON c.`idUtilisateur` = u.`idUtilisateur` LEFT JOIN Role r ON u.idRole = r.idRole LEFT JOIN Adresse a ON c.`idAdresse` = a.`idAdresse` WHERE c.idCommande = ?;");
            stmp.setInt(1, idCmd);
            ResultSet result = stmp.executeQuery();

            while (result.next()) {
                Commande commande = createCommandeInstance(result);
                connectionHelper.closeSqlResources(stmp, result);
                return commande;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Commande createCommande(Commande commande) {
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            PreparedStatement stmp = connection.prepareStatement("INSERT INTO Commande(totalCommande, idUtilisateur, idAdresse, statut, dateCommande, dateModification, version) VALUES (?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmp.setDouble(1, commande.getTotalCommande());
            stmp.setInt(2, commande.getUtilisateur().getIdUtilisateur());
            stmp.setInt(3, commande.getAdresse().getIdAdresse());
            stmp.setString(4, commande.getStatut());
            stmp.setTimestamp(5, timestamp);
            stmp.setTimestamp(6, timestamp);
            stmp.setInt(7, commande.getVersion());

            stmp.executeUpdate();
            ResultSet result = stmp.getGeneratedKeys();
            if (result.next()){
                commande.setIdCommande(result.getInt(1));
            }
            commande.setDateModification(timestamp);
            connectionHelper.closeSqlResources(stmp, result);
            return commande;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Commande updateCommande(Commande commande) {
        try {
            Timestamp timestamp = new Timestamp(new java.util.Date().getTime());
            PreparedStatement stmp = connection.prepareStatement("UPDATE Commande SET totalCommande = ?, idUtilisateur = ?, idAdresse = ?, statut = ?, dateCommande = ?, " +
                    "dateModification = ?, version = ? WHERE idCommande = ?");
            stmp.setDouble(1, commande.getTotalCommande());
            stmp.setInt(2, commande.getUtilisateur().getIdUtilisateur());
            stmp.setInt(3, commande.getAdresse().getIdAdresse());
            stmp.setString(4, commande.getStatut());
            stmp.setTimestamp(5, new Timestamp(commande.getDateCommande().getTime()));
            stmp.setTimestamp(6, timestamp);
            stmp.setInt(7, commande.getVersion());
            stmp.setInt(8, commande.getIdCommande());
            stmp.executeUpdate();

            connectionHelper.closeSqlResources(stmp, null);
            commande.setDateModification(timestamp);
            return commande;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteCommande(Commande commande) {
        try {
            PreparedStatement stmp = connection.prepareStatement("DELETE FROM Commande WHERE idCommande = ?");
            stmp.setInt(1, commande.getIdCommande());
            stmp.executeUpdate();

            connectionHelper.closeSqlResources(stmp, null);
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Commande createCommandeInstance(ResultSet result) {
        try {
            Utilisateur utilisateur = new Utilisateur(
                    result.getInt(9), result.getString(11),
                    result.getString(12), result.getString(13),
                    result.getString(14), result.getString(15),
                    result.getTimestamp(16), result.getTimestamp(17),
                    result.getTimestamp(18), result.getInt(19) > 0 ? true : false,
                    result.getInt(20) > 0 ? true : false, result.getInt(21),
                    new Role(
                            result.getInt(22), result.getString(23),
                            result.getString(24), result.getInt(26),
                            result.getInt(25) > 0 ? true : false)
            );

            Adresse adresse = new Adresse(
                    result.getInt(27), result.getString(29),
                    result.getString(30), result.getString(31),
                    result.getString(32), result.getString(33),
                    result.getString(34), result.getInt(35) > 0 ? true : false,
                    result.getInt(36), utilisateur);

            return new Commande(
                    result.getInt(1), result.getDouble(2),
                    result.getString(5), result.getTimestamp(6),
                    result.getTimestamp(7),result.getInt(8),
                    utilisateur, adresse);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
