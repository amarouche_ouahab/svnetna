/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.factory;

import com.cours.ebenus.dao.IRoleDao;
import com.cours.ebenus.dao.IUtilisateurDao;
import com.cours.ebenus.dao.IAdresseDao;
import com.cours.ebenus.dao.IArticleCommandeDao;
import com.cours.ebenus.dao.ICommandeDao;
import com.cours.ebenus.dao.IProduitDao;
import com.cours.ebenus.dao.impl.RoleDao;
import com.cours.ebenus.dao.impl.UtilisateurDao;
import com.cours.ebenus.dao.impl.AdresseDao;
import com.cours.ebenus.dao.impl.ArticleCommandeDao;
import com.cours.ebenus.dao.impl.CommandeDao;
import com.cours.ebenus.dao.impl.ProduitDao;

/**
 *
 * @author ElHadji
 */
public class DaoFactory extends AbstractDaoFactory {

    @Override
    public IUtilisateurDao getUtilisateurDao() {
        return new UtilisateurDao();
    }

    @Override
    public IRoleDao getRoleDao() {
        return new RoleDao();
    }
    @Override
    public IAdresseDao getAdresseDao() {
        return new AdresseDao();
    }

    @Override
    public IArticleCommandeDao getArticleCommandeDao() {
        return new ArticleCommandeDao();
    }

    @Override
    public ICommandeDao getCommandeDao() {
        return new CommandeDao();
    }

    @Override
    public IProduitDao getProduitDao() {
        return new ProduitDao();
    }

}
