/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao;

import com.cours.ebenus.dao.entities.ArticleCommande;

import java.util.List;

/**
 *
 * @author ElHadji
 */
public interface IArticleCommandeDao {

    public List<ArticleCommande> findAllArticlesCommande();

    public ArticleCommande findArticleCommandeById(int idArticleCommande);

    public ArticleCommande createArticleCommande(ArticleCommande articleCommande);

    public ArticleCommande updateArticleCommande(ArticleCommande articleCommande);


    public boolean deleteArticleCommande(ArticleCommande articleCommande);

}