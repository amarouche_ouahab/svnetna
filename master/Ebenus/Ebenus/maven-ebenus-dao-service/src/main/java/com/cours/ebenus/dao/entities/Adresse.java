/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.entities;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 *
 * @author elhad
 */
public class Adresse {

    //    private static final long serialVersionUID = 1L;
    private Integer idAdresse;
    private Utilisateur utilisateur;
    private String rue;
    private String codePostal;
    private String ville;
    private String pays;
    private String statut;
    private String typeAdresse;
    private Boolean principale = false;
    private Integer version = 0;

    public Adresse(){
    }

    public Adresse(Integer idAdresse, Utilisateur utilisateur, String rue, String codePostal, String ville, String pays, String statut, String typeAdresse, Boolean principale, Integer version) {
        this.idAdresse = idAdresse;
        this.utilisateur = utilisateur;
        this.rue = rue;
        this.codePostal = codePostal;
        this.ville = ville;
        this.pays = pays;
        this.statut = statut;
        this.typeAdresse = typeAdresse;
        this.principale = principale;
        this.version = version;
    }

    public Adresse(Integer idAdresse, String rue, String codePostal, String ville, String pays, String statut, String typeAdresse, Boolean principale, Integer version, Utilisateur utilisateur) {
        this.idAdresse = idAdresse;
        this.utilisateur = utilisateur;
        this.rue = rue;
        this.codePostal = codePostal;
        this.ville = ville;
        this.pays = pays;
        this.statut = statut;
        this.typeAdresse = typeAdresse;
        this.principale = principale;
        this.version = version;
    }

    public Adresse(Integer idAdresse, Utilisateur utilisateur, String rue, String codePostal, String ville, String pays, String statut, String typeAdresse, Boolean principale) {
        this(idAdresse, utilisateur, rue, codePostal, ville, pays, statut, typeAdresse, principale, 0);
    }

    public Adresse(Utilisateur utilisateur, String rue, String codePostal, String ville, String pays, String statut, String typeAdresse, Boolean principale) {
        this(null, utilisateur, rue, codePostal, ville, pays, statut, typeAdresse, principale, 0);
    }

    public Adresse(Integer idAdresse, Utilisateur utilisateur, String rue, String codePostal, String ville, String pays, String statut, String typeAdresse) {
        this(idAdresse, utilisateur, rue, codePostal, ville, pays, statut, typeAdresse, false, 0);
    }

    public Adresse(Integer idAdresse, Utilisateur utilisateur, String rue, String codePostal, String ville, String pays, String statut) {
        this(idAdresse, utilisateur, rue, codePostal, ville, pays, statut, null, false, 0);
    }

    public Adresse(Integer idAdresse, Utilisateur utilisateur, String rue, String codePostal, String ville, String pays) {
        this(idAdresse, utilisateur, rue, codePostal, ville, pays, null, null, false, 0);
    }

    public Adresse(Integer idAdresse, Utilisateur utilisateur, String rue, String codePostal, String ville) {
        this(idAdresse, utilisateur, rue, codePostal, ville, null, null, null, false, 0);
    }

    public Adresse(Integer idAdresse, Utilisateur utilisateur, String rue, String codePostal) {
        this(idAdresse, utilisateur, rue, codePostal, null, null, null, null, false, 0);
    }

    public Adresse(Integer idAdresse, Utilisateur utilisateur, String rue) {
        this(idAdresse, utilisateur, rue, null, null, null, null, null, false, 0);
    }

    public Adresse(Integer idAdresse, Utilisateur utilisateur) {
        this(idAdresse, utilisateur, null, null, null, null, null, null, false, 0);
    }

    public Adresse(Integer idAdresse) {
        this(idAdresse, null, null, null, null, null, null, null, false, 0);
    }

    public Integer getIdAdresse() {
        return idAdresse;
    }

    public void setIdAdresse(Integer idAdresse) {
        this.idAdresse = idAdresse;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getTypeAdresse() {
        return typeAdresse;
    }

    public void setTypeAdresse(String typeAdresse) {
        this.typeAdresse = typeAdresse;
    }

    public Boolean getPrincipale() {
        return principale;
    }

    public void setPrincipale(Boolean principale) {
        this.principale = principale;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAdresse != null ? idAdresse.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Adresse)) {
            return false;
        }
        Adresse other = (Adresse) object;
        if ((this.idAdresse == null && other.idAdresse != null) || (this.idAdresse != null && !this.idAdresse.equals(other.idAdresse))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Adresse{" +
                "idAdresse=" + idAdresse +
                ", utilisateur=" + utilisateur +
                ", rue='" + rue + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", ville='" + ville + '\'' +
                ", pays='" + pays + '\'' +
                ", statut='" + statut + '\'' +
                ", typeAdresse='" + typeAdresse + '\'' +
                ", principale=" + principale +
                ", version=" + version +
                '}';
    }
}
