package com.cours.ebenus.dao.test;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.entities.*;
import com.cours.ebenus.dao.exception.EbenusException;
import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;
import com.cours.ebenus.utils.Constants;

import java.io.*;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.ibatis.common.jdbc.ScriptRunner;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class JUnitQuestEbenus {

    public void verifyUsersDatas(List<Utilisateur> utilisateurs) {
        log.debug("Entree de la methode");
        if (utilisateurs != null) {
            log.debug("utilisateurs.size(): " + utilisateurs.size());
            for (Utilisateur user : utilisateurs) {
                verifyUserDatas(user);
            }
        } else if (utilisateurs == null || utilisateurs.isEmpty()) {
            Assert.fail("Aucun utilisateur n'a ete trouves dans votre liste");
        }
        log.debug("Sortie de la methode");
    }

    public void verifyAdresseData(Adresse adresse) {
        log.debug("Entree de la methode");
        if (adresse != null) {
            log.debug("idAdresse : " + adresse.getIdAdresse());
            Assert.assertNotNull(adresse.getRue());
            Assert.assertNotNull(adresse.getCodePostal());
            Assert.assertNotNull(adresse.getVille());
            Assert.assertNotNull(adresse.getPays());
            Assert.assertNotNull(adresse.getStatut());
            Assert.assertNotNull(adresse.getTypeAdresse());
        } else if (adresse == null) {
            Assert.fail("Adresse null");
        }
        log.debug("Sortie de la methode");
    }

    public void verifyProduitData(Produit produit) {
        log.debug("Entree de la methode");
        if (produit != null) {
            log.debug("idProduit : " + produit.getIdProduit());
            Assert.assertNotNull(produit.getNom());
            Assert.assertNotNull(produit.getVersion());
            Assert.assertNotNull(produit.getActive());
            Assert.assertNotNull(produit.getDescription());
            Assert.assertNotNull(produit.getMarquerEffacer());
            Assert.assertNotNull(produit.getPrix());
            Assert.assertNotNull(produit.getStock());
            Assert.assertNotNull(produit.getReference());
        } else if (produit == null) {
            Assert.fail("Produit null");
        }
        log.debug("Sortie de la methode");
    }

    public void verifyCommandeData(Commande commande) {
        log.debug("Entree de la methode");
        if (commande != null) {
            log.debug("idCommande : " + commande.getIdCommande());
            Assert.assertNotNull(commande.getAdresse());
            Assert.assertNotNull(commande.getStatut());
            Assert.assertNotNull(commande.getTotalCommande());
            Assert.assertNotNull(commande.getUtilisateur());
            Assert.assertNotNull(commande.getVersion());
            Assert.assertNotNull(commande.getDateCommande());
            Assert.assertNotNull(commande.getDateModification());
        } else if (commande == null) {
            Assert.fail("Commande null");
        }
        log.debug("Sortie de la methode");
    }

    public void verifyCommandeArticleCommande(ArticleCommande articleCommande) {
        log.debug("Entree de la methode");
        if (articleCommande != null) {
            log.debug("idArticleCommande : " + articleCommande.getIdArticleCommande());
            Assert.assertNotNull(articleCommande.getAdresse());
            Assert.assertNotNull(articleCommande.getVersion());
            Assert.assertNotNull(articleCommande.getQuantite());
            Assert.assertNotNull(articleCommande.getProduit());
            Assert.assertNotNull(articleCommande.getReference());
            Assert.assertNotNull(articleCommande.getTotalArticleCommande());
            Assert.assertNotNull(articleCommande.getUtilisateur());
            Assert.assertNotNull(articleCommande.getStatut());
            Assert.assertNotNull(articleCommande.getDateModification());
            Assert.assertNotNull(articleCommande.getCommande());
        } else if (articleCommande == null) {
            Assert.fail("ArticleCommande null");
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testFindAllUtilisateurs() {
        log.debug("Entree de la methode");
        if (utilisateurs != null) {
            log.debug("NB_UTILISATEURS_LIST: " + NB_UTILISATEURS_LIST + " , utilisateurs.size(): " + utilisateurs.size());
            Assert.assertEquals(NB_UTILISATEURS_LIST, utilisateurs.size());
            verifyUsersDatas(utilisateurs);
        } else {
            Assert.fail("Aucun utilisateur n'a ete trouves dans votre base de données");
        }
        log.debug("Sortie de la methode");
    }

    private static final Log log = LogFactory.getLog(JUnitQuestEbenus.class);
    private static IServiceFacade serviceFacade = null;
    // Compter le nombre d'utilisateurs et de roles dans votre base de données.
    private static final int NB_UTILISATEURS_LIST = 18;
    private static final int NB_ROLES_LIST = 6;
    private static final int NB_ADRESSES_LIST = 40;
    private static final int NB_PRODUITS_LIST = 35;
    private static final int NB_COMMANDES_LIST = 18;
    private static final int NB_ARTICLES_COMMANDE_LIST = 46;

    private static final String UTILISATEUR_FIND_BY_PRENOM = "Mathieu";
    private static final int NB_UTILISATEURS_FIND_BY_PRENOM = 2;

    private static final String UTILISATEUR_FIND_BY_NOM = "Cassas";
    private static final int NB_UTILISATEURS_FIND_BY_NOM = 2;

    private static final String UTILISATEUR_FIND_BY_IDENTIFIANT_ROLE_STANDARD = "Standard";
    private static final int NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_STANDARD = 7;

    private static final String UTILISATEUR_FIND_BY_IDENTIFIANT_ROLE_ACHETEUR = "Acheteur";
    private static final int NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_ACHETEUR = 8;

    private static final String UTILISATEUR_FIND_BY_IDENTIFIANT_ROLE_ADMIN = "Administrateur";
    private static final int NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_ADMIN = 1;

    private static final int NB_ROLES_FIND_BY_IDENTIFIANT_ADMIN = 1;
    private static final String ROLES_FIND_BY_IDENTIFIANT_ADMIN = "Administrateur";

    private static final int NB_ROLES_FIND_BY_IDENTIFIANT_ACHETEUR = 1;
    private static final String ROLES_FIND_BY_IDENTIFIANT_ACHETEUR = "Acheteur";

    private static final int NB_ROLES_FIND_BY_IDENTIFIANT_STANDARD = 1;
    private static final String ROLES_FIND_BY_IDENTIFIANT_STANDARD = "Standard";

    private static List<Utilisateur> utilisateurs = null;
    private static List<Role> roles = null;
    private static List<Adresse> adresses = null;
    private static List<Produit> produits = null;
    private static List<Commande> commandes = null;
    private static List<ArticleCommande> articlesCommande = null;


    @BeforeClass
    public static void init() throws Exception {
        initDataBase();
        serviceFacade = new ServiceFacade();
        utilisateurs = serviceFacade.getUtilisateurDao().findAllUtilisateurs();
        roles = serviceFacade.getRoleDao().findAllRoles();
        adresses = serviceFacade.getAdresseDao().findAllAdresses();
        produits = serviceFacade.getProduitDao().findAllProduits();
        commandes = serviceFacade.getCommandeDao().findAllCommandes();
        articlesCommande = serviceFacade.getArticleCommandeDao().findAllArticlesCommande();
    }

    @BeforeClass
    public static void initDataBase() throws FileNotFoundException, IOException, SQLException {
        ScriptRunner runner = new ScriptRunner(DriverManagerSingleton.getInstance().getConnectionInstance(), false, true);
        Reader reader = new BufferedReader(new FileReader(Constants.SQL_JUNIT_PATH_FILE));
        runner.runScript(reader);
        //String scriptSqlPath = Constants.SQL_JUNIT_PATH_FILE;
    }

    public void verifyRoleData(Role role) {
        log.debug("Entree de la methode");
        if (role != null) {
            log.debug("idRole : " + role.getIdRole());
            Assert.assertNotNull(role.getIdRole());
            Assert.assertNotNull(role.getIdentifiant());
            Assert.assertNotNull(role.getDescription());
        } else if (role == null) {
            Assert.fail("Role null");
        }
        log.debug("Sortie de la methode");
    }

    public void verifyUserDatas(Utilisateur user) {
        log.debug("Entree de la methode");
        if (user != null) {
            log.debug("idUtilisateur : " + user.getIdUtilisateur());
            Assert.assertNotNull(user.getIdUtilisateur());
            Assert.assertNotNull(user.getPrenom());
            Assert.assertNotNull(user.getNom());
            Assert.assertNotNull(user.getRole());
            Assert.assertNotNull(user.getRole().getIdRole());
            Assert.assertNotNull(user.getRole().getIdentifiant());
            Assert.assertNotNull(user.getRole().getDescription());
        } else if (user == null) {
            Assert.fail("Utilisateur null");
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testFindAllRoles() {
        log.debug("Entree de la methode");
        if (roles != null) {
            log.debug("NB_ROLES_LIST: " + NB_ROLES_LIST + " , roles.size(): " + roles.size());
            Assert.assertEquals(NB_ROLES_LIST, roles.size());
        } else {
            Assert.fail("Aucun Role n'a ete trouves dans votre base de données");
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testFindAllAdresses() {
        log.debug("Entree de la methode");
        if (roles != null) {
            log.debug("NB_ADRESSES_LIST: " + NB_ADRESSES_LIST + " , adresses.size(): " + adresses.size());
            Assert.assertEquals(NB_ADRESSES_LIST, adresses.size());
        } else {
            Assert.fail("Aucun adresse n'a ete trouves dans votre base de données");
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testFindAllProduits() {
        log.debug("Entree de la methode");
        if (roles != null) {
            log.debug("NB_PRODUITS_LIST: " + NB_PRODUITS_LIST + " , produits.size(): " + produits.size());
            Assert.assertEquals(NB_PRODUITS_LIST, produits.size());
        } else {
            Assert.fail("Aucun Produits n'a ete trouves dans votre base de données");
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testFindAllCommandes() {
        log.debug("Entree de la methode");
        if (roles != null) {
            log.debug("NB_COMMANDES_LIST: " + NB_COMMANDES_LIST + " , commandes.size(): " + commandes.size());
            Assert.assertEquals(NB_COMMANDES_LIST, commandes.size());
        } else {
            Assert.fail("Aucun Commandes n'a ete trouves dans votre base de données");
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testFindAllArticlesCommande() {
        log.debug("Entree de la methode");
        if (roles != null) {
            log.debug("NB_ARTICLES_COMMANDE_LIST: " + NB_ARTICLES_COMMANDE_LIST + " , articlesCommande.size(): " + articlesCommande.size());
            Assert.assertEquals(NB_ARTICLES_COMMANDE_LIST, articlesCommande.size());
        } else {
            Assert.fail("Aucun Role n'a ete trouves dans votre base de données");
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testFindByCriteria() {
        log.debug("Entree de la methode");
        List<Utilisateur> utilisateursByPrenom = serviceFacade.getUtilisateurDao().findUtilisateursByPrenom(UTILISATEUR_FIND_BY_PRENOM);
        List<Utilisateur> utilisateursByNom = serviceFacade.getUtilisateurDao().findUtilisateursByNom(UTILISATEUR_FIND_BY_NOM);
        List<Utilisateur> utilisateursByIdentifiantRoleAdmin = serviceFacade.getUtilisateurDao().findUtilisateursByIdentifiantRole(UTILISATEUR_FIND_BY_IDENTIFIANT_ROLE_ADMIN);
        List<Utilisateur> utilisateursByIdentifiantRoleAcheteur = serviceFacade.getUtilisateurDao().findUtilisateursByIdentifiantRole(UTILISATEUR_FIND_BY_IDENTIFIANT_ROLE_ACHETEUR);
        List<Utilisateur> utilisateursByIdentifiantRoleStandard = serviceFacade.getUtilisateurDao().findUtilisateursByIdentifiantRole(UTILISATEUR_FIND_BY_IDENTIFIANT_ROLE_STANDARD);

        List<Role> rolesByIdentifiantAdmin = serviceFacade.getRoleDao().findRoleByIdentifiant(ROLES_FIND_BY_IDENTIFIANT_ADMIN);
        List<Role> rolesByIdentifiantAcheteur = serviceFacade.getRoleDao().findRoleByIdentifiant(ROLES_FIND_BY_IDENTIFIANT_ACHETEUR);
        List<Role> rolesByIdentifiantStandard = serviceFacade.getRoleDao().findRoleByIdentifiant(ROLES_FIND_BY_IDENTIFIANT_STANDARD);

        if (utilisateursByPrenom != null && !utilisateursByPrenom.isEmpty()) {
            log.debug("NB_UTILISATEURS_FIND_BY_PRENOM: " + NB_UTILISATEURS_FIND_BY_PRENOM + " , utilisateursByPrenom.size(): " + utilisateursByPrenom.size());
            Assert.assertEquals(NB_UTILISATEURS_FIND_BY_PRENOM, utilisateursByPrenom.size());
            verifyUsersDatas(utilisateursByPrenom);
        } else {
            Assert.fail("Aucun utilisateur avec le prenom '" + UTILISATEUR_FIND_BY_PRENOM + "' n'a ete trouve dans votre base de données");
        }
        if (utilisateursByNom != null && !utilisateursByNom.isEmpty()) {
            log.debug("NB_UTILISATEURS_FIND_BY_NOM: " + NB_UTILISATEURS_FIND_BY_NOM + " , utilisateursByNom.size(): " + utilisateursByNom.size());
            Assert.assertEquals(NB_UTILISATEURS_FIND_BY_NOM, utilisateursByNom.size());
            verifyUsersDatas(utilisateursByNom);
        } else {
            Assert.fail("Aucun utilisateur avec le nom '" + UTILISATEUR_FIND_BY_NOM + "' n'a ete trouve dans votre base de données");
        }
        if (utilisateursByIdentifiantRoleStandard != null && !utilisateursByIdentifiantRoleStandard.isEmpty()) {
            log.debug("NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_STANDARD: " + NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_STANDARD + " , utilisateursByIdentifiantRoleStandard.size(): " + utilisateursByIdentifiantRoleStandard.size());
            Assert.assertEquals(NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_STANDARD, utilisateursByIdentifiantRoleStandard.size());
            verifyUsersDatas(utilisateursByIdentifiantRoleStandard);
        } else {
            Assert.fail("Aucun utilisateur avec le rôle '" + UTILISATEUR_FIND_BY_IDENTIFIANT_ROLE_STANDARD + "' n'a ete trouve dans votre base de données");
        }
        if (utilisateursByIdentifiantRoleAcheteur != null && !utilisateursByIdentifiantRoleAcheteur.isEmpty()) {
            log.debug("NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_ACHETEUR: " + NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_ACHETEUR + " , utilisateursByIdentifiantRoleAcheteur.size(): " + utilisateursByIdentifiantRoleAcheteur.size());
            Assert.assertEquals(NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_ACHETEUR, utilisateursByIdentifiantRoleAcheteur.size());
            verifyUsersDatas(utilisateursByIdentifiantRoleAcheteur);
        } else {
            Assert.fail("Aucun utilisateur avec le rôle '" + UTILISATEUR_FIND_BY_IDENTIFIANT_ROLE_ACHETEUR + "' n'a ete trouve dans votre base de données");
        }
        if (utilisateursByIdentifiantRoleAdmin != null && !utilisateursByIdentifiantRoleAdmin.isEmpty()) {
            log.debug("NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_ADMIN: " + NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_ADMIN + " , utilisateursByIdentifiantRoleAdmin.size(): " + utilisateursByIdentifiantRoleAdmin.size());
            Assert.assertEquals(NB_UTILISATEURS_FIND_BY_IDENTIFIANT_ROLE_ADMIN, utilisateursByIdentifiantRoleAdmin.size());
            verifyUsersDatas(utilisateursByIdentifiantRoleAdmin);
        } else {
            Assert.fail("Aucun utilisateur avec le rôle '" + UTILISATEUR_FIND_BY_IDENTIFIANT_ROLE_ADMIN + "' n'a ete trouve dans votre base de données");
        }

        if (rolesByIdentifiantAdmin != null) {
            log.debug("NB_ROLES_FIND_BY_IDENTIFIANT_ADMIN: " + NB_ROLES_FIND_BY_IDENTIFIANT_ADMIN + " , rolesByIdentifiantAdmin.size(): " + rolesByIdentifiantAdmin.size());
            Assert.assertEquals(NB_ROLES_FIND_BY_IDENTIFIANT_ADMIN, rolesByIdentifiantAdmin.size());
        } else {
            Assert.fail("Aucun rôle avec l'identifiant " + ROLES_FIND_BY_IDENTIFIANT_ADMIN + "' n'a ete trouve dans votre base de données");
        }
        if (rolesByIdentifiantAcheteur != null) {
            log.debug("NB_ROLES_FIND_BY_IDENTIFIANT_ACHETEUR: " + NB_ROLES_FIND_BY_IDENTIFIANT_ACHETEUR + " , rolesByIdentifiantAcheteur.size(): " + rolesByIdentifiantAcheteur.size());
            Assert.assertEquals(NB_ROLES_FIND_BY_IDENTIFIANT_ACHETEUR, rolesByIdentifiantAcheteur.size());
        } else {
            Assert.fail("Aucun rôle avec l'identifiant " + ROLES_FIND_BY_IDENTIFIANT_ACHETEUR + "' n'a ete trouve dans votre base de données");
        }
        if (rolesByIdentifiantStandard != null) {
            log.debug("NB_ROLES_FIND_BY_IDENTIFIANT_STANDARD: " + NB_ROLES_FIND_BY_IDENTIFIANT_STANDARD + " , rolesByIdentifiantStandard.size(): " + rolesByIdentifiantStandard.size());
            Assert.assertEquals(NB_ROLES_FIND_BY_IDENTIFIANT_STANDARD, rolesByIdentifiantStandard.size());
        } else {
            Assert.fail("Aucun rôle avec l'identifiant " + ROLES_FIND_BY_IDENTIFIANT_STANDARD + "' n'a ete trouve dans votre base de données");
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testCreateUpdateDeleteUtilisateur() {
        log.debug("Entree de la methode");
        Role roleCRUD = new Role("Superviseur", "Le rôle superviseur");
        roleCRUD = serviceFacade.getRoleDao().createRole(roleCRUD);
        verifyRoleData(roleCRUD);
        log.debug("Created roleCRUD : " + roleCRUD);
        log.debug("Created roleCRUD.getIdRole : " + roleCRUD.getIdRole());
        roleCRUD = serviceFacade.getRoleDao().findRoleById(roleCRUD.getIdRole());
        Assert.assertNotNull(roleCRUD);
        Utilisateur userCRUD = new Utilisateur("Mme", "Nicole", "Valentine", "nicole.valentine@gmail.com", "passw0rd", new Date(System.currentTimeMillis()), roleCRUD);
        userCRUD = serviceFacade.getUtilisateurDao().createUtilisateur(userCRUD);
        Assert.assertNotNull(userCRUD);
        Assert.assertNotNull(userCRUD.getIdUtilisateur());
        Assert.assertNotNull(userCRUD.getPrenom());
        Assert.assertNotNull(userCRUD.getNom());
        Assert.assertNotNull(userCRUD.getDateCreation());
        Assert.assertNotNull(userCRUD.getDateModification());
        log.debug("Created userCRUD : " + userCRUD);
        userCRUD = serviceFacade.getUtilisateurDao().findUtilisateurById(userCRUD.getIdUtilisateur());
        Assert.assertNotNull(userCRUD);
        userCRUD.setPrenom("Nicole Bis");
        userCRUD.setNom("Valentine Bis");
        userCRUD = serviceFacade.getUtilisateurDao().updateUtilisateur(userCRUD);
        Assert.assertNotNull(userCRUD);
        userCRUD = serviceFacade.getUtilisateurDao().findUtilisateurById(userCRUD.getIdUtilisateur());
        log.debug("Updated userCRUD : " + userCRUD);
        Assert.assertEquals("Nicole Bis", userCRUD.getPrenom());
        Assert.assertEquals("Valentine Bis", userCRUD.getNom());
        Assert.assertTrue(serviceFacade.getUtilisateurDao().deleteUtilisateur(userCRUD));
        userCRUD = serviceFacade.getUtilisateurDao().findUtilisateurById(userCRUD.getIdUtilisateur());
        Assert.assertNull(userCRUD);
        Assert.assertTrue(serviceFacade.getRoleDao().deleteRole(roleCRUD));
        roleCRUD = serviceFacade.getRoleDao().findRoleById(roleCRUD.getIdRole());
        Assert.assertNull(roleCRUD);
        // Cas des gestions des doublons d'identifiant (mail).
        userCRUD = new Utilisateur("Mr", "Admin", "Admin", "admin@gmail.com", "passw0rd", new Date(System.currentTimeMillis()));
        try {
            userCRUD = serviceFacade.getUtilisateurDao().createUtilisateur(userCRUD);
            log.debug("Duplicate userCRUD : " + userCRUD.getIdentifiant());
        } catch (EbenusException e) {
            log.debug("Bravo la gestion des doublons d'identifiant marche parfaitement");
            Assert.assertEquals(Constants.EXCEPTION_CODE_USER_ALREADY_EXIST, e.getCode());
        }
        List<Utilisateur> utilisateursFinal = serviceFacade.getUtilisateurDao().findAllUtilisateurs();
        if (utilisateursFinal != null) {
            Assert.assertEquals(NB_UTILISATEURS_LIST, utilisateursFinal.size());
            log.debug("utilisateursFinal.size() : " + utilisateursFinal.size() + " , NB_UTILISATEURS_LIST: " + NB_UTILISATEURS_LIST);
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testCreateUpdateDeleteRole() {
        log.debug("Entree de la methode");
        Role roleCRUD = new Role("Superviseur", "Le rôle superviseur");
        roleCRUD = serviceFacade.getRoleDao().createRole(roleCRUD);
        verifyRoleData(roleCRUD);
        log.debug("Created roleCRUD : " + roleCRUD);
        log.debug("Created roleCRUD.getIdRole : " + roleCRUD.getIdRole());
        roleCRUD = serviceFacade.getRoleDao().findRoleById(roleCRUD.getIdRole());
        Assert.assertNotNull(roleCRUD);
        roleCRUD.setIdentifiant("Superviseur Bis");
        roleCRUD.setDescription("Le rôle superviseur Bis");
        roleCRUD = serviceFacade.getRoleDao().updateRole(roleCRUD);
        Assert.assertNotNull(roleCRUD);
        roleCRUD = serviceFacade.getRoleDao().findRoleById(roleCRUD.getIdRole());
        log.debug("Updated roleCRUD : " + roleCRUD);
        Assert.assertEquals("Superviseur Bis", roleCRUD.getIdentifiant());
        Assert.assertEquals("Le rôle superviseur Bis", roleCRUD.getDescription());
        Assert.assertTrue(serviceFacade.getRoleDao().deleteRole(roleCRUD));
        roleCRUD = serviceFacade.getRoleDao().findRoleById(roleCRUD.getIdRole());
        Assert.assertNull(roleCRUD);
        List<Role> rolesFinal = serviceFacade.getRoleDao().findAllRoles();
        if (rolesFinal != null) {
            Assert.assertEquals(NB_ROLES_LIST, rolesFinal.size());
            log.debug("rolesFinal.size() : " + rolesFinal.size() + " , NB_ROLES_LIST: " + NB_ROLES_LIST);
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testCreateUpdateDeleteAdresse() {
        log.debug("Entree de la methode");
        Role roleCRUD = new Role("Superviseur", "Le rôle superviseur");
        roleCRUD = serviceFacade.getRoleDao().createRole(roleCRUD);
        verifyRoleData(roleCRUD);
        log.debug("Created roleCRUD : " + roleCRUD);
        log.debug("Created roleCRUD.getIdRole : " + roleCRUD.getIdRole());
        roleCRUD = serviceFacade.getRoleDao().findRoleById(roleCRUD.getIdRole());
        Assert.assertNotNull(roleCRUD);
        Utilisateur userCRUD = new Utilisateur("Mr", "Jean", "Duroc", "jean.duroc@gmail.com", "passw0rd", new Date(System.currentTimeMillis()), roleCRUD);
        userCRUD = serviceFacade.getUtilisateurDao().createUtilisateur(userCRUD);
        verifyUserDatas(userCRUD);
        log.debug("Created userCRUD : " + userCRUD);
        log.debug("Created userCRUD.getIdUtilisateur : " + userCRUD.getIdUtilisateur());
        userCRUD = serviceFacade.getUtilisateurDao().findUtilisateurById(userCRUD.getIdUtilisateur());
        Assert.assertNotNull(userCRUD);
        Adresse adresseCRUD = new Adresse(userCRUD, "14 rue de rivoli", "75012", "Paris", "France", "D", "L", true);
        adresseCRUD = serviceFacade.getAdresseDao().createAdresse(adresseCRUD);
        verifyAdresseData(adresseCRUD);
        log.debug("Created adresseCRUD : " + adresseCRUD);
        log.debug("Created adresseCRUD.getIdAdresse : " + adresseCRUD.getIdAdresse());
        adresseCRUD = serviceFacade.getAdresseDao().findAdresseById(adresseCRUD.getIdAdresse());
        Assert.assertNotNull(adresseCRUD);
        adresseCRUD.setRue("14 rue de rivoli Bis");
        adresseCRUD.setCodePostal("75012 Bis");
        adresseCRUD.setVille("Paris Bis");
        adresseCRUD.setPays("France Bis");
        adresseCRUD.setStatut("E");
        adresseCRUD.setTypeAdresse("O");
        adresseCRUD.setPrincipale(false);
        adresseCRUD = serviceFacade.getAdresseDao().updateAdresse(adresseCRUD);
        Assert.assertNotNull(adresseCRUD);
        adresseCRUD = serviceFacade.getAdresseDao().findAdresseById(adresseCRUD.getIdAdresse());
        log.debug("Updated adresseCRUD : " + adresseCRUD);
        Assert.assertEquals("14 rue de rivoli Bis", adresseCRUD.getRue());
        Assert.assertEquals("75012 Bis", adresseCRUD.getCodePostal());
        Assert.assertEquals("Paris Bis", adresseCRUD.getVille());
        Assert.assertEquals("France Bis", adresseCRUD.getPays());
        Assert.assertEquals("E", adresseCRUD.getStatut());
        Assert.assertEquals("O", adresseCRUD.getTypeAdresse());
        Assert.assertEquals(false, adresseCRUD.getPrincipale());
        Assert.assertTrue(serviceFacade.getAdresseDao().deleteAdresse(adresseCRUD));
        Assert.assertTrue(serviceFacade.getUtilisateurDao().deleteUtilisateur(userCRUD));
        Assert.assertTrue(serviceFacade.getRoleDao().deleteRole(roleCRUD));
        adresseCRUD = serviceFacade.getAdresseDao().findAdresseById(adresseCRUD.getIdAdresse());
        Assert.assertNull(adresseCRUD);
        List<Adresse> adressesFinal = serviceFacade.getAdresseDao().findAllAdresses();
        if (adressesFinal != null) {
            Assert.assertEquals(NB_ADRESSES_LIST, adressesFinal.size());
            log.debug("adressesFinal.size() : " + adressesFinal.size() + " , NB_ADRESSES_LIST: " + NB_ADRESSES_LIST);
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testCreateUpdateDeleteProduit() {
        log.debug("Entree de la methode");
        Produit produitCRUD = new Produit("REF-IPHONE-11", 1500.0, "iPhone 11", "Nouvelle iPhone 11", 999,true, false);
        produitCRUD = serviceFacade.getProduitDao().createProduit(produitCRUD);
        verifyProduitData(produitCRUD);
        log.debug("Created produitCRUD : " + produitCRUD);
        log.debug("Created produitCRUD.getIdProduit : " + produitCRUD.getIdProduit());
        produitCRUD = serviceFacade.getProduitDao().findProduitById(produitCRUD.getIdProduit());
        Assert.assertNotNull(produitCRUD);
        produitCRUD.setNom("iPhone 12");
        produitCRUD.setDescription("Renouvelle iPhone");
        produitCRUD.setStock(123);
        produitCRUD.setReference("REF-IPHONE-12");
        produitCRUD.setPrix(2000.0);
        produitCRUD.setActive(false);
        produitCRUD.setMarquerEffacer(true);
        produitCRUD = serviceFacade.getProduitDao().updateProduit(produitCRUD);
        Assert.assertNotNull(produitCRUD);
        produitCRUD = serviceFacade.getProduitDao().findProduitById(produitCRUD.getIdProduit());
        log.debug("Updated produitCRUD : " + produitCRUD);
        Assert.assertEquals("iPhone 12", produitCRUD.getNom());
        Assert.assertEquals("Renouvelle iPhone", produitCRUD.getDescription());
        Assert.assertEquals("REF-IPHONE-12", produitCRUD.getReference());
        Assert.assertEquals(false, produitCRUD.getActive());
        Assert.assertEquals(true, produitCRUD.getMarquerEffacer());
        Assert.assertTrue(serviceFacade.getProduitDao().deleteProduit(produitCRUD));
        produitCRUD = serviceFacade.getProduitDao().findProduitById(produitCRUD.getIdProduit());
        Assert.assertNull(produitCRUD);
        List<Produit> produitsFinal = serviceFacade.getProduitDao().findAllProduits();
        if (produitsFinal != null) {
            Assert.assertEquals(NB_PRODUITS_LIST, produitsFinal.size());
            log.debug("produitsFinal.size() : " + produitsFinal.size() + " , NB_PRODUITS_LIST: " + NB_PRODUITS_LIST);
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testCreateUpdateDeleteCommande() {
        log.debug("Entree de la methode");
        Role roleCRUD = new Role("TSS", "Le rôle superviseur");
        roleCRUD = serviceFacade.getRoleDao().createRole(roleCRUD);
        verifyRoleData(roleCRUD);
        log.debug("Created roleCRUD : " + roleCRUD);
        log.debug("Created roleCRUD.getIdRole : " + roleCRUD.getIdRole());
        roleCRUD = serviceFacade.getRoleDao().findRoleById(roleCRUD.getIdRole());
        Assert.assertNotNull(roleCRUD);
        Utilisateur userCRUD = new Utilisateur("Mr", "aw", "wa", "wa.menachez@gmail.com", "passw0rd", new Date(System.currentTimeMillis()), roleCRUD);
        userCRUD = serviceFacade.getUtilisateurDao().createUtilisateur(userCRUD);
        verifyUserDatas(userCRUD);
        log.debug("Created userCRUD : " + userCRUD);
        log.debug("Created userCRUD.getIdUtilisateur : " + userCRUD.getIdUtilisateur());
        userCRUD = serviceFacade.getUtilisateurDao().findUtilisateurById(userCRUD.getIdUtilisateur());
        Assert.assertNotNull(userCRUD);
        Adresse adresseCRUD = new Adresse(userCRUD, "14 rue de a", "75012", "Paris", "France", "D", "L", true);
        adresseCRUD = serviceFacade.getAdresseDao().createAdresse(adresseCRUD);
        verifyAdresseData(adresseCRUD);
        log.debug("Created adresseCRUD : " + adresseCRUD);
        log.debug("Created adresseCRUD.getIdAdresse : " + adresseCRUD.getIdAdresse());
        adresseCRUD = serviceFacade.getAdresseDao().findAdresseById(adresseCRUD.getIdAdresse());
        Assert.assertNotNull(adresseCRUD);
        Commande commandeCRUD = new Commande(133.4, userCRUD, adresseCRUD, "T", new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
        commandeCRUD = serviceFacade.getCommandeDao().createCommande(commandeCRUD);
        verifyCommandeData(commandeCRUD);
        log.debug("Created commandeCRUD : " + commandeCRUD);
        log.debug("Created commandeCRUD.getIdRole : " + commandeCRUD.getIdCommande());
        commandeCRUD = serviceFacade.getCommandeDao().findCommandeById(commandeCRUD.getIdCommande());
        Assert.assertNotNull(commandeCRUD);
        commandeCRUD.setStatut("R");
        commandeCRUD = serviceFacade.getCommandeDao().updateCommande(commandeCRUD);
        Assert.assertNotNull(commandeCRUD);
        commandeCRUD = serviceFacade.getCommandeDao().findCommandeById(commandeCRUD.getIdCommande());
        log.debug("Updated commandeCRUD : " + commandeCRUD);
        Assert.assertEquals("R", commandeCRUD.getStatut());
        Assert.assertTrue(serviceFacade.getCommandeDao().deleteCommande(commandeCRUD));
        Assert.assertTrue(serviceFacade.getUtilisateurDao().deleteUtilisateur(userCRUD));
        Assert.assertTrue(serviceFacade.getRoleDao().deleteRole(roleCRUD));
        commandeCRUD = serviceFacade.getCommandeDao().findCommandeById(commandeCRUD.getIdCommande());
        Assert.assertNull(commandeCRUD);
        List<Commande> commandesFinal = serviceFacade.getCommandeDao().findAllCommandes();
        if (commandesFinal != null) {
            Assert.assertEquals(NB_COMMANDES_LIST, commandesFinal.size());
            log.debug("commandesFinal.size() : " + commandesFinal.size() + " , NB_COMMANDES_LIST: " + NB_COMMANDES_LIST);
        }
        log.debug("Sortie de la methode");
    }

    @Test
    public void testCreateUpdateDeleteArticleCommande() {
        log.debug("Entree de la methode");
        Role roleCRUD = new Role("Superviseur", "Le rôle superviseur");
        roleCRUD = serviceFacade.getRoleDao().createRole(roleCRUD);
        verifyRoleData(roleCRUD);
        log.debug("Created roleCRUD : " + roleCRUD);
        log.debug("Created roleCRUD.getIdRole : " + roleCRUD.getIdRole());
        roleCRUD = serviceFacade.getRoleDao().findRoleById(roleCRUD.getIdRole());
        Assert.assertNotNull(roleCRUD);
        Utilisateur userCRUD = new Utilisateur("Mr", "Pierre", "Aymerick", "pierre.aymerick@gmail.com", "passw0rd", new Date(System.currentTimeMillis()), roleCRUD);
        userCRUD = serviceFacade.getUtilisateurDao().createUtilisateur(userCRUD);
        verifyUserDatas(userCRUD);
        log.debug("Created userCRUD : " + userCRUD);
        log.debug("Created userCRUD.getIdUtilisateur : " + userCRUD.getIdUtilisateur());
        userCRUD = serviceFacade.getUtilisateurDao().findUtilisateurById(userCRUD.getIdUtilisateur());
        Assert.assertNotNull(userCRUD);
        Adresse adresseCRUD = new Adresse(userCRUD, "14 rue de rivoli", "75012", "Paris", "France", "D", "L", true);
        adresseCRUD = serviceFacade.getAdresseDao().createAdresse(adresseCRUD);
        verifyAdresseData(adresseCRUD);
        log.debug("Created adresseCRUD : " + adresseCRUD);
        log.debug("Created adresseCRUD.getIdAdresse : " + adresseCRUD.getIdAdresse());
        adresseCRUD = serviceFacade.getAdresseDao().findAdresseById(adresseCRUD.getIdAdresse());
        Assert.assertNotNull(adresseCRUD);
        Commande commandeCRUD = new Commande(133.4, userCRUD, adresseCRUD, "T", new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
        commandeCRUD = serviceFacade.getCommandeDao().createCommande(commandeCRUD);
        verifyCommandeData(commandeCRUD);
        log.debug("Created commandeCRUD : " + commandeCRUD);
        log.debug("Created commandeCRUD.getIdRole : " + commandeCRUD.getIdCommande());
        commandeCRUD = serviceFacade.getCommandeDao().findCommandeById(commandeCRUD.getIdCommande());
        Assert.assertNotNull(commandeCRUD);
        Produit produitCRUD = new Produit("REF-IPHONE1", 100.0, "iPhone ", "Nouvelle iPhone", 99,true, false);
        produitCRUD = serviceFacade.getProduitDao().createProduit(produitCRUD);
        verifyProduitData(produitCRUD);
        log.debug("Created produitCRUD : " + produitCRUD);
        log.debug("Created produitCRUD.getIdProduit : " + produitCRUD.getIdProduit());
        produitCRUD = serviceFacade.getProduitDao().findProduitById(produitCRUD.getIdProduit());
        Assert.assertNotNull(produitCRUD);
        ArticleCommande articleCommandeCRUD = new ArticleCommande(195.0, "REF-IPHONE-11", 12, "D", new Date(System.currentTimeMillis()), 2, commandeCRUD, userCRUD, adresseCRUD, produitCRUD);
        articleCommandeCRUD = serviceFacade.getArticleCommandeDao().createArticleCommande(articleCommandeCRUD);
        verifyCommandeArticleCommande(articleCommandeCRUD);
        log.debug("Created articleCommandeCRUD : " + articleCommandeCRUD);
        log.debug("Created articleCommandeCRUD.getIdArticleCommande : " + articleCommandeCRUD.getIdArticleCommande());
        articleCommandeCRUD = serviceFacade.getArticleCommandeDao().findArticleCommandeById(articleCommandeCRUD.getIdArticleCommande());
        Assert.assertNotNull(articleCommandeCRUD);
        articleCommandeCRUD.setReference("iPhone 17");
        articleCommandeCRUD.setStatut("M");
        articleCommandeCRUD = serviceFacade.getArticleCommandeDao().updateArticleCommande(articleCommandeCRUD);
        Assert.assertNotNull(roleCRUD);
        articleCommandeCRUD = serviceFacade.getArticleCommandeDao().findArticleCommandeById(articleCommandeCRUD.getIdArticleCommande());
        log.debug("Updated articleCommandeCRUD : " + articleCommandeCRUD);
        Assert.assertEquals("iPhone 17", articleCommandeCRUD.getReference());
        Assert.assertEquals("M", articleCommandeCRUD.getStatut());
        Assert.assertTrue(serviceFacade.getArticleCommandeDao().deleteArticleCommande(articleCommandeCRUD));
        Assert.assertTrue(serviceFacade.getUtilisateurDao().deleteUtilisateur(userCRUD));
        Assert.assertTrue(serviceFacade.getRoleDao().deleteRole(roleCRUD));
        articleCommandeCRUD = serviceFacade.getArticleCommandeDao().findArticleCommandeById(articleCommandeCRUD.getIdArticleCommande());
        Assert.assertNull(articleCommandeCRUD);
        List<ArticleCommande> articlesCommandeList = serviceFacade.getArticleCommandeDao().findAllArticlesCommande();
        if (articlesCommandeList != null) {
            Assert.assertEquals(NB_ARTICLES_COMMANDE_LIST, articlesCommandeList.size());
            log.debug("articlesCommandeList.size() : " + articlesCommandeList.size() + " , NB_ARTICLES_COMMANDE_LIST: " + NB_ARTICLES_COMMANDE_LIST);
        }
        log.debug("Sortie de la methode");
    }

    @AfterClass
    public static void terminate() throws Exception {
        log.debug("Entree de la methode");
        serviceFacade = null;
        utilisateurs = null;
        roles = null;
        adresses = null;
        log.debug("Sortie de la methode");
    }
}

