package com.cours.servlet;

import com.cours.constants.URIsFront;
import com.cours.ebenus.dao.entities.Utilisateur;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

//    private static final IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(URIsFront.URL_LOGIN).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String redirect = URIsFront.SERVLET_LOGIN;

        Utilisateur user = serviceFacade.getUtilisateurDao().authenticate(
                request.getParameter("email"),
                request.getParameter("password")
        );

//        if (disconnect(request)) {
            redirect = URIsFront.SERVLET_PRODUCT;
            request.getSession().setAttribute("user", user);
//        }
//        System.out.println(redirect);
//        System.out.println(URIsFront.URL_PRODUCT);
        response.sendRedirect(redirect);
    }
    private boolean disconnect(HttpServletRequest request) {
        return "true".equals(request.getParameter("logout"));
    }

}

