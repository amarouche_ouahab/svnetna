package com.cours.constants;

public class FieldsValuesDB {

    // Utilisateur fields values
    public static final String CIVILITE_M = "Mr";
    public static final String CIVILITE_F = "Mme";

    // Address fields values
    public static final String BILLING = "F";
    public static final String SHIPPING = "L";

}
