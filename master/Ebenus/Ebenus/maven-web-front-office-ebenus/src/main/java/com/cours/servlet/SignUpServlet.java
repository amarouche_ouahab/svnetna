package com.cours.servlet;

import com.cours.constants.FieldsValuesDB;
import com.cours.constants.URIsFront;
import com.cours.ebenus.dao.entities.Adresse;
import com.cours.ebenus.dao.entities.Role;
import com.cours.ebenus.dao.entities.Utilisateur;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "SignUpServlet", urlPatterns = {"/SignUpServlet"})
public class SignUpServlet extends HttpServlet {

//    private static final IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(URIsFront.URL_SIGNUP).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String redirect = URIsFront.SERVLET_SIGNUP;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Role role = serviceFacade.getRoleDao().findRoleByIdentifiant("Acheteur").get(0);
        Utilisateur user = new Utilisateur();
        String birth = request.getParameter("select-day") + "/" +
                request.getParameter("select-month") + "/" +
                request.getParameter("select-year");

        user.setPrenom(request.getParameter("firstname"));
        user.setNom(request.getParameter("lastname"));
        user.setRole(role);
        user.setCivilite(request.getParameter("sex"));
        user.setIdentifiant(request.getParameter("email"));
        user.setMotPasse(request.getParameter("pass"));
        user.setDateCreation(new Date(System.currentTimeMillis()));
        user.setDateModification(new Date(System.currentTimeMillis()));

        Logger.getGlobal().log(Level.WARNING, birth);
        try { user.setDateNaissance(dateFormat.parse(birth)); }
        catch (Exception e) { Logger.getGlobal().log(Level.WARNING, e.getMessage()); }

        user = serviceFacade.getUtilisateurDao().createUtilisateur(user);

        serviceFacade.getUtilisateurDao().authenticate(
                request.getParameter("email"),
                request.getParameter("password")
        );

        if (user != null) {
            redirect = URIsFront.SERVLET_USER_INFO;
            request.getSession().setAttribute("user", user);

            if (request.getParameter("shippingAdress") != null) {
                addAdresse(request, user.getIdUtilisateur(), "Ship", FieldsValuesDB.SHIPPING);
            }

            if (request.getParameter("billAdress") != null) {
                addAdresse(request, user.getIdUtilisateur(), "Bill", FieldsValuesDB.BILLING);
            }

            if (request.getParameter("BillShip") != null) {
                addAdresse(request, user.getIdUtilisateur(), "BillShip", FieldsValuesDB.SHIPPING);
                addAdresse(request, user.getIdUtilisateur(), "BillShip", FieldsValuesDB.BILLING);
            }
        }

        response.sendRedirect(redirect);
    }

    private void addAdresse(HttpServletRequest request, int user_id, String type_parameter, String type_adresse) {
        Adresse adresse = new Adresse();
//        adresse.setIdUtilisateur(34);
        adresse.setRue(request.getParameter("street" + type_parameter));
//        adresse.setCodePostale(request.getParameter("postalcode" + type_parameter));
        adresse.setVille(request.getParameter("city" + type_parameter));
        adresse.setPays(request.getParameter("country" + type_parameter));
        adresse.setTypeAdresse(type_adresse);
        adresse.setPrincipale(true);
        serviceFacade.getAdresseDao().createAdresse(adresse);
    }
}
