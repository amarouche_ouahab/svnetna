package com.cours.servlet;

import com.cours.constants.URIsFront;
import com.cours.ebenus.dao.entities.Adresse;
import com.cours.ebenus.dao.entities.Utilisateur;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//@WebServlet(name = "UserInfoServlet", urlPatterns = {"/MyAccountServlet"})
@WebServlet(name = "UserInfoServlet", urlPatterns = {"/UserInfoServlet"})
public class UserInfoServlet extends HttpServlet {

//    private static final IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = URIsFront.URL_USER_INFO;

        if (request.getSession().getAttribute("user") == null || disconnect(request)) {
            request.getSession().removeAttribute("user");
            response.sendRedirect(URIsFront.SERVLET_LOGIN);
            return;
        }

//        Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
//        Adresse adresse =  serviceFacade.getAdresseDao().findMainAdresseByIdUtilisateur(user.getIdUtilisateur());
//        request.getSession().setAttribute("adresse", adresse);
        this.getServletContext().getRequestDispatcher(page).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getSession().getAttribute("user") == null) {
            request.getSession().removeAttribute("user");
            response.sendRedirect(URIsFront.SERVLET_HOME);
            return;
        }

        updateUser(request);

        this.getServletContext().getRequestDispatcher(URIsFront.URL_USER_INFO).forward(request, response);
    }

    private void updateUser(HttpServletRequest request) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");

        String birth = request.getParameter("select-day") + "/" +
                request.getParameter("select-month") + "/" +
                request.getParameter("select-year");

        user.setPrenom(request.getParameter("firstname"));
        user.setNom(request.getParameter("lastname"));
        user.setCivilite(request.getParameter("sex"));
        user.setIdentifiant(request.getParameter("email"));
        user.setMotPasse(request.getParameter("password"));
        user.setDateModification(new Date(System.currentTimeMillis()));

        try { user.setDateNaissance(dateFormat.parse(birth)); }
        catch (Exception e) { Logger.getGlobal().log(Level.WARNING, e.getMessage()); }

        serviceFacade.getUtilisateurDao().updateUtilisateur(user);

        request.getSession().setAttribute("user", user);
    }

    private void updateAdresse(HttpServletRequest request) {
        Adresse adresse = (Adresse) request.getSession().getAttribute("adresse");

        if (adresse == null) {
            adresse = new Adresse();
            Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
//            adresse.setIdUtilisateur(user.getIdUtilisateur());
            adresse.setTypeAdresse("billing and shipping");
            adresse.setPrincipale(true);
        } else {
            adresse.setRue(request.getParameter("street"));
//            adresse.setCodePostale(request.getParameter("zipcode"));
            adresse.setVille(request.getParameter("city"));
            adresse.setPays(request.getParameter("country"));
        }
    }

    private boolean disconnect(HttpServletRequest request) {
        return "true".equals(request.getParameter("logout"));
    }
}
