package com.cours.servlet;


import com.cours.constants.URIsFront;
import com.cours.ebenus.dao.entities.Adresse;
import com.cours.ebenus.dao.entities.Utilisateur;
import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet(name = "AddressBookServlet", urlPatterns = {"/AddressBookServlet"})
public class AddressBookServlet extends HttpServlet {

    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page = URIsFront.URL_ADDRESS_BOOK;
        Utilisateur user_ss = serviceFacade.getUtilisateurDao().findUtilisateurById(34);
        request.getSession().setAttribute("user", user_ss);
        if (request.getSession().getAttribute("user") == null) {
            request.getSession().removeAttribute("user");
            response.sendRedirect(URIsFront.SERVLET_HOME);
            return;
        }

        if (request.getParameter("add-address") != null) {
            page = URIsFront.URL_ADD_ADDRESS;
        } else if (request.getParameter("delete-address") != null) {
            page = URIsFront.URL_DELETE_ADDRESS;
        } else {
            Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
//            List<Adresse> adresses = serviceFacade.getAdresseDao().findAdressesByIdUtilisateur(user.getIdUtilisateur());
            Adresse adresseBill = new Adresse(null, null, null, null, null, null);
            adresseBill.getPays();
//            for (Adresse adresse : adresses) {
//                if (adresse.getTypeAdresse().equals("F") == true) {
//                    adresseBill = adresse;
//                }
//            }
//            request.getSession().setAttribute("adresses", adresses);
            request.getSession().setAttribute("adresseBill", adresseBill);
        }

        this.getServletContext().getRequestDispatcher(page).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        update_adresse_bill(request);
        update_adresses_ship(request);
        this.getServletContext().getRequestDispatcher(URIsFront.URL_ADDRESS_BOOK).forward(request, response);
    }

    public void update_adresse_bill(HttpServletRequest request) {
        Adresse adresseBill = (Adresse) request.getSession().getAttribute("adresseBill");
//        if (adresseBill.getIdUtilisateur() == null) {
//            return;
//        }
//        adresseBill.setCodePostale(request.getParameter("zipcodeBill"));
        adresseBill.setRue(request.getParameter("streetBill"));
        adresseBill.setVille(request.getParameter("cityBill"));
        adresseBill.setPays(request.getParameter("countryBill"));
    }

    public void update_adresses_ship(HttpServletRequest request) {
        Utilisateur user = (Utilisateur)  request.getSession().getAttribute("user");
//        List<Adresse> adresses = serviceFacade.getAdresseDao().findAdressesByIdUtilisateur(user.getIdUtilisateur());

        Enumeration<String> str = request.getParameterNames();
//        for(int index = 0; index <= adresses.size() - 1; index+=1) {
//            String param = request.getParameter("adresseShip["+index+"][idAdresse]");
//            String name = "adresseShip[0][idAdresse]";
//            String param1 = request.getParameter(name);
//            String param2 = request.getParameter("adresseShip[0][zipcode]");
//
//            if (request.getParameter("adresseShip["+index+"][idAdresse]") != null) {
//                int idAdresse = Integer.parseInt(request.getParameter("adresseShip["+index+"][idAdresse]"));
//                String street = request.getParameter("adresseShip["+index+"][street]");
//                String city = request.getParameter("adresseShip["+index+"][city]");
//                String zipcode = request.getParameter("adresseShip["+index+"][zipcode]");
//                String country = request.getParameter("adresseShip["+index+"][country]");
//
//                Adresse adresse = serviceFacade.getAdresseDao().findAdresseById(idAdresse);
//                adresse.setVille(city);
//                adresse.setPays(country);
//                adresse.setCodePostale(zipcode);
//                adresse.setRue(street);
//
//                serviceFacade.getAdresseDao().updateAdresse(adresse);
//            }
//        }
    }
}
