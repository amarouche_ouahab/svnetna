package com.cours.servlet;

import com.cours.constants.URIsFront;
import com.cours.ebenus.dao.entities.Produit;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ProductServlet", urlPatterns = {"/ProductServlet"})
public class ProductServlet extends HttpServlet {

//    private static final IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = URIsFront.URL_PRODUIT_ACCEUIL;
        if (request.getSession().getAttribute("user") == null || disconnect(request)) {
            request.getSession().removeAttribute("user");
            response.sendRedirect(URIsFront.SERVLET_LOGIN);
            return;
        }
        if (request.getParameter("list") != null) {
            getProduits(request);
            page = URIsFront.URL_PRODUCT;
        }else if(request.getParameter("produit") != null){
            getProduit(request);
            page = URIsFront.URL_PRODUCT_DETAIL;
        }else {
            getProduit10(request);
        }
        System.out.println(page);

//        List<Produit> products = serviceFacade.getProduitDao().findAllProduits();
//        request.getSession().setAttribute("products", products);
        this.getServletContext().getRequestDispatcher(page).forward(request, response);
    }

    private void getProduit10(HttpServletRequest request) {
        List<Produit> produit10 = serviceFacade.getProduitDao().findAllProduits();
        request.setAttribute("produit10", produit10.subList(0,10));
    }
    private void getProduits(HttpServletRequest request) {
        List<Produit> produits = serviceFacade.getProduitDao().findAllProduits();
        request.setAttribute("produits", produits);
    }

    private void getProduit(HttpServletRequest request) {
        Produit produit = serviceFacade.getProduitDao().findProduitById(Integer.parseInt(request.getParameter("produit")));
        request.setAttribute("produit", produit);
    }
    private boolean disconnect(HttpServletRequest request) {
        return "true".equals(request.getParameter("logout"));
    }

}

