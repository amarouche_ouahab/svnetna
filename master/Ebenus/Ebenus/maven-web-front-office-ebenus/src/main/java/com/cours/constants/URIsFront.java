package com.cours.constants;

public class URIsFront {
    private static final String URL_BASE = "/pages";

    public static final String URL_LOGIN = URL_BASE + "/login.jsp";
    public static final String URL_PRODUIT_ACCEUIL = URL_BASE + "/produit-acceuil.jsp";

    public static final String URL_SIGNUP = URL_BASE + "/sign_up.jsp";
//    public static final String URL_USER_INFO = URL_BASE + "/user_info_content.jsp";

    public static final String URL_USER_INFO = URL_BASE + "/user_info.jsp";
    public static final String URL_ADDRESS_BOOK = URL_BASE + "/address_book.jsp";
    public static final String URL_ADD_ADDRESS = URL_BASE + "/add_address.jsp";
    public static final String URL_DELETE_ADDRESS = URL_BASE + "/delete_address.jsp";
    public static final String URL_PRODUCT = URL_BASE + "/produits.jsp";
    public static final String URL_PRODUCT_DETAIL = URL_BASE + "/produit-detail.jsp";
    public static final String URL_HOME = URL_BASE + "/home.jsp";
    public static final String URL_BASKET = URL_BASE + "/basket.jsp";
    public static final String URL_MYHISTORIC = URL_BASE + "/my_historic.jsp";
    public static final String URL_COMMANDES =  URL_BASE + "/orders.jsp";

    private static final String SERVLET_BASE = "/frontOffice";

    public static final String SERVLET_SIGNUP = SERVLET_BASE + "/SignUp";
    public static final String SERVLET_USER_INFO = SERVLET_BASE + "/UserInfoServlet";
    public static final String SERVLET_PRODUCT = SERVLET_BASE + "/ProductServlet";
    public static final String SERVLET_HOME = SERVLET_BASE + "/Home/oServlet";
    public static final String SERVLET_LOGIN = SERVLET_BASE + "/LoginServlet";
    public static final String SERVLET_BASKET = SERVLET_BASE + "/BasketServlet";
    public static final String SERVLET_COMMAND = SERVLET_BASE + "/CommandeServlet";

}
