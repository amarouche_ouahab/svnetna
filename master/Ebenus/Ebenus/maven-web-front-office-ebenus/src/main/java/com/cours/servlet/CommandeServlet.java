package com.cours.servlet;

import com.cours.constants.URIsFront;
import com.cours.ebenus.dao.entities.Commande;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;



@WebServlet(name = "CommandeServlet", urlPatterns = {"/CommandeServlet"})
public class CommandeServlet extends HttpServlet {

//    private static final IServiceFacade serviceFacade = ServiceFactory.getDefaultServiceFacade();
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page = URIsFront.URL_COMMANDES;

        if (request.getSession().getAttribute("user") == null) {
            request.getSession().removeAttribute("user");
            response.sendRedirect(URIsFront.SERVLET_LOGIN);
            return;
        }

        getCommande(request, Integer.parseInt(request.getParameter("userId")));

        this.getServletContext().getRequestDispatcher(page).forward(request, response);
    }
//
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
////        if (request.getParameter("add-role") != null) {
////            request.setAttribute("responseAddRole", true);
////            request.setAttribute("titleModal", Display.TITLE_MODAL_ADD_ROLE);
////            addRole(request);
////        }
////        else if (request.getParameter("edit-role") != null) {
////            request.setAttribute("responseEditRole", true);
////            request.setAttribute("titleModal", Display.TITLE_MODAL_EDIT_ROLE);
////            editRole(request);
////        }
//
//        doGet(request, response);
//    }
    //
    private void getCommande(HttpServletRequest request, int userId) {
        List<Commande> commandes =  serviceFacade.getCommandeDao().findAllCommandes();
        List<Commande> UserCommandes = new ArrayList<Commande>(); ;
//        List<ArticleCommande> allArt = serviceFacade.getArticleCommandeDao().findAllArticlesCommande();
        for (Commande cmd: commandes) {
            if (cmd.getUtilisateur().getIdUtilisateur().equals(userId)) {
                UserCommandes.add(cmd);
            }
        }
        System.out.println(UserCommandes.size());
        request.setAttribute("commandes", UserCommandes);
    }

//    private static List<Commande> CmdList = serviceFacade.getCommandeDao().findAllCommandes();




//
//    private void getRole(HttpServletRequest request) {
//        Role role = serviceFacade.getRoleDao().findRoleById(Integer.parseInt(request.getParameter("edit-role")));
//        request.setAttribute("role", role);
//    }
//
//    private void addRole(HttpServletRequest request) {
//
//        Role role = new Role();
//        role.setIdentifiant(request.getParameter("id-role"));
//        role.setDescription(request.getParameter("description-role"));
//
//        role = serviceFacade.getRoleDao().createRole(role);
//
//        if (role.getIdRole() != null) {
//            request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_ROLE_SUCCESS);
//            request.setAttribute("typeModal", Display.SWAL_SUCCESS);
//        }
//        else {
//            request.setAttribute("txtModal", Display.TEXT_MODAL_ADD_ROLE_ERROR);
//            request.setAttribute("typeModal", Display.SWAL_ERROR);
//        }
//    }
//
//    private void editRole(HttpServletRequest request) {
//        Role role = new Role();
//        role.setIdRole(Integer.parseInt(request.getParameter("edit-role")));
//        role.setIdentifiant(request.getParameter("id-role"));
//        role.setDescription(request.getParameter("description-role"));
//
//        serviceFacade.getRoleDao().updateRole(role);
//
//        request.setAttribute("txtModal", Display.TEXT_MODAL_EDIT_ROLE_SUCCESS);
//        request.setAttribute("typeModal", Display.SWAL_SUCCESS);
//    }
}
