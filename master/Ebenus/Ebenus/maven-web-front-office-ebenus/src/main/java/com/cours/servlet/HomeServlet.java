package com.cours.servlet;

import com.cours.constants.URIsFront;
import com.cours.ebenus.service.ServiceFacade;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "HomeServlet", urlPatterns = {"/HomeServlet"})
public class HomeServlet extends HttpServlet {
    private ServiceFacade serviceFacade = null;

    @Override
    public void init() throws ServletException {
        serviceFacade = new ServiceFacade();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(URIsFront.URL_SIGNUP).forward(request, response);
    }
}
