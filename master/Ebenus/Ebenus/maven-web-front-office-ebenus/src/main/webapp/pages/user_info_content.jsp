<%@ page import="com.cours.ebenus.dao.entities.Utilisateur" %>
<%@ page import="com.cours.ebenus.dao.entities.Adresse" %>
<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<% Adresse adresse = (Adresse) request.getSession().getAttribute("adresse"); %>
<section>
    <div class="content">
        <div class="customer-acount">
            <h1>Compte Client</h1>
            <jsp:include page="../partials/_user_account.jsp" />
            <div class=" common-form-controls col-md-9 offset-md-3">
                <form id="common-info-form" class="row no-gutters" action="${pageContext.request.contextPath}/UserInfo" method="post">
                    <fieldset class="col-md-6">
                        <h2>Informations personnelles</h2>
                        <label for="firstname">Prénom</label>
                        <input value="<%= user.getPrenom() %>" name="firstname" id="firstname" type="text">
                        <label for="lastname">Nom</label>
                        <input value="<%= user.getNom() %>" name="lastname" id="lastname" type="text">
                        <label for="email">Identifiant</label>
                        <input value="<%= user.getIdentifiant() %>" name="email" id="email" type="email">
                        <label for="password">Mot de passe</label>
                        <input name="password" value="<%= user.getMotPasse() %>" id="password" type="password">
                        <label for="password">Confimer mot de psse</label>
                        <input name="password" value="<%= user.getMotPasse() %>" id="password-C" type="password">
                        <label>Civilité</label>
                        <div class="gender">
                            <% if (user.getCivilite().equals("male")) { %>
                            <input type="radio" id="male" name="sex" checked="checked"/>
                            <input type="radio" id="female" name="sex"/>
                            <% } else { %>
                            <input type="radio" id="male" name="sex"/>
                            <input type="radio" id="female" name="sex" checked="checked"/>
                            <% } %>
                            <label for="male">
                                <i class="fa fa-male" aria-hidden="true"></i>
                            </label>
                            <label for="female">
                                <i class="fa fa-female" aria-hidden="true"></i>
                            </label>
                        </div>
                        <label> Date de naissance</label>
                        <div class="birth-day justify-content-between">
                            <div class="sel-container">
                                <div class="sel">
                                    <select name="select-adress" id="select-day">
                                        <option value="Jour" disabled>Jour</option>
                                        <option value=" 1 "> 1 </option>
                                        <option value=" 2 "> 2 </option>
                                        <option value=" 3 "> 3 </option>
                                        <option value=" 4 "> 4 </option>
                                        <option value=" 5 "> 5 </option>
                                        <option value=" 6 "> 6 </option>
                                        <option value=" 7 "> 7 </option>
                                        <option value=" 8 "> 8 </option>
                                        <option value=" 9 "> 9 </option>
                                        <option value="10 ">10 </option>
                                        <option value="11 ">11 </option>
                                        <option value="12 ">12 </option>
                                        <option value="13 ">13 </option>
                                        <option value="14 ">14 </option>
                                        <option value="15 ">15 </option>
                                        <option value="16 ">16 </option>
                                        <option value="17 ">17 </option>
                                        <option value="18 ">18 </option>
                                        <option value="19 ">19 </option>
                                        <option value="20 ">20 </option>
                                        <option value="21 ">21 </option>
                                        <option value="22 ">22 </option>
                                        <option value="23 ">23 </option>
                                        <option value="24 ">24 </option>
                                        <option value="25 ">25 </option>
                                        <option value="26 ">26 </option>
                                        <option value="27 ">27 </option>
                                        <option value="28 ">28 </option>
                                        <option value="29 ">29 </option>
                                        <option value="30 ">30 </option>
                                        <option value="31 ">31 </option>

                                    </select>
                                </div>
                            </div>
                            <div class="sel-container">
                                <div class="sel">
                                    <select name="select-adress" id="select-month">
                                        <option value="Mois" disabled>Mois</option>
                                        <option value="Janvier ">Janvier </option>
                                        <option value="Février ">Février </option>
                                        <option value="Mars ">Mars </option>
                                        <option value="Avril ">Avril </option>
                                        <option value="Mai ">Mai </option>
                                        <option value="Juin ">Juin </option>
                                        <option value="Juillet ">Juillet </option>
                                        <option value="Aout ">Aout </option>
                                        <option value="Septembre ">Septembre </option>
                                        <option value="Octobre ">Octobre </option>
                                        <option value="Novembre ">Novembre </option>
                                        <option value="Décembre ">Décembre </option>

                                    </select>
                                </div>
                            </div>
                            <div class="sel-container">
                                <div class="sel">
                                    <select name="select-adress" id="select-year">
                                        <option value="Année" disabled>Année</option>
                                        <option value="1990 ">1990 </option>
                                        <option value="1989 ">1989 </option>
                                        <option value="1988 ">1988 </option>
                                        <option value="1987 ">1987 </option>
                                        <option value="1986 ">1986 </option>
                                        <option value="1985 ">1985 </option>
                                        <option value="1984 ">1984 </option>
                                        <option value="1983 ">1983 </option>
                                        <option value="1982 ">1982 </option>
                                        <option value="1981 ">1981 </option>
                                        <option value="1980 ">1980 </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="col-md-6">
                        <h2>Adresse Principale</h2>
                    </fieldset>
                    <div class="actions">
                        <button type="submit">Modifier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</section>

