<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ebenus</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800|Oswald:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/master.css">
</head>
<body>
<div class="outer">
    <jsp:include page="../partials/_header.jsp"/>
    <jsp:include page="./${param.content}.jsp"/>
    <jsp:include page="../partials/_footer.jsp"/>
</div>
<!-- JS files -->
<script src="js/bower.js" type="text/javascript"></script>
<script src="js/application.js" type="text/javascript"></script>
</body>
</html>
