<%--
  Created by IntelliJ IDEA.
  User: ouahab
  Date: 11/04/2019
  Time: 17:09
  To change this template use File | Settings | File Templates.
--%>


<%@ page import="com.cours.ebenus.dao.entities.Utilisateur" %>
<%@ page import="com.cours.ebenus.dao.entities.Produit" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>
<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<%--<% if (request.getAttribute("javax.servlet.forward.request_uri") == null) { %>--%>
<%--<% response.sendRedirect("/frontOffice/ProduitServlet"); %>--%>
<%--<% } else if (user == null) { %>--%>
<%--<% response.sendRedirect("/frontOffice/LoginServlet"); %>--%>
<%--<% } %>--%>
<!DOCTYPE HTML>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ebenus</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.png">
    <!-- CSS files -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800|Oswald:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/master.css">
</head>
<body>
<div class="outer">
    <div class="header-outer" id="header-outer">
        <header id="header"  class="header">
            <div class="header">
                <a href="ProductServlet" title="Ebenus Commerce" class="logo">
                    <img src="${pageContext.request.contextPath}/images/logo.png" alt="Ebenus Commerce">
                </a>
                <ul class="main-menu">
                    <li><a href="ProductServlet">Accueil</a></li>
                    <li><a href="ProductServlet?list">Produits</a></li>
                    <li><a href="${pagecontext.request.contextpath}/frontOffice/pages/qui_somme_nous.jsp">Qui sommes-nous</a></li>
                    <li><a href="${pagecontext.request.contextpath}/pages/contact.html">Contactez-nous</a></li>
                    <%--<li>--%>
                        <%--<a href="#" class="account-link">Mon Compte</a>--%>
                        <%--<ul class="sub-menu">--%>
                            <%--<% if (user != null) { %>--%>
                            <%--<li><a href="UserInfoServlet">Mon Compte</a></li>--%>
                            <%--<li class="guest">--%>
                                <%--<a href="ProductServlet?logout=true">Logout</a>--%>

                            <%--</li>--%>
                            <%--<% } else { %>--%>
                            <%--<li class="guest">--%>
                                <%--<a href="../LogoutServlet">Login</a>--%>
                            <%--</li>--%>
                            <%--<li><a href=SignUpServlet">Créer Compte</a></li>--%>
                            <%--<% } %>--%>
                            <%--<li><a href="../pages/panier.html">Panier</a></li>--%>
                        <%--</ul>--%>
                    <%--</li>--%>
                </ul>
                <div class="phone-block">
                    <i class="icon-phone"></i>
                    Appelez nous<br>
                    <b>+33 5678 890</b>
                </div>
                <div class="search-area">
                    <a href="javascript:void(0)" class="search-icon">
                        <!-- <i class="fa fa-search"></i> -->
                        <i class="icon-search"></i>
                    </a>
                    <form id="search_mini_form" action="" method="get">
                        <div class="form-search">
                            <input id="search" placeholder="Rechercher un produit" type="text" name="q" class="input-text" autocomplete="off">
                            <button type="submit" title="Search" class="button"><i class="icon-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </header>
    </div>
    <section>
        <div class="content">
            <h1>Qui Sommes-Nous</h1>
            <div class="row who-we-are">
                <div class="col-md-9">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, delectus reprehenderit accusamus repellendus provident, maxime placeat, ullam dignissimos autem mollitia libero expedita suscipit totam saepe? Tempora vel, obcaecati! Corporis eius veniam, nemo accusamus, tenetur aperiam dicta natus architecto! Deserunt impedit fugiat, et eveniet nesciunt doloribus laboriosam ducimus, ex tenetur explicabo vel nihil dolor consequatur officiis dolore suscipit assumenda. Tenetur saepe atque minima, commodi sequi repudiandae magni voluptates architecto ad maxime nostrum quo labore iste beatae ratione nisi, asperiores doloribus. Maxime, quo debitis voluptatem molestias! Reiciendis, quasi fugit ut consequuntur ipsam nobis illo quia magni nemo possimus. Eos voluptates, fugiat odio nihil ipsum iste dicta illum sapiente necessitatibus quidem explicabo voluptatibus.</p>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </div>
    </section>
</div>

<footer>
    <div class="footer-container ">
        <div class="footer">
            <div class="footer-middle">
                <div class="footer-container_">
                    <div class="row no-gutters">
                        <div class="col-sm-6 col-md-3">
                            <div class="block">
                                <div class="block-title"><strong><span>Contactez Nous</span></strong></div>
                                <div class="block-content">
                                    <ul class="contact-info">
                                        <li><i class="icon-location">&nbsp;</i><p><b>Addresse:</b><br>123 Rue la victoire, 75000 Paris, France</p></li>
                                        <li><i class="icon-phone">&nbsp;</i><p><b>Tél:</b><br>(123) 456-7890</p></li>
                                        <li><i class="icon-mail">&nbsp;</i><p><b>Email:</b><br><a href="mailto:mail@example.com">mail@example.com</a></p></li>
                                        <li><i class="icon-clock">&nbsp;</i><p><b>Horaire Jours/Heures:</b><br>Lun - Dim / 9:00AM - 8:00PM</p></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3"><div class="block">
                            <div class="block-title"><strong><span>Mon compte</span></strong></div>
                            <div class="block-content">
                                <ul class="links">
                                    <li><i class="icon-right-dir theme-color"></i><a href="#" title="About us">Mon compte</a></li>
                                    <li><i class="icon-right-dir theme-color"></i><a href="#" title="About us">A props de nous</a></li>
                                    <li><i class="icon-right-dir theme-color"></i><a href="#" title="Contact us">Contacez nous</a></li>
                                    <li><i class="icon-right-dir theme-color"></i><a href="pannier.html" title="Advanced search">Mon pannier</a></li>
                                </ul>
                            </div>
                        </div>
                        </div>

                        <div class="col-sm-6 col-md-3"><div class="block">
                            <div class="block-title"><strong><span>Information</span></strong></div>
                            <div class="block-content">
                                <ul class="features">
                                    <li><i class="icon-ok theme-color"></i><a href="#">Historique des commandes</a></li>
                                    <li><i class="icon-ok theme-color"></i><a href="#">Site Map</a></li>
                                </ul>
                            </div>
                        </div>
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <div class="block">
                                <div class="block-title"><strong><span>Nos Services</span></strong></div>
                                <div class="block-content">
                                    <ul class="features">
                                        <li><i class="icon-ok  theme-color"></i><a href="#">Service Client</a></li>
                                        <li><i class="icon-ok theme-color"></i><a href="#">Les Options de Transport</a></li>
                                        <li><i class="icon-ok  theme-color"></i><a href="#">Avoir et Change</a></li>
                                        <li><i class="icon-ok  theme-color"></i><a href="#">Politique d'Utilisation</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="footer-container_">
                    <div class="custom-block f-right"><div class="block-bottom">
                        <div class="custom-block" >
                            <img src="images/payment-icon.png" alt=""></div>

                    </div>
                    </div>
                    <address>© Ebenus eCommerce. 2018. Tous droit réservé</address>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="${pageContext.request.contextPath}/js/bower.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/application.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>