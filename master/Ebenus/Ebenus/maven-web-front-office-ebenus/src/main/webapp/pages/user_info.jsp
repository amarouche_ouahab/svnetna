<%@ page import="com.cours.ebenus.dao.entities.Utilisateur" %>
<%@page pageEncoding="UTF-8" %>
<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<% if (request.getAttribute("javax.servlet.forward.request_uri") == null) { %>
<% response.sendRedirect("/frontOffice/UserInfoServlet"); %>
<% } else if (user == null) { %>
<% response.sendRedirect("/frontOffice/LoginServlet"); %>
<% } %>
<!DOCTYPE HTML>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ebenus</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.png">
    <!-- CSS files -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800|Oswald:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/master.css">
</head>
<body>
<div class="outer">
    <div class="header-outer" id="header-outer">
        <!-- Header -->
        <header id="header"  class="header">
            <div class="header">
                <a href="ProductServlet" title="Ebenus Commerce" class="logo">
                    <img src="${pageContext.request.contextPath}/images/logo/logo.png" alt="Ebenus Commerce">
                </a>
                <ul class="main-menu">
                    <li><a href="ProductServlet">Accueil</a></li>
                    <li><a href="ProductServlet?list">Produits</a></li>
                    <%--<li><a href="${pagecontext.request.contextpath}/frontOffice/pages/qui_somme_nous.jsp">Qui sommes-nous</a></li>--%>
                    <li><a href="#">Qui sommes-nous</a></li>

                    <li><a href="#">Contactez-nous</a></li>
                    <li>
                        <a href="#" class="account-link">Mon Compte</a>
                        <ul class="sub-menu">
                            <% if (user != null) { %>
                            <li><a href="UserInfoServlet">Mon Compte</a></li>
                            <li class="guest">
                                <a href="ProductServlet?logout=true">Logout</a>

                            </li>
                            <% } else { %>
                            <li class="guest">
                                <a href="LogoutServlet">Login</a>
                            </li>
                            <li><a href=SignUpServlet">Créer Compte</a></li>
                            <% } %>
                            <li><a href="../pages/panier.html">Panier</a></li>
                        </ul>
                    </li>
                </ul>

                <div class="phone-block">
                    <i class="icon-phone"></i>
                    Appelez nous<br>
                    <b>+33 5678 890</b>
                </div>

                <div class="search-area">
                    <a href="javascript:void(0)" class="search-icon">
                        <!-- <i class="fa fa-search"></i> -->
                        <i class="icon-search"></i>
                    </a>
                    <form id="search_mini_form" action="" method="get">
                        <div class="form-search">
                            <input id="search" placeholder="Rechercher un produit" type="text" name="q" class="input-text" autocomplete="off">
                            <button type="submit" title="Search" class="button"><i class="icon-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </header>
    </div>
    <section>
        <div class="content">
            <div class="customer-acount">
            <h1>Compte Client</h1>
            <div class="account-container row">
                <div class="facet-navigation col-md-3">
                    <ul>
                        <li><a href="UserInfoServlet">Information personnelles</a></li>
                        <li><a href="#">Gérer mon carnet d'adresses</a></li>
                        <li><a href="CommandeServlet?userId=<%=user.getIdUtilisateur()%>">Historique des commandes</a></li>

                    </ul>
                </div>

                <div class=" common-form-controls col-md-9">
                    <p>Bienvenue Mr <strong><%=user.getNom() + " "+user.getPrenom()%></strong>,<br/>
                        Voici le récapitulatif de votre compte. Vous pouvez changer vos informations personnelles et gérer les options de votre compte.</p>
                    <img src="images/banner.jpg" alt="">
                </div>
                <div class=" common-form-controls col-md-9">
                    <form id="common-info-form" class="row no-gutters" action="UserInfoServlet?update" method="post">

                    <fieldset class="col-md-6">
                            <h2>Informations personnelles</h2>
                            <label for="firstname">Prénom</label>
                            <input value="<%=user.getPrenom()%>" name="firstname" id="firstname" type="text">
                            <label for="lastname">Nom</label>
                            <input value="<%=user.getNom()%>" name="lastname" id="lastname" type="text">
                            <label for="email">Identifiant</label>
                            <input value="<%=user.getIdentifiant()%>" name="email" id="email" type="email">
                            <label for="password">Mot de passe</label>
                            <input name="password" value="" id="password" type="password">
                            <label for="password">Confimer mot de psse</label>
                            <input name="password" value="" id="password-C" type="password">
                            <label>Civilité</label>
                            <div class="gender" style="float:left">
                                <div class="gender">
                                    <% if (user.getCivilite().equals("male")) { %>
                                    <input type="radio" id="male" name="sex" checked="checked"/>
                                    <label for="male">
                                        <i class="fa fa-male" aria-hidden="true"></i>
                                    </label>
                                    <input type="radio" id="female" name="sex"/>
                                    <label for="female">
                                        <i class="fa fa-female" aria-hidden="true"></i>
                                    </label>
                                    <% } else { %>
                                    <input type="radio" id="male" name="sex"/>
                                    <label for="male">
                                        <i class="fa fa-male" aria-hidden="true"></i>
                                    </label>
                                    <input type="radio" id="female" name="sex" checked="checked"/>
                                    <label for="female">
                                        <i class="fa fa-female" aria-hidden="true"></i>
                                    </label>
                                    <% } %>
                                </div>

                            </div>


                            <label> Date de naissance</label>
                            <div class="birth-day justify-content-between">
                                <div class="sel-container">
                                    <div class="sel">
                                        <select name="select-adress" id="select-day">
                                            <option value="Jour" disabled>Jour</option>

                                            <option value=" 1 "> 1 </option>
                                            <option value=" 2 "> 2 </option>
                                            <option value=" 3 "> 3 </option>
                                            <option value=" 4 "> 4 </option>
                                            <option value=" 5 "> 5 </option>
                                            <option value=" 6 "> 6 </option>
                                            <option value=" 7 "> 7 </option>
                                            <option value=" 8 "> 8 </option>
                                            <option value=" 9 "> 9 </option>
                                            <option value="10 ">10 </option>
                                            <option value="11 ">11 </option>
                                            <option value="12 ">12 </option>
                                            <option value="13 ">13 </option>
                                            <option value="14 ">14 </option>
                                            <option value="15 ">15 </option>
                                            <option value="16 ">16 </option>
                                            <option value="17 ">17 </option>
                                            <option value="18 ">18 </option>
                                            <option value="19 ">19 </option>
                                            <option value="20 ">20 </option>
                                            <option value="21 ">21 </option>
                                            <option value="22 ">22 </option>
                                            <option value="23 ">23 </option>
                                            <option value="24 ">24 </option>
                                            <option value="25 ">25 </option>
                                            <option value="26 ">26 </option>
                                            <option value="27 ">27 </option>
                                            <option value="28 ">28 </option>
                                            <option value="29 ">29 </option>
                                            <option value="30 ">30 </option>
                                            <option value="31 ">31 </option>

                                        </select>
                                    </div>
                                </div>
                                <div class="sel-container">
                                    <div class="sel">
                                        <select name="select-adress" id="select-month">
                                            <option value="Mois" disabled>Mois</option>
                                            <option value="Janvier ">Janvier </option>
                                            <option value="Février ">Février </option>
                                            <option value="Mars ">Mars </option>
                                            <option value="Avril ">Avril </option>
                                            <option value="Mai ">Mai </option>
                                            <option value="Juin ">Juin </option>
                                            <option value="Juillet ">Juillet </option>
                                            <option value="Aout ">Aout </option>
                                            <option value="Septembre ">Septembre </option>
                                            <option value="Octobre ">Octobre </option>
                                            <option value="Novembre ">Novembre </option>
                                            <option value="Décembre ">Décembre </option>

                                        </select>
                                    </div>
                                </div>
                                <div class="sel-container">
                                    <div class="sel">
                                        <select name="select-adress" id="select-year">
                                            <option value="Année" disabled>Année</option>
                                            <option value="1990 ">1990 </option>
                                            <option value="1989 ">1989 </option>
                                            <option value="1988 ">1988 </option>
                                            <option value="1987 ">1987 </option>
                                            <option value="1986 ">1986 </option>
                                            <option value="1985 ">1985 </option>
                                            <option value="1984 ">1984 </option>
                                            <option value="1983 ">1983 </option>
                                            <option value="1982 ">1982 </option>
                                            <option value="1981 ">1981 </option>
                                            <option value="1980 ">1980 </option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <fieldset class="col-md-6">
                            <h2>Adresse Principale</h2>
                            <label for="street">Rue</label>
                            <input value="victoire" name="street" id="street" type="text">
                            <label for="postalcode">Code Postale</label>
                            <input value="75000" name="postalcode" id="postalcode" type="text">
                            <label for="city">Ville</label>
                            <input value="Paris" name="city" id="city" type="text">
                            <label for="country">Pays</label>
                            <input value="France" name="country" id="country" type="text">
                        </fieldset>
                        <div class="actions">
                            <button type="submit">Modifier</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- Footer -->
    <footer>
        <div class="footer-container ">
            <div class="footer">
                <div class="footer-middle">
                    <div class="footer-container_">
                        <div class="row no-gutters">
                            <div class="col-sm-6 col-md-3">
                                <div class="block">
                                    <div class="block-title"><strong><span>Contactez Nous</span></strong></div>
                                    <div class="block-content">
                                        <ul class="contact-info">
                                            <li><i class="icon-location">&nbsp;</i><p><b>Addresse:</b><br>123 Rue la victoire, 75000 Paris, France</p></li>
                                            <li><i class="icon-phone">&nbsp;</i><p><b>Tél:</b><br>(123) 456-7890</p></li>
                                            <li><i class="icon-mail">&nbsp;</i><p><b>Email:</b><br><a href="mailto:mail@example.com">mail@example.com</a></p></li>
                                            <li><i class="icon-clock">&nbsp;</i><p><b>Horaire Jours/Heures:</b><br>Lun - Dim / 9:00AM - 8:00PM</p></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3"><div class="block">
                                <div class="block-title"><strong><span>Mon compte</span></strong></div>
                                <div class="block-content">
                                    <ul class="links">
                                        <li><i class="icon-right-dir theme-color"></i><a href="#" title="About us">Mon compte</a></li>
                                        <li><i class="icon-right-dir theme-color"></i><a href="#" title="About us">A props de nous</a></li>
                                        <li><i class="icon-right-dir theme-color"></i><a href="#" title="Contact us">Contacez nous</a></li>
                                        <li><i class="icon-right-dir theme-color"></i><a href="pannier.html" title="Advanced search">Mon pannier</a></li>
                                    </ul>
                                </div>
                            </div>
                            </div>

                            <div class="col-sm-6 col-md-3"><div class="block">
                                <div class="block-title"><strong><span>Information</span></strong></div>
                                <div class="block-content">
                                    <ul class="features">
                                        <li><i class="icon-ok theme-color"></i><a href="#">Historique des commandes</a></li>
                                        <li><i class="icon-ok theme-color"></i><a href="#">Site Map</a></li>
                                    </ul>
                                </div>
                            </div>
                            </div>

                            <div class="col-sm-6 col-md-3">
                                <div class="block">
                                    <div class="block-title"><strong><span>Nos Services</span></strong></div>
                                    <div class="block-content">
                                        <ul class="features">
                                            <li><i class="icon-ok  theme-color"></i><a href="#">Service Client</a></li>
                                            <li><i class="icon-ok theme-color"></i><a href="#">Les Options de Transport</a></li>
                                            <li><i class="icon-ok  theme-color"></i><a href="#">Avoir et Change</a></li>
                                            <li><i class="icon-ok  theme-color"></i><a href="#">Politique d'Utilisation</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="footer-container_">
                        <div class="custom-block f-right"><div class="block-bottom">
                            <div class="custom-block" >
                                <img src="images/payment-icon.png" alt=""></div>

                        </div>
                        </div>
                        <address>© Ebenus eCommerce. 2018. Tous droit réservé</address>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</div>


<!-- JS files -->

<script src="${pageContext.request.contextPath}/js/bower.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/application.js" type="text/javascript" charset="utf-8"></script>



</body>
</html>
