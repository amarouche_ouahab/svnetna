<%@ page import="com.cours.ebenus.dao.entities.Utilisateur" %>
<%@ page import="com.cours.ebenus.dao.entities.Produit" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>
<% Utilisateur user = (Utilisateur) request.getSession().getAttribute("user"); %>
<% if (request.getAttribute("javax.servlet.forward.request_uri") == null) { %>
<% response.sendRedirect("/frontOffice/ProduitServlet"); %>
<% } else if (user == null) { %>
<% response.sendRedirect("/frontOffice/LoginServlet"); %>
<% } %>
<!DOCTYPE HTML>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ebenus</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/logo/favicon.png">
    <!-- CSS files -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800|Oswald:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/master.css">
</head>
<body>

<div class="outer">
    <div class="header-outer" id="header-outer">
        <header id="header"  class="header">
            <div class="header">
                <a href="ProductServlet" title="Ebenus Commerce" class="logo">
                    <img src="${pageContext.request.contextPath}/images/logo.png" alt="Ebenus Commerce">
                </a>
                <ul class="main-menu">
                    <li><a href="ProductServlet">Accueil</a></li>
                    <li><a href="ProductServlet?list">Produits</a></li>
                    <li><a href="${pagecontext.request.contextpath}/pages/#">Qui sommes-nous</a></li>

                    <%--<li><a href="${pagecontext.request.contextpath}/frontOffice/pages/qui_somme_nous.jsp">Qui sommes-nous</a></li>--%>
                    <li><a href="${pagecontext.request.contextpath}/frontOffice/pages/panier.html">Contactez-nous</a></li>
                    <li>
                        <a href="UserInfoServlet" class="account-link">Mon Compte</a>
                        <ul class="sub-menu">
                            <% if (user != null) { %>
                            <li><a href="UserInfoServlet">Mon Compte</a></li>
                            <li class="guest">
                                <a href="LoginServlet?logout=true">Logout</a>
                            </li>
                            <% } else { %>
                            <li class="guest">
                                <a href="LoginServlet">Login</a>
                            </li>
                            <li><a href=SignUpServlet">Créer Compte</a></li>
                            <% } %>
                            <li><a href="../pages/panier.html">Panier</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="phone-block">
                    <i class="icon-phone"></i>
                    Appelez nous<br>
                    <b>+33 5678 890</b>
                </div>
                <div class="search-area">
                    <a href="javascript:void(0)" class="search-icon">
                        <!-- <i class="fa fa-search"></i> -->
                        <i class="icon-search"></i>
                    </a>
                    <form id="search_mini_form" action="" method="get">
                        <div class="form-search">
                            <input id="search" placeholder="Rechercher un produit" type="text" name="q" class="input-text" autocomplete="off">
                            <button type="submit" title="Search" class="button"><i class="icon-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </header>
    </div>
    <section>
        <div class="content">
        <div class="products-container">
    <h1>Les produits les plus achetés</h1>
    <div class="row no-gutters products-box">
        <div class="products col-md-12">
            <ul class="row no-gutters justify-content-between">
                <% if (request.getAttribute("produit10") == null) return; %>
                <% for (Produit produit : (List<Produit>) request.getAttribute("produit10")) { %>
                <li class="col-sm-6 col-md-3 col-lg-2">
                    <div class="image-wrapper">
                        <img src="https://www.materiel.net/live/360365.350.350.jpg" alt="">
                        <a href="ProductServlet?produit=<%=produit.getIdProduit()%>" class="add-to-cart-btn">
                            <button  class="addtocart" title="Add to Cart" >
                                <i class="icon-cart"></i>
                                <span>Ajouter au panier</span>
                            </button>
                        </a>
                    </div>
                    <div class="product-info">
                        <h2 class="product-name">
                            <a href="ProductServlet?produit=<%=produit.getIdProduit()%>">
                              <%=produit.getNom()%>
                            </a>
                        </h2>
                        <div class="price-box">
                            <span class="regular-price"><%=produit.getPrix()%>€</span>
                        </div>
                    </div>
                </li>
                <% } %>
                <%--<li class="col-sm-6 col-md-3 col-lg-2">--%>
                    <%--<div class="image-wrapper">--%>
                        <%--<img src="https://www.materiel.net/live/360591.350.350.jpg" alt="">--%>
                        <%--<a href="detail-produit-2.html" class="add-to-cart-btn">--%>
                            <%--<button  class="addtocart" title="Add to Cart" >--%>
                                <%--<i class="icon-cart"></i>--%>
                                <%--<span>Ajouter au panier</span>--%>
                            <%--</button>--%>
                        <%--</a>--%>
                    <%--</div>--%>
                    <%--<div class="product-info">--%>
                        <%--<h2 class="product-name">--%>
                            <%--<a href="detail-produit-2.html">--%>
                                <%--Smartphone Apple iPhone 6s (or) - 32 Go--%>
                            <%--</a>--%>
                        <%--</h2>--%>
                        <%--<div class="price-box">--%>
                            <%--<span class="regular-price">529,00€</span>--%>
                        <%--</div>--%>
                    <%--</div>--%>


                <%--</li>--%>
                <%--<li class="col-sm-6 col-md-3 col-lg-2">--%>
                    <%--<div class="image-wrapper">--%>
                        <%--<img src="https://www.materiel.net/live/194803.350.350.jpg" alt="">--%>
                        <%--<a href="detail-produit-3.html" class="add-to-cart-btn">--%>
                            <%--<button  class="addtocart" title="Add to Cart" >--%>
                                <%--<i class="icon-cart"></i>--%>
                                <%--<span>Ajouter au panier</span>--%>
                            <%--</button>--%>
                        <%--</a>--%>
                    <%--</div>--%>
                    <%--<div class="product-info">--%>
                        <%--<h2 class="product-name">--%>
                            <%--<a href="detail-produit-3.html">--%>
                                <%--Apple Câble Lightning / USB--%>
                            <%--</a>--%>
                        <%--</h2>--%>
                        <%--<div class="price-box">--%>
                            <%--<span class="regular-price">27,95€</span>--%>
                        <%--</div>--%>
                    <%--</div>--%>


                <%--</li>--%>
                <%--<li class="col-sm-6 col-md-3 col-lg-2">--%>
                    <%--<div class="image-wrapper">--%>
                        <%--<img src="https://www.materiel.net/live/199301.350.350.jpg" alt="">--%>
                        <%--<a href="detail-produit-4.html" class="add-to-cart-btn">--%>
                            <%--<button  class="addtocart" title="Add to Cart" >--%>
                                <%--<i class="icon-cart"></i>--%>
                                <%--<span>Ajouter au panier</span>--%>
                            <%--</button>--%>
                        <%--</a>--%>
                    <%--</div>--%>
                    <%--<div class="product-info">--%>
                        <%--<h2 class="product-name">--%>
                            <%--<a href="detail-produit-4.html">--%>
                                <%--Chargeur secteur Belkin Chargeur secteur USB 2,1A--%>
                            <%--</a>--%>
                        <%--</h2>--%>
                        <%--<div class="price-box">--%>
                            <%--<span class="regular-price">27,95€</span>--%>
                        <%--</div>--%>
                    <%--</div>--%>


                <%--</li>--%>
                <%--<li class="col-sm-6 col-md-3 col-lg-2">--%>
                    <%--<div class="image-wrapper">--%>
                        <%--<img src="https://www.materiel.net/live/398168.350.350.jpg" alt="">--%>
                        <%--<a href="detail-produit-5.html" class="add-to-cart-btn">--%>
                            <%--<button  class="addtocart" title="Add to Cart" >--%>
                                <%--<i class="icon-cart"></i>--%>
                                <%--<span>Ajouter au panier</span>--%>
                            <%--</button>--%>
                        <%--</a>--%>
                    <%--</div>--%>
                    <%--<div class="product-info">--%>
                        <%--<h2 class="product-name">--%>
                            <%--<a href="detail-produit-5.html">--%>
                                <%--Ordinateur portable Asus Zenbook Flip 58256-B - i5 - 8 Go - 256 Go SSD--%>
                            <%--</a>--%>
                        <%--</h2>--%>
                        <%--<div class="price-box">--%>
                            <%--<span class="regular-price">1 039,92€</span>--%>
                        <%--</div>--%>
                    <%--</div>--%>


                <%--</li>--%>
                <%--<li class="col-sm-6 col-md-3 col-lg-2">--%>
                    <%--<div class="image-wrapper">--%>
                        <%--<img src="https://www.materiel.net/live/391814.350.350.jpg" alt="">--%>
                        <%--<a href="detail-produit-6.html" class="add-to-cart-btn">--%>
                            <%--<button  class="addtocart" title="Add to Cart" >--%>
                                <%--<i class="icon-cart"></i>--%>
                                <%--<span>Ajouter au panier</span>--%>
                            <%--</button>--%>
                        <%--</a>--%>
                    <%--</div>--%>
                    <%--<div class="product-info">--%>
                        <%--<h2 class="product-name">--%>
                            <%--<a href="detail-produit-6.html">--%>
                                <%--PC portable gamer MSI GS73 7RE-006FR - i7 - 8 Go - SSD - GTX 1050 Ti--%>
                            <%--</a>--%>
                        <%--</h2>--%>
                        <%--<div class="price-box">--%>
                            <%--<span class="regular-price">1 299,90€</span>--%>
                        <%--</div>--%>
                    <%--</div>--%>


                <%--</li>--%>
                <%--<li class="col-sm-6 col-md-3 col-lg-2">--%>
                    <%--<div class="image-wrapper">--%>
                        <%--<img src="https://www.materiel.net/live/386236.350.350.jpg" alt="">--%>
                        <%--<a href="detail-produit-7.html" class="add-to-cart-btn">--%>
                            <%--<button  class="addtocart" title="Add to Cart" >--%>
                                <%--<i class="icon-cart"></i>--%>
                                <%--<span>Ajouter au panier</span>--%>
                            <%--</button>--%>
                        <%--</a>--%>
                    <%--</div>--%>
                    <%--<div class="product-info">--%>
                        <%--<h2 class="product-name">--%>
                            <%--<a href="detail-produit-7.html">--%>
                                <%--Ordinateur portable Acer Aspire E5-774-36WT - i3 - 4 Go - 1 To--%>
                            <%--</a>--%>
                        <%--</h2>--%>
                        <%--<div class="price-box">--%>
                            <%--<span class="regular-price">424,92€</span>--%>
                        <%--</div>--%>
                    <%--</div>--%>


                <%--</li>--%>
                <%--<li class="col-sm-6 col-md-3 col-lg-2">--%>
                    <%--<div class="image-wrapper">--%>
                        <%--<img src="https://www.materiel.net/live/394824.350.350.jpg" alt="">--%>
                        <%--<a href="detail-produit-8.html" class="add-to-cart-btn">--%>
                            <%--<button  class="addtocart" title="Add to Cart" >--%>
                                <%--<i class="icon-cart"></i>--%>
                                <%--<span>Ajouter au panier</span>--%>
                            <%--</button>--%>
                        <%--</a>--%>
                    <%--</div>--%>
                    <%--<div class="product-info">--%>
                        <%--<h2 class="product-name">--%>
                            <%--<a href="detail-produit-8.html">--%>
                                <%--PC portable gamer MSI GT75VR 7RE-064FR - i7 - 16 Go - SSD - GTX 1070--%>
                            <%--</a>--%>
                        <%--</h2>--%>
                        <%--<div class="price-box">--%>
                            <%--<span class="regular-price">2 599,90€</span>--%>
                        <%--</div>--%>
                    <%--</div>--%>


                <%--</li>--%>
                <%--<li class="col-sm-6 col-md-3 col-lg-2">--%>
                    <%--<div class="image-wrapper">--%>
                        <%--<img src="https://www.materiel.net/live/375167.350.350.jpg" alt="">--%>
                        <%--<a href="detail-produit-9.html" class="add-to-cart-btn">--%>
                            <%--<button  class="addtocart" title="Add to Cart" >--%>
                                <%--<i class="icon-cart"></i>--%>
                                <%--<span>Ajouter au panier</span>--%>
                            <%--</button>--%>
                        <%--</a>--%>
                    <%--</div>--%>
                    <%--<div class="product-info">--%>
                        <%--<h2 class="product-name">--%>
                            <%--<a href="detail-produit-9.html">--%>
                                <%--Appareil photo Compact Canon Ixus 185 Noir--%>
                            <%--</a>--%>
                        <%--</h2>--%>
                        <%--<div class="price-box">--%>
                            <%--<span class="regular-price">99,00€</span>--%>
                        <%--</div>--%>
                    <%--</div>--%>


                <%--</li>--%>

                <%--<li class="col-sm-6 col-md-3 col-lg-2">--%>
                    <%--<div class="image-wrapper">--%>
                        <%--<img src="https://www.materiel.net/live/371176.350.350.jpg" alt="">--%>
                        <%--<a href="detail-produit-10.html" class="add-to-cart-btn">--%>
                            <%--<button  class="addtocart" title="Add to Cart" >--%>
                                <%--<i class="icon-cart"></i>--%>
                                <%--<span>Ajouter au panier</span>--%>
                            <%--</button>--%>
                        <%--</a>--%>
                    <%--</div>--%>
                    <%--<div class="product-info">--%>
                        <%--<h2 class="product-name">--%>
                            <%--<a href="detail-produit-10.html">--%>
                                <%--Appareil photo instantané Fujifilm Instax WIDE 300--%>
                            <%--</a>--%>
                        <%--</h2>--%>
                        <%--<div class="price-box">--%>
                            <%--<span class="regular-price">119,00€</span>--%>
                        <%--</div>--%>
                    <%--</div>--%>


                </li>
            </ul>
        </div>
    </div>
</div>
        </div>

    </section>

</div>

    <footer>
        <div class="footer-container ">
            <div class="footer">
                <div class="footer-middle">
                    <div class="footer-container_">
                        <div class="row no-gutters">
                            <div class="col-sm-6 col-md-3">
                                <div class="block">
                                    <div class="block-title"><strong><span>Contactez Nous</span></strong></div>
                                    <div class="block-content">
                                        <ul class="contact-info">
                                            <li><i class="icon-location">&nbsp;</i><p><b>Addresse:</b><br>123 Rue la victoire, 75000 Paris, France</p></li>
                                            <li><i class="icon-phone">&nbsp;</i><p><b>Tél:</b><br>(123) 456-7890</p></li>
                                            <li><i class="icon-mail">&nbsp;</i><p><b>Email:</b><br><a href="mailto:mail@example.com">mail@example.com</a></p></li>
                                            <li><i class="icon-clock">&nbsp;</i><p><b>Horaire Jours/Heures:</b><br>Lun - Dim / 9:00AM - 8:00PM</p></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3"><div class="block">
                                <div class="block-title"><strong><span>Mon compte</span></strong></div>
                                <div class="block-content">
                                    <ul class="links">
                                        <li><i class="icon-right-dir theme-color"></i><a href="#" title="About us">Mon compte</a></li>
                                        <li><i class="icon-right-dir theme-color"></i><a href="#" title="About us">A props de nous</a></li>
                                        <li><i class="icon-right-dir theme-color"></i><a href="#" title="Contact us">Contacez nous</a></li>
                                        <li><i class="icon-right-dir theme-color"></i><a href="pannier.html" title="Advanced search">Mon pannier</a></li>
                                    </ul>
                                </div>
                            </div>
                            </div>

                            <div class="col-sm-6 col-md-3"><div class="block">
                                <div class="block-title"><strong><span>Information</span></strong></div>
                                <div class="block-content">
                                    <ul class="features">
                                        <li><i class="icon-ok theme-color"></i><a href="#">Historique des commandes</a></li>
                                        <li><i class="icon-ok theme-color"></i><a href="#">Site Map</a></li>
                                    </ul>
                                </div>
                            </div>
                            </div>

                            <div class="col-sm-6 col-md-3">
                                <div class="block">
                                    <div class="block-title"><strong><span>Nos Services</span></strong></div>
                                    <div class="block-content">
                                        <ul class="features">
                                            <li><i class="icon-ok  theme-color"></i><a href="#">Service Client</a></li>
                                            <li><i class="icon-ok theme-color"></i><a href="#">Les Options de Transport</a></li>
                                            <li><i class="icon-ok  theme-color"></i><a href="#">Avoir et Change</a></li>
                                            <li><i class="icon-ok  theme-color"></i><a href="#">Politique d'Utilisation</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="footer-container_">
                        <div class="custom-block f-right"><div class="block-bottom">
                            <div class="custom-block" >
                                <img src="images/payment-icon.png" alt=""></div>

                        </div>
                        </div>
                        <address>© Ebenus eCommerce. 2018. Tous droit réservé</address>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<script src="${pageContext.request.contextPath}/js/bower.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/application.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>