/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.manual.map.impl;

import com.cours.ebenus.dao.DataSource;
import com.cours.ebenus.dao.IUtilisateurDao;
import com.cours.ebenus.dao.entities.Utilisateur;

import java.util.*;

import com.cours.ebenus.dao.exception.CustomException;
import com.cours.ebenus.utils.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ElHadji
 */
public class ManualMapUtilisateurDao /*extends AbstractMapDao<Utilisateur>*/ implements IUtilisateurDao {

    private static final Log log = LogFactory.getLog(ManualMapUtilisateurDao.class);
    private Map<Integer, Utilisateur> utilisateursMapDataSource =  DataSource.getInstance().getUtilisateursMapDataSource();;

    public ManualMapUtilisateurDao() {
//        utilisateursMapDataSource = DataSource.getInstance().getUtilisateursMapDataSource();
//       super(Utilisateur.class, DataSource.getInstance().getUtilisateursMapDataSource());
    }
    /**
     * Méthode qui retourne la liste de tous les utilisateurs de la database
     * (ici utilisateursMapDataSource)
     *
     * @return La liste de tous les utilisateurs de la database
     */
    @Override
    public List<Utilisateur> findAllUtilisateurs() {
        List<Utilisateur> list = new ArrayList<Utilisateur>(utilisateursMapDataSource.values());
        return list;
    }

    /**
     * Méthode qui retourne l'utilisateur d'id passé en paramètre de la database
     * (ici utilisateursMapDataSource)
     *
     * @param idUtilisateur L'id de l'user à récupérer
     * @return L'utilisateur d'id passé en paramètre, null si non trouvé
     */
    @Override
    public Utilisateur findUtilisateurById(int idUtilisateur) {
        return utilisateursMapDataSource.get(idUtilisateur);
    }

    /**
     * Méthode qui retourne une liste de tous les utilisateurs de la database
     * (ici utilisateursMapDataSource) dont le prénom est égal au paramètre
     * passé
     *
     * @param prenom Le prénom des utilisateurs à récupérer
     * @return Une liste de tous les utilisateurs dont le prénom est égal au
     * paramètre passé
     */
    @Override
    public List<Utilisateur> findUtilisateursByPrenom(String prenom) {
        List<Utilisateur> list = new ArrayList<>();
        Set<Map.Entry<Integer, Utilisateur>> st = utilisateursMapDataSource.entrySet();
        for (Map.Entry<Integer, Utilisateur> me:st)
        {
            if(prenom.equals(me.getValue().getPrenom())){
                list.add(me.getValue());
            }
        }
        return list;
    }

    /**
     * Méthode qui retourne une liste de tous les utilisateurs de la database
     * (ici utilisateursMapDataSource) dont le nom est égal au paramètre passé
     *
     * @param nom Le nom des utilisateurs à récupérer
     * @return Une liste de tous les utilisateurs dont le nom est égal au
     * paramètre passé
     */
    @Override
    public List<Utilisateur> findUtilisateursByNom(String nom) {
        List<Utilisateur> list = new ArrayList<>();
        Set<Map.Entry<Integer, Utilisateur>> st = utilisateursMapDataSource.entrySet();
        for (Map.Entry<Integer, Utilisateur> me:st)
        {
            if(nom.equals(me.getValue().getNom())){
                list.add(me.getValue());
            }
        }
        return list;
    }

    /**
     * Méthode qui retourne une liste de tous les utilisateurs de la database
     * (ici utilisateursListDataSource) dont l'identifiant est égal au paramètre
     * passé
     *
     * @param identifiant Le nom des utilisateurs à récupérer
     * @return Une liste de tous les utilisateurs dont l'identifiant est égal au
     * paramètre passé
     */
    @Override
    public List<Utilisateur> findUtilisateurByIdentifiant(String identifiant) {
        List<Utilisateur> list = new ArrayList<>();
        Set<Map.Entry<Integer, Utilisateur>> st = utilisateursMapDataSource.entrySet();
        for (Map.Entry<Integer, Utilisateur> me:st)
        {
            if(identifiant.equals(me.getValue().getIdentifiant())){
                list.add(me.getValue());
            }
        }
        return list;
    }

    /**
     * Méthode qui retourne une liste de tous les utilisateurs de la database
     * (ici utilisateursMapDataSource) dont le rôle a comme id celui passé en
     * paramètre
     *
     * @param idRole L'id du rôle des utilisateurs à récupérer
     * @return Une liste de tous les utilisateurs dont le rôle a comme id celui
     * passé en paramètre
     */
    @Override
    public List<Utilisateur> findUtilisateursByIdRole(int idRole) {
        List<Utilisateur> list = new ArrayList<>();
        Set<Map.Entry<Integer, Utilisateur>> st = utilisateursMapDataSource.entrySet();
        for (Map.Entry<Integer, Utilisateur> me:st)
        {
            if(idRole == me.getValue().getRole().getIdRole()){
                list.add(me.getValue());
            }
        }
        return list;
    }

    /**
     * Méthode qui retourne une liste de tous les utilisateurs de la database
     * (ici utilisateursMapDataSource) dont le rôle a comme identifiant celui
     * passé en paramètre
     *
     * @param identifiantRole L'identifiant du rôle des utilisateurs à récupérer
     * @return Une liste de tous les utilisateurs dont le rôle a comme
     * identifiant celui passé en paramètre
     */
    @Override
    public List<Utilisateur> findUtilisateursByIdentifiantRole(String identifiantRole) {
        List<Utilisateur> list = new ArrayList<>();
        Set<Map.Entry<Integer, Utilisateur>> st = utilisateursMapDataSource.entrySet();
        for (Map.Entry<Integer, Utilisateur> me:st)
        {
            if(identifiantRole.equals(me.getValue().getRole().getIdentifiant())){
                list.add(me.getValue());
            }
        }
        return list;
    }

    /**
     * Méthode qui permet d'ajouter un utilisateur dans la database (ici
     * utilisateursMapDataSource)
     *
     * @param user L'utilisateur à ajouter
     * @return L'utilisateur ajouté ou null si échec
     */
    @Override
    public Utilisateur createUtilisateur(Utilisateur user) {
        if (user != null && user.getIdentifiant() != null){
            Set<Map.Entry<Integer, Utilisateur>> st = utilisateursMapDataSource.entrySet();
            for (Map.Entry<Integer, Utilisateur> me:st)
            {
                if(user.getIdentifiant().equals(me.getValue().getIdentifiant())){
                    throw new CustomException(Constants.EXCEPTION_CODE_USER_ALREADY_EXIST);
                }
            }
            user.setDateCreation(new Date(System.currentTimeMillis()));
            user.setDateModification(new Date(System.currentTimeMillis()));
            user.setIdUtilisateur(this.generateId());
            utilisateursMapDataSource.put(this.generateId(),user);
            return user;
        }
        return null;
    }

    /**
     * Méthode qui permet d'update un utilisateur existant dans la database (ici
     * utilisateursMapDataSource)
     *
     * @param user L'utilisateur à modifier
     * @return L'utilisateur modifié ou null si ce dernier n'existe pas dans la
     * database
     */
    @Override
    public Utilisateur updateUtilisateur(Utilisateur user) {
        if(user != null && user.getIdUtilisateur() != null) {
            user.setDateModification(new Date(System.currentTimeMillis()));
            utilisateursMapDataSource.put(user.getIdUtilisateur(), user);
            return user;
        }
        return null;
    }

    /**
     * Méthode qui permet de supprimer un utilisateur existant dans la database
     * (ici utilisateursMapDataSource)
     *
     * @param user L'utilisateur à supprimer
     * @return True si suppression effectuée, false sinon
     */
    @Override
    public boolean deleteUtilisateur(Utilisateur user) {
        if(user != null && user.getIdUtilisateur() != null) {
            utilisateursMapDataSource.remove(user.getIdUtilisateur());
            return true;
        }
        return false;
    }

    public int generateId() {
        int id = 0;
        for (Utilisateur use : this.findAllUtilisateurs()){
            if(id < use.getIdUtilisateur()){
                id =  use.getIdUtilisateur();
            }
        }
        return id + 1;
    }
}
