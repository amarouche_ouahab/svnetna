/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.impl;

import com.cours.ebenus.dao.ConnectionHelper;
import com.cours.ebenus.dao.DriverManagerSingleton;
import com.cours.ebenus.dao.IProduitDao;
import com.cours.ebenus.dao.entities.Produit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ElHadji
 */
public class ProduitDao /*extends AbstractDao<Produit>*/ implements IProduitDao {

    private static final Log log = LogFactory.getLog(ProduitDao.class);
    private static Connection connection = DriverManagerSingleton.getInstance().getConnectionInstance();

    //    private static Connection connection = DriverManagerSingleton.getConnectionInstance();
    private static ConnectionHelper connectionHelper = new ConnectionHelper();

    //public ProduitDao() {
    //    super(Produit.class);
    //}
    @Override
    public List<Produit> findAllProduits() {
        try {
            PreparedStatement stmp = connection.prepareStatement("SELECT * FROM Produit");
            ResultSet result = stmp.executeQuery();
            List<Produit> produits = new ArrayList<Produit>();

            while(result.next()) {
                Produit role = new Produit(
                        result.getInt("idProduit"),
                        result.getString("reference"),
                        result.getDouble("prix"),
                        result.getString("nom"),
                        result.getString("description"),
                        result.getInt("stock"),
                        result.getInt("active") > 0 ? true : false,
                        result.getInt("marquerEffacer") > 0 ? true : false,
                        result.getInt("version"));
                produits.add(role);
            }

            connectionHelper.closeSqlResources(stmp, result);
            return produits;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Produit findProduitById(int idProduit) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Produit WHERE idProduit = ?");
            preparedStatement.setInt(1, idProduit);
            ResultSet result = preparedStatement.executeQuery();

            while(result.next()) {
                Produit produit = new Produit(
                        result.getInt("idProduit"),
                        result.getString("reference"),
                        result.getDouble("prix"),
                        result.getString("nom"),
                        result.getString("description"),
                        result.getInt("stock"),
                        result.getInt("active") > 0 ? true : false,
                        result.getInt("marquerEffacer") > 0 ? true : false,
                        result.getInt("version"));
                connectionHelper.closeSqlResources(preparedStatement, result);
                return (produit);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Produit createProduit(Produit produit) {
        try {
            PreparedStatement stmp = connection.prepareStatement("INSERT INTO Produit(reference, prix, nom, description, stock, active, marquerEffacer, version)" +
                    " VALUES (?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmp.setString(1, produit.getReference());
            stmp.setDouble(2, produit.getPrix());
            stmp.setString(3, produit.getNom());
            stmp.setString(4, produit.getDescription());
            stmp.setInt(5, produit.getStock());
            stmp.setInt(6, produit.getActive() ? 1 : 0);
            stmp.setInt(7, produit.getMarquerEffacer() ? 1 : 0);
            stmp.setInt(8, produit.getVersion() != null ? produit.getVersion() : 0);

            stmp.executeUpdate();

            ResultSet result = stmp.getGeneratedKeys();

            if (result.next())
                produit.setIdProduit(result.getInt(1));
            connectionHelper.closeSqlResources(stmp, result);

            return produit;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Produit updateProduit(Produit produit) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE Produit SET reference = ?, prix = ?, nom = ?, description = ?, stock = ?, active = ?, marquerEffacer = ?, version = ? WHERE idProduit = ?");
            preparedStatement.setString(1, produit.getReference());
            preparedStatement.setDouble(2, produit.getPrix());
            preparedStatement.setString(3, produit.getNom());
            preparedStatement.setString(4, produit.getDescription());
            preparedStatement.setInt(5, produit.getStock());
            preparedStatement.setInt(6, produit.getActive() ? 1 : 0);
            preparedStatement.setInt(7, produit.getMarquerEffacer() ? 1 : 0);
            preparedStatement.setInt(8, produit.getVersion() != null ? produit.getVersion() : 0);
            preparedStatement.setInt(9, produit.getIdProduit());
            preparedStatement.executeUpdate();

            connectionHelper.closeSqlResources(preparedStatement, null);

            return produit;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteProduit(Produit produit) {
        try {
            PreparedStatement stmp = connection.prepareStatement("DELETE FROM Produit WHERE idProduit = ?");
            stmp.setInt(1, produit.getIdProduit());
            stmp.executeUpdate();
            connectionHelper.closeSqlResources(stmp, null);

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
