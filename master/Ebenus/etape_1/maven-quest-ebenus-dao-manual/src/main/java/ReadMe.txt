Reponse question 1 :

toString:
	une fonctione qui permet de retourner une chaine de caractere lors l'appel de l'instance pour l'afficher par exemple:
	si on on affiche 'user' user est une instance de la class User, si on implÈmente la fonction toString()"return this.nom"
	Áa nous affiche  le nom de user.

equals:
	la fontion equals permet de comparer deux objet d'une faÁon primitif c'est ‡ dire elle va comprarer les adresses de chaque objet
	donc il faut la override pour la personnaliser selon nos besoins, si pour nous deux objets sont identiques s'ils ont le mem Id ou
	le meme nom donc il faudra faire le traitement pour faire cela.

hashcode:
	L'utilisation des collection Hash(Map|Set) classe les objets par hashcode qui est par dÈfaut l'adresse de l'objet
	converti en entier, donc ce qui rend pas possible de localiser l'objet voulu car ce n'est pas le meme hashcode et Áa peut
	creer des doublons. Pour ces raison il vaut meiux override la fonction hashcode.


Reponse question 2 :

List et Tableau ?
    Un tableau à une taille définit (fixe) a l'opposé d'une List qui est dynamique et qui donc facilite l'ajoute et la suppression des éléments, une maniabilité que les tableaux ne possède pas
List et ArrayList ?

    List est une interface où chaque élément est un objet et les éléments sont accessibles par leur position (index).
ArrayList est une classe qui implémente l'interface List, en quelque sorte, un tableau dynamique d'objets dont la taille augmente ou diminue à chaque fois que nécessaire c'est à dire qui n'a pas de taille définie

Map et HashMap ?

    Map est une interface, qui garantit un certain nombre de méthodes implémentées et qui permet d'associer une clé à un objet.
HashMap est une implémentation d'une Map. elle fournit toutes les méthodes spécifiées dans l'interface, elle utilise une collection de valeurs de clé hachées pour effectuer sa recherche.
HashMap et Map peuvent contenir plusieurs paires clé / valeur, mais elles ne peuvent pas contenir de clés en double.


Set et HashSet ?

    Set est interface qui représent un ensemble générique de valeurs sans éléments dupliqués.
HashSet implémente une interface est Set où les éléments ne sont ni triés ni ordonnés. C'est plus rapide qu'un TreeSet,elle offre
des performances constantes dans le temps pour les opérations de base (ajout, suppression, contenu et taille).

List et Set ?
    Une List est une collection ordonnée qui maintient l'ordre d'insertion. Cela signifie que lors de l'affichage du contenu de la liste,
les éléments sont affichés dans le même ordre dans lequel ils ont été insérés dans la liste.
Set est une collection non ordonnée, elle ne maintient aucun ordre.


Reponse question 3 :

Quels sont les avantages de l'utilisation du Design Pattern DAO ?

    Le Design pattern DAO (Data Access Object) permet de faire le lien entre la couche métier et la couche métier d'une application.
Il permet de mieux maîtriser les changements susceptibles d'être opérés sur le système de stockage des données,  donc de préparer une migration d'un système à un autre (BDD vers fichiers XML par exemple).
Il permet aussi de prévenir un changement éventuel de système de stockage de données (de PostgreSQL vers Oracle par exemple).



