/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.main;

import com.cours.ebenus.dao.entities.Role;
import com.cours.ebenus.dao.entities.Utilisateur;
import com.cours.ebenus.factory.AbstractDaoFactory;
import com.cours.ebenus.service.IServiceFacade;
import com.cours.ebenus.service.ServiceFacade;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;

/**
 *
 * @author elhad
 */
public class Main {

    private static final Log log = LogFactory.getLog(Main.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        IServiceFacade serviceFacade = new ServiceFacade(AbstractDaoFactory.FactoryDaoType.MANUAL_ARRAY_DAO_FACTORY);
        Utilisateur user =  new Utilisateur(28, "Mr", "ouahab", "amarouche", "admin@gmail.com",
                "passw0rd", new Date(System.currentTimeMillis()), new Role(1, "Administrateur", "Le rôle administrateur"));

//        System.out.println(serviceFacade.getUtilisateurDao().createUtilisateur(null));
        System.out.println(serviceFacade.getRoleDao().findRoleById(43));
    }
}
