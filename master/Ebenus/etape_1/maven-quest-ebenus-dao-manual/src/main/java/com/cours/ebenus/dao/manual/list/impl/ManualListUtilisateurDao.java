/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.manual.list.impl;

import com.cours.ebenus.dao.DataSource;
import com.cours.ebenus.dao.IUtilisateurDao;
import com.cours.ebenus.dao.entities.Utilisateur;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cours.ebenus.dao.exception.CustomException;
import com.cours.ebenus.utils.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ElHadji
 */
public class ManualListUtilisateurDao /*extends AbstractListDao<Utilisateur>*/ implements IUtilisateurDao {

    private static final Log log = LogFactory.getLog(ManualListUtilisateurDao.class);
    private List<Utilisateur> utilisateursListDataSource = DataSource.getInstance().getUtilisateursListDataSource();

//    public ManualListUtilisateurDao() {
//        utilisateursListDataSource = DataSource.getInstance().getUtilisateursListDataSource();
//
//        //    super(Utilisateur.class, DataSource.getInstance().getUtilisateursListDataSource());
//    }
    /**
     * Méthode qui retourne la liste de tous les utilisateurs de la database
     * (ici utilisateursListDataSource)
     *
     * @return La liste de tous les utilisateurs de la database
     */
    @Override
    public List<Utilisateur> findAllUtilisateurs() {
        return utilisateursListDataSource;
    }

    /**
     * Méthode qui retourne l'utilisateur d'id passé en paramètre de la database
     * (ici utilisateursListDataSource)
     *
     * @param idUtilisateur L'id de l'user à récupérer
     * @return L'utilisateur d'id passé en paramètre, null si non trouvé
     */
    @Override
    public Utilisateur findUtilisateurById(int idUtilisateur) {
        for (Utilisateur user :utilisateursListDataSource){
            if(idUtilisateur == user.getIdUtilisateur()){
                return user;
            }
        }
        return null;
    }

    /**
     * Méthode qui retourne une liste de tous les utilisateurs de la database
     * (ici utilisateursListDataSource) dont le prénom est égal au paramètre
     * passé
     *
     * @param prenom Le prénom des utilisateurs à récupérer
     * @return Une liste de tous les utilisateurs dont le prénom est égal au
     * paramètre passé
     */
    @Override
    public List<Utilisateur> findUtilisateursByPrenom(String prenom) {
        List<Utilisateur> users = new ArrayList();
        for (Utilisateur user :utilisateursListDataSource){
            if(prenom.equals(user.getPrenom())){
                users.add(user);
            }
        }
        return users;
    }

    /**
     * Méthode qui retourne une liste de tous les utilisateurs de la database
     * (ici utilisateursListDataSource) dont le nom est égal au paramètre passé
     *
     * @param nom Le nom des utilisateurs à récupérer
     * @return Une liste de tous les utilisateurs dont le nom est égal au
     * paramètre passé
     */
    @Override
    public List<Utilisateur> findUtilisateursByNom(String nom) {
        List<Utilisateur> users = new ArrayList();
        for (Utilisateur user :utilisateursListDataSource){
            if(nom.equals(user.getNom())){
                users.add(user);
            }
        }
        return users;
    }

    /**
     * Méthode qui retourne une liste de tous les utilisateurs de la database
     * (ici utilisateursListDataSource) dont l'identifiant est égal au paramètre
     * passé
     *
     * @param identifiant Le nom des utilisateurs à récupérer
     * @return Une liste de tous les utilisateurs dont l'identifiant est égal au
     * paramètre passé
     */
    @Override
    public List<Utilisateur> findUtilisateurByIdentifiant(String identifiant) {
        List<Utilisateur> users = new ArrayList();
        for (Utilisateur user :utilisateursListDataSource){
            if(identifiant.equals(user.getIdentifiant())){
                users.add(user);
            }
        }
        return users;
    }

    /**
     * Méthode qui permet d'ajouter un utilisateur dans la database (ici
     * utilisateursListDataSource)
     *
     * @param user L'utilisateur à ajouter
     * @return L'utilisateur ajouté ou null si échec
     */
    @Override
    public Utilisateur createUtilisateur(Utilisateur user) {
        if(user != null && user.getIdentifiant() != null) {
            for (Utilisateur us : utilisateursListDataSource) {
                if (us.getIdentifiant().equals(user.getIdentifiant())) {
                    throw new CustomException(Constants.EXCEPTION_CODE_USER_ALREADY_EXIST);
                }
            }
            user.setDateCreation(new Date(System.currentTimeMillis()));
            user.setDateModification(new Date(System.currentTimeMillis()));
            user.setIdUtilisateur(this.generateId());
            utilisateursListDataSource.add(user);
            return user;
        }
        return null;
    }

    /**
     * Méthode qui permet d'update un utilisateur existant dans la database (ici
     * utilisateursListDataSource)
     *
     * @param user L'utilisateur à modifier
     * @return L'utilisateur modifié ou null si ce dernier n'existe pas dans la
     * database
     */
    @Override
    public Utilisateur updateUtilisateur(Utilisateur user) {
        if(user != null && user.getIdUtilisateur() != null) {
            int i = 0;
            for (Utilisateur us :utilisateursListDataSource){
                if(user.getIdUtilisateur() == us.getIdUtilisateur()){
                    user.setDateModification(new Date(System.currentTimeMillis()));
                    utilisateursListDataSource.set(i,user);
                    return user;
                }
                i++;
            }
        }
        return null;
    }

    /**
     * Méthode qui permet de supprimer un utilisateur existant dans la database
     * (ici utilisateursListDataSource)
     *
     * @param user L'utilisateur à supprimer
     * @return True si suppression effectuée, false sinon
     */
    @Override
    public boolean deleteUtilisateur(Utilisateur user) {
        if(user != null && user.getIdUtilisateur() != null) {
            for (Utilisateur us : utilisateursListDataSource) {
                if (user.getIdUtilisateur() == us.getIdUtilisateur()) {
                    utilisateursListDataSource.remove(us);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Méthode qui retourne une liste de tous les utilisateurs de la database
     * (ici utilisateursListDataSource) dont le rôle a comme id celui passé en
     * paramètre
     *
     * @param idRole L'id du rôle des utilisateurs à récupérer
     * @return Une liste de tous les utilisateurs dont le rôle a comme id celui
     * passé en paramètre
     */
    @Override
    public List<Utilisateur> findUtilisateursByIdRole(int idRole) {
        List<Utilisateur> users = new ArrayList();
        for (Utilisateur user :utilisateursListDataSource){
            if(idRole == user.getRole().getIdRole()){
                users.add(user);
            }
        }
        return users;
    }

    /**
     * Méthode qui retourne une liste de tous les utilisateurs de la database
     * (ici utilisateursListDataSource) dont le rôle a comme identifiant celui
     * passé en paramètre
     *
     * @param identifiantRole L'identifiant du rôle des utilisateurs à récupérer
     * @return Une liste de tous les utilisateurs dont le rôle a comme
     * identifiant celui passé en paramètre
     */
    @Override
    public List<Utilisateur> findUtilisateursByIdentifiantRole(String identifiantRole) {
        List<Utilisateur> users = new ArrayList();
        for (Utilisateur user :utilisateursListDataSource){
            if(identifiantRole == user.getRole().getIdentifiant()){
                users.add(user);
            }
        }
        return users;
    }

    public int generateId() {
        int id = 0;
        for (Utilisateur use : utilisateursListDataSource){
            if(id < use.getIdUtilisateur()){
                id =  use.getIdUtilisateur();
            }
        }
        return id + 1;
    }
}
