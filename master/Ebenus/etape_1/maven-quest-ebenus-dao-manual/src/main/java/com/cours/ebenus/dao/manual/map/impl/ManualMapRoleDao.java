/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cours.ebenus.dao.manual.map.impl;

import com.cours.ebenus.dao.DataSource;
import com.cours.ebenus.dao.IRoleDao;
import com.cours.ebenus.dao.entities.Role;

import java.util.*;

import com.cours.ebenus.dao.entities.Utilisateur;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ElHadji
 */
public class ManualMapRoleDao /*extends AbstractMapDao<Role>*/ implements IRoleDao {

    private static final Log log = LogFactory.getLog(ManualMapRoleDao.class);
    private Map<Integer, Role> rolesListDataSource = DataSource.getInstance().getRolesMapDataSource();

    //public ManualMapRoleDao() {
    //    super(Role.class, DataSource.getInstance().getRolesMapDataSource());
    //}
    /**
     * Méthode qui retourne la liste de tous les rôles de la database (ici
     * rolesListDataSource)
     *
     * @return La liste de tous les rôles de la database
     */
    @Override
    public List<Role> findAllRoles() {
        List<Role> role = new ArrayList<Role>(rolesListDataSource.values());
        return role;
    }

    /**
     * Méthode qui retourne le rôle d'id passé en paramètre de la database (ici
     * rolesListDataSource)
     *
     * @param idRole L'id du rôle à récupérer
     * @return Le rôle d'id passé en paramètre, null si non trouvé
     */
    @Override
    public Role findRoleById(int idRole) {
        return rolesListDataSource.get(idRole);
    }

    /**
     * Méthode qui retourne une liste de tous les rôles de la database (ici
     * rolesListDataSource) dont l'identifiant est égal au paramètre passé
     *
     * @param identifiantRole L'identifiant des rôles à récupérer
     * @return Une liste de tous les rôles dont l'identifiant est égal au
     * paramètre passé
     */
    @Override
    public List<Role> findRoleByIdentifiant(String identifiantRole) {
        List<Role> list = new ArrayList<>();
        for (Role role: this.findAllRoles())
        {
            if(identifiantRole.equals(role.getIdentifiant())){
                list.add(role);
            }
        }
        return list;
    }

    /**
     * Méthode qui permet d'ajouter à rôle dans la database (ici
     * rolesListDataSource)
     *
     * @param role Le rôle à ajouter
     * @return Le rôle ajouté ou null si échec
     */
    @Override
    public Role createRole(Role role) {
        if (role != null) {
            role.setIdRole(this.generateId());
            rolesListDataSource.put(this.generateId(), role);
            return role;
        }
        return null;
    }



    /**
     * Méthode qui permet d'update un rôle existant dans la database (ici
     * rolesListDataSource)
     *
     * @param role Le rôle à modifier
     * @return Le rôle modifié ou null si ce dernier n'existe pas dans la
     * database
     */
    @Override
    public Role updateRole(Role role) {
        if(role != null && role.getIdentifiant() != null && findRoleById(role.getIdRole()) != null){
            rolesListDataSource.put(role.getIdRole(), role);
            return role;
        }
        return null;
    }

    /**
     * Méthode qui permet de supprimer un rôle existant dans la database (ici
     * rolesListDataSource)
     *
     * @param role Le rôle à supprimer
     * @return True si suppression effectuée, false sinon
     */
    @Override
    public boolean deleteRole(Role role) {
        Set<Map.Entry<Integer, Role>> st = rolesListDataSource.entrySet();
        for (Map.Entry<Integer, Role> me:st)
        {
            if(role.getIdRole() == me.getValue().getIdRole()){
                rolesListDataSource.remove(role.getIdRole());
                return true;
            }
        }
        return false;
    }

    public int generateId() {
        List<Role> list = new ArrayList<Role>(rolesListDataSource.values());
        int id = 0;
        for (Role r : list){
            if(id < r.getIdRole()){
                id = r.getIdRole();
            }
        }
        return id + 1;
    }


}
