CREATE TABLE Produits (
    ID INT AUTO_INCREMENT,
        libelle VARCHAR(40) NOT NULL,
	     Marque VARCHAR(40) NOT NULL,
	         Type_de_produit VARCHAR(40) NOT NULL,
		     Type_de_livraison VARCHAR(40) NOT NULL,
		         Image VARCHAR(40) NOT NULL,
			       Description TEXT NOT NULL,
			       		   Prix_vente FLOAT NOT NULL,
					   	      PRIMARY KEY (ID)
						      );
CREATE TABLE Utilisateurs (
    ID INT AUTO_INCREMENT,
        Nom VARCHAR(50) NOT NULL,
	    Prenom VARCHAR(50) NOT NULL,
	    	   Age VARCHAR(5) NOT NULL,
		       Tel VARCHAR(10) NOT NULL,
		       	   Adresse VARCHAR(100) NOT NULL,
			   	   Login VARCHAR(50) NOT NULL,
				   	 Password VARCHAR(50) NOT NULL
					     PRIMARY KEY (ID)
					     );


INSERT INTO Produits (libelle, Marque, Type_de_produit, Type_de_livraison, Image, Description, Prix_vente) VALUES
('SUPRA KHAN', '../image/downloadgtt.png','Produit NEUF', 'Livraison Gratuite', '../image/supra1.jpeg', "SUPRA SPECTRE KHAN [BLANC NOIR BLANC]La collection Spectre de Supra représente
	parfaitement l'esprit de la culture hip-hop. C'est pourquoi Supra s'est associé avec nul autre que Lil Wayne pour 
	lancer sa nouvelle collection. La Khan séduit par son mélange raffiné de cuir et textile. Cette superbe chaussure Supra ne 
	laisse rien à désirer ! dessus cuir/textile doublure textile semelle synthétique couronne Supra sur la languette tige rembourrée
	 collection Spectre de Lil Wayne excellente qualité", '120€')
